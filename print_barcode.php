<?php
include 'session.php';
require_once("classes/class.database.php");
require_once("classes/class.family.php");
$cls_family = new Mtx_family();
$families = $cls_family->get_FileNo();

$max = count($fId);
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Barcode Sticker</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
      @font-face
      {
        font-family: barcode;
        src: url(asset/fonts/IDAutomationHC39M.ttf);
      }
      .row{
        width: 900px;

        /*border: 1px dotted black;*/
        padding-left: 20px;
      }
      #outer-square{
        display: inline-block;
        height: 60px;
        width: 120px;
        border: 1px solid #999;
        background-color: #fff;
        margin-bottom: 5px;
        -webkit-print-color-adjust:exact;
      }
      .barcode{
        display: inline-block;
        font-family: barcode;
        font-size: 15px;
        text-align: center;
        margin: 5px;
      }
    </style>
  </head>
  <body>
    <?php
    $count = 0;
    foreach ($families as $family) {
      //$name = $cls_family->get_name(trim($fId[$i]));
      if ($count == 0) {
        echo '<div class="row">';
      }
      ?>
      <div class="" id="outer-square">
        <center>
          <div class="barcode">
            *<?php echo $family['FileNo']; ?>*
          </div>
        </center>
      </div>
      <?php
      if ($count == 6) {
        echo '</div>';
        $count = 0;
      } else {
        $count++;
      }
    }
    ?>
  </body>
</html>