<?php
include 'session.php';
$page_number = 39;
require_once("classes/class.database.php");
require_once('classes/class.receipt.php');
$cls_receipt = new Mtx_Receipt();

$receipts = $cls_receipt->get_settlement_receipts();
$title = 'Settlement Report';
$active_page = 'report';

include('includes/header.php');
include('page_rights.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Hub</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <!--div class="col-md-3 pull-left">
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Search</h3></div>
            <div class="panel-body">
              <?php include('includes/search_bar.php');?>
            </div>
          </div>
        </div-->
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <form method="post" role="form" class="form-horizontal">
            <table class="table table-bordered table-hover table-condensed">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>FileNo</th>
                  <th>Name</th>
                  <th class="text-right">Paid Amount</th>
                  <th class="text-right">Original Amount</th>
                  <th class="text-right">Difference</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $original = 0;
                $paid = 0;
                $difference = 0;
                if($receipts){
                  $i = 1;
                  foreach($receipts as $receipt){
                    $original += $receipt['original_amount'];
                    $paid += $receipt['amount'];
                    ?>
                <tr>
                  <td><?php echo $i++;?></td>
                  <td><?php echo $receipt['FileNo'];?></td>
                  <td><?php echo $receipt['name'];?></td>
                  <td class="text-right"><?php echo number_format($receipt['amount']).'/-';?></td>
                  <td class="text-right"><?php echo number_format($receipt['original_amount']).'/-'; ?></td>
                  <td class="text-right"><?php $diff = $receipt['original_amount'] - $receipt['amount']; echo number_format($diff).'/-'; $difference += $diff;?></td>
                </tr>
                <?php
                  }
                  if(isset($paid) or isset($difference) or isset($original)){
                    echo '<tr><td colspan="3" class="text-right"><strong>Total</strong></td><td class="text-right">'.  number_format($paid).'/-</td><td class="text-right">'.  number_format($original).'/-</td><td class="text-right">'.  number_format($difference).'/-</td></tr>';
                  }
                } else {
                  echo '<tr><td colspan="6" class="alert-danger">Sorry! no receipt found.</td></tr>';
                }
      ?>
              </tbody>
            </table>
          </form>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>