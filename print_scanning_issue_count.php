<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/class.family.php');
require_once('classes/class.barcode.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();
$cls_barcode = new Mtx_Barcode();
$title = 'Tiffin count report';
$active_page = 'report';
$from_date = $to_date = $post = FALSE;
$mohallah = $cls_family->get_all_Mohallah();
if (isset($_GET['search'])) {
  $post = TRUE;
  $from_date = $_GET['from_date'];
  $to_date = $_GET['to_date'];
  $tanzeem = $_GET['tanzeem'];
  $result = $cls_family->scanning_tot_family($from_date, $to_date, $tanzeem);
  $tot_family = $result['TOT_FAMILY'];
  $open_acct = $result['OPEN_ACCT'];
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print scanning report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <style type="text/css">
      @media all {
        body { font-size: 16px; }
        table { page-break-inside:auto; }
        tr    { page-break-inside:avoid; page-break-after:auto; }
        thead { display:table-header-group; }
        tfoot { display:table-footer-group; }
      }
    </style>
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->
  <!-- Center Bar -->
  <div class="col-md-8">
    <div class="col-md-12">&nbsp;</div>
    <?php if ($post) {
      require_once 'includes/inc.scanning.php';
    }
    ?>
  </div>
  <!-- /Center Bar -->
    </div>
    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
