<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.family.php");
$cls_family = new Mtx_family();

if (isset($_POST['transfer'])) {
  $transfer = $cls_family->transfer_FileNo($_POST['FileNo'], $_POST['Mohallah']);
  if($transfer) {
    $_SESSION[SUCCESS_MESSAGE] = 'File No transfered successfully';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Oops! something went wrong!';
  }
  
}
$mohallah = $cls_family->get_all_tanzeem();
$title = 'Transfer';
$active_page = 'settings';

include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <div class="form-group">
              <label class="control-label col-md-3"><?php echo THALI_ID; ?></label>
              <div class="col-md-4">
                <input type="text" class="form-control" name="FileNo" id="FileNo">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Tanzeem</label>
              <div class="col-md-4">
                <select id="Mohallah" name="Mohallah" id="Mohallah" class="form-control">
                  <option value="All">All</option>
                  <?php foreach ($mohallah as $mohalla) { ?>
                  <option value="<?php echo $mohalla['prefix'] ?>"><?php echo ucwords($mohalla['name']); ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">&nbsp;</label>
              <div class="col-md-4">
                <button class="btn btn-success" type="submit" name="transfer" id="transfer">Transfer</button>
              </div>
            </div>

          </form>
        </div>
        <!-- /Center Bar -->
      <script>
        $('#transfer').click(function() {
        var file = $('#FileNo').val();
        var mh = $('#Mohallah').val();
        var error = 'Whoops! following errors are occurred!\n';
        var validate = true;
        if (file == '')
        {
          error += 'Please enter File No\n';
          validate = false;
        }
        if (mh == 'All')
        {
          error += 'Please select Mohallah\n';
          validate = false;
        }
        if (validate == false)
        {
          alert(error);
          return validate;
        }
        });
      </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>