<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.receipt.php');
require_once('classes/hijri_cal.php');
require_once('classes/class.user.php');
$cls_user = new Mtx_User();
$cls_family = new Mtx_family();
$cls_receipt = new Mtx_Receipt();
$mohallah = $_GET['mh'];
$takh_year = $_GET['year'];
$pending_kisht = $_GET['kisht'];
if (isset($_GET['show_all']) && $_GET['show_all'] == 1)
  $show_all_records = TRUE;
else
  $show_all_records = FALSE;
$show_tbl = TRUE;
$search = $cls_family->get_family_by_mohallah($mohallah, FALSE);
$setting = $cls_user->get_general_settings();
$default_year = $setting[0]['start_year'];
$no_years = $setting[0]['no_of_years'];
$default_kisht = $setting[0]['default_kisht'];
$h = HijriCalendar::GregorianToHijri();
$date = $h[1] . ' ' . HijriCalendar::monthName($h[0]) . ', ' . $h[2];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print Hub pending report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8 ">
        <div>
          <strong>Hub pending report<span class="pull-right"><?php echo $date; ?></span></strong>
        </div>
        <?php
        
        include('includes/inc.hub_pending.php');
        ?>
      </div>

      <!-- /Center Bar -->
    </div>
    <!-- /Content -->

  </body>
</html>
