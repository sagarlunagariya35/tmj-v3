<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.product.php');
require_once('classes/class.family.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();
$cls_product = new Mtx_Product();
$user_id = $_SESSION[USER_ID];
$gblIssue = 0;
$gblDirect = 0;
$gblCharges = 0;

if(isset($_GET['date'])) {
  $items = $cls_product->get_issued_item_in_current_day($_GET['date']);
  $purchases = $cls_product->get_issued_direct_purchase($_GET['date']);
  $all_tiffin = $cls_family->get_all_tiffin_size($_GET['date']);
  $charges = $cls_product->get_other_charges($_GET['date']);
}

$title = 'Inventory issue report';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

  <!-- Center Bar -->
  <div class="col-md-12 ">
    <?php
    if(isset($_POST['search']) OR isset($_GET['date'])) {
    ?>
    <div class="col-md-12">
      <table class="table table-hover table-condensed table-bordered">
        <thead>
          <tr>
            <th colspan="5">Inventory Issue</th>
          </tr>
        <tr>
          <th>No.</th>
          <th>Name</th>
          <th class="text-right">Quantity</th>
          <th class="text-right">Unit</th>
          <th class="text-right">Amount</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i = 1;
        if($items) {
        foreach($items as $item) {
        ?>
        <tr>
          <td><?php echo $i++;?></td>
          <td><?php echo $item['name'];?></td>
          <td class="text-right"><?php echo number_format($item['quantity'], 3);?></td>
          <td class="text-right"><?php echo $item['unit'];?></td>
          <td class="text-right"><?php echo 'Rs. ' . number_format($item['act_cost'], 2); $gblIssue += $item['act_cost'];?></td>
        </tr>
        <?php } 
        ?>
        <tr>
          <td colspan="4"><strong>Total:</strong></td>
          <td class="text-right"><?php echo 'Rs. ' . number_format($gblIssue, 2);?></td>
        </tr>
        <?php
        } else { ?>
        <tr>
          <td colspan="5" class="alert-danger">No results found</td>
        </tr>
        <?php } ?>
      </tbody>
      </table>
    </div>
    <div class="col-md-12">
      <table class="table table-hover table-condensed table-bordered">
        <thead>
          <tr>
            <th colspan="6">Direct Purchase</th>
          </tr>
        <tr>
          <th>Sr. No.</th>
          <th>Id</th>
          <th>Item Name</th>
          <th class="text-right">Quantity</th>
          <th class="text-right">Unit</th>
          <th class="text-right">Amount</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $k = 1;
        if($purchases) {
          
        foreach($purchases as $purchase) {
        ?>
        <tr>
          <td><?php echo $k++;?></th>
          <td><a target="_blank" href="upd_bill_details.php?cmd=show_direct&bid=<?php echo $purchase['id'];?>"><?php echo $purchase['id'];?></a></td>
          <td><?php echo $purchase['item_name'];?></td>
          <td class="text-right"><?php echo number_format($purchase['quantity'], 3);?></td>
          <td class="text-right"><?php echo number_format($purchase['unit'], 2);?></td>
          <td class="text-right"><?php $sum_direct = $purchase['quantity'] * $purchase['unit']; echo 'Rs. ' . number_format($sum_direct, 2);$gblDirect += $sum_direct;?></td>
        </tr>
        <?php } 
        
        ?>
        <tr>
          <td colspan="5"><font style="text-align: right;font-weight: bold">Total:</font></td>
          <td class="text-right"><?php echo 'Rs. ' . number_format($gblDirect, 2);?></td>
        </tr>
        <?php
        } else { ?>
        <tr>
          <td colspan="6" class="alert-danger">No results found</td>
        </tr>
        <?php } ?>
      </tbody>
      </table>
    </div>
    <div class="col-md-12">
      <table class="table table-hover table-condensed table-bordered">
        <thead>
          <tr>
            <td colspan="4">Other Charges</td>
          </tr>
          <tr>
            <th>Sr. No</th>
            <th>Id</th>
            <th>Shop Name</th>
            <th class="text-right">Other charges</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $number = 1;
          
          if($charges) {
            foreach ($charges as $charge) {
              $gblCharges += $charge['other_charges'];
          ?>
          <tr>
            <td><?php echo $number++;?></td>
            <td><?php echo $charge['id'];?></td>
            <td><?php echo $charge['name'];?></td>
            <td class="text-right"><?php echo 'Rs. ' . $charge['other_charges'];?></td>
          </tr>
          <?php } ?>
          <?php
          } else {?>
          <tr>
            <td colspan="4" class="alert-danger">No results found</td>
          </tr>
          <?php } ?>
          <tr>
            <td colspan="3"><strong>Grand Total (Inventory issue + Direct Purchase + Other charges):</strong></td>
            <td class="text-right"><?php echo 'Rs. ' . number_format($gblDirect + $gblIssue + $gblCharges, 2);?></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-md-12">
      <table class="table table-hover table-condensed table-bordered">
        <thead>
          <tr>
            <th>Tiffin Size</th>
            <th>Count</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($all_tiffin as $key => $value) {?>
          <tr>
            <td><?php echo $key;?></td>
            <td><?php echo $value;?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <?php } ?>
  </div>
  <!-- /Center Bar -->
</div>
<!-- /Content -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
