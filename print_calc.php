<?php
include('session.php');
$page_number = 49;
require_once("classes/class.database.php");
require_once("classes/class.menu.php");
require_once("classes/class.family.php");
$cls_menu = new Mtx_Menu();
$cls_family = new Mtx_family();
$tiffin_count = $cls_family->get_tiffin_group();

if (isset($_GET['cmd'])) {
  $ary_roti = $_SESSION['roti'];
  $sum = $cls_menu->calculate_roti_qty($ary_roti, $tiffin_count);
}

$title = 'Roti calculator';
$active_page = 'menu';

?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print roti calculation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="asset/dist/css/print.css" media="print">
    <style type="text/css">
      @media all {
        body{font-size: 14px; }
        .page-break  { display: none; }
      }

      @media print {
        .page-break  { display: block; page-break-before: always; }
      }
    </style>
  </head>
  <body onload="window.print();">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->
  <!-- Center Bar -->
  <div class="col-md-8">
        <div>
          <strong><span class="pull-right"><?php echo date('d F,Y'); ?></span></strong>
        </div>
    <form method="post" role="form" class="form-horizontal">
      <table class="table table-hover table-condensed">
        <thead>
          <tr>
            <th>Tiffin size</th>
            <th>Count</th>
            <th>Roti</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if ($tiffin_count) {
            $i = 0;
            $ary_size_count = array();
            foreach ($tiffin_count as $tfn => $cnt) {
              ?>
              <tr>
                <td><?php echo $tfn; ?></td>
                <td id="count<?php echo $i; ?>"><?php echo $cnt;
          array_push($ary_size_count, $cnt); ?></td>

                <td><p class="form-control-static"><?php
                  if (isset($_GET['cmd'])) {
                    echo $ary_roti[$i];
                  }$i++;
                  ?></p></td>
              </tr>
              <?php
            }
          }
          ?>
        <tr>
          <td>&nbsp;</td>
          <td><span class="pull-right"><strong>Total Roti:</strong></span></td>
          <td id="Result_Panel"><?php if (isset($sum)) echo number_format($sum, 0, '.', ','); ?></td>
        </tr>
        <tr>
          
        </tr>
        </tbody>
      </table>
    </form>
  </div>
  <!-- /Center Bar -->
</div>
<!-- /Content -->

  </body>
</html>
