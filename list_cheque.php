<?php
include 'session.php';
$pg_link = 'hub_rcpt_book';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
$cls_receipt = new Mtx_Receipt();

if ($_GET) {
  if (isset($_GET['hid']))
    $clear = $cls_receipt->make_cheque_clear($_GET['hid'], 'fmb_receipt_hub');
  if (isset($_GET['vid']))
    $clear = $cls_receipt->make_cheque_clear($_GET['vid'], 'receipt');
  if (isset($_GET['cid']))
    $clear = $cls_receipt->make_cheque_clear($_GET['cid'], 'credit_voucher');
  if (isset($_GET['bid']))
    $clear = $cls_receipt->make_cheque_clear($_GET['bid'], 'account_bill');
  if (isset($_GET['dpid']))
    $clear = $cls_receipt->make_cheque_clear($_GET['dpid'], 'direct_purchase');
  if (isset($_GET['did']))
    $clear = $cls_receipt->make_cheque_clear($_GET['did'], 'debit_voucher');
  if ($clear) {
    $_SESSION[SUCCESS_MESSAGE] = 'Cheque cleared successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing...';
  }
  location();
}

function location() {
  header('Location: list_cheque.php');
  exit();
}

$hub_rcpts = $cls_receipt->get_all_cheque('`fmb_receipt_hub`');
$vol_rcpts = $cls_receipt->get_all_cheque('`receipt`', 'type');
$credit_voucher = $cls_receipt->get_all_cheque('`credit_voucher`');
$bills = $cls_receipt->get_all_cheque('`account_bill`');
$direct_purchase = $cls_receipt->get_all_cheque('direct_purchase');
$debit_voucher = $cls_receipt->get_all_cheque('`debit_voucher`');

function no_results_found($label) {
  $div = '<div class="panel-body">No results found.</div>';
  panel($label, $div);
}

function panel($tbl_name, $table) {
  $panel = <<<PANEL
          <div class="panel panel-info">
            <div class="panel-heading">{$tbl_name}</div>
            $table
          </div>
PANEL;
            echo $panel;
}

function create_tbody($array, $id_link, $label, $slug) {
//  echo '<pre>';
//  print_r(func_get_args());
  $start = <<<START
          <table class="table table-hover table-condensed table-bordered">
            <thead>
              <tr>
                <th>ReceiptID</th>
                <th>Clear</th>
                <th>File No</th>
                <th>Cheque No</th>
                <th>Name</th>
                <th class='text-right'>Amount</th>
                <th>Cheque Date</th>
              </tr>
            </thead>
          <tbody>
START;
  $tr = '';
  foreach ($array as $rcpt) {//    print_r($rcpt);
    $rcpt_id = $id_link ? "<a href='$id_link{$rcpt['id']}' target=blank>{$rcpt['id']}</a>" : $rcpt['id'];
    $checked = $rcpt['cheque_clear'] == 1 ? 'checked' : '';
    $checkbox = "<input type='checkbox' name='clear' id='clear-{$rcpt['id']}' onclick='Chequecleared({$rcpt['id']})' {$checked} value='$slug'>";
    $file = isset($rcpt['FileNo']) ? $rcpt['FileNo'] : '--';
    //if($slug == 'credit') print_r($rcpt);
    $name = isset($rcpt['name']) ? $rcpt['name'] : $rcpt['hubber_name'];
    $name = ucwords(strtolower($name));
    $amount = number_format($rcpt['amount'], 2);
    $cheque_date = date('d F, Y', $rcpt['cheque_date']);
    $cheque_no = $rcpt['cheque_no'];
    $tr_cls = ($rcpt['cancel'] == 1) ? ' class=alert-danger' : '';
    $tr .= <<<TABLE
          <tr>
            <td>{$rcpt_id}</td>
            <td>{$checkbox}</td>
            <td>{$file}</td>
            <td>{$cheque_no}</td>
            <td>{$name}</td>
            <td class='text-right'>{$amount}</td>
            <td>{$cheque_date}</td>
          </tr>
TABLE;
  }
  $end = "</tbody></table>";
  $table = $start.$tr.$end;
  panel($label, $table);
}

$title = 'Cheque Book';
$active_page = 'account';

require_once 'includes/header.php';

$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-12">&nbsp;</div>

          <?php
          $book = 'Hub Cheque Book';
          $slug = 'hub';
          ($hub_rcpts) ? create_tbody($hub_rcpts, 'print_hub_receipt.php?id=', $book, $slug) : no_results_found($book);
          $book = 'Voluntary Cheque Book';
          $slug = 'vol';
          ($vol_rcpts) ? create_tbody($vol_rcpts, 'print_voluntary_receipt.php?id=', $book, $slug) : no_results_found($book);
          $book = 'Credit Voucher Cheque Book';
          $slug = 'credit';
          ($credit_voucher) ? create_tbody($credit_voucher, FALSE, $book, $slug) : no_results_found($book);
          $slug = 'bill';
          $book = 'Bill Cheque Book';
          ($bills) ? create_tbody($bills, FALSE, $book, $slug) : no_results_found($book);
          $slug = 'direct';
          $book = 'Direct Cheque Book';
          ($direct_purchase) ? create_tbody($direct_purchase, FALSE, $book, $slug) : no_results_found($book);
          $slug = 'debit';
          $book = 'Debit Voucher Cheque Book';
          ($debit_voucher) ? create_tbody($debit_voucher, FALSE, $book, $slug) : no_results_found($book);
          ?>

        </div>
        <!-- /Center Bar -->
        <script>
          function Chequecleared(val) {
            var slug = $('#clear-' + val).val();
            var id = '';
            switch (slug) {
              case 'hub':
                id = 'hid';
                break;
              case 'vol':
                id = 'vid';
                break;
              case 'credit':
                id = 'cid';
                break;
              case 'debit':
                id = 'did';
                break;
              case 'direct':
                id = 'dpid';
                break;
              case 'bill':
                id = 'bid';
                break;
            }
            window.location.href = 'list_cheque.php?' + id + '=' + val;
            return true;
          }
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>
<?php
include('includes/footer.php');
?>