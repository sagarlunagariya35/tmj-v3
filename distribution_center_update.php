<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.family.php");
$cls_family = new Mtx_family();

if(isset($_POST['Update_center'])){
  $address = urlencode(ucwords(strtolower($_POST['address'])));
  $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=India";
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $response = curl_exec($ch);
  curl_close($ch);
  $response_a = json_decode($response);
  $latitude = $response_a->results[0]->geometry->location->lat;
  $longitude = $response_a->results[0]->geometry->location->lng;
  
  $result = $cls_family->update_distribution_center(ucfirst(strtolower($_POST['title'])), ucwords(strtolower($_POST['address'])), $latitude, $longitude);
  if($result){
    $_SESSION[SUCCESS_MESSAGE] = 'Distribution center added successfully';
    header('location: distribution_center.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while updating Distribution center ';
    header('location:distribution_center.php');
    exit();
  }
}

$center = $cls_family->get_distribution_center($_GET['id']);

$title = 'Update distribution center';
$active_page = 'menu';

require_once 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <div class="col-md-2 pull-left">
          &nbsp;
        </div>
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-10">
          <form method="post" role="form" class="form-horizontal">

            <div class="form-group">
              <label class="control-label col-md-2">Title</label>
              <div class="col-md-6">
                <input type="text" name="title" id="title" class="form-control" value="<?php
                       if($center){
                         echo trim($center['title']);
                       }
                       ?>">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Address</label>
              <div class="col-md-6">
                <textarea name="address" id="address" rows="5" class="form-control"><?php
                  if($center){
                    echo $center['address'];
                  }
                  ?></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">&nbsp;</label>
              <div class="col-md-10">
                <input type="submit" name="Update_center" id="Update_center" value="Add" class="btn btn-success col-md-7">
              </div>
            </div>

          </form>
        </div>
        <!-- /Center Bar -->
        <script>
          $('#Update_center').click(function() {
            var title = $('#title').val();
            var address = $('#address').val();
            var error = '';
            var validate = true;
            if (title == 0)
            {
              error += 'Please enter title\n';
              validate = false;
            }
            if (address == '')
            {
              error += 'Please enter address\n';
              validate = false;
            }
            if (validate == false)
            {
              alert(error);
              return validate;
            }
          });
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>