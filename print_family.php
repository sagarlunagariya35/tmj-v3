<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/hijri_cal.php');
$file = $_REQUEST['family_id'];
$cls_family = new Mtx_family($file);
$cls_fly = new Mtx_family();
$latest = $cls_fly->latest_tiffin_size_year($file);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print family details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="asset/dist/css/print.css" media="print">
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8 ">
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <table class="table table-condensed table-hover">
          <thead>
            <tr>
              <th colspan="4" style="text-align: center">Family Details of <?php echo $file;?></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Thali ID</td>
              <td><?php echo $cls_family->getSabil_id(); ?></td>
              <td>Occupation</td>
              <td><?php echo $cls_family->getOccupation(); ?></td>
            </tr>
            <tr>
              <td>Card Status</td>
              <td><?php
              $card = ucfirst($cls_family->getCard());
              if ($card == 'G')
                echo 'Green';
              else if ($card == 'Y')
                echo 'Yellow';
              else if ($card == 'R')
                echo 'Red';
              ?></td>
              <td>Email</td>
              <td><?php echo $cls_family->getEmail(); ?></td>
            </tr>
            <tr>
              <td>Age</td>
              <td><?php echo $age = date('Y') - $cls_family->getAge();?></td>
              <td>Ejamat Id</td>
              <td><?php echo $cls_family->getEjamaat(); ?></td>
            </tr>
            <tr>
              <td>Prefix</td>
              <td><?php echo $cls_family->getPrefix();?></td>
              <td>Res Phone</td>
              <td><?php echo $cls_family->getPhone();?></td>
            </tr>
            <tr>
              <td>First Name</td>
              <td><?php echo $cls_family->getFirstName();?></td>
              <td>Off Phone</td>
              <td><?php echo $cls_family->getOff_phone();?></td>
            </tr>
            <tr>
              <td>Father's Prefix</td>
              <td><?php echo $cls_family->getFatherPrefix();?></td>
              <td>Mobile</td>
              <td><?php echo $cls_family->getMobile();?></td>
            </tr>
            <tr>
              <td>Father Name</td>
              <td><?php echo $cls_family->getFatherName();?></td>
              <td>No. of Adults</td>
              <td><?php echo $cls_family->getAdults();?></td>
            </tr>
            <tr>
              <td>Surname</td>
              <td><?php echo $cls_family->getSurname();?></td>
              <td>No. of Kids</td>
              <td><?php echo $cls_family->getChild();?></td>
            </tr>
            <tr>
              <td>Gender</td>
              <td><?php echo $cls_family->getGender();?></td>
              <td>No. of Diabetics</td>
              <td><?php echo $cls_family->getDiabetic();?></td>
            </tr>
            <tr>
              <td>Mohallah</td>
              <td><?php echo $cls_family->getMohallah();?></td>
              <td>Diabetics Roti</td>
              <td><?php echo $cls_family->getDiabeticRoti();?></td>
            </tr>
            <tr>
              <td>Marital Status</td>
              <td><?php echo $cls_family->getMarital();?></td>
              <td>Monthly Hub</td>
              <td><?php if($latest['new_fmb_hub'] != NULL) echo number_format($latest['new_fmb_hub']).'/-'; else echo 'Default'; ?></td>
            </tr>
            <tr>
              <td>Address</td>
              <td><?php echo $cls_family->getAddress();?></td>
              <td>Last Paid Month</td>
              <td><?php $mon = HijriCalendar::GregorianToHijri($latest['paid_till']);echo HijriCalendar::monthName($mon[0]);?></td>
            </tr>
            <tr>
              <td>Tiffin Size</td>
              <td><?php echo $latest['new_tiffin_size'];?></td>
              <td>Last Hijri Year</td>
              <td><?php echo $mon[2]; ?></td>
            </tr>
          </tbody>
        </table>
    </div>

    <!-- /Center Bar -->

    <!-- Right Bar -->

    <!-- /Right Bar -->

  </div>
  <!-- /Content -->

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
</body>
</html>
<?php
unset($_SESSION[FILE]);
?>