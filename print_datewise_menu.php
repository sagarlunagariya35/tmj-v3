<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.menu.php");
require_once("classes/class.family.php");
require_once("classes/hijri_cal.php");
$cls_menu = new Mtx_Menu();
$cls_family = new Mtx_family();

$from_date = $_GET['from'];
$to_date = $_GET['to'];
$msg = $_GET['msg'];
$fsp = explode('-', $from_date);
$from = mktime(0, 0, 0, $fsp[1], $fsp[2], $fsp[0]);
$tsp = explode('-', $to_date);
$to = mktime(23, 59, 59, $tsp[1], $tsp[2], $tsp[0]);
$print_limit = $_GET['print_limit'];
$menus = $cls_menu->get_menu_between_dates($from, $to);
$h = HijriCalendar::GregorianToHijri();
$date = $h[1] . ' ' . HijriCalendar::monthName($h[0]) . ', ' . $h[2];
$title = 'Datewise menu report';
$active_page = 'report';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <style type="text/css">
      @media all {
        body { font-size: 16px; }
        table { page-break-inside:auto; }
        tr    { page-break-inside:avoid; page-break-after:auto; }
        thead { display:table-header-group; }
        tfoot { display:table-footer-group; }
      }
    </style>
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8">
        <?php
        $print = TRUE;
        require_once './includes/inc.datewise_menu_report.php';
        ?>
      </div>
    </div>
    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>