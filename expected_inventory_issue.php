<?php
include 'session.php';
$pg_link = 'voluntary_rcpt_book';
require_once('classes/class.database.php');
require_once('classes/class.product.php');
$cls_product = new Mtx_Product();
$fromDate = FALSE;
$toDate = FALSE;
$post = FALSE;

$title = 'Expected inventory issue';
$active_page = 'report';

$default_pricing = FALSE;
if (isset($_POST['search'])) {
  // year-m-d
  $post = TRUE;
  if(isset($_POST['price'])) $default_pricing = TRUE;
  $fromDate = $_POST['from_date'];
  $toDate = $_POST['to_date'];
  $result = $cls_product->get_all_estimate_inventory_between_dates($fromDate, $toDate);
  $bills = $cls_product->get_all_estimate_direct_bills_between_dates($fromDate, $toDate);
//  $total_items = $cls_product->get_all_estimate_inventory_between_dates($fromDate, $toDate, 'count');
}

require_once 'includes/header.php';

$page_number = REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Inventory</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <div class="col-md-12">
              <label class="col-md-1 control-label">From</label>
              <div class="col-md-3">
                <input type="date" name="from_date" class="form-control" id="from_date" placeholder="Date of Birth" value="<?php echo $fromDate ?>">
              </div>
              <label class="col-md-1 control-label">To</label>
              <div class="col-md-3">
                <input type="date" name="to_date" class="form-control" id="to_date" placeholder="Date of Birth" value="<?php echo $toDate ?>">
              </div>

              <input type="checkbox" name="price" id="price" value="1" <?php echo ($default_pricing) ? 'checked' : '';?> class="checkbox checkbox-inline"> <label for="price">Only use price table</label>
              <input type="submit" class="btn btn-success validate" name="search" id="search" value="Search">
              <a href="print_exp_inventory.php?from_date=<?php echo $fromDate; ?>&to_date=<?php echo $toDate; ?>&default_pricing=<?php echo $default_pricing;?>" target="blank" class="btn btn-primary validate <?php echo !$post ? 'disabled' : ''; ?>" id="print">Print</a>
            </div>
          </form>
          <script>
            $('.validate').click(function() {
              var from_date = $('#from_date').val();
              var to_date = $('#to_date').val();
              var error = '';
              var validate = true;
              if (from_date == '')
              {
                error += 'Please select From date\n';
                validate = false;
              }
              if (to_date == '')
              {
                error += 'Please select To date\n';
                validate = false;
              }
              if (validate == false) {
                alert(error);
                return validate;
              }
            });
          </script>
          <div class="col-md-12">&nbsp;</div>
          <?php if (isset($_POST['search'])) { ?>
            <div class="col-md-12">
              <table class="table table-hover table-condensed table-bordered">
                <thead>
                <th>Sr No.</th>
                <th>Item Name</th>
                <th class="text-right alert-info">Expected Quantity</th>
                <th class="text-right">Current Inventory</th>
                <th class="text-right alert-warning">Purchase Quantity</th>
                <th class="text-right">Unit Price</th>
                <th class="text-right alert-success">Estimated Cost</th>
                <th class="text-right alert-info">Category</th>
                </thead>
                <tbody>
                  <?php
                  if ($result) {
                    $i = 1;
                    $total_cost = 0;
                    foreach ($result as $item) {
                      $purchase_qty = $item['qty'] > $item['current_quantity'] ? ($item['qty'] - $item['current_quantity']) : 0;
                      $cost = 0;
                      if ($purchase_qty > 0) {
                        if($default_pricing) $est_cost = $cls_product->get_estimate_cost($item['inventory_id'], 'USE_DEFAULT_COSTING');
                        else $est_cost = $cls_product->get_estimate_cost($item['inventory_id']);
                        $cost = $purchase_qty * $est_cost;
                        $total_cost += $cost;
                      }
                      ?>
                      <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $item['item_name']; ?></td>
                        <td class="text-right alert-info"><?php echo number_format($item['qty'], 3) . ' ' . $item['unit']; ?></td>
                        <td class="text-right"><?php echo number_format($item['current_quantity'], 3) . ' ' . $item['unit']; ?></td>
                        <td class="text-right alert-warning"><?php echo number_format($purchase_qty, 3) . ' ' . $item['unit']; ?></td>
                        <td class="text-right"><?php echo 'Rs. ' . number_format($est_cost, 2) . ' / ' . $item['unit']; ?></td>
                        <td class="text-right alert-success"><?php echo 'Rs. ' . number_format($cost, 2) ?></td>
                        <td class="text-right alert-info"><?php echo $item['category'] ?></td>
                      </tr>
                    <?php }
                    ?>
                    <tr>
                      <td colspan="5"></td>
                      <td class="text-right"><strong>Total:</strong></td>
                      <td class="text-right"><?php echo 'Rs. ' . number_format($total_cost, 2); ?></td>
                      <td></td>
                    </tr>

                    <?php } else {
                    ?>
                    <tr>
                      <td colspan="7" class="alert-danger">No results found.</td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
<!--            <div class="col-md-6">
              <table class="table table-hover table-condensed table-bordered">
                <thead>
                <th>Sr No.</th>
                <th>Item Name</th>
                <th class="text-right alert-warning">Quantity</th>
                <th class="text-right">Unit Price</th>
                <th class="text-right alert-success">Estimated Cost</th>
                <th class="text-right alert-info">Category</th>
                </thead>
                <tbody>
                  <?php
                  if ($bills) {
                    $i = 1;
                    $direct_cost = 0;
                    foreach ($bills as $bill) {
                      if($default_pricing)
                      $est_cost = $cls_product->get_estimate_cost_direct($bill['item'], 'USE_DEFAULT_COSTING');
                      else $est_cost = $cls_product->get_estimate_cost_direct($bill['item']);
                      $cost = $bill['qty'] * $est_cost;
                      $direct_cost += $cost;
                      ?>
                      <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $bill['item']; ?></td>
                        <td class="text-right alert-warning"><?php echo number_format($bill['qty'], 3); ?></td>
                        <td class="text-right"><?php echo 'Rs. ' . number_format($est_cost);?></td>
                        <td class="text-right alert-success"><?php echo 'Rs. ' . number_format($cost);?></td>
                        <td class="text-right alert-info"><?php echo $bill['category'];?></td>
                      </tr>
                    <?php }
                    ?>
                    <tr>
                      <td colspan="3"></td>
                      <td class="text-right"><strong>Total:</strong></td>
                      <td class="text-right"><?php echo 'Rs. ' . number_format($direct_cost, 2); ?></td>
                      <td></td>
                    </tr>
                    <?php } else {
                    ?>
                    <tr>
                      <td colspan="6" class="alert-danger">No results found.</td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>-->
          <?php } ?>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>