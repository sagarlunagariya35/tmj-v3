<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/class.family.php');
require_once('classes/hijri_cal.php');
$cls_receipt = new Mtx_Receipt();
$cls_family = new Mtx_family();

$fId = $_SESSION[FILE];
$PartialAmount = $_SESSION[PARTIAL_AMOUNT];
$name = $_SESSION[NAME];
$monthly_hub = $_SESSION[MONTHLY_HUB];
$paid_till = $_SESSION[PAID_TILL];
$form_id1 = $_SESSION[FORM_ID1];
$form_id2 = $_SESSION[FORM_ID2];
$on_hand = $_SESSION[ON_HAND];
$total_amount = $_SESSION[TOTAL_PARTIAL_AMOUNT];
// create a unique hash for the transaction
$len = mt_rand(4, 10);
$form_id = '';
for ($i = 0; $i < $len; $i++) {
  $d = rand(1, 30) % 2;
  $form_id .= $d ? chr(rand(65, 90)) : chr(rand(48, 57));
}

$form_id = md5(time() . $form_id);
$close = time();
$type = 'Cash';
$month = 1;
$bank = '';
$cheque = '';
$date = '';
$total = 0;
$key = '';
$ary_payment = array();
$i = 1;
while ($total <= $total_amount) {

  if (isset($ary_payment) && (!empty($ary_payment))) {
    if (array_key_exists($key, $ary_payment))
      $ary_payment[$key] = $total;
    $i++;
  }
  $result = $cls_family->get_last_year_month($fId);
  if ($result) {
    $paidTill = $result['paid_till'];
    $till_dt = HijriCalendar::GregorianToHijri($result['paid_till']);
    $till_mon_plus_one = $till_dt[0] + $i;
    $till_year = $till_dt[2];
    $key = $till_mon_plus_one . '-' . $till_year;
    $ary_payment[$key] = '';
    $user_ts = HijriCalendar::HijriToUnix($till_mon_plus_one, '01', $till_year);
    $hub = $cls_family->get_hub_by_month($fId, $user_ts);
    if ($hub) {
      $hub_mon = $hub['Hub_raqam'];
      $total += $hub_mon;
    }
  }
}
$result = $cls_family->get_last_year_month($fId);
if ($result) {
  $till_dt = HijriCalendar::GregorianToHijri($result['paid_till']);
  $till_mon_plus_one = $till_dt[0] + 1;
  $till_year = $till_dt[2];
  if ($till_mon_plus_one > 12) {
    $till_mon_plus_one = 1;
    $till_year = $till_year + 1;
  }

  $to_month = ($till_mon_plus_one + ($i - 2)) % 12;
  $to_year = floor(($till_dt[0] + ($i - 2)) / 12) + $till_year;
  //$to_year = floor(($last_month + $for_months) / 12) + $last_year;
  // FIX: to solve round months error in upto date
  if ($to_month == 0) {
    $to_month = 12;
    $to_year = $to_year - 1;
  }

  $till = HijriCalendar::HijriToUnix($till_mon_plus_one, '01', $till_year);
  $upto = HijriCalendar::HijriToUnix($to_month, '01', $to_year);
}

$ary_count_payment = count($ary_payment); //1
$values = array_values($ary_payment);
$used_amount = $values[$ary_count_payment - 2];
$remaining_amount = ($total_amount - $values[$ary_count_payment - 2]);

$user_id = $_SESSION[USER_ID];

if (isset($_POST['yes'])) {
  $result = $cls_receipt->add_partial_payment($fId, $used_amount - $on_hand, $name, $user_id, $form_id2, $close);
  if ($result) {
    $sep = explode(',', $result);
    $max = count($sep);
    $remaining = $cls_receipt->add_remaining_payment($fId, $remaining_amount, $name, $user_id, $form_id);
    if ($remaining) {
      $hub = $cls_receipt->add_receipt_hub($fId, $name, $used_amount, $till, $upto, $month, $type, $bank, $cheque, $date, $user_id, $form_id1, $used_amount, 'N', $result);
      header('Location: print_partial_receipt.php?id=' . $sep[$max - 1] . '&cmd=another_receipt&hub=' . $hub . '&ap_rcpt=' . $remaining);
      exit;
    }
  }
}
if (isset($_POST['no'])) {
  $paid_amt = $cls_receipt->add_partial_payment($fId, $used_amount - $on_hand, $name, $user_id, $form_id2, $close, 0, 'return_id');
  if ($result) {
    $sep = explode(',', $result);
    $max = count($sep);
    $remaining = $cls_receipt->add_remaining_payment($fId, $remaining_amount, $name, $user_id, $form_id);
    if ($remaining) {
      $hub = $cls_receipt->add_receipt_hub($fId, $name, $used_amount, $till, $upto, $month, $type, $bank, $cheque, $date, $user_id, $form_id1, $used_amount, 'N', $result);
      header('Location: print_partial_receipt.php?id=' . $paid_amt . '&cmd=normal');
      exit;
    }
  }
}

$title = "Receipt confirmation";
$active_page = "account";

include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Credit</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <div class="col-md-3 pull-left">
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Search</h3></div>
            <div class="panel-body">
              <?php include('includes/search_bar.php'); ?>
            </div>
          </div>
        </div>
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-8 ">
          <form method="post" role="form" class="form-horizontal">
            <p style="font-size: 20px">Do you want to print receipt for hub too?</p>
            <input type="submit" name="yes" value="Yes" class="btn btn-success">
            <!--input type="submit" name="no" value="No" class="btn btn-danger"-->
          </form>
        </div>
        <!-- /Center Bar -->


      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>