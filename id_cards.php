<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.menu.php');
$cls_menu = new Mtx_Menu();
$cls_family = new Mtx_family();
$user_id = $_SESSION[USER_ID];

$title = 'Id Cards';
$active_page = 'settings';

require_once 'includes/header.php';

$page_number = REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" target="blank" class="form-horizontal" action="print_id_cards.php" enctype="multipart/form-data">
            <div class="data">
              <div class="form-group">
                <label class="control-label col-md-4">Name</label>
                <div class="col-md-5">
                  <input type="text" name="name[]" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-4">ITS</label>
                <div class="col-md-5">
                  <input type="text" name="its[]" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-4">Mobile No</label>
                <div class="col-md-5">
                  <input type="text" name="mobile[]" class="form-control" maxlength="13">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-4">Department</label>
                <div class="col-md-5">
                  <input type="text" name="department[]" class="form-control" maxlength="13">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-4">Upload Pic</label>
                <div class="col-md-5">
                  <input type="file" name="Upload[]">
                </div>
              </div>
            </div>

            <div class="col-md-offset-4 col-md-4">
              <button id="add_more" class="btn btn-link" type="button">Add More</button>
              <button name="add_labels" id="add_labels" type="submit" class="btn btn-success">Print</button>
              <a href="index.php" class="btn btn-info">Home</a>

            </div>

          </form>
          <div style="display: none" id="copy">
            <div class="form-group">
              <label class="control-label col-md-4">Name</label>
              <div class="col-md-5">
                <input type="text" name="name[]" class="form-control">
              </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">ITS</label>
                <div class="col-md-5">
                  <input type="text" name="its[]" class="form-control">
                </div>
              </div>
            <div class="form-group">
              <label class="control-label col-md-4">Mobile No</label>
              <div class="col-md-5">
                <input type="text" name="mobile[]" class="form-control" maxlength="13">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Department</label>
              <div class="col-md-5">
                <input type="text" name="department[]" class="form-control" maxlength="13">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Upload Pic</label>
              <div class="col-md-5">
                <input type="file" name="Upload[]">
              </div>
            </div>
          </div>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>

<script>
  $('#add_more').click(function() {
    $('#copy').children().clone().appendTo('.data');
  });
</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>