<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.menu.php');
$cls_menu = new Mtx_Menu();

$from_date = $_GET['from_date'];
$to_date = $_GET['to_date'];
$fdate = explode('-', $from_date);
$fromDate = mktime(0, 0, 0, $fdate[1], $fdate[2], $fdate[0]);
$tdate = explode('-', $to_date);
$toDate = mktime(23, 59, 59, $tdate[1], $tdate[2], $tdate[0]);
$menus = $cls_menu->get_menu_between_dates($fromDate, $toDate);
$title = 'Costing Report';
$active_page = 'report';
?><!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->


  <!-- Center Bar -->
  <div class="col-md-8">
    <div class="col-md-12">&nbsp;</div>
      <div class="col-md-12">
        <table class="table table-hover table-condensed table-bordered">
          <thead>
            <tr>
              <th colspan="4" class="text-right"><?php echo date('d F, Y');?></th>
            </tr>
            <tr>
              <th>Sr No.</th>
              <th>Hijri Date</th>
              <th>Menu</th>
              <th class="text-right">Estimated Cost</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if ($menus) {
              $i = 1;
              $ary_avg_cost = array();
              $total_est_cost = 0;
              foreach ($menus as $menu) {
                $menu_name = $cls_menu->get_base_menu_name($menu['menu_id']);
                $est_cost = $menu['est_costing'];
                if($est_cost > 0) {
                  array_push($ary_avg_cost, $est_cost);
                  $total_est_cost += $est_cost;
                }
                $hijari = HijriCalendar::GregorianToHijri($menu['timestamp']);
                $hday = $hijari[1];
                $month = HijriCalendar::monthName($hijari[0]);
                $hyear = $hijari[2];
                ?>
                <tr>
                  <td><?php echo $i++; ?></td>
                  <td><?php echo $hday . ' ' . $month . ', ' . $hyear . ' H';?></td>
                  <td><?php echo $menu_name; ?></td>
                  <td class="text-right"><?php echo number_format($est_cost, 2); ?></td>
                </tr>
              <?php }
              ?>
              <tr>
                <td colspan="4" class="alert-info"><strong>Total Estimated Cost: <?php echo number_format($total_est_cost, 2);?></strong></td>
              </tr>
              <tr>
                <td colspan="4" class="alert-warning"><strong>Average Per Day Cost: <?php echo number_format(($total_est_cost / count($ary_avg_cost)), 2);?></strong></td>
              </tr>

              <?php } else {
              ?>
              <tr>
                <td colspan="4" class="alert-danger">No results found.</td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
  </div>
  <!-- /Center Bar -->


</div>
<!-- /Content -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>