<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.menu.php");
require_once("classes/class.family.php");
$cls_menu = new Mtx_Menu();
$post = FALSE;
$items = $cls_menu->get_inventory_asset();
if($items) $post = TRUE;
$title = 'Inventory Asset report';
$active_page = 'report';

require_once 'includes/header.php';

$page_number = INVENTORY_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-12">
            <a href="print_inventory_asset.php" target="_blank" class="btn btn-primary btn-xs pull-right <?php echo !$post ? 'disabled' : ''; ?>">Print</a>
            <div class="clearfix"></div>
          </div>

          <div class="col-md-12">
          <?php
          $print = FALSE;
          include 'includes/inc.inventory_asset.php';
          ?>
            </div>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>