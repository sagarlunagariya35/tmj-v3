<?php

function get_menu_link($user_type, $key_page, $user_rights, $link, $link_text, $user_name = FALSE) {
  $class = '';
  if ($user_type != 'A') {
    if (array_key_exists($key_page, $user_rights) AND $user_rights[$key_page] == 1) {
      return "<li $class><a href=\"$link\" ><i class='fa fa-circle-o'></i>$link_text</a></li>";
    }
  }else {
    if ($user_name == FALSE OR $user_name == MATRIX) {
      return "<li $class><a href=\"$link\" ><i class='fa fa-circle-o'></i>$link_text</a></li>";
    }
  }
}

function show_menu_folder($user_type, $key_pages, $user_rights, $user_name = FALSE) {
  if ($user_type != 'A') {
    foreach ($key_pages as $key_page){
      if (array_key_exists($key_page, $user_rights) AND $user_rights[$key_page] == 1) {
        return TRUE;
      }
    }
    return FALSE;
  }else {
    if ($user_name == FALSE OR $user_name == MATRIX) {
      return TRUE;
    }
  }
}

function get_right_menu_link($user_type, $key_page, $user_rights, $link, $link_text, $user_name = FALSE) {
  $class = '';
  if ($user_type != 'A') {
    if (array_key_exists($key_page, $user_rights) AND $user_rights[$key_page] == 1) {
      return "<li $class><a style='padding: 5px 0 0 10px;' href=\"$link\" ><i class='fa fa-files-o'></i><span style='color: #FFF;margin-left:5px;'> $link_text</span></a></li>";
    }
  }else {
    if ($user_name == FALSE OR $user_name == MATRIX) {
      return "<li $class><a style='padding: 5px 0 0 10px;' href=\"$link\" ><i class='fa fa-files-o'></i><span style='color: #FFF;margin-left:5px;'> $link_text</span></a></li>";
    }
  }
}

function get_all_suggestions($page_num, $row_count)
{
  global $database;
  $limit = ($page_num - 1) * $row_count;
  
  $query = "SELECT * FROM `suggestion` ORDER BY `id` DESC LIMIT $limit,$row_count";
  $result = $database->query_fetch_full_result($query);
  return $result;
}

function get_total_suggestions()
{
  global $database;  
  $query = "SELECT COUNT(*) AS `Total` FROM `suggestion`";
  $result = $database->query_fetch_full_result($query);
  return $result[0]['Total'];
}

function get_family_data_by_its($its)
{
  global $database;
  $query = "SELECT * FROM `family` WHERE `ejamatID` = '$its'";
  $result = $database->query_fetch_full_result($query);
  return $result[0];
}

function reply_suggestion($id, $reply, $email)
{
  global $database;
  $date = date('Y-m-d');
  
  $query = "UPDATE `suggestion` SET `reply_message` = '$reply', `reply_timestamp` = '$date' WHERE `id` = '$id'";
  $result = $database->query($query);
  
  
  if($result){
    if($email != '1'){
      $to = $email;
      $subject = "Anjuman E Mohammedi - Surat";
      $message = $reply;        
      $headers = 'From: '.FROM_EMAIL. "\r\n";

      mail($to,$subject,$message,$headers);
    }
  }
  return $result;
}

function read_suggestion($id)
{
  global $database;
  $query = "UPDATE `suggestion` SET `read` = '1' WHERE `id` = '$id'";
  
  $result = $database->query($query);
  return $result;
}

function get_total_unread_suggestions()
{
  global $database;  
  $query = "SELECT COUNT(*) AS `Total` FROM `suggestion` WHERE `read` = '0'";
  $result = $database->query_fetch_full_result($query);
  return $result[0]['Total'];
}

function get_list_unread_suggestions()
{
  global $database;
  $query = "SELECT * FROM `suggestion` ORDER BY `read` ASC, `id` DESC LIMIT 5";
  $result = $database->query_fetch_full_result($query);
  return $result;
}

function count_total_family()
{
  global $database;  
  $query = "SELECT COUNT(*) AS `Total` FROM `family`";
  $result = $database->query_fetch_full_result($query);
  return $result[0]['Total'];
}

function sent_to_do_text($to_do_creator, $to_do_receiver, $to_do_text)
{
  global $database;
  $date = date('Y-m-d');
  $query = "INSERT INTO to_do_list(`to_do_creator`, `to_do_receiver`, `to_do_text`, `to_do_ts`) VALUES('$to_do_creator', '$to_do_receiver', '$to_do_text', '$date')";
  
  $result = $database->query($query);
  return $result;
}

function get_all_to_do_list($to_do_receiver, $page_num, $row_count)
{
  global $database;
  $limit = ($page_num - 1) * $row_count;
  
  $query = "SELECT * FROM `to_do_list` WHERE `to_do_receiver` = '$to_do_receiver' && `to_do_read` = '0' ORDER BY `id` DESC LIMIT $limit,$row_count";
  
  $result = $database->query_fetch_full_result($query);
  return $result;
}

function get_total_to_do_list($to_do_receiver)
{
  global $database;  
  $query = "SELECT COUNT(*) AS `Total` FROM `to_do_list` WHERE `to_do_receiver` = '$to_do_receiver' && `to_do_read` = '0'";
  $result = $database->query_fetch_full_result($query);
  return $result[0]['Total'];
}

function update_to_do_text($to_do_read, $to_do_id)
{
  global $database;
  $date = date('Y-m-d');
  $query = "UPDATE `to_do_list` SET `to_do_read` = '$to_do_read', `to_do_read_ts` = '$date' WHERE `id` = '$to_do_id'";
  
  $result = $database->query($query);
  return $result;
}

function delete_to_do_list($to_do_id)
{
  global $database;
  $query = "DELETE FROM `to_do_list` WHERE `id` = $to_do_id";
  $result = $database->query($query);
  return $result;
}

function csv_to_array($filename='', $delimiter=',')
{
    if(!file_exists($filename) || !is_readable($filename))
        return FALSE;

    $header = NULL;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== FALSE)
    {
        while (($row = fgetcsv($handle)) !== FALSE)
        {
            if(!$header)
                $header = $row;
            else
                $data[] = array_merge($header, $row);
        }
        fclose($handle);
    }
    return $data;
}
?>