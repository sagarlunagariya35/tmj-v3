<?php
include('session.php');
$page_number = 70;
require_once("classes/class.database.php");
require_once("classes/class.receipt.php");
$cls_receipt = new Mtx_Receipt();

$hubReceipt = $cls_receipt->get_all_hub_receipt_cheque();
$volReceipt = $cls_receipt->get_all_vol_receipt_cheque();
if($hubReceipt) $hubReceipt = sync_data ($hubReceipt, 'HUB');
if($volReceipt) $volReceipt = sync_data ($volReceipt, 'VOL');
function sync_data($ary_result, $rcpt_type) {
  foreach($ary_result as $key => $val) {
    $ary_result[$key]['RECEIPT'] = $rcpt_type;
  }
  return $ary_result;
}
if(!$volReceipt) $volReceipt = array();
if(!$hubReceipt) $hubReceipt = array();
$receipt = array_merge($hubReceipt, $volReceipt);

$title = 'Today\'s Activities';
$active_page = 'report';

include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <table class="table table-hover table-condensed table-bordered">
            <thead>
              <tr>
                <th>No.</th>
                <th>Receipt ID</th>
                <th>Name</th>
                <th class="text-right">Amount</th>
                <th>Bank Name</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
              <?php
              function create_link($page, $rcpt_id) {
                return "<a href='$page.php?id=$rcpt_id' target='_blank' >$rcpt_id</a>";
              }
              if($receipt){
                $i = 1;
                foreach($receipt as $key => $rcpt){
                  $link =  ($rcpt['RECEIPT'] == 'HUB') ? create_link('print_hub_receipt', $rcpt['id']) : create_link('print_voluntary_receipt', $rcpt['id']);
                  ?>
              <tr>
                <td><?php echo $i++;?></td>
                <td><?php echo $link;?></td>
                <td><?php echo $rcpt['name'];?></td>
                <td class="text-right"><?php echo $rcpt['amount'];?></td>
                <td><?php echo ucwords(strtolower($rcpt['bank']));?></td>
                <td><?php echo date('d F, Y', $rcpt['timestamp']);?></td>
              </tr>
              <?php
                  }
              } else {
                echo '<tr><td colspan="6" class="alert-danger">Sorry! no activity to do today.</td></tr>';
              }
              ?>
            </tbody>
          </table>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>