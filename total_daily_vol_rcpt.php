<?php
include 'session.php';
$page_number = 41;
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/hijri_cal.php');
$cls_receipt = new Mtx_Receipt();

$from_date = $to_date = date('Y-m-d');

if (isset($_POST['search'])) {
  $from_date = $_POST['from_date'];
  $to_date = $_POST['to_date'];
}
$from_split = explode('-', $from_date);
$from_ts = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
$to_split = explode('-', $to_date);
$to_ts = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);  

$receipts = $cls_receipt->get_daily_vol_rcpt_between_days($from_ts, $to_ts);
$title = 'Total vol receipt ' . date('d M,Y');
$active_page = 'report';

include('includes/header.php');
include('page_rights.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Hub</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <div class="col-md-12">
              <label class="col-md-1 control-label">From</label>
              <div class="col-md-4">
                <input type="date" name="from_date" class="form-control" id="from_date" value="<?php echo $from_date; ?>">
              </div>
              <label class="col-md-1 control-label">To</label>
              <div class="col-md-4">
                <input type="date" name="to_date" class="form-control" id="to_date" value="<?php echo $to_date; ?>">
              </div>

              <input type="submit" class="btn btn-success validate" name="search" id="search" value="Search">
                <a target="_blank" href="print_total_daily_vol.php?from=<?php echo $from_date; ?>&to=<?php echo $to_date; ?>" class="btn btn-primary">Print</a>
            </div>
          </form>
          <div class="col-md-12">&nbsp;</div>
      <?php
      if ($receipts)
        echo $receipts;
      ?>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>