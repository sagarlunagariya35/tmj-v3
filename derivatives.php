<?php
include('session.php');
require_once("classes/class.database.php");
require_once 'classes/class.quadratic_derivatives.php';
require_once 'classes/functions.php';

$title = 'Base Menu';
$ary_x = array(50,500,1000);
$step = 0.1;

if(isset($_POST['btn_submit'])) {
  
  $csv_file = $_FILES['csv_file']['name'];
  $csv_file_temp = $_FILES['csv_file']['tmp_name'];
  $pathANDname = "csv_files/" . $csv_file;
  move_uploaded_file($csv_file_temp, $pathANDname);
  
  $result = csv_to_array($pathANDname, ',');
  if($result){
    $i=0;
    foreach ($result as $row){
      $base_menu_id = $row[$i];
      $item_id = $row[$i + 4];
      $a = $row[$i + 5];
      $b = $row[$i + 6];
      $c = $row[$i + 7];

      $ary_y = array($a,$b,$c);
      $calc = new Mtx_Quad_derivative($ary_x, $ary_y, $step);

      $var_a = $calc->get_a();
      $var_b = $calc->get_b();
      $var_c = $calc->get_c();

      global $database;
      $query = "INSERT INTO `base_menu_der`(`base_menu_id`,`item_id`,`var_a`,`var_b`,`var_c`,`var_step`) VALUES('$base_menu_id','$item_id','$var_a','$var_b','$var_c','$step')";
      $rslt = $database->query($query);

      if($rslt){
        $_SESSION[SUCCESS_MESSAGE] = 'Data Inserted Successfully..';
      }else {
        $_SESSION[ERROR_MESSAGE] = $query;
      }
    }
  }
}

?>

<html>
  <head>
    <meta charset="UTF-8">
    <title><?php echo $title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="asset/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="asset/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
  </head>
  <body class="login-page">
    <div class="row">
      <div class="col-md-12"><?php include 'includes/messages.php'; ?></div>
    </div>
    <div class="col-md-12">&nbsp;</div>
    <form action="" method="post" enctype="multipart/form-data">
      <div class="form-group col-md-8 col-xs-12">
        <label for="base_menu_file" class="col-md-3 text-right">Base Menu File (CSV) :</label>
        <input type="file" name="csv_file" class="file col-md-4">
      </div>

      <div class="clearfix"></div>
      <div class="form-group col-md-8 col-xs-12">
        <label class="col-md-3">&nbsp;</label>
        <div class="form-group col-md-2 col-xs-12">
          <input type="submit" name="btn_submit" id="btn_submit" value="Submit" class="btn btn-primary btn-block form-control">
        </div>
      </div>
    </form>

    <!-- jQuery 2.1.4 -->
    <script src="asset/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="asset/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="asset/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
