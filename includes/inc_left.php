<?php
  $rights = $_SESSION[USER_RIGHTS];
?>
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="treeview<?php if ($active_page == 'dashboard') echo ' active'; ?>">
        <a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
      </li>
      <?php
        if(show_menu_folder($_SESSION[USER_TYPE], array(PROFILE_ENTRY, TAKHMEEN), $rights)) {
      ?>
      <li class="treeview<?php if ($active_page == 'family') echo ' active'; ?>">
        <a href="#">
          <i class="fa fa-files-o"></i>
          <span>Profiles</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <?php 
            echo get_menu_link($_SESSION[USER_TYPE], PROFILE_ENTRY, $rights, 'list_family.php', 'List of families');
            echo get_menu_link($_SESSION[USER_TYPE], PROFILE_ENTRY, $rights, 'add_family.php', 'Add Family');
            echo get_menu_link($_SESSION[USER_TYPE], PROFILE_ENTRY, $rights, 'hub_tiffin_change.php', 'Change Hub & Tiffin');
            echo get_menu_link($_SESSION[USER_TYPE], TAKHMEEN, $rights, 'takhmeen_amount.php', 'Yearly Takhmeen Amount');
            echo get_menu_link($_SESSION[USER_TYPE], TAKHMEEN, $rights, 'cal_dates.php', 'Takhmeen dates');
            echo get_menu_link($_SESSION[USER_TYPE], PROFILE_ENTRY, $rights, 'barcode_scanning.php', 'Scanning..');
          ?>
        </ul>
      </li>
      <?php 
        } 
        if(show_menu_folder($_SESSION[USER_TYPE], array(ACCOUNTS_ENTRY, INVENTORY_ENTRY, ACCOUNTS_REPORTS, INVENTORY_REPORTS), $rights)) {
      ?>
      <li class="treeview<?php if ($active_page == 'menu') echo ' active'; ?>">
        <a href="#">
          <i class="fa fa-pie-chart"></i> 
          <span>Maedat</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <?php
            if(show_menu_folder($_SESSION[USER_TYPE], array(ACCOUNTS_ENTRY, INVENTORY_ENTRY, ACCOUNTS_REPORTS), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-share"></i> Set Up <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'direct_pur_item.php', 'Manage Direct Purchase');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'base_category.php', 'Manage Base Menu Category');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'inv_category.php', 'Manage Inventory Category');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'costing.php', 'Costing');
              ?>
            </ul>
          </li>
          <?php
            }
            if(show_menu_folder($_SESSION[USER_TYPE], array(INVENTORY_ENTRY), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-share"></i> Menu <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'drag_cat.php', 'Drag & Drop Category');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'base_menus.php', 'Manage Base Menu');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'get_base_menu_qty.php', 'Get Base Menu Quantity');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'calc.php', 'Roti calculator');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'daily_menu.php', 'Manage Daily Menu');
              ?>
            </ul>
          </li>
          <?php
            }
            if(show_menu_folder($_SESSION[USER_TYPE], array(INVENTORY_ENTRY, INVENTORY_REPORTS), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-share"></i> Inventory <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'add_inventory.php', 'Add Inventory');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'adjust_inventory.php', 'Adjust Inventory');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_REPORTS, $rights, 'inventory.php', 'List of Inventory');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'item_add.php', 'Add Item');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'return_inventory.php', 'Return Inventory');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_REPORTS, $rights, 'inventory_latest_price.php', 'Latest Price', $_SESSION[USERNAME]);
              ?>
            </ul>
          </li>
          <?php
            }
          echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'distribution_center.php', 'Manage Distribution Center', $_SESSION[USERNAME]);
          ?>
        </ul>
      </li>
      <?php 
        }
        if(show_menu_folder($_SESSION[USER_TYPE], array(ACCOUNTS_ENTRY, INVENTORY_ENTRY, ACCOUNTS_REPORTS), $rights)) {
      ?>
      <li class="treeview<?php if ($active_page == 'account') echo ' active'; ?>">
        <a href="#">
          <i class="fa fa-laptop"></i>
          <span>Accounts</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <?php
            if(show_menu_folder($_SESSION[USER_TYPE], array(ACCOUNTS_ENTRY), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-folder"></i> Credit <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'hub_receipt.php', 'FMB Hub');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'voluntary_rcpt.php', 'Volun. Contri.');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'credit_voucher.php', 'Credit Voucher');
              ?>
            </ul>
          </li>
          <?php
            }
          echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'inventory_asset.php', 'Asset Inventory');
            if(show_menu_folder($_SESSION[USER_TYPE], array(ACCOUNTS_REPORTS), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-folder"></i> Receipt Books <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'hub_rcpt_book.php', 'Hub Receipt Book');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'voluntary_rcpt_book.php', 'Vol. Receipt Book');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'book_credit.php', 'Credit Voucher Book');
              ?>
            </ul>
          </li>
          <?php
            }
            if(show_menu_folder($_SESSION[USER_TYPE], array(ACCOUNTS_ENTRY), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-folder"></i> Debit <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'add_bill.php', 'Bill Payment');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'direct_purchase.php', 'Direct Purchase');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'debit_voucher.php', 'Debit Voucher');
              ?>
            </ul>
          </li>
          <?php
            }
            if(show_menu_folder($_SESSION[USER_TYPE], array(ACCOUNTS_ENTRY, ACCOUNTS_REPORTS), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-folder"></i> Bill Books <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'grocery_bill_book.php', 'Bill Book');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'direct_plus_payment.php', 'Direct Purchase Book');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'bill_pending.php', 'Bill Pending');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'book_debit.php', 'Debit Voucher Books');
              ?>
            </ul>
          </li>
          <?php
            }
          echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'daily_cash.php', 'Daily Cash');
          echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'daily_cash_list.php', 'List of Daily Cash');
          echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'list_cheque.php', 'Manage all cheques');
          echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_ENTRY, $rights, 'unclear_cheque_and_receipt.php', 'Manage Unclear Cheque and Receipts');
          echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'daily_income_expense.php', 'Daily Income and Expense');
          ?>
        </ul>
      </li>
      <?php
        }
        if(show_menu_folder($_SESSION[USER_TYPE], array(REPORTS, HUB_PENDING_REPORTS, ACCOUNTS_REPORTS, INVENTORY_REPORTS, INVENTORY_ENTRY, PROFILE_REPORTS, ACCOUNTS_BALANCESHEET, IDARAH_REPORTS), $rights)) {
      ?>
      <li class="treeview<?php if ($active_page == 'report') echo ' active'; ?>">
        <a href="#">
          <i class="fa fa-table"></i> <span>Reports</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <?php
          echo get_menu_link($_SESSION[USER_TYPE], REPORTS, $rights, 'list_families.php', 'List of Families');
          if(show_menu_folder($_SESSION[USER_TYPE], array(REPORTS, HUB_PENDING_REPORTS, ACCOUNTS_REPORTS), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-book"></i> Hub <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              if($_SESSION[USER_TYPE] == 'A'){ $s_link = 'hub_pending.php?cmd=hub_pending'; }
              else { $s_link = 'hub_pending_standard.php?cmd=hub_pending'; }

              echo get_menu_link($_SESSION[USER_TYPE], HUB_PENDING_REPORTS, $rights, $s_link, 'Hub Pending Report');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'by_date_finance.php?cmd=hub_pending', 'Finance at a Glance', $_SESSION[USERNAME]);
              echo get_menu_link($_SESSION[USER_TYPE], HUB_PENDING_REPORTS, $rights, 'total_daily_hub_rcpt.php', 'Total daily hub receipt');
              echo get_menu_link($_SESSION[USER_TYPE], REPORTS, $rights, 'total_daily_vol_rcpt.php', 'Total daily Voluntary receipt', $_SESSION[USERNAME]);
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'calculated_muasaat.php', 'Calculated Muasaat', $_SESSION[USERNAME]);
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'FMB_details.php', 'FMB Hub Details', $_SESSION[USERNAME]);
              ?>
            </ul>
          </li>
          <?php
            }
            if(show_menu_folder($_SESSION[USER_TYPE], array(REPORTS, ACCOUNTS_REPORTS, PROFILE_REPORTS), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-book"></i> Thali <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], REPORTS, $rights, 'tiffin_issue_report.php', 'Issued Thaali Report');
              echo get_menu_link($_SESSION[USER_TYPE], REPORTS, $rights, 'tiffin_not_issue_report.php', 'Thaali not issue Report');
              echo get_menu_link($_SESSION[USER_TYPE], REPORTS, $rights, 'tiffin_issue_count.php', 'Thaali issue / not issued count report');
              echo get_menu_link($_SESSION[USER_TYPE], REPORTS, $rights, 'takhmeen_report.php', 'Takhmeen Report', $_SESSION[USERNAME]);
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'costing_report.php', 'Costing Report', $_SESSION[USERNAME]);
              echo get_menu_link($_SESSION[USER_TYPE], PROFILE_REPORTS, $rights, 'scanning_issue_count.php', 'Consolidated Report', $_SESSION[USERNAME]);
              ?>
            </ul>
          </li>
          <?php
            }
            if(show_menu_folder($_SESSION[USER_TYPE], array(REPORTS, INVENTORY_REPORTS, INVENTORY_ENTRY), $rights, $_SESSION[USERNAME])) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-book"></i> Inventory <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_REPORTS, $rights, 'inventory_issue_report.php', 'Inventory Issue Report');
              echo get_menu_link($_SESSION[USER_TYPE], REPORTS, $rights, 'expected_inventory_issue.php', 'Expected Inventory Report');
              echo get_menu_link($_SESSION[USER_TYPE], REPORTS, $rights, 'estimate_inventory_report.php', 'Estimate Inventory Report');
              echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_ENTRY, $rights, 'confirm_menus.php', 'Confirm Menu');
              ?>
            </ul>
          </li>
          <?php
            }
          echo get_menu_link($_SESSION[USER_TYPE], PROFILE_REPORTS, $rights, 'new_tiffin_report.php', 'New Tiffin Report', $_SESSION[USERNAME]);
          echo get_menu_link($_SESSION[USER_TYPE], REPORTS, $rights, 'fmb_thali_barakat.php', 'FMB Thaali Barakat Details', $_SESSION[USERNAME]);
          echo get_menu_link($_SESSION[USER_TYPE], REPORTS, $rights, 'thali_quantity.php', 'Thali Count Report', $_SESSION[USERNAME]);
          echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'todays_activities.php', 'Todays Activities', $_SESSION[USERNAME]);
          echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_REPORTS, $rights, 'suggestion_list.php', 'List of Suggestions', $_SESSION[USERNAME]);
          echo get_menu_link($_SESSION[USER_TYPE], REPORTS, $rights, 'datewise_menu_report.php', 'Datewise menu Report', $_SESSION[USERNAME]);
          echo get_menu_link($_SESSION[USER_TYPE], INVENTORY_REPORTS, $rights, 'inventory_asset_report.php', 'Inventory asset Report');
          
          if(show_menu_folder($_SESSION[USER_TYPE], array(ACCOUNTS_BALANCESHEET), $rights)) {
          ?>
          <li>
            <a href="#">
              <i class="fa fa-book"></i> Balance Sheet <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_BALANCESHEET, $rights, 'new_balancesheet.php', 'Complete Balance Sheet', $_SESSION[USERNAME]);
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_BALANCESHEET, $rights, 'datewise_balancesheet.php', 'Date Wise Balance Sheet');
              echo get_menu_link($_SESSION[USER_TYPE], ACCOUNTS_BALANCESHEET, $rights, 'balancesheet3.php', 'Balance Sheet2');
              ?>
            </ul>
          </li>
          <?php
            }
          echo get_menu_link($_SESSION[USER_TYPE], IDARAH_REPORTS, $rights, 'idarah_report.php', 'Idarah Report', $_SESSION[USERNAME]);
          echo get_menu_link($_SESSION[USER_TYPE], IDARAH_REPORTS, $rights, 'quarter_idarah_report.php', 'Quarter Idarah Report', $_SESSION[USERNAME]);
          ?>
        </ul>
      </li>
      <?php
        }
        if ($_SESSION[USER_TYPE] == 'A') {
      ?>
      <li class="treeview<?php if ($active_page == 'settings') echo ' active'; ?>">
        <a href="#">
          <i class="fa fa-edit"></i> <span>Settings</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li><a href="general_settings.php"><span class="glyphicon glyphicon-plus-sign"></span> General Settings</a></li>
            <li><a href="send_sms_h.php"><span class="glyphicon glyphicon-plus-sign"></span> Send SMS</a></li>
            <li><a href="send_email.php"><span class="glyphicon glyphicon-plus-sign"></span> Send Email</a></li>
            <?php if ($_SESSION[USERNAME] == MATRIX) { ?>
            <li><a href="correct_name.php"><span class="glyphicon glyphicon-plus-sign"></span> Correct HOF Name</a></li>
            <?php } ?>
            <li><a href="shop_entry.php"><span class="glyphicon glyphicon-plus-sign"></span> Manage Company</a></li>
            <li><a href="daily_menu_costing.php"><span class="glyphicon glyphicon-plus-sign"></span> Refresh Menu costing</a></li>
            <?php if ($_SESSION[USERNAME] == MATRIX) { ?>
            <li><a href="user_add.php"><span class="glyphicon glyphicon-plus-sign"></span> Add User</a></li>
            
            <li><a href="user_manage.php"><span class="glyphicon glyphicon-refresh"></span> Manage Users</a></li>
            
            <!--li><a href="tiffin_cost.php"><span class="glyphicon glyphicon-edit"></span> Thali Hub</a></li-->
            <li><a href="tiffin_size_add.php"><span class="glyphicon glyphicon-folder-close"></span> Tiffin Size</a></li>
            <li><a href="tanzeem.php"><span class="glyphicon glyphicon-globe"></span> Tanzeem</a></li>
            <li><a href="commitment_dates.php"><span class="glyphicon glyphicon-globe"></span> Commitment Dates</a></li>
            <li><a href="voluntary_remarks.php"><span class="glyphicon glyphicon-pencil"></span> Vol. Contri. remarks</a></li>
            <li><a href="niyaz_entry_form.php"><span class="glyphicon glyphicon-pencil"></span> Niyaz / Salwaat</a></li>
            <?php } ?>
            <li><a href="message_add.php"><span class="glyphicon glyphicon-pencil"></span> Messages</a></li>
            <!--li><a href="fmb_hub_exception.php"><span class="glyphicon glyphicon-pencil"></span> FMB Hub Exception</a></li-->
            <li><a href="account_heads_master.php"><span class="glyphicon glyphicon-pencil"></span> Account Master Heads</a></li>
            <li><a href="account_heads.php"><span class="glyphicon glyphicon-pencil"></span> Account Heads</a></li>
            <li><a href="add_inventory_min_qty.php"><span class="glyphicon glyphicon-pencil"></span> Set Min Quantity</a></li>
            <li>
            <a href="#">
              <i class="fa fa-book"></i> Print <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li><a href="labels.php"><span class="glyphicon glyphicon-pencil"></span> Print Labels</a></li>
              <li><a href="print_barcode.php"><span class="glyphicon glyphicon-pencil"></span> Print Barcode</a></li>
              <li><a href="id_cards.php"><span class="glyphicon glyphicon-pencil"></span> Print Id Cards</a></li>
            </ul>
          </li>
          <li><a href="transfer.php"><span class="glyphicon glyphicon-pencil"></span> Transfer Family</a></li>
        </ul>
      </li>
      <?php } ?>
      <li class="treeview<?php if ($active_page == 'search') echo ' active'; ?>">
        <a href="#">
          <i class="fa fa-circle-o text-red"></i>
          <span>Search</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li data-toggle="modal" data-target="#SEARCH_HOF_DETAILS"><a>Search</a></li>
          <?php if (SHOW_REF_FMB_HUB) { ?>
            <li><a data-toggle="modal" data-target="#SEARCH_FC_CODE_FRM">Fc Code</a></li>
          <?php 
          } 
           if ($_SESSION[USERNAME] == MATRIX) {
          ?>
          <li><a data-toggle="modal" data-target="#SEARCH_TAKHMEEN_CALCULATION_FORM">Takhmeen Calculation</a></li>
          <?php } ?>
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>

  <div class="modal fade" id="SEARCH_TAKHMEEN_CALCULATION_FORM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Takhmeen Calculation</h4>
        </div>
        <div class="modal-body" id="takh_body"><?php include 'inc.takhmeen_calc.php'; ?></div>
        <div class="modal-body" id="result_takh_body" style="display: none;"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- TAKHMEEN CALCULATION -->
  <!-- FC CODE -->
  <div class="modal fade" id="SEARCH_FC_CODE_FRM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Fc Code</h4>
        </div>
        <div class="modal-body" id="searched_fc_code_body"><?php include 'fc_codes.php'; ?></div>
        <div class="modal-body" id="result_searched_fc_code_body" style="display: none;"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- FC CODE -->
  <!-- SEARCH HOF DETAILS -->
  <div class="modal fade" id="SEARCH_HOF_DETAILS" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Search Details</h4>
        </div>
        <div class="modal-body" id="searched_hof_detail_body"><?php include 'search_bar.php'; ?></div>
        <div class="modal-body" id="result_searched_hof_detail_body" style="display: none;"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- SEARCH HOF DETAILS -->
  <!-- TRIGGER WHEN CLOSE BUTTON IS CLICKED -->
  <script>
    $('#SEARCH_HOF_DETAILS').on('hidden.bs.modal', function(e) {
      $('#searched_hof_detail_body').show();
      $('#result_searched_hof_detail_body').empty().hide();
    });
    $('#SEARCH_FC_CODE_FRM').on('hidden.bs.modal', function(e) {
      $('#searched_fc_code_body').show();
      $('#result_searched_fc_code_body').empty().hide();
    });
    $('#SEARCH_TAKHMEEN_CALCULATION_FORM').on('hidden.bs.modal', function(e) {
      $('#takh_body').show();
      $('#result_takh_body').empty().hide();
    });
  </script>


  <!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script-->
  <!-- Slidebars -->
  <script src="asset/slidebars/slidebars.min.js"></script>
  <script src="asset/slidebars/slidebars-theme.js"></script>   
  <script src="asset/slidebars/slidebars.js"></script>
  <script>
    (function($) {
      $(document).ready(function() {
        $.slidebars();
      });
    })(jQuery);
  </script>
  
  <div class="row">
    <div class="col-md-12"><?php include 'includes/messages.php'; ?></div>
  </div>
