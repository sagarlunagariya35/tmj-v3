<?php
require_once 'classes/class.database.php';
?>
<style>
  .val{
    display: block;
    float: left;
    width: 250px;
    padding-left: 15px;
  }
  .right {
    display: block;
    float: left;
    text-align: right;
    font-weight: 600;
    width: 250px;
    padding-left: 15px;
  }
  .menu_name{
    display: block;
    width: 100%;
    font-size: 18px;
    font-weight: 600;
    min-height: 50px;
  }
  .text-right{
    text-align: right;
  }
  td, th{
    border: 1px solid black;
    padding: 3px 5px;
    margin: 0;
  }
</style>
<form method="post">
  <input type="text" name="qty">
  <input type="submit" value="Get Qty">
</form>
<div class="menu_name">namak+rawo pinepale+paav halwo+dabba gosht+cholai bhaji+kheema khichdi+tometo soup+salad+sarabat+chass ichar
</div>
<?php
if (isset($_POST['qty'])) {
  require_once 'classes/class.quadratic_derivatives.php';
  require_once 'classes/functions.php';
  ?>
  <div class="menu_name">
    Calculations for <?php echo $_POST['qty']; ?> Thaal
  </div>
  <table>
    <tr>
      <th>Item Name</th>
      <th>Qty unit</th>
      <th>Inv Name</th>
      <th>Unit Price</th>
      <th>Total</th>
    </tr>
    <?php
    $fh = fopen('wdata/3rd_muharram.csv', 'r');

    if ($fh == FALSE)
      echo "fail";

    $qty = $_POST['qty'];

    $calc = new Mtx_Quad_derivative(array(1, 2, 3), array(1, 2, 3), 0.1);

    $total_cost = 0;

    while ($row = fgetcsv($fh)) {
      $calc->a = $row[2];
      $calc->b = $row[3];
      $calc->c = $row[4];
      $calc->step = $row[5];
      $inv_id = $row[6];
      $inv_query = "SELECT name, unit_price FROM inventory WHERE id LIKE '$inv_id'";
      $inv_rslt = $database->query_fetch_full_result($inv_query);
      $inv_name = $inv_rslt[0]['name'];
      $inv_unit_price = $inv_rslt[0]['unit_price'];

      $item_qty = $calc->get_qty_y($qty);
      $item_name = $row[0];
      $item_unit = $row[1];
      $item_cost = number_format($item_qty * $inv_unit_price, 2);

      echo "<tr><td>$item_name:</td><td>$item_qty $item_unit</td><td>$inv_name</td><td>$inv_unit_price</td><td class=\"text-right\"> Rs: " . $item_cost . "</td></tr>\n";
      $total_cost += $item_cost;
    }

    fclose($fh);
    ?>
    <tr>
      <td class="text-right" colspan="5"><strong>Total: Rs <?php echo number_format($total_cost, 2); ?></strong></td>
    </tr>
  </table>
  <?php
}
?>
