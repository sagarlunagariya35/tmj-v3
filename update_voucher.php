<?php
include('session.php');
$page_number = 45;
$pg_link = 'debit_voucher';
require_once("classes/class.database.php");
require_once("classes/class.receipt.php");
$cls_receipt = new Mtx_Receipt();
$user_id = $_SESSION[USER_ID];

if(isset($_GET['did'])) $voucherID = $_GET['did'];
else $voucherID = $_GET['cid'];

if(isset($_POST['cancel'])) {
  $tbl_name = (isset($_GET['cmd']) && $_GET['cmd'] == 'credit') ? 'credit_voucher' : 'debit_voucher';
  $page = (isset($_GET['cmd']) && $_GET['cmd'] == 'credit') ? 'book_credit.php' : 'book_debit.php';
  $time = time();
  $query = "UPDATE `$tbl_name` SET `cancel` = '1', `cancel_user_id` = '$user_id', `cancel_ts` = '$time' WHERE `id` = '$voucherID'";
  $result = $database->query($query);
  if($result) $_SESSION[SUCCESS_MESSAGE] = 'Voucher has been cancelled successfully.';
  else $_SESSION[ERROR_MESSAGE] = 'Error encountered while canceling the voucher!';
  header('Location: '.$page);
  exit();
}

switch ($_GET['cmd']) {
  case 'debit' :
    $voucher = $cls_receipt->get_voucher($voucherID, 'debit_voucher');
    $back = 'book_debit.php';
    break;
  case 'credit' :
    $voucher = $cls_receipt->get_voucher($voucherID, 'credit_voucher');
    $back = 'book_credit.php';
    break;
}

$heads = $cls_receipt->get_account_heads();
$title = 'Debit Voucher';
$active_page = 'account';

include('includes/header.php');
include('page_rights.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <div class="col-md-3 pull-left">
          <?php include 'links.php';?>
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Search</h3></div>
            <div class="panel-body">
              <?php include('includes/search_bar.php'); ?>
            </div>
          </div>
        </div>
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-8">
          <form method="post" role="form" class="form-horizontal">
              <div class="form-group">
                <label class="control-label col-md-3">Name</label>
                <div class="col-md-4">
                  <p class="form-control-static"><?php echo $voucher['name']?></p>
                  <!--input type="text" name="name" id="name" class="form-control"-->
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Account Heads</label>
                <div class="col-md-4">
                  <p class="form-control-static"><?php echo $voucher['acct_heads']; ?></p>
                  <!--select class="form-control" name="heads" id="heads">
                    <option>--Select One--</option>
                    <?php if($heads){foreach($heads as $head){

                      ?>
                    <option value="<?php echo $head['head']?>" <?php echo ($head['head'] == $voucher['acct_heads']) ? 'selected' : '';?>><?php echo $head['head'];?></option>
                    <?php }} ?>
                  </select-->
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Description</label>
                <div class="col-md-4">
                  <p class="form-control-static"><?php echo $voucher['description']?></p>
                  <!--input type="text" name="description" id="description" class="form-control"-->
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Amount</label>
                <div class="col-md-4">
                  <p class="form-control-static"><?php echo number_format($voucher['amount'], 2)?></p>
                  <!--input type="text" name="amount" id="amount" class="form-control"-->
                </div>
              </div>
              <?php
              $cash = ''; $cheque = '';
              if($voucher['payment_type'] == 'cash') $cash = 'checked';
              if($voucher['payment_type'] == 'cheque') $cheque = 'checked';
              ?>
              <div class="form-group">
                <label class="control-label col-md-3">Type</label>
                <div class="col-md-3">
                  <input type="radio" id="radio" name="type" value="cash" <?php echo $cash;?> >&nbsp;Cash&emsp;
                  <input type="radio" id="radio" name="type" value="cheque" <?php echo $cheque;?>>&nbsp;Cheque
                </div>
              </div>
              <div id="cheque_detail" style="display: <?php if(!empty($cheque)) echo 'block'; else echo 'none';?>">
                <div></div>
                <div class="form-group" >
                  <label class="control-label col-md-3">Bank Name</label>
                  <div class="col-md-4">
                    <p class="form-control-static"><?php echo $voucher['bank_name']?></p>
                    <!--input type="text" name="bankname" id="bankname" class="form-control"-->
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Cheque No.</label>
                  <div class="col-md-4">
                    <p class="form-control-static"><?php echo $voucher['cheque_no']?></p>
                    <!--input type="text" name="cheque" id="cheque" class="form-control"-->
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Date</label>
                  <div class="col-md-4">
                    <p class="form-control-static"><?php echo $voucher['cheque_date']?></p>
                      <!--input type="date" name="dob" class="form-control" id="dob" placeholder=""-->
                  </div>
                </div>
              </div>
              <div class="form-group col-md-offset-4">
                <label class="control-label col-md-3">&nbsp;</label>
                <div class="col-md-8">
                  <a href="<?php echo $back;?>" class="btn btn-success">Back to voucher book</a>
                  <button type="submit" class="btn btn-danger" name="cancel" id="debit">Cancel</button>
                  <a href="<?php echo 'printDebit.php?vid='.$voucherID; ?>" class="btn btn-info" target="_blank">Print</a>
                </div>
              </div>
          </form>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
<script>
  //for radio buttons
  $("input:radio[name=type]").click(function() {
    var radio = $(this).val();
    if (radio === 'cheque')
    {
      $('#cheque_detail').show();
    }
    else
    {
      $('#cheque_detail').hide();
    }
  });
  $('#debit').click(function(){
    var name = $('#name').val();
    var heads = $('#heads').val();
    var desc = $('#description').val();
    var amt = $('#amount').val();
    var errors = '';
    var validate = true;
    if(name == ''){
      errors += 'Please enter name\n';
      validate = false;
    }
    if(heads == '0'){
      errors += 'Please select account heads\n';
      validate = false;
    }
    if(desc == ''){
      errors += 'Please enter description\n';
      validate = false;
    }
    if(amt == ''){
      errors += 'Please enter an amount\n';
      validate = false;
    }
    if(validate == false){
      alert(errors);
      return validate;
    }
  });
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>