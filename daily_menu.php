<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.menu.php");
$cls_menu = new Mtx_Menu();
$counter = (int) date('m');

if (isset($_GET['cmd']) && ($_GET['cmd'] == 'changeStatus')) {
  $status = $cls_menu->changeStatus(urldecode($_GET['ts']), $_GET['status']);
  header('Location: daily_menu.php');
  exit;
}
if (isset($_GET['cmd']) && ($_GET['cmd'] == 'del')) {
  $delete = $cls_menu->delete_menu($_GET['id'], $_GET['ts']);
}
if (isset($_GET['m'])) {
  $month = (int) $_GET['m'];
  $year = (int) $_GET['y'];
} else {
  $month = (int) date('m');
  $year = (int) date('Y');
}

// set prev and next month
if ($month == 1) {
  $prev_month = 12;
  $prev_year = $year - 1;
} else {
  $prev_month = $month - 1;
  $prev_year = $year;
}
if ($month == 12) {
  $next_month = 1;
  $next_year = $year + 1;
} else {
  $next_month = $month + 1;
  $next_year = $year;
}

$daily_menus = $cls_menu->get_daily_menu($month, $year, FALSE, FALSE, FALSE, 'HIJRI_DATE');

$title = 'Add daily menu';
$active_page = 'menu';


require_once 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profiles</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="form-group pull-right">
            <a href="?m=<?php echo $prev_month; ?>&y=<?php echo $prev_year; ?>" name="previous" id="previous" class="btn btn-primary">Previous</a>
            <a href="?m=<?php echo date('m'); ?>&y=<?php echo date('Y'); ?>" name="current" id="current" class="btn">Current</a>
            <a href="?m=<?php echo $next_month; ?>&y=<?php echo $next_year; ?>" name="next" id="next" class="btn btn-primary">Next</a>
            <input type="hidden" id="value" name="value" value ="<?php
            if (isset($_POST['previous'])) {
              echo $counter;
            }
            if (isset($_POST['next'])) {
              echo $counter;
            }
            ?>">
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="col-md-12" id="daily_menus">
          <?php
          // if daily menu found, display in three rows
          $data = '';
          if ($daily_menus) {
            $i = 1; // a counter used for inserting break after every 4 menus
            // display each menu in col-md-4
            foreach ($daily_menus as $menu) {
                $menu_name = $cls_menu->get_base_menu_name($menu['menu_id']);
              $est_cost = $cls_menu->get_daily_menu_cost($menu['menu_id'], $menu['timestamp']);
              $date = date('D d, M', $menu['timestamp']);
              $hdate = HijriCalendar::GregorianToHijri($menu['timestamp']);
              $hdt = "$hdate[1] " . HijriCalendar::monthName($hdate[0]);
              $is_checked = ($menu['active'] == 1) ? 'checked' : '';
              $active = '<input type="checkbox" name="action" id="action' . $menu['timestamp'] . '" onclick="ChangeStatus(' . $menu['timestamp'] . ')" ' . $is_checked . '>';
              $delete = $menu['timestamp'] > time() ? ' <button id="menu-' . $menu['menu_id'] . '-' . $menu['timestamp'] . '" class="btn btn-danger btn-xs del_menu">Delete</button><!--a href="daily_menu.php?id=' . $menu['menu_id'] . '&cmd=del&ts=' . $menu['timestamp'] . '" value="Delete" class="btn btn-danger btn-xs" name="delete">Delete</a-->' : '';
              $cook_est = ' <a href="cook_estimate2.php?date=' . date('Y-m-d', $menu['timestamp']) . '" class="btn btn-primary btn-xs">Cook Est.</a>';
              $inv_issue = ' <a href="estimate_inventory_report.php?date=' . date('Y-m-d', $menu['timestamp']) . '" class="btn btn-primary btn-xs">Issue Inven.</a>';
              $menu_type = ($i % 2 == 0) ? " menu_even" : '';
              $data .= '<div class="col-md-3 col-sm-6 menu_box'.$menu_type.'"><p><span class="text-info">'.$date.'</span><span class="text-warning pull-right">'.$hdt.'</span></p><p class="text-danger"><strong>'.$menu_name.'</strong></p><p>Est. Cost: '.number_format($est_cost, 2).'</p><p>Status: '.$active . $delete.'</p><p class="fix_btm">'.$cook_est . $inv_issue.'</p></div>';
              $data .= ($i % 4 == 0) ? "<div class=\"clearfix\"></div>" : '';
              $i++;
            }
          } else {
            $data = 'No Menus found';
          }
          echo $data;
          ?>
        </div>

        <!-- /Center Bar -->
        <script>
          function ChangeStatus(name) {
            var val = document.getElementById('action' + name).checked;
            //alert(name);return false;
            if (val == true)
              val = 1;
            else
              val = 0;
            window.location.href = 'daily_menu.php?ts=' + name + '&status=' + val + '&cmd=changeStatus';
          }
          $('.del_menu').click(function(e) {
            e.preventDefault();

            var answer = confirm('Are you sure you want to delete this?');
            if (answer) {
              myApp.showPleaseWait();
              var menu = this.id.split('-');
              jQuery.ajax({
                type: "POST", // HTTP method POST or GET
                url: "ajax.php", //Where to make Ajax calls
                data: "cmd=del_menu&mid=" + menu[1] + "&timestamp=" + menu[2] + "&month=<?php echo $month; ?>&year=<?php echo $year; ?>", //Form variables
                success: function(data, status) {
                  //on success, show new data.
                  var menus = $.parseJSON(data);
                  var contents = '';
                  var menu_type;
                  var i = 1;
                  var btn_del = '';
                  $.each(menus, function() {
                    menu_type = (i % 2 == 0) ? " menu_even" : '';
                    var is_checked = (this.active == 1) ? 'checked' : '';
                    var active = '<input type="checkbox" name="action" id="action' + this.timestamp + '" onclick="ChangeStatus(' + this.timestamp + ')" ' + is_checked + '>';
                    if(this.Delete == true) {
                      btn_del = ' <button id="menu-' + this.menu_id + '-' + this.timestamp + '" class="btn btn-danger btn-xs del_menu">Delete</button>';
                    }
                    var cook_est = ' <a href="cook_estimate2.php?date=' + this.Date + '" class="btn btn-primary btn-xs">Cook Est.</a>';
                    var inv_issue = ' <a href="inventory_issue_report.php?cmd=daily_menu&date=' + this.date + '" class="btn btn-primary btn-xs">Issue Inven.</a>';
                    contents += '<div class="col-md-2 menu_box'+menu_type+'"><p class="text-info">'+this.greg_date+'</p><p class="text-info">'+this.Hijri_date+'</p><p class="text-danger">Menu: '+this.menu_name+'</p><p>Status: '+ active + btn_del +'</p><p class="fix_btm">'+ cook_est + inv_issue + '</p></div>';
                    contents += (i % 6 == 0) ? "<div class=\"clearfix\"></div>" : '';
                    i++;
                  });
                  $('#daily_menus').html(contents);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                  //On error, we alert user
                  alert(thrownError);
                }
              });
              myApp.hidePleaseWait();
            } else {
              alert('Operation Canceled!');
            }
          });

          var myApp;
          myApp = myApp || (function() {
            var pleaseWaitDiv = $('<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Processing . . .</h4></div><div class="modal-body"><img src="asset/img/loader.gif"></div></div></div></div>');
            return {
              showPleaseWait: function() {
                pleaseWaitDiv.modal();
              },
              hidePleaseWait: function() {
                pleaseWaitDiv.modal('hide');
              },
            };
          })();
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>