<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.menu.php");
$cls_menu = new Mtx_Menu();

$items = $cls_menu->get_inventory_asset();
$title = 'Inventory Asset report';
$active_page = 'report';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <style type="text/css">
      @media all {
        body { font-size: 16px; }
        table { page-break-inside:auto; }
        tr    { page-break-inside:avoid; page-break-after:auto; }
        thead { display:table-header-group; }
        tfoot { display:table-footer-group; }
      }
    </style>
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8">
        <?php
        $print = TRUE;
        include 'includes/inc.inventory_asset.php';
        ?>
      </div>
    </div>
    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>