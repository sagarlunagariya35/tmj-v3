<?php
include('session.php');

require_once("classes/class.database.php");
require_once("classes/class.menu.php");
require_once("classes/class.family.php");
$cls_menu = new Mtx_Menu();
$cls_family = new Mtx_family();

$date = $menu_details = $ary_thaali = $total_thali_count = FALSE;
$base_count = array();
$per_thali_cost = 0;

if (isset($_POST['date'])) {
  $date = $_POST['date'];
}

$date2 = strtotime($date);
$menu_details = $cls_menu->get_daily_menu_details($date2);
$tiffin_size = $cls_family->get_tiffin_size();

foreach ($tiffin_size as $tfn) {
  $ary_size[$tfn['size']] = $tfn['cost_multiplier'];
}

if (isset($_POST['calculate'])) {
  $ary_thaali = $_POST['thaali'];
  // get the actual thaali count
  foreach ($ary_thaali as $size => $count) {
    $base_count[$size] = $count * $ary_size[$size];
    $total_thali_count += $base_count[$size];
  }
  $per_thali_cost = $menu_details['act_cost'] / $total_thali_count;
  $date = $_POST['date'];
}

$title = 'Thaali calculator';
$active_page = 'menu';

include 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
<?php
include 'includes/inc_left.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?php echo $title; ?></h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Maedat</a></li>
      <li><a href="#">Menu</a></li>
      <li class="active"><?php echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">&nbsp;</div>

      <!-- Center Bar -->
      <div class="col-md-12">
        <form method="post" role="form" class="form-horizontal">
          <div class="row"></div>
          <div class="form-group">
            <label class="col-md-3 control-label">From</label>
            <div class="col-md-4">
              <input type="date" name="date" class="form-control" value="<?php echo $date; ?>">
            </div>
            <div class="col-md-2">
              <input type="submit" class="btn btn-success" name="search" id="search" value="Search">
            </div>
          </div>
          <div class="row">&nbsp;</div>
          <?php
          if ($menu_details) {
            ?>
            <h4 class="box-title">Menu : <?php echo $menu_details['custom_menu'] ?></h4>
            <h4 class="box-title">Menu Cost : <?php echo number_format($menu_details['act_cost'], 2); ?></h4>

            <table class="table table-hover table-condensed">
              <thead>
                <tr>
                  <th>Tiffin size</th>
                  <th>Thali Count</th>
                  <th class="text-right">Size Cost</th>
                  <th class="text-right">Per Thali Cost</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $total_size_cost = $total_thali = $thali_cost = 0;
                if ($tiffin_size) {
                  foreach ($tiffin_size as $tfn) {


                    //$size_cost = $ary_thaali[$i] * $tfn['cost_multiplier'];
                    //$total_size_cost += $size_cost;
                    //$total_thali += $ary_thaali[$i];

                    $thali_cost = round($per_thali_cost * $tfn['cost_multiplier'],2);
                    $size_cost = round($thali_cost * $ary_thaali[$tfn['size']],2);
                    ?>
                    <tr>
                      <td><?php echo $tfn['size']; ?></td>
                      <td><input type="text" name="thaali[<?php echo $tfn['size']; ?>]" id="thaali<?php echo $tfn['size']; ?>" class="form-control" value="<?php
                        if (isset($_POST['calculate'])) {
                          echo $ary_thaali[$tfn['size']];
                        }
                        ?>"></td>
                      <td class="text-right"><?php echo number_format($size_cost, 2); ?></td>
                      <td class="text-right"><?php echo number_format($thali_cost, 2); ?></td>
                    </tr>
                    <?php
                  }
                }
                ?>
                <tr>
                  <td colspan="2"><span class="pull-right"><strong>Total Thali : </strong><?php echo $total_thali_count; ?></span></td>
                </tr>
                <tr>
                  <td><button type="submit" name="calculate" class="btn btn-success" id="calculate">Calculate</button></td>
                </tr>
              </tbody>
            </table>
            <?php
          }
          ?>
        </form>
      </div>
      <!-- /Center Bar -->
    </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php
include 'includes/footer.php';
?>