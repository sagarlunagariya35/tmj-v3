<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/hijri_cal.php');
$cls_receipt = new Mtx_Receipt();
$user_id = $_SESSION[USER_ID];

if(isset($_POST['Cancel'])){
  $reason = $cls_receipt->insert_reason_for_vol_cancellation($_POST['reason'], $_GET['rid'], $user_id);
  if($reason){
    $_SESSION[SUCCESS_MESSAGE] = "Receipt has been cancelled successfully.";
    header('Location: voluntary_rcpt_book.php');
    exit;
  } else {
    $_SESSION[ERROR_MESSAGE] = "Errors encountered while processing Cancellation of Voluntary Receipt";
    header('Location: voluntary_rcpt_book.php');
    exit;
  }
}

$rcpt = $cls_receipt->get_total_amount_vol_receipt($_GET['rid']);
$title = "Cancel Receipt";
$active_page = "receipt";

require_once 'includes/header.php';

$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Credit</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12">&nbsp;</div>

      <!-- Left Bar -->
      <div class="col-md-3 pull-left">
        <div class="panel panel-default">
          <div class="panel-heading"><h3 class="panel-title">Menu This week</h3></div>
          <div class="panel-body">
            <?php include('includes/search_bar.php'); ?>
          </div>
        </div>
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8 ">
        <form method="post" role="form" class="form-horizontal">
          <div class="form-group">
            <label class="control-label col-md-2">Receipt No.</label>
            <div class="col-md-4">
              <p class="form-control-static"><?php echo $rcpt['id']; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2">Thaali No.</label>
            <div class="col-md-4">
              <p class="form-control-static"><?php echo $rcpt['FileNo']; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2">Name</label>
            <div class="col-md-4">
              <p class="form-control-static"><?php echo $rcpt['hubber_name'];?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2">Reason</label>
            <div class="col-md-4">
              <textarea class="form-control" name="reason" rows="4" placeholder="Write a reason for cancellation"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2">&nbsp;</label>
            <div class="col-md-6">
              <button type="submit" name="Cancel" id="Cancel" class="btn btn-danger">Cancel</button>
              <a href="index.php" class="btn btn-info">Back to Home</a>
              <a href="voluntary_rcpt.php" class="btn btn-info">Back to Receipt</a>
            </div>
          </div>
        </form>
      </div>
      <!-- /Center Bar -->
      <script>
        $('#Cancel').click(function(){
          var question = confirm("Are you sure you want to cancel the Voluntary Contribution receipt?");
          if(question){
            return true;
          } else {
            return false;
            //window.location.href="";
          }
        });
      </script>
      </div>
      <!-- /Content -->
    </section>
  </div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>