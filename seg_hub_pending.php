<?php
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.receipt.php');
require_once('classes/class.user.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();
$cls_receipt = new Mtx_Receipt();
$cls_user = new Mtx_User();
$show_all_records = $post = FALSE;
$takh_year = $ps_mohallah = $show_tbl = $pending_kisht = $search = '';
if (isset($_POST['Takh_year'])) {
  $post = TRUE;
  $show_tbl = TRUE;
  $takh_year = $_POST['takhmeen_year_pending'];
  $ps_mohallah = $_POST['Mohallah'];
  $pending_kisht = $_POST['pending_kisht'];
  if (isset($_POST['show_all_records']))
    $show_all_records = TRUE;

  $search = $cls_family->get_family_by_mohallah($ps_mohallah, FALSE);
}

if (isset($_POST['send_sms'])) {
  $return_success = array();
  $return_fail = array();
  $post = TRUE;
  $takh_year = $_POST['takhmeen_year_pending'];
  $default_kisht = $setting[0]['default_kisht'];
  $pending_kisht = $_POST['pending_kisht'];
  $ps_mohallah = $_POST['Mohallah'];
  $msg = $setting[0]['hub_pending_tpl'];
  $senderID = $setting[0]['sender_id'];
  $user_name = $setting[0]['user_name'];
  $password = $setting[0]['password'];
  
  $search = $cls_family->get_family_by_mohallah($ps_mohallah, FALSE);
  foreach ($search as $data) {
    $thali_id = $data['FileNo'];
    $mob = $data['MPh'];
    $takh_amount = $cls_family->get_takhmeen_record($thali_id, FALSE, "AND `year` = '$takh_year'");
    $takhmeen_amount = ($takh_amount[0]['amount'] / $default_kisht) * $pending_kisht;

    $paid_amount = $cls_family->get_paid_amount($thali_id, $takh_year);

    $pending_amount = $takhmeen_amount - $paid_amount;

    if (!$show_all_records) {
      if (!$takhmeen_amount > 0)
        continue;
      if ($paid_amount >= $takhmeen_amount)
        continue;
    }

    if ($mob != '' || $mob > 0) {
      $sms_sent = $cls_family->send_sms($mob, $message);
    } else {
      $sms_sent = FALSE;
    }

    if ($sms_sent) {
      $return_success[] = $thali_id;
    } else {
      $return_fail[] = $thali_id;
    }
  }
  if (count($return_success)) {
    $sent_msg .= "SMS To the following IDs sent successfully!<br>";
    $sent_msg .= join('<br>', $return_success) . '<br>';
  }

  if (count($return_fail)) {
    $sent_msg .= "SMS To the following IDs Could Not be Sent!<br>";
    $sent_msg .= join('<br>', $return_fail);
  }
}

$mohallah = $cls_family->get_all_Mohallah();
$default_kisht = $setting[0]['default_kisht'];
$title = 'Send SMS to Hub pending'
?>
<div class="col-md-12">
  <?php
  switch (USE_CALENDAR) {
    case 'Hijri':
      $cur_hijri_date = HijriCalendar::GregorianToHijri();
      echo $cur_hijri_date[1] . ', ' . HijriCalendar::monthName($cur_hijri_date[0]) . ', ' . $cur_hijri_date[2] . ' H';
      break;
    case 'Greg':
      echo date('l d F, Y');
      break;
  }
  ?>
</div>
<form method="post" class="form-horizontal">
  <div class="form-group">
    <label class="col-md-2 control-label">Tanzeem</label>
    <div class="col-md-8">
      <select id="Mohallah" name="Mohallah" class="form-control">
        <option value="">All</option>
        <?php
        if ($mohallah) {
          foreach ($mohallah as $mohalla) {
            ?>
            <option value="<?php echo $mohalla['Mohallah'] ?>" <?php if ($ps_mohallah && $mohalla['Mohallah'] == $ps_mohallah) echo 'selected'; ?>><?php echo ucwords($mohalla['Mohallah']); ?></option>
            <?php
          }
        }
        ?>
      </select>

    </div>

  </div>
  <div class="form-group">
    <label class="col-md-2 control-label">Kisht</label>
    <div class="col-md-8">
      <input type="text" class="form-control" name="pending_kisht" id="pending_kisht" required="" value="<?php echo $pending_kisht; ?>">
    </div>
  </div>
  <div class="form-group">
    <label class="col-md-2 control-label">Show all Records</label>
    <div class="col-md-8">
      <input type="checkbox" class="" name="show_all_records" id="show_all_records" <?php echo ($show_all_records) ? 'checked' : ''; ?>>
    </div>
  </div>
  <div class="form-group">
    <label class="col-md-2 control-label"><?php echo YEAR; ?></label>
    <div class="col-md-8">
      <select id="takhmeen_year_pending" name="takhmeen_year_pending" class="form-control" required="">
        <option value ="">-- Select One --</option>
        <?php
        for ($i = 1; $i <= $no_years; $i++) {
          $year = (($i == 1) ? $default_year : ($default_year + ($i - 1))) . '-' . (substr($default_year, -2) + $i);
          if ($takh_year == $year)
            $selected = 'selected';
          else
            $selected = '';
          ?>
          <option value="<?php echo $year; ?>" <?php echo $selected; ?>><?php echo $year; ?></option>
<?php } ?>
      </select>
    </div>
  </div>
    <div class="form-group">
      <div class="col-md-2"></div>
      <div class="col-md-4">
        <input type="submit" class="btn btn-success validate" value="Search" name="Takh_year" id="Takh_year">
        <!--a href="print_hub_pending_report.php?mh=<?php echo $ps_mohallah; ?>&cmd=hub_pending&year=<?php echo $takh_year; ?>&show_all=<?php echo $show_all_records; ?>&kisht=<?php echo $pending_kisht; ?>" target="blank" class="btn btn-primary validate <?php echo !$post ? 'disabled' : ''; ?>" name="print">Print</a-->
    <input type="submit" name="send_sms" value="Send SMS" class="btn btn-success validate <?php echo !$post ? 'disabled' : ''; ?>">
      </div>
    </div>
</form>

<div class="col-md-12">&nbsp;</div>
<div class="col-md-12" id="report">
<?php
$use_ts = 0;
include('includes/inc.hub_pending.php');
?>
</div>