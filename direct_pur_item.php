<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.menu.php");
require_once('classes/class.product.php');
$cls_menu = new Mtx_Menu();
$cls_product = new Mtx_Product();

if (isset($_POST['add_menu'])) {
  $result = $cls_menu->add_direct_pur_items(ucfirst(strtolower($_POST['item_name'])));
  if($result){
    $_SESSION[SUCCESS_MESSAGE] = "Menu added successfully";
    header('Location: direct_pur_item.php');
    exit;
  } else{
    $_SESSION[ERROR_MESSAGE] = "Errors encountered while processing Direct Purchase Item";
    header('Location: direct_pur_item.php');
    exit;
  }
}

if(isset($_POST['update'])) {
  $update = $cls_menu->update_direct_pur_items($_POST['old_item_name'], ucfirst(strtolower($_POST['item_name'])), $_POST['hid_manu_id']);
  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Item updated successfully';
    header('Location: direct_pur_item.php');
    exit;
  } else {
    $_SESSION[ERROR_MESSAGE] = '';
  }
}
if(isset($_GET['cmd']) && $_GET['cmd'] = 'update') {
  $did = $_GET['did'];
  $update = $cls_menu->get_direct_purchase_items_from_id($did);
}
$direct_items = $cls_product->get_all_direct_items();

$title = 'Add direct purchase items';
$active_page = 'menu';

require_once 'includes/header.php';

$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profiles</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-5">
          <form method="post" role="form" class="form-horizontal">
            <div class="form-group">
              <label class="control-label col-md-3">Name</label>
              <div class="col-md-8">
                <input type="text" name="item_name" id="item_name" class="form-control" value="<?php if(isset($_GET['cmd'])) echo $update['name']?>">
              </div>
            </div>
            <?php
              if(isset($_GET['cmd'])) echo '<input type="hidden" value="'.$update['id'].'" name="hid_manu_id"><input type="hidden" value="'.$update['name'].'" name="old_item_name">';
              ?>
            <div class="form-group">
              <label class="control-label col-md-3">&nbsp;</label>
              <div class="col-md-8">
                <?php
                  echo (isset($_GET['cmd'])) ? '<input type="submit" value="Update" class="btn btn-success" name="update" id="update">' : '<button name="add_menu" id="add_menu" type="submit" class="btn btn-success">Add</button>';
                  ?>
              </div>
            </div>
          </form>
          </div>
          <div class="col-md-7">
            <?php if($direct_items) {?>
            <table class="table table-hover table-condensed table-bordered">
              <thead>
                <th>No</th>
                <th>Item Name</th>
                <th>Action</th>
              </thead>
              <tbody>
                <?php
                $i = 1;
                foreach($direct_items as $item){
                ?>
                <tr>
                  <td><?php echo $i++;?></td>
                  <td><?php echo $item['name'];?></td>
                  <td><a href="direct_pur_item.php?did=<?php echo $item['id']?>&cmd=update" class="btn btn-success btn-xs">Update</a></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php } ?>
          </div>
        </div>
        <!-- /Center Bar -->
        <script>
        $('#add_menu').click(function(){
          var item = $('#item_name').val();
          var dt = $('#date').val();
          if(item == ''){
            alert('Please enter item name');
            return false;
          }
        });
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>