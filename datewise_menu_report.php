<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.menu.php");
require_once("classes/class.family.php");
$cls_menu = new Mtx_Menu();
$cls_family = new Mtx_family();

$from_date = $to_date = $print_limit = $post = FALSE;

if ($_GET) {
  $post = TRUE;
  $from_date = $_GET['from_date'];
  $to_date = $_GET['to_date'];
  $print_limit = $_GET['print'];
  if (!is_numeric($print_limit))
    $print_limit = 1;
  $fsp = explode('-', $from_date);
  $from = mktime(0, 0, 0, $fsp[1], $fsp[2], $fsp[0]);
  $tsp = explode('-', $to_date);
  $to = mktime(23, 59, 59, $tsp[1], $tsp[2], $tsp[0]);
  $menus = $cls_menu->get_menu_between_dates($from, $to);
}

$title = 'Datewise menu report';
$active_page = 'report';

require_once 'includes/header.php';

$page_number = REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="get" role="form" class="form-horizontal">
            <div></div>
            <div class="col-md-12">
              <label class="col-md-1 control-label">From</label>
              <div class="col-md-2">
                <input type="date" name="from_date" class="form-control" id="from_date" value="<?php echo $from_date; ?>">
              </div>
              <label class="col-md-1 control-label">To</label>
              <div class="col-md-2">
                <input type="date" name="to_date" class="form-control" id="to_date" value="<?php echo $to_date; ?>">
              </div>
              <label class="col-md-2 control-label">Print #</label>
              <div class="col-md-2">
                <input type="text" name="print" class="form-control" id="print" value="<?php echo $print_limit; ?>">
              </div>

              <input type="submit" class="btn btn-success validate" name="search" id="search" value="Search">
              <a href="print_datewise_menu.php?from=<?php echo $from_date; ?>&to=<?php echo $to_date; ?>&print_limit=<?php echo $print_limit; ?>" target="blank" class="btn btn-primary <?php echo!$post ? 'disabled' : ''; ?>" id="print_link">Print</a>
            </div>
          </form>
          <div class="col-md-12">&nbsp;</div>
            <?php if ($_GET) { ?>
            <div class="col-md-12">
              <?php
              $print = FALSE;
              require_once './includes/inc.datewise_menu_report.php';
              ?>
            </div>
      <?php } ?>
        </div>
        <!-- /Center Bar -->
      </div>
    </section>
  </div>

<script>
  $('#print_link').click(function(e) {
    e.preventDefault();
    var msg = $('#footer_msg').val();
    var limit = $('#print').val();
    window.open('print_datewise_menu.php?from=<?php echo $from_date; ?>&to=<?php echo $to_date; ?>&print_limit='+limit+'&msg='+msg, '_blank');
  });
  $('.validate').click(function() {
    var from = $('#from_date').val();
    var to = $('#to_date').val();
    var errors = [];
    var key = 0;
    if (from === '')
      errors[key++] = 'from';
    if (to === '')
      errors[key++] = 'to';
    if (errors.length) {
      alert('Please select ' + errors.join(' & ') + ' date to proceed..');
      return false;
    }
  });
</script>
<!-- /Content -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>