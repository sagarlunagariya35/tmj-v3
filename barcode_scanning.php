<?php
require_once('session.php');
$page_number = 11;
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.barcode.php');
require_once('classes/hijri_cal.php');

$cls_family = new Mtx_family();
$cls_barcode = new Mtx_Barcode();

$thaali_date = date('Y-m-d');
if (isset($_POST['date_search'])) {
  $thaali_date = $_POST['thalidate'];
}

$tanzeem = $cls_family->get_all_Mohallah();
$result = $cls_family->get_FileNo();
$title = 'Barcode Scanner';
$active_page = 'family';

require_once 'includes/header.php';

$page_number = PROFILE_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profiles</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-12">
            <label id="tanzeem" class=" alert-info col-md-2"></label>
            <label id="total_thali" class="alert-info col-md-2"></label>
            <label id="issued_thali" class="alert-success col-md-2"></label>
            <label id="not_issue_thali" class="alert-warning col-md-2"></label>
          </div>
          <div class="clearfix"></div>
          <form method="post" class="form-horizontal" role="form" onsubmit="return false;">
            <div class="form-group">
              <label class="col-md-2"><?php echo THALI_ID; ?></label>
              <div class="col-md-4">
                <input type="text" autofocus="autofocus" name="family" id="family" class="form-control">
              </div>
              <input type="submit" class="btn btn-success" name="add_barcode" id="add_barcode" value="Add">

            </div>
            <div class="form-group">
              <label class="col-md-2">Tanzeem</label>
              <div class="col-md-4">
                <select id="Mohallah" name="Mohallah" id="Mohallah" class="form-control">
                  <option value="All">All</option>
                  <?php
                  if ($tanzeem) {
                    foreach ($tanzeem as $mh) {
                      if (isset($_GET['tanzeem'])) {
                        if ($_GET['tanzeem'] == $mh['Mohallah'])
                          $selected = ' selected';
                        else
                          $selected = '';
                      } else
                        $selected = '';
                      echo '<option value="' . $mh['Mohallah'] . '"' . $selected . '>' . $mh['Mohallah'] . '</option>';
                    }
                  }
                  ?>
                </select>
              </div>
              <input type="submit" class="btn btn-success" name="search" id="search" value="Search">

            </div>
          </form>

          <form method="POST" class="form-horizontal" role="form">
            <!----Date For ---->
            <div class="form-group">
              <label class="col-md-2">Date</label>
              <div class="col-md-4">
                <input type="date" name="thalidate" id="thalidate" class="form-control" value="<?php echo $thaali_date ?>">
              </div>
              <input type="submit" class="btn btn-success" name="date_search" id="date_search" value="Update">

            </div>
          </form>
          <!---END FOR DATE -->
          <div class="col-md-12">
            <div class="col-md-6">
              <div class="panel panel-info">
                <div class="panel-heading text-center" id="issue_title">Thali Issue list</div>
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th><?php echo THALI_ID; ?></th>
                      <th>HOF</th>
                      <th>Mobile No.</th>
                      <th>Size</th>
                    </tr>
                  </thead>
                  <tbody id="issued_tiffin">
                  <td colspan="4">Loading Please wait <img src="asset/img/loader.gif"> </td>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-md-6">
              <div class="panel panel-info">
                <div class="panel-heading text-center" id="not_issued_title">Thali not issue list</div>
                <table class="table table-condensed table-hover">
                  <thead>
                    <tr>
                      <th><?php echo THALI_ID; ?></th>
                      <th>HOF</th>
                      <th>Mobile No.</th>
                      <th>Size</th>
                    </tr>
                  </thead>
                  <tbody id="not_issued">
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /Center Bar -->
      </div>
  </section>
</div>
<script>
  var thaaliDate = $('#thalidate').val();
  var fly;
  var ary_tfn = [];
  var tanzeem = '<?php echo (isset($_GET['tanzeem'])) ? $_GET['tanzeem'] : 'All'; ?>';

  $('#search').click(function() {

    var tanzeem = $('#Mohallah').val();
    window.location.href = "barcode_scanning.php?tanzeem=" + tanzeem;
  });

  $(document).ready(function() {
    get_ajax_data();
  });

  function get_ajax_data() {
    //alert('cmd=get_not_issued_tiffin_by_Mohallah&tanzeem=' + tanzeem + '&thaaliDate=' + thaaliDate);
    $.ajax({
      url: 'ajax.php',
      type: "POST",
      data: 'cmd=get_not_issued_tiffin_by_Mohallah&tanzeem=' + tanzeem + '&thaaliDate=' + thaaliDate,
      success: function(data)
      {
        var obj = $.parseJSON(data);
        var tfn = obj.TIFFIN;
        if (tfn != '[]')
          tanzeem_wise_tfn_count(tfn, 'not_issued_title', 'Thali not issue list');
        var issue = obj.ISSUE_TIFFIN;
        if (issue != '[]')
          tanzeem_wise_tfn_count(issue, 'issue_title', 'Thali Issue list');
        fly = obj.DATA;
        sync_data();
      },
      async: true
    });
  }

  function tanzeem_wise_tfn_count(tiffin, id, title) {
    return $('#' + id).html(title + ' <h5><strong>' + tiffin + '</strong></h5>');
  }

  var ary_issue_length = 0;
  var ary_not_issue_length = 0;

  function sync_data() {
    $('#not_issued').text('');
    $('#issued_tiffin').text('');

    $.each(fly, function() {
      var HOF = this.prefix + ' ' + this.first_name + ' ' + this.father_prefix + ' ' + this.father_name + ' ' + this.surname;
      var text = '<tr style="background-color:' + this.bg + '"><td>' + this.FileNo + '</td>\n\n<td>' + this.HOF + '</td><td>' + this.MPh + '</td><td>' + this.tiffin_size + '</td></tr>';

      //if color is green than count length of issue else count not issue
      if (this.bg == '#CDFF9B') {
        ary_issue_length += 1;
      }
      else {
        ary_not_issue_length += 1;
      }

      $('#issued_thali').text("Issued: " + ary_issue_length);
      $('#not_issue_thali').text("Pending: " + ary_not_issue_length);
      //Total Thali
      var total = Number(ary_issue_length) + Number(ary_not_issue_length);
      $('#total_thali').text(' Total Thali:' + total);
      $('#tanzeem').text('Tanzeem: ' + tanzeem);
      if (this.family_id == null || this.family_id == '') {
        $('#not_issued').append(text);
      } else {
        $('#issued_tiffin').append(text);
      }
    });
// To reset the counter              
    ary_issue_length = 0;
    ary_not_issue_length = 0;

  }

  $('#add_barcode').click(function() {
    var barcode = $('#family').val();
    barcode = barcode.toUpperCase();
    $('#family').val('');
    var validate = true;
    if (barcode == '')
    {
      new Messi('Please enter family number...', {title: 'File No. Missing!', titleClass: 'anim warning', buttons: [{id: 'cls', label: 'Close', val: 'X'}], modal: true, callback: function() {
          $('#family').focus();
        }});
      $('.btnbox .btn').focus();
      return false;
    }
    $.each(fly, function() {
      if (this.family_id == barcode)
        validate = false;
    });

    var tiffin_exists = false;
    $.each(fly, function() {
      if (this.FileNo == barcode)
        tiffin_exists = true;
    });
    if (!tiffin_exists) {
      new Messi("Tiffin '" + barcode + "' Does not exists", {title: 'Error!', titleClass: 'anim error', buttons: [{id: 'cls', label: 'Close', val: 'X'}], modal: true, callback: function() {
          $('#family').focus();
        }});
      $('.btnbox .btn').focus();
      return false;
    }

    // delete from not issued table
    // insert into issued table
    // -- foreach fly['FileNo'] == textbox then fly['family_id'] = textbox
    if (validate) {
      $.each(fly, function() {
        if (this.FileNo == barcode)
          this['family_id'] = barcode;
      });
      sync_data();


      // sync_data();
      // send async request to server
      // -- foreach fly['FileNo'] == data.FileNo then fly['bg'] = set
      // if server responds 1 set bg green
      // else set bg red
      //alert('cmd=insert_issued_tiffin&barcode=' + barcode + '&tanzeem=' + tanzeem + '&thalidate=' + thaaliDate);
      $.ajax({
        url: 'ajax.php',
        type: "POST",
        async: true,
        data: 'cmd=insert_issued_tiffin&barcode=' + barcode + '&tanzeem=' + tanzeem + '&thalidate=' + thaaliDate,
        success: function(data)
        {
          var obj = JSON.parse(data);
          var issue = obj.TIFFIN;
          tanzeem_wise_tfn_count(issue, 'issue_title', 'Thali Issue list');
          var not_issue = obj.NOT_ISSUE;
          tanzeem_wise_tfn_count(not_issue, 'not_issued_title', 'Thali not issue list');
          var rtn = obj.DATA;
          if (rtn.result == 1)
          {
            $.each(fly, function() {
              if (this['FileNo'] == rtn.FileNo) {
                this.bg = '#CDFF9B'; // green
              }
            });
          } else {
            $.each(fly, function() {
              if (this['FileNo'] != rtn.FileNo)
                this.bg = '#FF8282'; // red
            });
          }
          sync_data();
        }
      });
    }
    else
    {
      new Messi("Tiffin '" + barcode + "' already issued", {title: 'Error!', titleClass: 'anim error', buttons: [{id: 'cls', label: 'Close', val: 'X'}], modal: true, callback: function() {
          $('#family').focus();
        }});
      $('.btnbox .btn').focus();
    }
  });

</script>
<!-- /Content -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>