<?php
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.user.php');
require_once('classes/functions.php');

$cls_user = new Mtx_User();
//pagination code start here
$cls_family = new Mtx_family();
$page = 1;
if (isset($_GET['page']) && $_GET['page'] != '') {
  $page = $_GET['page'];
}
//for takhmeen calculation start
if (isset($_POST['update_takh_amt'])) {
  $data = $database->clean_data($_POST);
  $thali_id = $data['takhmeen_thali'];
  $amount = $database->clean_currency($data['takhmeen_amount']);
  $year = $data['takhmeen_year'];
  $update_takh = $cls_family->update_takhmeen_record($thali_id, $amount, $year);
  if ($update_takh)
    $_SESSION[SUCCESS_MESSAGE] = 'Takhmeen amount updated successfully for thali id ' . $thali_id;
  else
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing...';
}
//for takhmeen calculation end

$setting = $cls_user->get_general_settings();
$default_year = $setting[0]['start_year'];
$no_years = $setting[0]['no_of_years'];
$db_tanzeem_name = $setting[0]['tanzeem_name'];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?php echo $title; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="asset/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <link href="asset/dist/css/tmj2.css" rel="stylesheet" type="text/css" />
    <link href="asset/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <link href="asset/plugins/iCheck/flat/green.css" rel="stylesheet" type="text/css" />
    <link href="asset/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="asset/plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <link href="asset/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="asset/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <link href="asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <link href="asset/plugins/messi/messi.min.css" rel="stylesheet" type="text/css" />
    
    <script src="asset/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="asset/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src='asset/dist/js/numeral.min.js' type='text/javascript'></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="asset/plugins/morris/morris.min.js" type="text/javascript"></script>
    <script src="asset/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="asset/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="asset/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <script src="asset/plugins/knob/jquery.knob.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
    <script src="asset/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <script src="asset/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <script src="asset/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="asset/plugins/fastclick/fastclick.min.js" type="text/javascript"></script>
    <script src="asset/plugins/messi/messi.min.js" type="text/javascript"></script>
  </head>
  
  <body class="skin-green-light sidebar-mini sidebar-collapse">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>T</b>MJ</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Admin</b>Panel</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <?php
                $total_sug = get_total_unread_suggestions();
                $sugg_list = get_list_unread_suggestions();
              ?>
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success"><?php if($total_sug > 0) echo $total_sug; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have <?php echo $total_sug.' unread '; ?> Suggestions</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <?php 
                        foreach ($sugg_list as $sugg_lst){
                          $sender_family_data = get_family_data_by_its($sugg_lst['its']);
                      ?>
                      <li class="<?php if($sugg_lst['read'] == '0') { echo 'alert-info'; } ?>">
                        <a href="suggestion_list.php">
                          <h4 style="margin: 0 0 0 5px;">
                            <?php echo $sender_family_data['first_name'].' '.$sender_family_data['surname']; ?>
                            <small><i class="fa fa-clock-o"></i> <?php echo $sugg_lst['timestamp']; ?></small>
                          </h4>
                          <p style="margin: 0 0 0 5px;"><?php echo substr($sugg_lst['message'], 0, 50);?></p>
                        </a>
                      </li>
                      <?php } ?>
                    </ul>
                  </li>
                  <li class="footer"><a href="suggestion_list.php">See All Suggestions</a></li>
                </ul>
              </li>
              
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="hidden-xs"><?php echo $_SESSION[FULLNAME]; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li style="height: auto;" class="user-header">
                    <p><?php echo $_SESSION[FULLNAME]; ?></p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="user_update.php?userid=<?php echo $_SESSION[USERNAME]; ?>" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>