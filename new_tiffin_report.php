<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/class.family.php');
require_once('classes/hijri_cal.php');
$cls_receipt = new Mtx_Receipt();
$cls_family = new Mtx_family();

$from_date = $to_date = $status = $post = FALSE;
if (isset($_GET['search'])) {
  $post = TRUE;
  $from_date = $_GET['from_date'];
  $date = explode('-', $from_date);
  $fromDate = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
  $to_date = $_GET['to_date'];
  $date = explode('-', $to_date);
  $toDate = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
  $status = $_GET['status'];
  if($status == 'Added'){
    $tiffins = $cls_family->get_added_tiffin_between_dates($fromDate, $toDate);
  } else {
    $tiffins = $cls_family->get_closed_tiffin_between_dates($fromDate, $toDate);
  }
} else {
  $tiffins = array();
}
$mohallah = $cls_family->get_all_tanzeem();

$title = 'New Tiffin Report';
$active_page = 'report';

require_once 'includes/header.php';

$page_number = PROFILE_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <form method="get" role="form" class="form-horizontal">
            <div></div>
            <div class="col-md-12">
              <label class="col-md-1 control-label">From</label>
              <div class="col-md-2">
                <input type="date" name="from_date" class="form-control" id="from_date" placeholder="From Date" value="<?php echo $from_date; ?>">
              </div>

              <label class="col-md-1 control-label">To</label>
              <div class="col-md-2">
                <input type="date" name="to_date" class="form-control" id="to_date" placeholder="To Date" value="<?php echo $to_date; ?>">
              </div>

              <label class="control-label col-md-1">Status</label>
              <div class="col-md-2">
                <select class="form-control" name="status" id="status">
                  <option value="Added" <?php if($status == 'Added') echo 'selected';?>>Added</option>
                  <option value="Closed" <?php if($status == 'Closed') echo 'selected';?>>Closed</option>
                </select>
              </div>

              <div class="col-md-3">
                <input type="submit" class="btn btn-success" name="search" id="search" value="Search">
                <a target="_blank" href="print_new_tiffin_report.php?from_date=<?php echo $from_date; ?>&to_date=<?php echo $to_date; ?>&status=<?php echo $status; ?>" class="btn btn-primary <?php echo !$post ? 'disabled' : ''; ?>">Print</a>
              </div>
              <div class="clearfix"></div>
            </div>
          </form>
          <div class="col-md-12">&nbsp;</div>
      <?php
      if ($tiffins)
        echo $tiffins;
      ?>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>