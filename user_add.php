<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.user.php');
require_once('classes/class.family.php');
$cls_user = new Mtx_User();
$cls_family = new Mtx_family();

if(isset($_POST['user_add'])) {

  if(!empty($_POST['rights'])) $user_rights = array_sum($_POST['rights']);

  $result = $cls_user->add_user($_POST['name'], $_POST['email'], $_POST['mobile'],$_POST['user_id'],$_POST['password'], $_POST['mohallah'], $user_rights);
  if ($result)
    $_SESSION[SUCCESS_MESSAGE] = 'User has been added successfully.';
  else
    $_SESSION[ERROR_MESSAGE] = 'Sorry! try again.';
}

$mohallah = $cls_family->get_all_tanzeem();

$title = 'Add user';
$active_page = 'settings';

include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" name="myForm" id="myForm" role="form" class="form-horizontal">
            <div></div>

            <div class="form-group">
              <label class="col-md-2 control-label">Full Name</label>
              <div class="col-md-4">
                <input type="text" id="name" name="name" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-2 control-label">Email ID</label>
              <div class="col-md-4">
                <input type="text" id="email" name="email" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-2 control-label">Mobile</label>
              <div class="col-md-4">
                <input type="text" id="mobile" name="mobile" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-2 control-label">User ID</label>
              <div class="col-md-4">
                <input type="text" id="user_id" name="user_id" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-2 control-label">Password</label>
              <div class="col-md-4">
                <input type="password" name="password" id="password" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Mohallah</label>
              <div class="col-md-4">
                <select name="mohallah" id="mohallah" class="form-control">
                  <option value="">All</option>
                  <?php foreach ($mohallah as $name) { ?>
                    <option value="<?php echo $name['name']; ?>"><?php echo $name['name']; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <?php if($_SESSION[USER_TYPE] == 'A'){?>
            <h3>User Rights</h3>
            <!--Accounts-->
            <div class="form-group">
              <label class="control-label col-md-2">Select Groups</label>
            </div>

            <div class="form-group">
        <label class="control-label col-md-2">&nbsp;</label>
        <div class="checkbox col-md-9">
          <label>
            <input type="checkbox" class="check_account" id="acc_entry" name="rights[]" value="<?php echo KEY_ACCOUNTS_ENTRY;?>"> Accounts Entry
          </label>
          <br>
          <div class="acc_entry">
            <?php 
              $account_rights = $cls_user->get_page_rights(KEY_ACCOUNTS_ENTRY);
              if($account_rights){
                foreach ($account_rights as $pr)
                {
            ?>
            <div class="col-md-3">
              <lable><?php echo $pr['page_name']; ?></lable>
            </div>
            <?php } } ?>
          </div>
        </div>
      </div>
      
      <div class="form-group">
        <label class="control-label col-md-2">&nbsp;</label>
        <div class="checkbox col-md-9">
          <label>
            <input type="checkbox" name="rights[]" id="acc_rprt" class="check_account" value="<?php echo KEY_ACCOUNTS_REPORTS;?>"> Accounts Reports
          </label>
          <br>
          <div class="acc_rprt">
          <?php 
            $account_reports_rights = $cls_user->get_page_rights(KEY_ACCOUNTS_REPORTS);
            if($account_reports_rights){
              foreach ($account_reports_rights as $pr)
              {
          ?>
            <div class="col-md-3">
              <lable><?php echo $pr['page_name']; ?></lable>
            </div>
            <?php } } ?>
          </div>
        </div>
      </div>
        
      <div class="form-group">
        <label class="control-label col-md-2">&nbsp;</label>
        <div class="checkbox col-md-9">
          <label>
            <input type="checkbox" class="check_account" id="acc_blncst" name="rights[]" value="<?php echo KEY_ACCOUNTS_BALANCESHEET;?>"> Balance Sheet
          </label>
          <br>
          <div class="acc_blncst">
          <?php 
            $account_balancesheet_rights = $cls_user->get_page_rights(KEY_ACCOUNTS_BALANCESHEET);
            if($account_balancesheet_rights){
              foreach ($account_balancesheet_rights as $pr)
              {
          ?>
            <div class="col-md-3">
              <lable><?php echo $pr['page_name']; ?></lable>
            </div>
              <?php } } ?>
          </div>
        </div>
      </div>
        
      <div class="form-group">
        <label class="control-label col-md-2">&nbsp;</label>
        <div class="checkbox col-md-9">
          <label>
            <input type="checkbox" class="check_account" name="rights[]" value="<?php echo KEY_PROFILE_ENTRY;?>"> Profile Entry
          </label>
          <br>
          <div class="pro_entry">
            <?php 
              $profile_rights = $cls_user->get_page_rights(KEY_PROFILE_ENTRY);
              if($profile_rights){
                foreach ($profile_rights as $pr)
                {
            ?>
              <div class="col-md-3">
                <lable><?php echo $pr['page_name']; ?></lable>
              </div>
              <?php } } ?>
          </div>
        </div>
      </div>
      
      <div class="form-group">
        <label class="control-label col-md-2">&nbsp;</label>
        <div class="checkbox col-md-9">
          <label>
            <input type="checkbox" class="check_account" name="rights[]" value="<?php echo KEY_PROFILE_REPORTS;?>"> Profile Reports
          </label>
          <br>
          <div class="pro_rprt">
            <?php 
              $profile_reports_rights = $cls_user->get_page_rights(KEY_PROFILE_REPORTS);
              if($profile_reports_rights){
                foreach ($profile_reports_rights as $pr)
                {
            ?>
              <div class="col-md-3">
                <lable><?php echo $pr['page_name']; ?></lable>
              </div>
              <?php } } ?>
          </div>
        </div>
      </div>
        
      <div class="form-group">
        <label class="control-label col-md-2">&nbsp;</label>  
        <div class="checkbox col-md-9">
          <label>
            <input type="checkbox" class="check_account" name="rights[]" value="<?php echo KEY_INVENTORY_ENTRY;?>"> Inventory Entry
          </label>
          <br>
          <div class="inv_entry">
            <?php 
              $inventory_rights = $cls_user->get_page_rights(KEY_INVENTORY_ENTRY);
              if($inventory_rights){
                foreach ($inventory_rights as $pr){
            ?>
              <div class="col-md-3">
                <lable><?php echo $pr['page_name']; ?></lable>
              </div>
              <?php } } ?>
          </div>
        </div>
      </div>
        
      <div class="form-group">
        <label class="control-label col-md-2">&nbsp;</label>
        <div class="checkbox col-md-9">
          <label>
            <input type="checkbox" class="check_account" name="rights[]" value="<?php echo KEY_INVENTORY_REPORTS;?>"> Inventory Reports
          </label>
          <br>
          <div class="inv_rprt">
            <?php 
              $inventory_reports_rights = $cls_user->get_page_rights(KEY_INVENTORY_REPORTS);
              if($inventory_reports_rights){
                foreach ($inventory_reports_rights as $pr)
                {
            ?>
              <div class="col-md-3">
                <lable><?php echo $pr['page_name']; ?></lable>
              </div>
              <?php } } ?>
          </div>
        </div>
      </div>
        
      <div class="form-group">
        <label class="control-label col-md-2">&nbsp;</label>  
        <div class="checkbox col-md-9">
          <label>
            <input type="checkbox" class="check_account" name="rights[]" value="<?php echo KEY_REPORTS;?>"> Reports
          </label>
          <br>
          <div class="rprt">
            <?php 
              $reports_rights = $cls_user->get_page_rights(KEY_REPORTS);
              if($reports_rights){
                foreach ($reports_rights as $pr)
                {
            ?>
              <div class="col-md-3">
                <lable><?php echo $pr['page_name']; ?></lable>
              </div>
              <?php } } ?>
          </div>
        </div>
      </div>
            
      <div class="form-group">
        <label class="control-label col-md-2">&nbsp;</label>  
        <div class="checkbox col-md-9">
          <label>
            <input type="checkbox" class="check_account" name="rights[]" value="<?php echo KEY_IDARAH_REPORTS;?>"> Idarah Reports
          </label>
          <br>
          <div class="ida_rprt">
            <?php 
              $reports_rights = $cls_user->get_page_rights(KEY_IDARAH_REPORTS);
              if($reports_rights){
                foreach ($reports_rights as $pr)
                {
            ?>
              <div class="col-md-3">
                <lable><?php echo $pr['page_name']; ?></lable>
              </div>
              <?php } } ?>
          </div>
        </div>
      </div>
            
      <div class="form-group">
        <label class="control-label col-md-2">&nbsp;</label>  
        <div class="checkbox col-md-9">
          <label>
            <input type="checkbox" class="check_account" name="rights[]" value="<?php echo KEY_HUB_PENDING_REPORTS;?>"> Hub Pending Reports
          </label>
          <br>
          <div class="hub_rprt">
            <?php 
              $reports_rights = $cls_user->get_page_rights(KEY_HUB_PENDING_REPORTS);
              if($reports_rights){
                foreach ($reports_rights as $pr)
                {
            ?>
              <div class="col-md-3">
                <lable><?php echo $pr['page_name']; ?></lable>
              </div>
              <?php } } ?>
          </div>
        </div>
      </div>
      
      <div class="form-group">
        <label class="control-label col-md-2">&nbsp;</label>  
        <div class="checkbox col-md-9">
          <label>
            <input type="checkbox" class="check_account" name="rights[]" value="<?php echo KEY_TAKHMEEN;?>"> Takhmeen
          </label>
          <br>
          <div class="tkhmn">
            <?php 
              $reports_rights = $cls_user->get_page_rights(KEY_TAKHMEEN);
              if($reports_rights){
                foreach ($reports_rights as $pr)
                {
            ?>
              <div class="col-md-3">
                <lable><?php echo $pr['page_name']; ?></lable>
              </div>
              <?php } } ?>
          </div>
        </div>
      </div>
          <!-- new -->

            <?php } ?>
            <div class="form-group">
              <label class="col-md-2 control-label">&nbsp;</label>
              <div class="col-md-4">
                <input type="submit" id="user_add" name="user_add" value="Add" class="btn btn-success">
              </div>
            </div>
          </form>
        </div>
        <!-- /Center Bar -->
        <script>
          $('#user_add').click(function() {
            var name = $('#name').val();
            var email = $('#email').val();
            var mobile = $('#mobile').val();
            var user_id = $('#user_id').val();
            var pwd = $('#password').val();
            var error = 'Following error(s) are occurred\n\n';
            var validate = true;
            if (name == '')
            {
              error += 'Please enter User name\n';
              validate = false;
            }
            if (email == '')
            {
              error += 'Please enter email\n';
              validate = false;
            }
            if (mobile == '')
            {
              error += 'Please enter mobile\n';
              validate = false;
            }
            if (user_id == '')
            {
              error += 'Please enter your desired user id\n';
              validate = false;
            }
            if (pwd == '')
            {
              error += 'Please enter password\n';
              validate = false;
            }
            if (validate == false)
            {
              alert(error);
              return validate;
            }
          });
          $('#FileNo').keyup(function() {
            var FileNo = $(this).val().toUpperCase();
            $(this).val(FileNo);
          });

          $('#user_id').change(function() {
            var userid = $('#user_id').val();
            $.ajax({
              url: "ajax.php",
              type: "GET",
              data: 'userid=' + userid + '&cmd=check_user_exist',
              success: function(data)
              {
                if(data == '1'){
                  alert('User Id already exists');
                  return false;
                } else {
                  return true;
                }
              }
            });
          });

	$(document).ready(function(){
	      $('input[type="checkbox"]').click(function(){
		if($(this).attr("value")=="<?php echo KEY_ACCOUNTS_ENTRY; ?>"){
		    $(".acc_entry").show(600);
		}
		if($(this).attr("value")=="<?php echo KEY_ACCOUNTS_REPORTS; ?>"){
		    $(".acc_rprt").show(600);
		}
		if($(this).attr("value")=="<?php echo KEY_ACCOUNTS_BALANCESHEET; ?>"){
		    $(".acc_blncst").show(600);
		}
		if($(this).attr("value")=="<?php echo KEY_PROFILE_ENTRY; ?>"){
		    $(".pro_entry").show(600);
		}
		if($(this).attr("value")=="<?php echo KEY_PROFILE_REPORTS; ?>"){
		    $(".pro_rprt").show(600);
		}
		if($(this).attr("value")=="<?php echo KEY_INVENTORY_ENTRY; ?>"){
		    $(".inv_entry").show(600);
		}
		if($(this).attr("value")=="<?php echo KEY_INVENTORY_REPORTS; ?>"){
		    $(".inv_rprt").show(600);
		}
		if($(this).attr("value")=="<?php echo KEY_REPORTS; ?>"){
		    $(".rprt").show(600);
		}
        if($(this).attr("value")=="<?php echo KEY_IDARAH_REPORTS; ?>"){
		    $(".ida_rprt").show(600);
		}
        if($(this).attr("value")=="<?php echo KEY_HUB_PENDING_REPORTS; ?>"){
		    $(".hub_rprt").show(600);
		}
		if($(this).attr("value")=="<?php echo KEY_TAKHMEEN; ?>"){
		    $(".tkhmn").show(600);
		}
	      });
	      
	      $(".acc_entry").hide();
	      $(".acc_rprt").hide();
	      $(".acc_blncst").hide();
	      $(".pro_entry").hide();
	      $(".pro_rprt").hide();
	      $(".inv_entry").hide();
	      $(".inv_rprt").hide();
	      $(".rprt").hide();
	      $(".ida_rprt").hide();
	      $(".hub_rprt").hide();
	      $(".tkhmn").hide();
		
	    });
        </script>    

      </div>
      <!-- /Content -->
    </section>
  </div>
  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>
