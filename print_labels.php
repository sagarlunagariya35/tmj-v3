<?php
include 'session.php';
require_once("classes/class.database.php");
require_once("classes/class.family.php");
require_once("classes/class.user.php");
$cls_user = new Mtx_User();
$cls_family = new Mtx_family();
if(isset($_POST['add_labels'])){
  $thalis = $_POST['tid'];
  $fId = explode("\n", $thalis);
  foreach($fId as $key=>$value){
    if(!$value){
      unset ($fId[$key]);
    }
  }
  $max = count($fId);
}
$setting = $cls_user->get_general_settings();
$db_tanzeem_name = $setting[0]['tanzeem_name'];
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Barcode Sticker</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
      @font-face
      {
        font-family: barcode;
        src: url(asset/fonts/IDAutomationHC39M.ttf);
      }
      .row{
        width: 900px;
        height: 277px;
        /*border: 1px dotted black;*/
        padding-left: 20px;
      }
      #outer-square{
        display: inline-table;
        height: 250px;
        width: 377px;
        margin-left: 5px;
        border: 1px solid #999;
        background-color: #9900CC;
        -webkit-print-color-adjust:exact;
      }
      #inner-square{
        margin-left: 20px;
        margin-top: 25px;
        height: 210px;
        width: 343px;
        border: 1px solid #999;
        background-color: #FFFFFF;
      }
      .small-square{
        display: inline-block;
        font-size: 45px;
        width: 60px;
        top: 4px;
        text-align: center;
        vertical-align: top;
        border: 1px solid #999;
      }
      .logo{
        display: inline-block;
        font-size: 20px;
        width:268px;
        text-align: right;
        color: #FF9900;
      }
      .details{
        display: block;
        clear: both;
        margin: 0px;
        padding: 0px;
        text-align: center;
      }
      .name{
        font-size: 18px;
        font-weight: 700;
        margin-top: 20px;
        line-height: 20px;
        color: #800000;
      }
      .ph{
        font-size: 18px;
        line-height: 16px;
        margin: 0px;
        padding-bottom: 15px;
      }
      .barcode{
        display: inline-block;
        font-family: barcode;
        font-size: 14px;
        text-align: center;
      }
      #thaali-id{
        display: inline-block;
        text-align: left;
        font-size: 54px;
        font-weight: 900;
        width: 220px;
        color: #33CC33;
      }
    </style>
  </head>
  <body>
    <?php
    $count = 0;
    for($i = 0; $i < $max; $i++){
      $family = $cls_family->get_single_family(trim($fId[$i]));
      $name = $cls_family->get_name(trim($fId[$i]));
      if($count == 0){
        echo '<div class="row">';
      }
    ?>
      <div class="" id="outer-square">
        <div id="inner-square">
          <div class="col-md-2 small-square">
            <?php echo $family['tiffin_size'];?>
          </div>
          <div class="logo">
            <img src="includes/logo.jpg" width="180px">
            <br>
            <?php echo $db_tanzeem_name;?>
          </div>
          <div class="details">
            <div class="name"><?php echo $name;?></div>
            <div class="ph"><?php echo $family['MPh'];?></div>
          </div>
          <div id="thaali-id">
            <?php echo $family['FileNo'];?>
          </div>
          <div class="barcode">
            *<?php echo $family['FileNo'];?>*
          </div>
        </div>
      </div>
    <?php
    if($count == 1){
      echo '</div>';
      $count = 0;
    } else {
      $count++;
    }
    }
    ?>
  </body>
</html>