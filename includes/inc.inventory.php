<table id="example1" class="table table-bordered table-striped">
  <thead>
  <th>No.</th>
  <th>Item Name</th>
  <th>Category</th>
  <th class="text-right">Initial Quantity</th>
  <th class="text-right">Current Quantity</th>
  <th>Unit</th>
  <?php if(isset($print) && !$print) {?>
  <th>Action</th>
  <?php } ?>
</thead>
<tbody>
  <?php
  if ($result) {
    $i = 1;
    foreach ($result as $ingredient) {
      ?>
      <tr>
        <td><?php echo $i++; ?></td>
        <td><a href="item_history.php?item=<?php echo $ingredient['id']; ?>" target="_blank"><?php echo $ingredient['name'] ?></a></td>
        <td><?php if($ingredient['category'] > 0) {
          $query = "SELECT *  FROM `lp_item_category` WHERE `id` = '$ingredient[category]'";
          $result = $database->query_fetch_full_result($query);
          echo $result[0]['name'];
        } else echo '';
          ?>
        </td>
        <td class="text-right"><?php echo $ingredient['quantity'] ?></td>
        <td class="text-right"><?php echo $ingredient['cqty']; ?></td>
        <td><?php echo $ingredient['unit'] ?></td>
          <?php if(isset($print) && !$print) {?>
        <td><a href="update_inventory.php?id=<?php echo $ingredient['id']; ?>" class="btn btn-success btn-xs">Update</a></td>
          <?php } ?>
      </tr>
    <?php }
    ?>
      <tfoot>
		<tr>
          <th>No.</th>
          <th>Item Name</th>
          <th>Category</th>
          <th class="text-right">Initial Quantity</th>
          <th class="text-right">Current Quantity</th>
          <th>Unit</th>
          <?php if(isset($print) && !$print) {?>
          <th>Action</th>
          <?php } ?>
		</tr>
	</tfoot>
      <?php
  } else {
    ?>
    <tr>
      <td colspan="5" class="alert-danger">No inventory to show.</td>
    </tr>
<?php } ?>
</tbody>
</table>
