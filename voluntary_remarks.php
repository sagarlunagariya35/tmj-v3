<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.receipt.php");
$cls_receipt = new Mtx_Receipt();

if(isset($_POST['add_remarks'])){
  $result = $cls_receipt->add_remarks($_POST['remarks']);
  if($result){
    $_SESSION[SUCCESS_MESSAGE] = 'Remarks Added successfully';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while processing Voluntary Remarks';
  }
}

$remarks = $cls_receipt->get_remarks();
$title = 'Add Voluntary Contribution Remarks';
$active_page = 'settings';

include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-4">Remark:</label>
                <div class="col-md-8">
                  <input type="text" name="remarks" id="remarks" class="form-control">
                </div>
              </div>
              <div class="form-group col-md-offset-4">
                <label class="control-label col-md-4">&nbsp;</label>
                <div class="col-md-5">
                  <button class="btn btn-success" name="add_remarks" id="add_remarks">Add remark</button>
                </div>
              </div>
            </div>
            <script>
              $('#add_remarks').click(function() {
                var remark = $('#remarks').val();
                if(remark === '') return false;
              });
            </script>
            <?php if($remarks){$i = 1;?>
            <div class="col-md-5">
              <table class="table table-bordered table-condensed table-hover">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($remarks as $remark){?>
                  <tr>
                    <td><?php echo $i++;?></td>
                    <td><?php echo $remark['remarks'];?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <?php } ?>
          </form>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>