<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.menu.php');
require_once('classes/hijri_cal.php');
require_once('classes/class.user.php');
$cls_menu = new Mtx_Menu();
$cls_user = new Mtx_User();

if(isset($_POST['add_update'])) {
    $ary_cost = $_POST['tfn_cost'];
    $result = $cls_menu->add_tiffin_cost($ary_cost, $_POST['value']);
}
$current = HijriCalendar::GregorianToHijri();
$cur_year = $current[2];

$max_tfn = $cls_menu->get_all_tiffin_size(1);
$size = $cls_menu->get_all_tiffin_size();
$setting = $cls_user->get_general_settings();
$default_year = $setting[0]['start_year'];
$no_years = $setting[0]['no_of_years'];
$title = 'Add Tiffin cost';
$active_page = 'settings';

include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <!--div class="form-group pull-right">
              <button type="submit" name="previous" id="previous" class="btn btn-primary">Previous</button>
              <button type="submit" name="current" id="current" class="btn">Current</button>
              <button type="submit" name="next" id="next" class="btn btn-primary">Next</button>
              <input type="hidden" id="value" name="value" value ="<?php
              if(isset($_POST['previous'])){
                echo $counter;
              }
              if(isset($_POST['next'])){
                echo $counter;
              }
      ?>">
            </div-->
            <table class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th rowspan="2" style="width: 8%">Year</th>
                        <th colspan="<?php echo $max_tfn;?>" class="text-center">Tiffin Size(s)</th>
                    </tr>
                    <tr>
                        <?php foreach($size as $tfn_size) {?>
                        <th><?php echo $tfn_size['size'];?></th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $j = 0;
                    for($i = 1; $i <= $no_years; $i++) {
                        $year = (($i == 1) ? $default_year : ($default_year + ($i - 1))) . '-' . (substr($default_year, -2) + $i);

                        ?>
                    <tr>
                        <td><?php echo $year;?></td>
                        <?php
                        foreach($size as $tfn_size) {
                            $tfn_cost = $cls_menu->get_cost_data($year, $tfn_size['id']);
                            ?>
                        <td><input type="text" name="tfn_cost[<?php echo $year . ' '.  trim($tfn_size['id'])?>]" value="<?php echo ($tfn_cost) ? number_format($tfn_cost, 2) : '';?>" class="form-control"></td>
                        <?php } ?>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>

            <input type="submit" id="cost" name="add_update" value="Add/Update" class="btn btn-success">

          </form>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>