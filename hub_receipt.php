<?php
include('session.php');
require_once('classes/class.receipt.php');
require_once('classes/class.database.php');
$cls_receipt = new Mtx_Receipt();
$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
require_once 'daily_cash_entry.php';
require_once('classes/class.user.php');
$cls_user = new Mtx_User();
// create a unique hash for the transaction
$len = mt_rand(4, 10);
$form_id = '';
for ($i = 0; $i < $len; $i++) {
  $d = rand(1, 30) % 2;
  $form_id .= $d ? chr(rand(65, 90)) : chr(rand(48, 57));
}

$form_id = md5(time() . $form_id);
$setting = $cls_user->get_general_settings();
$default_year = $setting[0]['start_year'];
$no_years = $setting[0]['no_of_years'];
$title = 'FMB Hub';
$active_page = 'account';
$pg_link = 'hub';

require_once 'includes/header.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Credit</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-6">
            <form method="post" role="form" target="blank" class="form-horizontal" action="print_hub_receipt.php">
              <div></div>
              <div class="form-group">
                <label class="control-label col-md-3"><?php echo YEAR; ?></label>
                <div class="col-md-6">
                  <select class="form-control get_record" name="takhmeen_year_hub_rcpt" id="takhmeen_year_hub_rcpt">
                    <option value ="">-- Select One --</option>
                    <?php
                      for($i = 1; $i <= $no_years; $i++) {
                      $year = (($i == 1) ? $default_year : ($default_year + ($i - 1))) . '-' . (substr($default_year, -2) + $i);
                    ?>
                    <option value="<?php echo $year;?>" <?php if($setting[0]['default_year'] == $year){ echo 'selected'; } ?>><?php echo $year; ?></option>
                  <?php } ?>

                  </select>
                </div>
              </div>
              <div class="form-group" id="File">
                <label class="control-label col-md-3"><?php echo THALI_ID; ?></label>
                <div class="col-md-6">
                  <input type="text" id="FileNo" name="FileNo" class="form-control get_record">
                </div>
                <div class="col-md-1" id="loader">

                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Name</label>
                <div class="col-md-6">
                  <input type="text" name="thali_name" id="thali_name" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Amount</label>
                <div class="col-md-6">
                  <input type="text" id="amount" name="amount" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Ref. No.</label>
                <div class="col-md-6">
                  <input type="text" id="ref_no" name="ref_no" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Date</label>
                <div class="col-md-6">
                  <input type="date" name="timestamp" class="form-control" id="timestamp" value="<?php echo date('Y-m-d'); ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">Type</label>
                <div class="col-md-6">
                  <input type="radio" id="radio" name="type" value="cash" checked>&nbsp;Cash&emsp;
                  <input type="radio" id="radio" name="type" value="cheque">&nbsp;Cheque&emsp;
                  <input type="radio" id="radio" name="type" value="neft">&nbsp;NEFT
                </div>
              </div>

              <div id="cheque_detail" style="display: none">
                <div></div>
                <div class="form-group" >
                  <label class="control-label col-md-3">Bank Name</label>
                  <div class="col-md-6">
                    <input type="text" name="bankname" id="bankname" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3" id="cheque_type">Cheque No.</label>
                  <div class="col-md-6">
                    <input type="text" name="chk_no" id="cheque" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Date</label>
                  <div class="col-md-6">
                      <input type="date" name="chk_date" class="form-control" id="chk_date">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3" for="sms">Alert Via SMS</label>
                  <div class="col-md-6">
                    <input type="checkbox" name="sms" id="sms" class="checkbox">
                  </div>
              </div>
              <div class="form-group" id="show_mobile" style="display: none;">
                <label class="control-label col-md-3">Mobile</label>
                <div class="col-md-6">
                  <input type="text" id="mobile" name="mobile" class="form-control">
                </div>
              </div>
              <input type="hidden" name="form_id" value="<?php echo $form_id; ?>">
              <input type="submit" id="print" name="print" value="Print" class="btn btn-success col-md-offset-3">
            </form>
          </div>
          <div class="col-md-6" id="records">

          </div>
        </div>
        <!-- /Center Bar -->
        <script>
          var gbl_thali_id = true;
          $('#print').click(function() {
            var thali_id = $('#FileNo').val();
            var year = $('#takhmeen_year_hub_rcpt').val();
            var amount = $('#amount').val();
            var ts = $('#timestamp').val();
            var error = 'Following error(s) occurred\n\n';
            var validate = true;
            if (year == '') {
              error += 'Please select year\n';
              validate = false;
            }
            if (thali_id == '') {
              error += 'Please select Thali ID\n';
              validate = false;
            }
            if (gbl_thali_id == false) {
              error += 'Invalid thali ID\n';
              validate = false;
            }
            if (amount == '') {
              error += 'Please enter amount\n';
              validate = false;
            }
            if (ts == '') {
              error += 'Please select date\n';
              validate = false;
            }
            if (validate == false) {
              alert(error);
              return validate;
            }
          });

          $("input:radio[name=type]").click(function() {
            var radio = $(this).val();
            switch(radio) {
              case 'cheque':
                $('#cheque_detail').show();
                $('#cheque_type').text('Cheque No.');
                break;
              case 'neft':
                $('#cheque_detail').show();
                $('#cheque_type').text('NEFT No.');
                break;
              default:
                $('#cheque_detail').hide();
            }
          });
          
          $("input:checkbox[name=sms]").click(function() {
            var $this = $(this);
            if ($this.is(':checked')) {
                $('#show_mobile').show();
            } else {
                $('#show_mobile').hide();
            }
          });

          //change
          $('.get_record').change(function() {
            $('#records').empty(); // to update the table for new either new thali id or year
            var year = $('#takhmeen_year_hub_rcpt').val();
            var FileNo = $('#FileNo').val().toUpperCase();
            $('#FileNo').val(FileNo);
            if (year == '' || FileNo == '')
              return false;
            myApp.showPleaseWait();
            $.ajax({
              url: "ajax.php",
              type: "POST",
              data: 'fId=' + FileNo + '&cmd=get_name_and_mobile',
              success: function(data)
              {
                if (data != 'invalid') {
                  var user_data = JSON.parse(data);
                  $('#File').addClass('has-success');
                  $('#thali_name').val(user_data.full_name);
                  $('#mobile').val(user_data.mobile);
                  $('#amount').focus();
                  var year = $('#takhmeen_year_hub_rcpt').val();
                  gbl_thali_id = true;
                  $.ajax({
                    url: "ajax.php",
                    type: "POST",
                    data: 'fId=' + FileNo + '&cmd=takhmeen_hub_receipt&year=' + year,
                    success: function(response) {
                      if (response) {
                        var obj = JSON.parse(response);
                        div_element('Takhmeen Amount', obj.TAKHMEEN_AMOUNT);
                        div_element('Paid Amount', obj.PAID_AMOUNT);
                        div_element('Pending Amount', obj.PENDING_AMOUNT);
                        div_element('Last Year Pending Amount', obj.LAST_YEAR_PENDING_AMOUNT);
                        var ary_obj = JSON.parse(obj.FIVE_RECEIPT);
                        var tbl = '<div class="col-md-12">&nbsp;</div><table class="table table-hover table-condensed table-bordered"><thead><tr><th colspan="4" class="text-center">Last 5 Transactions</th></tr><tr><th>ReceiptID</th><th>Ref. No.</th><th class="text-right">Takhmeen</th><th>Year</th><th>Date</th></tr></thead><tbody>';
                        var monthNames = ["January", "February", "March", "April", "May", "June",
                          "July", "August", "September", "October", "November", "December"];
                        $.each(ary_obj, function() {
                          var sp_dt = this.date.split('-');
                          var fullDate = sp_dt[2] + ' ' + monthNames[parseInt(sp_dt[1]) - 1] + ', ' + sp_dt[0];
                          var cls = (this.cancel == '1') ? 'class="alert-danger"' : '';
                          tbl += '<tr ' + cls +'><td><a href="print_hub_receipt.php?id=' + this.id + '" target="blank">' + this.id + '</a></td><td>' + this.ref_no + '</td><td class="text-right">Rs. ' + numeral(this.amount).format('0,0.00') + '</td><td>' + this.year + '</td><td>' + fullDate + '</td></tr>';
                        });
                        tbl += '</tbody></table>';
                        $('#records').append(tbl);
                      }
                    }
                  });
                  myApp.hidePleaseWait();
                  return true;
                } else {
                  alert('Invalid thali ID');
                  $('#loader').text('');
                  $('#File').addClass('has-error');
                  $('#FileNo').focus();
                  gbl_thali_id = false;
                  myApp.hidePleaseWait();
                  return false;
                }
              }
            });
          });

          $("#FileNo").keydown(function(e) {
            if (e.keyCode === 9) {
              if (gbl_thali_id === false) {
                $('#FileNo').focus();
                return false;
              } else
                return true;
            }
          });

          function div_element(label, val) {
            var cls = '';
            var amount = val;
            if (label == 'Pending Amount') {
              cls = ' alert-info';
              amount = '<strong>' + val + '</strong>';
            }
            if (label == 'Last Year Pending Amount') {
              cls = ' alert-danger';
              amount = '<strong>' + val + '</strong>';
            }
            var div_element = '<div class="col-md-12' + cls + '"><label class="control-label col-md-6">' + label + ':</label><div class="col-md-6"><span class="form-control-static">' + amount + '</span></div><div class="clearfix"></div></div>';
            $('#records').append(div_element);
          }

          $(document).ready(function() {
            $('#FileNo').focus(function() {
              $(this).select();
            });
          });
          var myApp;
          myApp = myApp || (function() {
            var pleaseWaitDiv = $('<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Processing . . .</h4></div><div class="modal-body"><img src="asset/img/loader.gif"></div></div></div></div>');
            return {
              showPleaseWait: function() {
                pleaseWaitDiv.modal();
              },
              hidePleaseWait: function() {
                pleaseWaitDiv.modal('hide');
              },
            };
          })();

        </script>



      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>