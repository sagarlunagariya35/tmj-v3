<?php

class Mtx_Barcode {

  private $id;

  public function Mtx_Barcode($id = '') {
    if ($id) {
      if ($this->set_id($id))
        return TRUE;
      else
        return FALSE;
    }
  }

  public function __destruct() {
    // TODO: destructor code
  }

  public function get_all_barcode() {
    global $database;
    $month = date('m');
    $day = date('d');
    $year = date('Y');
    $start_time = mktime(0, 0, 0, $month, $day, $year);
    $end_time = mktime(23, 59, 59, $month, $day, $year);
    $query = "Select f.FileNo, f.prefix, f.first_name,(SELECT new_tiffin_size FROM tiffin_size WHERE FileNo LIKE f.FileNo ORDER BY id DESC LIMIT 0,1) tiffin_size, f.father_prefix, f.father_name, f.surname, f.MPh, d.family_id, d.`date` from family f LEFT JOIN (SELECT family_id, `date` FROM daily_entry WHERE `date` BETWEEN '$start_time' AND '$end_time') d ON f.FileNo = d.family_id WHERE close_date='0' AND show_in_scanning = '1'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_barcode_by_mohallah($mohallah, $date) {
    global $database;

    $date = explode('-', $date);
    $month = (int)$date[1];
    $day = (int)$date[2];
    $year = (int)$date[0];
    $start_time = mktime(0, 0, 0, $month, $day, $year);
    $end_time = mktime(23, 59, 59, $month, $day, $year);
    if ($mohallah == 'All')
      $condition = '';
    else
      $condition = "AND `Mohallah` = $mohallah";
    if ($mohallah == 'All')
      $query = "Select f.FileNo, f.prefix, f.first_name,(SELECT new_tiffin_size FROM tiffin_size WHERE FileNo LIKE f.FileNo ORDER BY id DESC LIMIT 0,1) tiffin_size, f.father_prefix, f.father_name, f.surname, f.MPh, f.Mohallah, f.HOF, d.family_id, d.`date` from family f LEFT JOIN (SELECT family_id, `date` FROM daily_entry WHERE `date` BETWEEN '$start_time' AND '$end_time') d ON f.FileNo = d.family_id WHERE close_date='0' AND show_in_scanning = '1'";
    else {
      $query = "Select f.FileNo, f.prefix, f.first_name,(SELECT new_tiffin_size FROM tiffin_size WHERE FileNo LIKE f.FileNo ORDER BY id DESC LIMIT 0,1) tiffin_size, f.father_prefix, f.father_name, f.surname, f.MPh, f.Mohallah, f.HOF, d.family_id, d.`date` from family f LEFT JOIN (SELECT family_id, `date` FROM daily_entry WHERE `date` BETWEEN '$start_time' AND '$end_time') d ON f.FileNo = d.family_id WHERE close_date='0' AND `Mohallah` = '$mohallah' AND show_in_scanning = '1'";
    }

    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_hub_exceptions() {
    global $database;
    $month = date('m');
    $day = date('d');
    $year = date('Y');
    $timestamp = mktime(0, 0, 0, $month, $day, $year);
    $query = "SELECT `FileNo` FROM `fmb_hub_exception` WHERE `from_ts` <= '$timestamp' AND `to_ts` >= '$timestamp'";
    $result = $database->query_fetch_full_result($query);
    if ($result) {
      foreach ($result as $row) {
        $ary_ret[] = $row['FileNo'];
      }
      return $ary_ret;
    } else {
      return array();
    }
  }

  function get_today_tiffin($tanzeem = FALSE, $date = FALSE) {
    global $database;

    if (!$date)
      $date = date('Y-m-d');

    $date = explode('-', $date);
    $m = $date['1'];
    $d = $date['2'];
    $y = $date['0'];
    $start = mktime(0, 0, 0, $m, $d, $y);
    $end = mktime(23, 59, 59, $m, $d, $y);
    $query = "SELECT *, (SELECT `tiffin_size` FROM `family` f WHERE f.FileNo LIKE d.family_id) tiffin_size FROM `daily_entry` d WHERE `date` BETWEEN '$start' AND '$end'";
    if ($tanzeem && $tanzeem != 'All')
      $query .= " AND (SELECT `Mohallah` FROM `family` f WHERE f.FileNo LIKE d.family_id) = '$tanzeem'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function insert_issued_tiffin($family_id, $date) {
    global $database;
    $date = explode('-', $date);
    $date = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    $query = "INSERT INTO daily_entry(`family_id`,`date`) values('$family_id','$date')";
    $fh = fopen('text.txt', 'a+');
    fwrite($fh, $query);
    fclose($fh);
    $result = $database->query($query);
    if ($result)
      return 1;
    else
      return 0;
  }

  public function get_issued_tiffin($date) {
    global $database;
    
    $date = explode('-', $date);
    $m = $date['1'];
    $d = $date['2'];
    $y = $date['0'];
    $start = mktime(0, 0, 0, $m, $d, $y);
    $end = mktime(23, 59, 59, $m, $d, $y);
    
    $query = "Select COUNT(*) AS `Total` from `daily_entry` WHERE `date` BETWEEN '$start' AND '$end'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['Total'];
  }

  public function get_day_count($from_ts, $to_ts) {
    global $database;
    $query = "SELECT count(distinct(from_unixtime(`date`, '%Y %m %d'))) AS cnt FROM `daily_entry` WHERE `date` BETWEEN '$from_ts' AND '$to_ts'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['cnt'];
  }

  //getter and setter
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

}

?>
