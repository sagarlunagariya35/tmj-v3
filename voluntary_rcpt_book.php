<?php
include 'session.php';
$pg_link = 'voluntary_rcpt_book';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
$cls_receipt = new Mtx_Receipt();

$title = 'Voluntary Receipt Book';
$active_page = 'account';
$page = 1;
if (isset($_GET['page']) && $_GET['page'] != '') {
  $page = $_GET['page'];
}

$from_date = $to_date = FALSE;
if (isset($_GET['search']) OR $_GET) {
  $from_date = $_GET['from_date'];
  $to_date = $_GET['to_date'];
  $result = $cls_receipt->get_all_receipt($page, 20, $from_date, $to_date);
  $total_vol_receipt = $cls_receipt->get_total_voluntary_receipt($from_date, $to_date);
  $amount = $cls_receipt->get_vol_total_between_dates($from_date, $to_date);
} else {
  $result = $cls_receipt->get_all_receipt($page, 20);
  $total_vol_receipt = $cls_receipt->get_total_voluntary_receipt();
  $amount = FALSE;
}

require_once 'includes/header.php';

$page_number = ACCOUNTS_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Receipt Books</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <?php include 'includes/inc.dates.php'; ?>
          <?php if ($_GET) { ?>
            <div class="col-md-12">&nbsp;</div>
            <div class="alert-success">
              <strong>Total Amount: </strong><?php echo number_format($amount[0]['Amount'], 2); ?>
            </div>
          <?php } ?>

          <div class="col-md-12">&nbsp;</div>
          <table class="table table-hover table-condensed table-bordered">
            <thead>
            <th>Id</th>
            <th>Name</th>
            <th>Acct. Heads</th>
            <th class="text-right">Amount</th>
            <th>Date</th>
            </thead>
            <tbody>
              <?php
              if ($result) {
                foreach ($result as $receipt) {
                  $red = ($receipt['cancel'] == 1) ? ' class=alert-danger' : '';
                  ?>
                  <tr<?php echo $red; ?>>
                    <td><a href="print_voluntary_receipt.php?id=<?php echo $receipt['id'] ?>&fId=<?php echo $receipt['FileNo'] ?>" target=_blank" ><?php echo $receipt['id'] ?></a></td>
                    <td><?php echo ucfirst($receipt['hubber_name']); ?></td>
                    <td><?php
                      if ($receipt['acct_heads'] > 0) {
                        $hname = $cls_receipt->get_single_acct_heads($receipt['acct_heads']);
                        echo $hname;
                      } else
                        echo '----';
                      ?></td>
                    <td class="text-right"><?php echo number_format($receipt['amount'], 2, '.', ','); ?></td>
                    <td><?php echo date('d F, Y', $receipt['timestamp']); ?></td>
                  </tr>
                  <?php
                }
              } else {
                ?>
                <tr>
                  <td colspan="6" class="alert-danger">Sorry! No receipts found.</td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
          <?php
          require_once("pagination.php");
          $getData = ($from_date && $to_date) ? 'from_date=' . $from_date . '&to_date=' . $to_date . '&' : '';
          echo pagination(20, $page, '?' . $getData . 'page=', $total_vol_receipt);
          ?>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>