<table class="table table-hover table-condensed">
  <thead>
    <?php if($print) {?>
    <tr>
      <th colspan="6" style="text-align: center">Families <?php if ($mohallah != '')
  echo 'from ' . ucwords($mohallah);
else
  echo '';
?><span class="pull-right"><?php echo $date; ?></span></th>
    </tr>
    <?php } ?>
    <tr>
      <th>Sr No</th>
      <th><?php echo THALI_ID; ?></th>
      <th>HOF Name</th>
      <th>Mobile No.</th>
      <th class="text-right">Takhmeen</th>
      <th class="text-center">FC Code</th>
      <th>Size</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $tiffin_count = array();
    if ($search) {
      $i = 1;
      foreach ($search as $family) {
        $gblTotal += $family['takhmeen_amount'];
        if (isset($tiffin_count[$family['tiffin_size']]))
          $tiffin_count[$family['tiffin_size']] += 1;
        else
          $tiffin_count[$family['tiffin_size']] = 1;
        if ($family['close_date'] > 0)
          $cls = 'class="alert-danger"';
        else
          $cls = '';
        $hof_name = ($family['first_name'] != '') ? $family['prefix'] . ' ' . $family['first_name'] . ' ' . $family['father_prefix'] . ' ' . $family['father_name'] . ' ' . $family['surname'] : $family['HOF'];
        ?>
        <tr <?php echo $cls; ?>>
          <td><?php echo $i++; ?></td>
          <td><?php echo $family['FileNo']; ?></td>
          <td><?php echo $hof_name; ?></td>
          <td><?php echo $family['MPh']; ?></td>
          <td class="text-right"><?php echo 'Rs. ' . number_format($family['takhmeen_amount'], 2); ?></td>
          <td class="text-center"><?php echo $family['fc_code']; ?></td>
          <td><?php echo $family['tiffin_size']; ?></td>
        </tr>

        <?php
      }

      if ($gblTotal > 0) {
        ?>
        <tr>
          <td style="text-align: right" colspan="4"><strong>Total :</strong></td>
          <td class="text-right"><?php echo 'Rs. ' . number_format($gblTotal, 2); ?></td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <?php
      }
      if (count($tiffin_count) > 0) {
        ?>
        <tr>
          <td colspan="6" style="text-align: right">
            <span class="pull-right">
              <?php
              foreach ($tiffin_count as $key => $val) {
                echo "<strong>$key: </strong> $val ";
              }
              ?>
            </span>
          </td>
        </tr>
  <?php }
} else {
  ?>
      <tr>
        <td colspan="7" class="alert-danger">Sorry! no records found.</td>
      </tr>
<?php } ?>
  </tbody>
</table>