<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/class.family.php');
require_once('classes/class.user.php');
require_once('classes/hijri_cal.php');

$cls_receipt = new Mtx_Receipt();
$cls_family = new Mtx_family();
$cls_user = new Mtx_User();

$from_date = $to_date = date('Y-m-d');
$tanzeem = FALSE;
if (isset($_POST['search'])) {
  $from_date = $_POST['from_date'];
  $to_date = $_POST['to_date'];
  $tanzeem = $_POST['tanzeem'];
}

$receipts = $cls_receipt->get_daily_rcpt_between_days($from_date, $to_date, $tanzeem);
$mohallah = $cls_user->get_single_user($_SESSION[USERNAME]);
$mohallahs = $cls_family->get_all_tanzeem();
$title = 'Total hub receipt ' . date('d M,Y');

$active_page = 'report';
include('includes/header.php');

$page_number = HUB_PENDING_REPORTS;
include('page_rights.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Hub</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <div class="col-md-12">
              <label class="col-md-1 control-label">From</label>
              <div class="col-md-2">
                <input type="date" name="from_date" class="form-control" id="from_date" value="<?php echo $from_date; ?>">
              </div>
              <label class="col-md-1 control-label">To</label>
              <div class="col-md-2">
                <input type="date" name="to_date" class="form-control" id="to_date" value="<?php echo $to_date; ?>">
              </div>
              <label class="col-md-2 control-label">Tanzeem</label>
              <div class="col-md-2">
                <?php if($mohallah['Mohallah'] != '') { ?>
                  <p class="form-control static"><?php echo $mohallah['Mohallah']; ?></p>
                  <input type="hidden" name="tanzeem" value="<?php echo $mohallah['Mohallah']; ?>">
                <?php }else { ?>
                  <select class="form-control" name="tanzeem">
                    <option value="">All</option>
                    <?php foreach ($mohallahs as $mh) { ?>
                      <option value="<?php echo $mh['name']; ?>" <?php echo ($tanzeem == $mh['name']) ? 'selected' : ''; ?>><?php echo $mh['name']; ?></option>
        <?php } ?>
                  </select>
                <?php } ?>
              </div>

              <input type="submit" class="btn btn-success validate" name="search" id="search" value="Search">
              <a href="print_total_daily_hub.php?from=<?php echo $from_date; ?>&to=<?php echo $to_date; ?>&tanzeem=<?php echo $tanzeem; ?>" target="blank" class="btn btn-primary validate" id="print">Print</a>
            </div>
          </form>
          <div class="col-md-12">&nbsp;</div>
      <?php
      if ($receipts)
        echo $receipts;
      ?>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
<script>
  $('.validate').click(function() {
    var from = $('#from_date').val();
    var to = $('#to_date').val();
    var errors = [];
    var key = 0;
    if (from === '')
      errors[key++] = 'from';
    if (to === '')
      errors[key++] = 'to';
    if (errors.length) {
      alert('Please select ' + errors.join(' & ') + ' date to proceed..');
      return false;
    }
  });
</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>