<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.menu.php");
$cls_menu = new Mtx_Menu();

if (isset($_POST['Download_Inventory_Costing'])) {
  $query = "SELECT i.id, i.name, i.unit, (SELECT `cost` FROM `lp_inventory_costing` lp WHERE lp.inventory_id = i.id) cost FROM `inventory` i ORDER BY `id` ASC";
  $result = $database->query_fetch_full_result($query);
  csv_download($result, 'inventory_costing.csv');
  exit();
}

if (isset($_POST['Download_Direct_Costing'])) {
  $query = "SELECT dp.id, dp.name, dp.unit, (SELECT `cost` FROM `lp_direct_costing` lp WHERE lp.inventory_id = dp.name) cost FROM `dp_lookup` dp ORDER BY `id` ASC";
  $result = $database->query_fetch_full_result($query);
  csv_download($result, 'direct_costing.csv');
  exit();
}

function csv_download($ary_data, $filename) {
  // output headers so that the file is downloaded rather than displayed
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=' . $filename);

// create a file pointer connected to the output stream
  $output = fopen('php://output', 'w');

// output the column headings
  fputcsv($output, array('inventory_id', 'name', 'unit', 'cost'));

// fetch the data
// loop over the rows, outputting them
  foreach ($ary_data as $row) {
    fputcsv($output, $row);
  }
}

if (isset($_POST['Upload_Inventory_Costing'])) {
  $fileName = $_FILES['fileUpload'];
  $result = upload_csv($fileName, 'lp_inventory_costing');
}

if (isset($_POST['Upload_Direct_Costing'])) {
  $fileName = $_FILES['fileUpload'];
  $result = upload_csv($fileName, 'lp_direct_costing');
}

function upload_csv($up_file, $table_name) {
  global $database;
  $ext = strtoupper(substr($up_file['name'], -3));
  if ($ext == 'CSV') {
    $file = fopen($up_file['tmp_name'], "r");
    
    while (!feof($file)) {
      $line = fgetcsv($file);
      $id = $line[0];
      $inv_id = ($table_name == 'lp_direct_costing') ? $line[1] : $id;
      if($table_name == 'lp_direct_costing') $condition = $inv_id != NULL && $inv_id != 'inventory_id';
      else $condition = $inv_id != 'inventory_id' && is_numeric($inv_id);
      if ($condition) {
        $name = trim($line[1]);
        $cost = $database->clean_currency($line[3]);
        $query = "SELECT * FROM `$table_name` WHERE `inventory_id` = '$inv_id'";
        $found = $database->query_fetch_full_result($query);
        if ($found) {
          $query = "UPDATE `$table_name` SET `cost` = '$cost' WHERE `inventory_id` = '$inv_id'";
        } else {
          $query = "INSERT INTO `$table_name`(`inventory_id`, `cost`) VALUES ('$inv_id', '$cost')";
        }
        $result = $database->query($query);
      }
    }
    if($result) {
      $_SESSION[SUCCESS_MESSAGE] = 'File uploaded successfully.';
    } else {
      $_SESSION[ERROR_MESSAGE] = 'Error encountered while uploading the file..';
    }
    fclose($file);
    unlink($up_file['tmp_name']);
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Please Upload only csv file';
  }
}

$title = 'Upload costing details';
$active_page = 'menu';

require_once 'includes/header.php';

$page_number = ACCOUNTS_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profiles</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal" enctype="multipart/form-data">
            <div class="form-group">
              <label class="control-label col-md-3">Upload</label>
              <div class="col-md-9">
                <input type="file" class="" name="fileUpload" id="fileUpload">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">&nbsp;</label>
              <div class="col-md-4">
                <button class="btn btn-success validate" type="submit" name="Upload_Inventory_Costing" id="save"><span class="glyphicon glyphicon-upload"></span> Upload Inventory Costing</button>
              </div>
              <div class="col-md-4">
                <button class="btn btn-info" type="submit" name="Download_Inventory_Costing" id="save"><span class="glyphicon glyphicon-download"></span> Download Inventory Costing</button>

              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">&nbsp;</label>
              <div class="col-md-4">
                <button class="btn btn-success validate" type="submit" name="Upload_Direct_Costing" id="save"><span class="glyphicon glyphicon-upload"></span> Upload Direct Costing</button>
              </div>
              <div class="col-md-4">
                <button class="btn btn-info" type="submit" name="Download_Direct_Costing" id="save"><span class="glyphicon glyphicon-download"></span> Download Direct Costing</button>
              </div>
            </div>

          </form>
        </div>
        <!-- /Center Bar -->
        <script>
          $('.validate').click(function() {
            var file = $('#fileUpload').val();
            if(file === '') {
              alert('Please select csv file to upload');
              return false;
            }
          });
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>