<?php
include 'session.php';
$page_number = 38;
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.receipt.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();
$cls_receipt = new Mtx_Receipt();

if (isset($_POST['Print'])) {
  header('Location: print_sabil_home_pending_report.php?mh=' . $_POST['Mohallah']);
}
if (isset($_POST['Search'])) {
  $search = $cls_family->get_family_by_mohallah($_POST['Mohallah']);
}
$mohallah = $cls_family->get_all_Mohallah();
$fly = $cls_family->get_sabil_home_pending();

$title = 'Sabil home Pending';
$active_page = 'report';

include('includes/header.php');
include('page_rights.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <div class="col-md-3 pull-left">
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Menu This week</h3></div>
            <div class="panel-body">
              <?php include('includes/search_bar.php'); ?>
            </div>
          </div>
        </div>
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-8 ">
          <div class="col-md-12">
            <?php
            $cur_hijri_date = HijriCalendar::GregorianToHijri();
            echo $cur_hijri_date[1] . ', ' . HijriCalendar::monthName($cur_hijri_date[0]) . ', ' . $cur_hijri_date[2] . ' H';
            echo date('d-m-Y H:i:s');
            ?>
          </div>
          <form method="post" class="form-horizontal">
            <div class="form-group">
              <label class="col-md-2 control-label">Mohallah</label>
              <div class="col-md-8 input-group">
                <select id="Mohallah" name="Mohallah" class="form-control">
                  <option value="">All</option>
                  <?php
                  if ($mohallah) {
                    foreach ($mohallah as $mohalla) {
                      ?>
                      <option value="<?php echo $mohalla['Mohallah'] ?>" <?php
                      if (isset($_POST['Search'])) {
                        if ($mohalla['Mohallah'] == $_POST['Mohallah']) {
                          echo 'selected';
                        }
                      }
                      ?>><?php echo ucwords($mohalla['Mohallah']); ?></option>
                              <?php
                            }
                          }
                          ?>
                </select>
                <span class="input-group-btn">
                  <input type="submit" class="btn btn-success" value="Search" name="Search">
                </span>
              </div>
                  <input type="submit" class="btn btn-primary" value="Print" name="Print">
            </div>
          </form>

          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12" id="report">
            <table class="table table-hover table-bordered table-condensed">
              <thead>
              <th style="text-align: center">Sr No</th>
              <th style="text-align: center">Family No</th>
              <th style="text-align: center">HOF Name</th>
              <th style="text-align: center">Mobile No.</th>
              <th style="text-align: center">Paid Till</th>
              <th style="text-align: center">Close date</th>
              <th style="text-align: center">Pending Amount</th>
              <th style="text-align: center">On hand Cash</th>
              <th style="text-align: center">Actual Amount</th>
              <th style="text-align: center">Mohallah</th>

              </thead>
              <tbody>
                <?php
                if (isset($_POST['Search'])) {
                  if (is_array($fly) && !empty($fly)) {
                    $num = 1;
                    $total_pending_amount = 0;
                    $total_on_hand_amount = 0;
                    $total_actual_amount = 0;
                    foreach ($search as $family) {
                      $partial = $cls_receipt->get_particular_partial_receipt($family['FileNo']);
                      if ($family['close_date'] != 0)
                        $hijri_close_date = HijriCalendar::GregorianToHijri($family['close_date']);
                      if ($family['paid_till'])
                        $hijri_date = HijriCalendar::GregorianToHijri($family['paid_till']);
                      ?>
                      <tr>
                        <td><?php echo $num++; ?></td>
                        <td><?php echo $family['FileNo']; ?></td>
                        <td><?php $name = $cls_family->get_name($family['FileNo']); echo $name; ?></td>
                        <td><?php echo $family['MPh']; ?></td>
                        <?php if ($family['paid_till'] != NULL) { ?>
                          <td><?php echo HijriCalendar::monthName($hijri_date[0]) . ', ' . $hijri_date[2] . ' H'; ?></td>
                        <?php } else { ?>
                          <td>--</td>
                        <?php } ?>
                        <?php if ($family['close_date'] != 0) { ?>
                          <td style="background-color: #FF9999;"><?php echo HijriCalendar::monthName($hijri_close_date[0]) . ', ' . $hijri_date[2] . ' H'; ?></td>
                        <?php } else { ?>
                          <td>--</td>
                          <?php } ?>
                        <td>
                          <?php
                          if($family['paid_till'] > 0){
                          $hijri_date = HijriCalendar::GregorianToHijri($family['paid_till']);
                          $last_year = $hijri_date[2];

                          $cur_ts = HijriCalendar::GregorianToHijri();
                          $cur_month = $cur_ts[0];
                          if($family['close_date'] != 0){
                            $cur_ts = HijriCalendar::GregorianToHijri($family['close_date']);
                            $cur_month = $cur_ts[0];
                          }

                          if($last_year == $cur_ts[2]){
                            $diff_months = abs($hijri_date[0] - $cur_month);
                          } else if($last_year < $cur_ts[2]){
                            $diff_year = $cur_ts[2] - $last_year;
                            if($diff_year > 1){
                              $remaining_months_from_last = 12 -  $hijri_date[0];
                              $diff_months = abs((12 * $diff_year) + $remaining_months_from_last);
                            } else {
                              $remaining_months_from_last = 12 -  $hijri_date[0];
                              $diff_months = abs($remaining_months_from_last + $cur_month);
                            }
                          } else if($last_year > $cur_ts[2]){
                            $diff_months = 0;
                          }

                          $home = $cls_receipt->get_single_sabil_home($family['FileNo']);
                          $pending_amount = $home['amount'] * $diff_months;

                          echo number_format($pending_amount).'/-';
                          $total_pending_amount += $pending_amount;
                          }
                          ?>
                        </td>
                        <td><?php
                          if ($partial) {
                            echo number_format($partial['TotalPayment']) . '/-';
                            $total_on_hand_amount += $partial['TotalPayment'];
                          }
                          else
                            echo '--';
                          ?></td>
                        <td><?php
                          if ($partial)
                            $on_hand = $partial['TotalPayment'];
                          $actual = $pending_amount - $on_hand;
                          $total_actual_amount += $actual;
                          echo number_format($actual) . '/-';
                          ?></td>
                        <td><?php echo $family['Mohallah']; ?></td>

                      </tr>

                      <?php
                    }
                  }
                } else {
                  if (is_array($fly) && !empty($fly)) {
                    $num = 1;
                    $total_pending_amount = 0;
                    $total_on_hand_amount = 0;
                    $total_actual_amount = 0;
                    foreach ($fly as $family) {
                      if ($family['close_date'] != 0)
                        $hijri_close_date = HijriCalendar::GregorianToHijri($family['close_date']);
                      $partial = $cls_receipt->get_particular_partial_receipt($family['FileNo']);
                      if ($family['paid_till'])
                        $hijri_date = HijriCalendar::GregorianToHijri($family['paid_till']);
                      ?>
                      <tr>
                        <td><?php echo $num++; ?></td>
                        <td><?php echo $family['FileNo']; ?></td>
                        <td><?php $name = $cls_family->get_name($family['FileNo']); echo $name; ?></td>
                        <td><?php echo $family['MPh']; ?></td>
                        <?php if ($family['paid_till'] > 0) { ?>
                          <td><?php echo HijriCalendar::monthName($hijri_date[0]) . ', ' . $hijri_date[2] . ' H'; ?></td>
                        <?php } else { ?>
                          <td>--</td>
                        <?php } ?>
                        <?php if ($family['close_date'] != 0) { ?>
                          <td style="background-color: #FF9999;"><?php echo HijriCalendar::monthName($hijri_close_date[0]) . ', ' . $hijri_date[2] . ' H'; ?></td>
                          <?php } else { ?>
                          <td>--</td>
                          <?php } ?>
                        <td>
                          <?php
                          if($family['paid_till'] > 0){
                          $hijri_date = HijriCalendar::GregorianToHijri($family['paid_till']);
                          $last_year = $hijri_date[2];

                          $cur_ts = HijriCalendar::GregorianToHijri();
                          $cur_month = $cur_ts[0];
                          if($family['close_date'] != 0){
                            $cur_ts = HijriCalendar::GregorianToHijri($family['close_date']);
                            $cur_month = $cur_ts[0];
                          }

                          if($last_year == $cur_ts[2]){
                            $diff_months = abs($hijri_date[0] - $cur_month);
                          } else if($last_year < $cur_ts[2]){
                            $diff_year = $cur_ts[2] - $last_year;
                            if($diff_year > 1){
                              $remaining_months_from_last = 12 -  $hijri_date[0];
                              $diff_months = abs((12 * $diff_year) + $remaining_months_from_last);
                            } else {
                              $remaining_months_from_last = 12 -  $hijri_date[0];
                              $diff_months = abs($remaining_months_from_last + $cur_month);
                            }
                          } else if($last_year > $cur_ts[2]){
                            $diff_months = 0;
                          }

                          $home = $cls_receipt->get_single_sabil_home($family['FileNo']);
                          $pending_amount = $home['amount'] * $diff_months;

                          echo number_format($pending_amount).'/-';
                          $total_pending_amount += $pending_amount;
                          }
                          ?>
                        </td>
                        <td><?php
                          if ($partial) {
                            echo number_format($partial['TotalPayment']) . '/-';
                            $total_on_hand_amount += $partial['TotalPayment'];
                          }
                          else
                            echo '--';
                          ?></td>
                        <td><?php
                          if ($partial)
                            $on_hand = $partial['TotalPayment'];
                          $actual = $pending_amount - $on_hand;
                          $total_actual_amount += $actual;
                          echo number_format($actual) . '/-';
                          ?></td>
                        <td><?php echo $family['Mohallah']; ?></td>

                      </tr>
            <?php
          }
        } else {
          ?>
                    <tr>
                      <td colspan="10">No Record Found</td>
                    </tr>
                    <?php
                  }
                }
                if (isset($total_pending_amount) && $total_pending_amount > 0 && isset($total_on_hand_amount) && isset($total_actual_amount)) {
                  ?>
                  <tr>
                    <td style="text-align: right" colspan="6">Total:</td>
                    <td colspan="1"><strong><?php echo number_format($total_pending_amount) . '/-' ?></strong></td>
                    <td colspan="1"><strong><?php echo number_format($total_on_hand_amount) . '/-' ?></strong></td>
                    <td colspan="2"><strong><?php echo number_format($total_actual_amount) . '/-' ?></strong></td>
                  </tr>
        <?php
      }
      ?>
              </tbody></table> 

          </div>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>
  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>