<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.family.php");
require_once("classes/class.user.php");
$cls_family = new Mtx_family();
$cls_user = new Mtx_User();
$setting = $cls_user->get_general_settings();
if(isset($_POST['send_all']) OR isset($_POST['send_above']) OR isset($_POST['close_acct'])) {
  sendEmail($_POST);
}

function sendEmail($data) {
  global $setting;
  $back_up_email = $setting[0]['back_up_email'];
  $ary_fails = array();
  $message = trim($data['message']);
  if(isset($data['send_above'])) {
    $thali_id = trim($data['thali_id']);
    $thali_id = explode(',', str_replace("\r\n", ',', $thali_id));
  $query = "SELECT *  FROM `family` WHERE `FileNo` IN ('" . implode("','", $thali_id) . "')";
  }
  
  if(isset($data['send_all'])) {
    $query = "SELECT *  FROM `family` WHERE `email` != ''";
  }
  
  if(isset($data['close_acct'])) {
    $query = "SELECT *  FROM `family` WHERE `close_date` > '0'";
  }
  
  $data = $database->query_fetch_full_result($query);
  foreach ($data as $result) {
    $to_email = trim($result['email']);
    $thali_id = trim($result['FileNo']);
    if(empty($to_email)) {
      $ary_fails[] = $thali_id;
      continue;
    }
    $subject = 'Subject will go here';
    $from_email = $back_up_email;
    if(mail($to_email, $subject, $message)) {
      $ary_success[] = $thali_id;
    } else {
      $ary_fails[] = $thali_id;
    }
  }
  
  
  if(count($ary_success)) $_SESSION[SUCCESS_MESSAGE] = 'Email sent successfully to followind '.THALI_ID.'<br>'.  implode(',', $ary_success);
  
  if(count($ary_fails) > 0) $_SESSION[ERROR_MESSAGE] = 'Email failed to the followind '.THALI_ID.'<br>'.  implode(',', $ary_fails);
  header('Location: send_email.php');
  exit();
}

if(isset($_POST['hub_pending_msg'])) {
  
}

$title = 'Send Email';
$active_page = 'settings';
include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <div class="form-group">
              <label class="control-label col-md-3">Message</label>
              <div class="col-md-4">
                <textarea type="text" maxlength="160" onkeyup="textCounter(this, 160);" class="form-control" name="message" id="msg"></textarea>
              </div>
              <div class="col-md-4">
                <span id="how_many"></span>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3"><?php echo THALI_ID; ?>(s)</label>
              <div class="col-md-4">
                <textarea class="form-control" rows="8" name="thali_id" id="tid"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">&nbsp;</label>
              <div class="col-md-8">
                <input class="btn btn-success validate" type="submit" name="send_all" id="sms" value="Send To All">
                <input class="btn btn-success validate" type="submit" name="send_above" id="above" value="Send To above">
                <input class="btn btn-success validate" type="submit" name="close_acct" id="above" value="Send To close account">
                <!--input class="btn btn-success" type="submit" name="hub_pending_msg" id="hub_pending_msg" value="Hub Pending"-->
              </div>
            </div>

          </form>
        </div>
        <!-- /Center Bar -->
      <script>
        $('.validate').click(function() {
      //    alert($(this).val());
        var msg = $('#msg').val();
          if (msg == '')
          {
            var field = ($(this).val() == 'Send To above') ? ' and specify thali id(s)!' : '';
            alert('Please enter a Message' + field);
            return false;
          }
        });
        function textCounter(field, maxlimit)
        {
          var len = field.value.length;
          if(field.value.length < maxlimit) {
            $('#how_many').addClass('alert alert-success').text((maxlimit - len) + ' Characters remaining');
          } else {
            $('#how_many').addClass('alert alert-danger').text('0 Character remaining');
            field.blur();
            field.focus();
            return false;
          }
        }
      </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>



