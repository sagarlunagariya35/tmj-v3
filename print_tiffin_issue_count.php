<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/class.family.php');
require_once('classes/hijri_cal.php');

$cls_family = new Mtx_family();
$title = 'Tiffin count report';
$active_page = 'account';

if(isset($_GET['from'])) $fromDate = $_GET['from'];
else $fromDate = FALSE;
if(isset($_GET['to'])) $toDate = $_GET['to'];
else $toDate = FALSE;

$fmonth = date('m', $fromDate);
$fyear = date('Y', $fromDate);
if(($fromDate != '') && ($toDate != ''))
  $filenos = $cls_family->get_FileNo_daily_entry($fromDate, $toDate);
else
  $filenos = $cls_family->get_FileNo_daily_entry();
$h = HijriCalendar::GregorianToHijri();
$date = $h[1] . ' ' . HijriCalendar::monthName($h[0]) . ', ' . $h[2];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print tiffin issue / not issued count report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8 ">
    <table class="table table-hover table-condensed table-bordered">
      <thead>
        <tr>
          <th colspan="5">Tiffin issue / not issued count report<span class="pull-right"><?php echo $date;?></span></th>
        </tr>
        <tr>
          <th>No</th>
          <th><?php echo THALI_ID; ?></th>
          <th>Total days</th>
          <th>Issued Count</th>
          <th>not issued days</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i = 1;
        if($filenos){
          foreach($filenos as $id){
              $issued_count = $cls_family->get_entry_from_daily_entry($id['family_id'], $fromDate, $toDate);
              $days_in_month = cal_days_in_month(CAL_GREGORIAN, $fmonth, $fyear);
            ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $id['family_id']; ?></td>
                <td><?php echo $days_in_month; ?></td>
                <td><?php echo $issued_count; ?></td>
                <td><?php echo abs($issued_count - $days_in_month) . ' days'; ?></td>
              </tr>
        <?php
          }
          } else {
          echo '<tr><td colspan="5">No results found.</td></tr>';
        }
        ?>
        
      </tbody>
    </table>
          <?php  ?>
  </div>
  <!-- /Center Bar -->


</div>
<!-- /Content -->
<?php
include('includes/footer.php');
?>