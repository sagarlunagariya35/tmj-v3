<form class="form-horizontal" role="form" method="post" action="" id="frm_inc_fc_code">
  <div class="form-group">
    <div class="col-md-12">
      <input type="text" name="frm_search_fc_code" class="form-control" id="frm_search_fc_code" placeholder="By Fc code">
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-12">
      <button type="button" class="btn btn-success btn-block" id="frm_inc_search_code">Search</button>
    </div>
  </div>
</form>
<script>
  $('#frm_inc_search_code').click(function() {
    var fly_code = $('#frm_search_fc_code').val();
    if (fly_code === '') {
      alert('Please enter Fc code..');
      return false;
    } else {
      $.ajax({
        url: 'ajax.php',
        type: 'POST',
        data: 'code=' + fly_code + '&cmd=FC_CODE',
        success: function(data) {
          if (data != '0') {
            var fly = $.parseJSON(data);
            var text = '';
            $.each(fly, function() {
              var thali_id = '<div class="form-group"><label class="control-lable col-md-4"><?php echo THALI_ID; ?></label><p class="form-control-static"><a href="update_family.php?cmd=show&family_id=' + this.FileNo + '" target="blank">' + this.FileNo + '</a></p></div>';
              var File_data = '<div class="form-group"><label class="control-lable col-md-4">Name</label>' + this.HOF_NAME + '</div>';

              var fly_Ejamat_ID = '<div class="form-group"><label class="control-label col-md-4">Ejamat ID</label><p class="form-control-static">' + this.ejamatID + '</p></div>';
              var cls = (this.receives_barakat == 'Yes') ? 'alert-success' : 'alert-danger';
              var rcpt_id = '<div class="form-group ' + cls + '"><label class="control-label col-md-4">Status</label><p class="form-control-static">' + this.receives_barakat + '</p></div><hr>';
              text += thali_id + File_data + fly_Ejamat_ID + rcpt_id;

            });
            $('#searched_fc_code_body').hide();
            $('#result_searched_fc_code_body').html(text).show();
          } else {
            $('#searched_fc_code_body').hide();
            $('#result_searched_fc_code_body').text('No results found.').show();
            return false;
          }
        }
      });
    }
  });
</script>