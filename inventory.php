<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.product.php');
require_once('classes/class.menu.php');
$cls_product = new Mtx_product();
$cls_menu = new Mtx_Menu();

$sub = '';

if (isset($_POST['Update'])) {
  $item_name = $_POST['item_name'];
  $category = $_POST['category'];
  $id = $_POST['hidden_id'];
  $update = $cls_product->update_inventory($item_name, $category, $id);
}

$print = FALSE;
$result = $cls_product->get_all_ingredients();
if (isset($_GET['id']) && $_GET['id'] != '') {
  $result = $cls_product->get_all_ingredients($_GET['id']);
  $sub = ' for ' . $_GET['name'];
}

$categories = $cls_menu->get_inventory_category();

$title = "List of Inventory" . $sub;
$active_page = "menu";

require_once 'includes/header.php';

$page_number = INVENTORY_REPORTS;
require_once 'page_rights.php';
?>

<link href="asset/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="asset/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="asset/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>

<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Maedat</a></li>
        <li><a href="#">Inventory</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="pull-right">
            <a href="print_inventory.php" target="_blank" class="btn btn-primary btn-xs">Print</a>
          </div>
          <br><br>
          <div class="box">
            <div class="box-body">
              <?php
                include('includes/inc.inventory.php');
              ?>
            </div>
          </div>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript">
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<?php
include('includes/footer.php');
?>