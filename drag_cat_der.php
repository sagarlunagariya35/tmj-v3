<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.menu.php");
$cls_menu = new Mtx_Menu();

if (isset($_POST['add_menu'])) {
  $items = $_POST['item'];
  $ary_item = array();
  foreach($items as $key => $val) {
    if($val > 0) $ary_item[] = $val;
  }
  $ary_item = array_unique($ary_item);
  $date = $_POST['date'];
  $person_count = $_POST['person_count'];
  $menu_id = implode(',', $ary_item);
  $custom_menu = $_POST['custom_menu'];
  $add_menu = $cls_menu->add_daily_menu($menu_id, $date, $person_count, $custom_menu);
  if ($add_menu) {
    $_SESSION[SUCCESS_MESSAGE] = 'Menu added successfully';
    header("location:cook_estimate_der.php?date=" . $date);
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while processing....';
  }
}
$categories = $cls_menu->get_all_category();
$title = 'Copy menu';
$active_page = 'menu';

require_once 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profiles</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-2">
            <div class="form-group">
              <label class="col-md-12 text-left">Category</label>
              <br>
              <select size="20" id="category" name="category" class="form-control">
                <?php foreach ($categories as $category) { ?>
                  <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
                <?php } ?>          </select>

            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label class="col-md-12 text-left">Menu</label>
              <div class="col-md-12 menu_list">

              </div>
            </div>
          </div>
          <div class="col-md-8">
            <form method="post" role="form" class="form-horizontal">
              <div class="col-md-12">
                <div class="col-md-6">
                  <label class="col-md-4">Tarkadi</label>
                  <div class="col-md-8 dropbox" id="droppable" ondrop="drop(event, 'tarkadi')" ondragover="allowDrop(event)">
                    <input type="hidden" class="form-control" name="item[]" id="tarkadi">
                  </div>
                </div>
                <div class="col-md-6">
                  <label class="col-md-4">Rice</label>
                  <div class="col-md-8 dropbox" ondrop="drop(event, 'rice')" ondragover="allowDrop(event)">
                    <input type="hidden" class="form-control" name="item[]" id="rice">
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="col-md-6">
                  <label class="col-md-4">Sweet</label>
                  <div class="col-md-8 dropbox" ondrop="drop(event, 'sweet')" ondragover="allowDrop(event)">
                    <input type="hidden" class="form-control" name="item[]" id="sweet">
                  </div>
                </div>
                <div class="col-md-6">
                  <label class="col-md-4">Kharas</label>
                  <div class="col-md-8 dropbox" ondrop="drop(event, 'kharas')" ondragover="allowDrop(event)">
                    <input type="hidden" class="form-control" name="item[]" id="kharas">
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="col-md-6">
                  <label class="col-md-4">Side Dish</label>
                  <div class="col-md-8 dropbox" ondrop="drop(event, 'dish')" ondragover="allowDrop(event)">
                    <input type="hidden" class="form-control" name="item[]" id="dish">
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="col-md-6">
                  <label class="col-md-4">Other</label>
                  <div class="col-md-8 dropbox" ondrop="drop(event, 'other')" ondragover="allowDrop(event)">
                    <input type="hidden" class="form-control" name="item[]" id="other">
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-12">&nbsp;</div>
              <div class="col-md-12">
                <div class="col-md-6">
                  <label class="col-md-4">Date</label>
                  <div class="col-md-8">
                    <input type="date" name="date" class="form-control" id="date">
                  </div>
                </div>
                <div class="col-md-6">
                  <label class="col-md-4">Thali Quantity</label>
                  <div class="col-md-8">
                    <input type="text" name="person_count" class="form-control" id="person_count">
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-12">&nbsp;</div>
              <div class="col-md-12">
                <div class="col-md-6">
                  <label class="col-md-4">Custom Menu</label>
                  <div class="col-md-8">
                    <input type="text" name="custom_menu" class="form-control" id="custom_menu">
                  </div>
                </div>
                <div class="col-md-6">
                  <input type="submit" value="Add Daily Menu" name="add_menu" class="btn btn-success pull-right" id="add_menu">
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>
<script>
  $('#add_menu').click(function() {
    var tarkadi = $('#tarkadi').val();
    var rice = $('#rice').val();
    var sweet = $('#sweet').val();
    var kharas = $('#kharas').val();
    var dish = $('#dish').val();
    var other = $('#other').val();
    var dt = $('#date').val();
    var errors = [];
    var i = 0;
    if (tarkadi == '' && rice == '' && sweet == '' && kharas == '' && dish == '' && other == '') {
      errors[i++] = 'atleast one item from Tarkadi, Rice, Sweet, Kharas & Dish';
    }
    var err_dt = '';
    if (errors.length)
      err_dt = 'AND Date';
    else
      err_dt = 'Date';
    if (dt == '')
      errors[i++] = err_dt;
    if (errors.length) {
      alert('Please select ' + errors.join(', ') + ' to proceed...');
      return false;
    }
  });
  function allowDrop(ev) {
    ev.preventDefault();
  }

  function drag(ev) {
    ev.dataTransfer.setData("Text", ev.target.id);
  }

  function drop(ev, type) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("Text"); //source-id
    if ($('#' + type).val() == '')
      ev.target.appendChild(document.getElementById(data));
    else
      return false;
    //alert(data.replace('source-', ''));
    var item_id = data.replace('source-', '');
    $('#' + type).val(item_id);
    sync_data(type, item_id);
  }
  function sync_data(type, item_id) {
    var items = new Array('tarkadi', 'sweet', 'dish', 'rice', 'kharas', 'other');
    //alert('Items ' + items);
    //alert('Type ' + type);
    var index = items.indexOf(type);
    //alert('index ' + index);
    items.splice(index, 1);
    //alert('remove ' + items);
    var i = 0;
    $.each(items, function() {
      //alert(items[i++]);
      var menu_type_id = $('#'+items[i]).val();
      //alert('item '+items[i]+' menu type id '+menu_type_id);
      //if(menu_type_id != item_id) 
        //$('#'+items[i++]).val('');
    });
  }
  $('#category').on('click', function() {
    var cid = $(this).val();
    if(cid === null) return false;
    myApp.showPleaseWait();
    jQuery.ajax({
      type: "POST",
      url: "ajax.php",
      data: "cmd=get_base_menu_from_cat&cid=" + cid,
      success: function(data, status) {
        //on success, show new data.
        var contents = '';
        if (data != 0) {
          var menus = $.parseJSON(data);
          $.each(menus, function() {
            contents += '<div class="list_item" draggable="true" id="source-' + this.id + '" ondragstart="drag(event)">' + this.name.trim() + '</div>';
          });
        } else {
          contents = 'Sorry! no menus found.';
        }
        $('.menu_list').html(contents);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        //On error, we alert user
        alert(thrownError);
      }
    });
    myApp.hidePleaseWait();
  });
  var myApp;
  myApp = myApp || (function() {
    var pleaseWaitDiv = $('<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Processing . . .</h4></div><div class="modal-body"><img src="asset/img/loader.gif"></div></div></div></div>');
    return {
      showPleaseWait: function() {
        pleaseWaitDiv.modal();
      },
      hidePleaseWait: function() {
        pleaseWaitDiv.modal('hide');
      },
    };
  })();
</script>