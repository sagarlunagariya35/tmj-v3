<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.family.php');
$cls_family = new Mtx_family();
if(isset($_POST['Tanzeem_add'])){
  $result = $cls_family->add_tanzeem(ucfirst($_POST['tanzeem']));
  if($result){
    $_SESSION[SUCCESS_MESSAGE] = 'Tanzeem added successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while inserting new Tanzeem';
  }
}
$mohallah = $cls_family->get_all_tanzeem();
$title = 'Add New Tanzeem';
$active_page = 'settings';

include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div class="col-md-6">
            <div class="form-group">
              <label class="control-label col-md-4">Tanzeem</label>
              <div class="col-md-8">
                <input type="text" name="tanzeem" id="tanzeem" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">&nbsp;</label>
              <div class="col-md-8">
                <input type="submit" id="Tanzeem_add" name="Tanzeem_add" value="Add" class="btn btn-success">
              </div>
            </div>
            </div>
            <div class="col-md-6">
              <?php if($mohallah){?>
              <table class="table table-bordered table-condensed table-hover">
                <thead>
                  <tr>
                    <th colspan="2" style="text-align: center">List of tanzeem</th>
                  </tr>
                  <tr>
                    <th>No.</th>
                    <th>Name</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($mohallah as $tanzeem){?>
                  <tr>
                    <td><?php echo $tanzeem['id'];?></td>
                    <td><?php echo $tanzeem['name'];?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
              <?php } ?>
            </div>
          </form>
        </div>
        <!-- /Center Bar -->
        <script>
          $('#tiffin_size').keyup(function(){
            var upper_case = $('#tiffin_size').val().toUpperCase();
            $(this).val(upper_case);
          });
          $('#Tanzeem_add').click(function() {
            var tiffin = $('#tiffin_size').val();
            if (tiffin == '')
            {
              alert("Please enter Tanzeem");
              return false;
            }
          });
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>