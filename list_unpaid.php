<?php
include 'session.php';
$pg_link = 'hub_rcpt_book';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
$cls_receipt = new Mtx_Receipt();

if ($_GET) {
  if (isset($_GET['hid']))
    $clear = $cls_receipt->make_cheque_clear($_GET['hid'], 'fmb_receipt_hub');
  if (isset($_GET['vid']))
    $clear = $cls_receipt->make_cheque_clear($_GET['vid'], 'receipt');
  if (isset($_GET['cid']))
    $clear = $cls_receipt->make_cheque_clear($_GET['cid'], 'credit_voucher');
  if (isset($_GET['bid']))
    $clear = $cls_receipt->make_cheque_clear($_GET['bid'], 'account_bill');
  if (isset($_GET['dpid']))
    $clear = $cls_receipt->make_cheque_clear($_GET['dpid'], 'direct_purchase');
  if (isset($_GET['did']))
    $clear = $cls_receipt->make_cheque_clear($_GET['did'], 'debit_voucher');
  if ($clear) {
    $_SESSION[SUCCESS_MESSAGE] = 'Cheque cleared successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing...';
  }
  location();
}

function location() {
  header('Location: list_cheque.php');
  exit();
}

$bills = $cls_receipt->get_all_unpaid('`account_bill`');
$direct_purchase = $cls_receipt->get_all_unpaid('direct_purchase');

function no_results_found($label) {
  $div = '<div class="panel-body">No results found.</div>';
  panel($label, $div);
}

function panel($tbl_name, $table) {
  $panel = <<<PANEL
          <div class="panel panel-info">
            <div class="panel-heading">{$tbl_name}</div>
            $table
          </div>
PANEL;
            echo $panel;
}

function create_tbody($array, $id_link, $label, $slug) {
//  echo '<pre>';
//  print_r(func_get_args());
  $start = <<<START
          <table class="table table-hover table-condensed table-bordered">
            <thead>
              <tr>
                <th>ID</th>
                <th>Bill Date</th>
                <th>Pay</th>
                <th>Name</th>
                <th class='text-right'>Amount</th>
              </tr>
            </thead>
          <tbody>
START;
  $tr = '';
  foreach ($array as $rcpt) {//    print_r($rcpt);
    $rcpt_id = $id_link ? "<a href='$id_link{$rcpt['id']}' target=blank>{$rcpt['id']}</a>" : $rcpt['id'];
    $checkbox = "<a href='$id_link{$rcpt['id']}' target=blank>Open</a>";
    $timestamp = date('d-m-Y',$rcpt['timestamp']);
    $name = isset($rcpt['name']) ? $rcpt['name'] : $rcpt['hubber_name'];
    $name = ucwords(strtolower($name));
    $amount = number_format($rcpt['amount'], 2);
    $cheque_date = date('d F, Y', $rcpt['cheque_date']);
    $cheque_no = $rcpt['cheque_no'];
    $tr_cls = ($rcpt['cancel'] == 1) ? ' class=alert-danger' : '';
    $tr .= <<<TABLE
          <tr>
            <td>{$rcpt_id}</td>
            <td>{$timestamp}</td>
            <td>{$checkbox}</td>
            <td>{$name}</td>
            <td class='text-right'>{$amount}</td>
          </tr>
TABLE;
  }
  $end = "</tbody></table>";
  $table = $start.$tr.$end;
  panel($label, $table);
}

$title = 'Unpaid Bills';
$active_page = 'account';

require_once 'includes/header.php';

$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Balance Sheet</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-12">&nbsp;</div>

          <?php
          $slug = 'bill';
          $book = 'Bill Cheque Book';
          ($bills) ? create_tbody($bills, 'upd_bill_details.php?cmd=show_bills&bid=', $book, $slug) : no_results_found($book);
          $slug = 'direct';
          $book = 'Direct Cheque Book';
          ($direct_purchase) ? create_tbody($direct_purchase, 'upd_bill_details.php?cmd=show_direct_bills&bid=', $book, $slug) : no_results_found($book);
          ?>

        </div>
        <!-- /Center Bar -->
        <script>
          function Chequecleared(val) {
            var slug = $('#clear-' + val).val();
            var id = '';
            switch (slug) {
              case 'hub':
                id = 'hid';
                break;
              case 'vol':
                id = 'vid';
                break;
              case 'credit':
                id = 'cid';
                break;
              case 'debit':
                id = 'did';
                break;
              case 'direct':
                id = 'dpid';
                break;
              case 'bill':
                id = 'bid';
                break;
            }
            window.location.href = 'list_cheque.php?' + id + '=' + val;
            return true;
          }
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>
  
<?php
include('includes/footer.php');
?>