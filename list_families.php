<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.family.php');
$cls_family = new Mtx_family();
$gblTotal = 0;
if (isset($_POST['Search'])) {
  $search = $cls_family->get_all_familyfor_report($_POST['Mohallah'],FALSE);
} else {
  $search = $cls_family->get_all_familyfor_report('all', TRUE);
}
$mohallah = $cls_family->get_all_Mohallah();

$title = 'List of All Families';
$active_page = 'report';

require_once 'includes/header.php';

$page_number = REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-12">
            <?php
            switch (USE_CALENDAR) {
              case 'Hijri':
                $cur_hijri_date = HijriCalendar::GregorianToHijri();
                echo $cur_hijri_date[1] . ', ' . HijriCalendar::monthName($cur_hijri_date[0]) . ', ' . $cur_hijri_date[2] . ' H';
                break;
              case 'Greg':
                echo date('l d F, Y');
                break;
            }
            ?>
          </div>
          <form method="post" class="form-horizontal">
            <div class="form-group">
              <label class="col-md-2 control-label">Mohallah</label>
              <div class="col-md-7 input-group">
                <select id="Mohallah" name="Mohallah" class="form-control">
                  <option value="all">All</option>
                  <?php
                  if ($mohallah) {
                    foreach ($mohallah as $mohalla) {
                      ?>
                      <option value="<?php echo $mohalla['Mohallah'] ?>" <?php
                      if (isset($_POST['Search'])) {
                        if ($mohalla['Mohallah'] == $_POST['Mohallah']) {
                          echo 'selected';
                        }
                      }
                      ?>><?php echo ucwords($mohalla['Mohallah']); ?></option>
                              <?php
                            }
                          }
                          ?>
                </select>
                <span class="input-group-btn">
                  <input type="submit" class="btn btn-success" value="Search" name="Search">
                  <input type="hidden" value="<?php
                  if (isset($_POST['Search']))
                    echo $_POST['Mohallah'];
                  else
                    echo '';
                  ?>" class="btn" name="mh">
                </span>
              
                <a href="print_all_families.php?mh=<?php if (isset($_POST['Search'])) echo $_POST['Mohallah']; else 'all' ?>" target="blank" class="btn btn-primary pull-left" style="margin-left: 10px;"  name="print">Print</a>
                <!--a href="print_all_families_with_hub_record.php?mh=<?php if (isset($_POST['Search'])) echo $_POST['Mohallah']; else echo 'all' ?>&cmd=show_all" target="blank" name="Mohallah_wise_hub_details" class="btn btn-primary">Print with hub details</a-->
              </div>
            </div>
          </form>
          <div class="com-md-12">
          </div>
          <div class="col-md-12" id="report">
            <?php
              $print = FALSE;
              include 'includes/inc.list_families.php';
            ?>

          </div>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>