<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.user.php');
require_once('classes/class.family.php');

$cls_user = new Mtx_User();
$cls_family = new Mtx_family();

$profile = 0;
$acct = 0;
$report = 0;
$maedat = 0;

if (isset($_POST['Update_user'])) {

  if(!empty($_POST['rights'])) $user_rights = array_sum($_POST['rights']);

  $result = $cls_user->Update_user($_POST['name'], $_POST['email'], $_POST['mobile'], $_POST['Confirm_password'], $_POST['New_password'], $_GET['userid'], $_POST['mohallah'], $user_rights);
  if ($result){
    $_SESSION[SUCCESS_MESSAGE] = 'User has been updated successfully.';
    header('Location: user_update.php?userid='.$_GET['userid']);
    exit;
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Sorry! try again.';
    header('Location: user_update.php?userid='.$_GET['userid']);
    exit;
  }
}

$user = $cls_user->get_single_user($_GET['userid']);
$bin = strrev(decbin($user['user_rights']));
$ary_user_rights = str_split($bin);
$mohallah = $cls_family->get_all_tanzeem();

$title = 'Update User';
$active_page = 'settings';

include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" name="myForm" role="form" class="form-horizontal">
            <div></div>

            <div class="form-group">
              <label class="col-md-2 control-label">Full Name</label>
              <div class="col-md-4">
                <input type="text" id="name" name="name" class="form-control" value="<?php echo $user['full_name'];?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-2 control-label">Email ID</label>
              <div class="col-md-4">
                <input type="text" id="email" name="email" class="form-control" value="<?php echo $user['email'];?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-2 control-label">Mobile</label>
              <div class="col-md-4">
                <input type="text" id="mobile" name="mobile" class="form-control" value="<?php echo $user['mobile'];?>">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-2 control-label">User ID</label>
              <div class="col-md-4">
                <p class="form-control-static"><?php echo $user['username'];?></p>
                <!--input type="text" id="user_id" name="user_id" class="form-control" value="<?php echo $user['username'];?>"-->
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-2 control-label">New Password</label>
              <div class="col-md-4">
                <input type="password" name="New_password" id="New_password" class="form-control" value="">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-2 control-label">Confirm Password</label>
              <div class="col-md-4">
                <input type="password" name="Confirm_password" id="Confirm_password" class="form-control" value="">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Mohallah</label>
              <div class="col-md-4">
                <select name="mohallah" id="mohallah" class="form-control">
                  <option value="">All</option>
                  <?php foreach ($mohallah as $name) { ?>
                    <option value="<?php echo $name['name']; ?>" <?php if($user['Mohallah'] == $name['name']) { echo 'selected'; } ?>><?php echo $name['name']; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <?php if($_SESSION[USER_TYPE] == 'A'){?>
            <h3>User Rights</h3>
            <!--Accounts-->
            <div class="form-group">
              <label class="control-label col-md-2">Select Groups</label>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">&nbsp;</label>
              <div class="checkbox col-md-9">
                <label>
                  <input type="checkbox" class="check_account" id="acc_entry" name="rights[]" <?php
                  if(array_key_exists(ACCOUNTS_ENTRY, $ary_user_rights) && ($ary_user_rights[ACCOUNTS_ENTRY] == 1)){ $val = 1;
                      echo " checked";
                  }else { $val = 2; }
                  ?> value="<?php echo KEY_ACCOUNTS_ENTRY;?>"> Accounts Entry
                </label>
                <br>
                <div class="acc_entry <?php if($val != 1){ echo 'box'; } ?>">
                  <?php 
                    $account_rights = $cls_user->get_page_rights(KEY_ACCOUNTS_ENTRY);
                    if($account_rights){
                      foreach ($account_rights as $pr)
                      {
                  ?>
                  <div class="col-md-3">
                    <lable><?php echo $pr['page_name']; ?></lable>
                  </div>
                  <?php } } ?>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">&nbsp;</label>
              <div class="checkbox col-md-9">
                <label>
                  <input type="checkbox" name="rights[]" id="acc_rprt" class="check_account"<?php
                  if(array_key_exists(ACCOUNTS_REPORTS, $ary_user_rights) && ($ary_user_rights[ACCOUNTS_REPORTS] == 1)){ $val = 1;
                      echo " checked";
                  }else { $val = 2; }
                  ?> value="<?php echo KEY_ACCOUNTS_REPORTS;?>"> Accounts Reports
                </label>
                <br>
                <div class="acc_rprt <?php if($val != 1){ echo 'box'; } ?>">
                <?php 
                  $account_reports_rights = $cls_user->get_page_rights(KEY_ACCOUNTS_REPORTS);
                  if($account_reports_rights){
                    foreach ($account_reports_rights as $pr)
                    {
                ?>
                  <div class="col-md-3">
                    <lable><?php echo $pr['page_name']; ?></lable>
                  </div>
                  <?php } } ?>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">&nbsp;</label>
              <div class="checkbox col-md-9">
                <label>
                  <input type="checkbox" class="check_account" id="acc_blncst" name="rights[]"<?php
                  if(array_key_exists(ACCOUNTS_BALANCESHEET, $ary_user_rights) && ($ary_user_rights[ACCOUNTS_BALANCESHEET] == 1)){ $val = 1;
                      echo " checked";
                  }else { $val = 2; }
                  ?> value="<?php echo KEY_ACCOUNTS_BALANCESHEET;?>"> Balance Sheet
                </label>
                <br>
                <div class="acc_blncst <?php if($val != 1){ echo 'box'; } ?>">
                <?php 
                  $account_balancesheet_rights = $cls_user->get_page_rights(KEY_ACCOUNTS_BALANCESHEET);
                  if($account_balancesheet_rights){
                    foreach ($account_balancesheet_rights as $pr)
                    {
                ?>
                  <div class="col-md-3">
                    <lable><?php echo $pr['page_name']; ?></lable>
                  </div>
                    <?php } } ?>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">&nbsp;</label>
              <div class="checkbox col-md-9">
                <label>
                  <input type="checkbox" class="check_account" name="rights[]"<?php
                  if(array_key_exists(PROFILE_ENTRY, $ary_user_rights) && ($ary_user_rights[PROFILE_ENTRY] == 1)){ $val = 1;
                      echo " checked";
                  }else { $val = 2; }
                  ?> value="<?php echo KEY_PROFILE_ENTRY;?>"> Profile Entry
                </label>
                <br>
                <div class="pro_entry <?php if($val != 1){ echo 'box'; } ?>">
                  <?php 
                    $profile_rights = $cls_user->get_page_rights(KEY_PROFILE_ENTRY);
                    if($profile_rights){
                      foreach ($profile_rights as $pr)
                      {
                  ?>
                    <div class="col-md-3">
                      <lable><?php echo $pr['page_name']; ?></lable>
                    </div>
                    <?php } } ?>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">&nbsp;</label>
              <div class="checkbox col-md-9">
                <label>
                  <input type="checkbox" class="check_account" name="rights[]"<?php
                  if(array_key_exists(PROFILE_REPORTS, $ary_user_rights) && ($ary_user_rights[PROFILE_REPORTS] == 1)){ $val = 1;
                      echo " checked";
                  }else { $val = 2; }
                  ?> value="<?php echo KEY_PROFILE_REPORTS;?>"> Profile Reports
                </label>
                <br>
                <div class="pro_rprt <?php if($val != 1){ echo 'box'; } ?>">
                  <?php 
                    $profile_reports_rights = $cls_user->get_page_rights(KEY_PROFILE_REPORTS);
                    if($profile_reports_rights){
                      foreach ($profile_reports_rights as $pr)
                      {
                  ?>
                    <div class="col-md-3">
                      <lable><?php echo $pr['page_name']; ?></lable>
                    </div>
                    <?php } } ?>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">&nbsp;</label>  
              <div class="checkbox col-md-9">
                <label>
                  <input type="checkbox" class="check_account" name="rights[]"<?php
                  if(array_key_exists(INVENTORY_ENTRY, $ary_user_rights) && ($ary_user_rights[INVENTORY_ENTRY] == 1)){ $val = 1;
                      echo " checked";
                  }else { $val = 2; }
                  ?> value="<?php echo KEY_INVENTORY_ENTRY;?>"> Inventory Entry
                </label>
                <br>
                <div class="inv_entry <?php if($val != 1){ echo 'box'; } ?>">
                  <?php 
                    $inventory_rights = $cls_user->get_page_rights(KEY_INVENTORY_ENTRY);
                    if($inventory_rights){
                      foreach ($inventory_rights as $pr){
                  ?>
                    <div class="col-md-3">
                      <lable><?php echo $pr['page_name']; ?></lable>
                    </div>
                    <?php } } ?>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">&nbsp;</label>
              <div class="checkbox col-md-9">
                <label>
                  <input type="checkbox" class="check_account" name="rights[]" value="<?php echo KEY_INVENTORY_REPORTS;?>"<?php
                  if(array_key_exists(INVENTORY_REPORTS, $ary_user_rights) && ($ary_user_rights[INVENTORY_REPORTS] == 1)){ $val = 1;
                      echo " checked";
                  }else { $val = 2; }
                  ?>> Inventory Reports
                </label>
                <br>
                <div class="inv_rprt <?php if($val != 1){ echo 'box'; } ?>">
                  <?php 
                    $inventory_reports_rights = $cls_user->get_page_rights(KEY_INVENTORY_REPORTS);
                    if($inventory_reports_rights){
                      foreach ($inventory_reports_rights as $pr)
                      {
                  ?>
                    <div class="col-md-3">
                      <lable><?php echo $pr['page_name']; ?></lable>
                    </div>
                    <?php } } ?>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">&nbsp;</label>  
              <div class="checkbox col-md-9">
                <label>
                  <input type="checkbox" class="check_account" name="rights[]" value="<?php echo KEY_REPORTS;?>"<?php if(array_key_exists(REPORTS, $ary_user_rights) && ($ary_user_rights[REPORTS] == 1)){ $val = 1;
                      echo " checked";
                  }else { $val = 2; }
                  ?>> Reports
                </label>
                <br>
                <div class="rprt <?php if($val != 1){ echo 'box'; } ?>">
                  <?php 
                    $reports_rights = $cls_user->get_page_rights(KEY_REPORTS);
                    if($reports_rights){
                      foreach ($reports_rights as $pr)
                      {
                  ?>
                    <div class="col-md-3">
                      <lable><?php echo $pr['page_name']; ?></lable>
                    </div>
                    <?php } } ?>
                </div>
              </div>
            </div>
            
            <div class="form-group">
              <label class="control-label col-md-2">&nbsp;</label>  
              <div class="checkbox col-md-9">
                <label>
                  <input type="checkbox" class="check_account" name="rights[]" value="<?php echo KEY_IDARAH_REPORTS;?>"<?php if(array_key_exists(IDARAH_REPORTS, $ary_user_rights) && ($ary_user_rights[IDARAH_REPORTS] == 1)){ $val = 1;
                      echo " checked";
                  }else { $val = 2; }
                  ?>> Idarah Reports
                </label>
                <br>
                <div class="ida_rprt <?php if($val != 1){ echo 'box'; } ?>">
                  <?php 
                    $reports_rights = $cls_user->get_page_rights(KEY_IDARAH_REPORTS);
                    if($reports_rights){
                      foreach ($reports_rights as $pr)
                      {
                  ?>
                    <div class="col-md-3">
                      <lable><?php echo $pr['page_name']; ?></lable>
                    </div>
                    <?php } } ?>
                </div>
              </div>
            </div>
            
            <div class="form-group">
              <label class="control-label col-md-2">&nbsp;</label>  
              <div class="checkbox col-md-9">
                <label>
                  <input type="checkbox" class="check_account" name="rights[]" value="<?php echo KEY_HUB_PENDING_REPORTS;?>"<?php if(array_key_exists(HUB_PENDING_REPORTS, $ary_user_rights) && ($ary_user_rights[HUB_PENDING_REPORTS] == 1)){ $val = 1;
                      echo " checked";
                  }else { $val = 2; }
                  ?>> Hub Pending Reports
                </label>
                <br>
                <div class="hub_rprt <?php if($val != 1){ echo 'box'; } ?>">
                  <?php 
                    $reports_rights = $cls_user->get_page_rights(KEY_HUB_PENDING_REPORTS);
                    if($reports_rights){
                      foreach ($reports_rights as $pr)
                      {
                  ?>
                    <div class="col-md-3">
                      <lable><?php echo $pr['page_name']; ?></lable>
                    </div>
                    <?php } } ?>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">&nbsp;</label>  
              <div class="checkbox col-md-9">
                <label>
                  <input type="checkbox" class="check_account" name="rights[]" value="<?php echo KEY_TAKHMEEN;?>" <?php if(array_key_exists(TAKHMEEN, $ary_user_rights) && ($ary_user_rights[TAKHMEEN] == 1)){ $val = 1;
                      echo " checked";
                  }else { $val = 2; }
                  ?>> Takhmeen
                </label>
                <br>
                <div class="tkhmn <?php if($val != 1){ echo 'box'; } ?>">
                  <?php 
                    $reports_rights = $cls_user->get_page_rights(KEY_TAKHMEEN);
                    if($reports_rights){
                      foreach ($reports_rights as $pr)
                      {
                  ?>
                    <div class="col-md-3">
                      <lable><?php echo $pr['page_name']; ?></lable>
                    </div>
                    <?php } } ?>
                </div>
              </div>
            </div>
          <!-- new -->
          <?php
          function check_exist($key) {
            global $ary_user_rights;
            if(array_key_exists($key, $ary_user_rights) && ($ary_user_rights[$key] == 1))
                    echo 'checked';
            else echo '';
          }
          ?>
          <!-- new -->

            <?php } ?>
            <div class="form-group">
              <label class="col-md-2 control-label">&nbsp;</label>
              <div class="col-md-4">
                <input type="submit" id="Update_user" name="Update_user" value="Update" class="btn btn-success">
              </div>
            </div>
          </form>
        </div>
        <!-- /Center Bar -->
        <script>
          var right = true; //if old password is right than right = true else right = false;
          $('#Update_user').click(function() {
            var name = $('#name').val();
            var email = $('#email').val();
            var mobile = $('#mobile').val();
            //var old_pwd = $('#Old_password').val();
            var new_pwd = $('#New_password').val();
            var confirm_pwd = $('#Confirm_password').val();
            var error = 'Following error(s) are occurred\n\n';
            var validate = true;
            if (name == '')
            {
              error += 'Please enter User name\n';
              validate = false;
            }
            if (email == '')
            {
              error += 'Please enter email\n';
              validate = false;
            }
            if (mobile == '')
            {
              error += 'Please enter mobile\n';
              validate = false;
            }
            
            if(new_pwd == ''){
                error += 'Please enter new password\n';
                validate = false;
            } else if(confirm_pwd == ''){
                error += 'Please enter confirm password\n';
                validate = false;
            } else if(new_pwd != confirm_pwd){
                error += 'New password and Confirm password are not same!\n';
                validate = false;
            }
            
            if (validate == false)
            {
              alert(error);
              return validate;
            }

          });
          $('#FileNo').keyup(function() {
            var FileNo = $(this).val().toUpperCase();
            $(this).val(FileNo);
          });

          /*$('#Old_password').change(function() {
            var old_pwd = $('#Old_password').val();
            var id = <?php echo $_SESSION[USER_ID];?>;
            if(old_pwd != ''){ 
            $.ajax({
              url: "ajax.php",
              type: "GET",
              data: 'old_pwd=' + old_pwd + '&cmd=check_old_password&id='+id,
              success: function(data)
              { // 1 success
                if(data != '1'){
                  alert('Old password is not correct');
                  $('#Old_password').focus();
                  right = false;
                  $('#old').attr('class', 'form-group has-error');
                  $('#op').val('1');
                  return false;
                } else {
                  right = true;
                  $('#new_password').focus();
                  $('#old').attr('class', 'form-group has-success');
                  return true;
                }
              }
            });
            }
          });
  
          $(document).ready(function(){
            $('#Old_password').focus(function(){
              $(this).select();
            });
          });*/
          
          $(document).ready(function(){
              $('input[type="checkbox"]').click(function(){
                  if($(this).attr("value")=="<?php echo KEY_ACCOUNTS_ENTRY; ?>"){
                      $(".acc_entry").toggle();
                  }
                  if($(this).attr("value")=="<?php echo KEY_ACCOUNTS_REPORTS; ?>"){
                      $(".acc_rprt").toggle();
                  }
                  if($(this).attr("value")=="<?php echo KEY_ACCOUNTS_BALANCESHEET; ?>"){
                      $(".acc_blncst").toggle();
                  }
                  if($(this).attr("value")=="<?php echo KEY_PROFILE_ENTRY; ?>"){
                      $(".pro_entry").toggle();
                  }
                  if($(this).attr("value")=="<?php echo KEY_PROFILE_REPORTS; ?>"){
                      $(".pro_rprt").toggle();
                  }
                  if($(this).attr("value")=="<?php echo KEY_INVENTORY_ENTRY; ?>"){
                      $(".inv_entry").toggle();
                  }
                  if($(this).attr("value")=="<?php echo KEY_INVENTORY_REPORTS; ?>"){
                      $(".inv_rprt").toggle();
                  }
                  if($(this).attr("value")=="<?php echo KEY_REPORTS; ?>"){
                      $(".rprt").toggle();
                  }
                  if($(this).attr("value")=="<?php echo KEY_IDARAH_REPORTS; ?>"){
                      $(".ida_rprt").toggle();
                  }
                  if($(this).attr("value")=="<?php echo KEY_HUB_PENDING_REPORTS; ?>"){
                      $(".hub_rprt").toggle();
                  }
                  if($(this).attr("value")=="<?php echo KEY_TAKHMEEN; ?>"){
                      $(".tkhmn").toggle();
                  }
              });
          });
        </script>
        <style>
        .box{ display: none; }
        </style>

      </div>
      <!-- /Content -->
    </section>
  </div>
  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>
