<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.product.php');
require_once('classes/class.family.php');
require_once('classes/class.menu.php');
require_once('classes/class.billbook.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();
$cls_menu = new Mtx_Menu();
$cls_product = new Mtx_Product();
$cls_billbook = new Mtx_BillBook();
$gblIssue = 0;
$gblDirect = 0;
$gblCharges = 0;
$user_id = $_SESSION[USER_ID];
$post = FALSE;

if (isset($_POST['finalize'])) {
  $post = TRUE;
  $date = $_POST['date'];

  $finalize = $cls_product->is_date_finalized($date);

  if (!$finalize) {
    $finalize = $cls_product->finalize_inventory_issue($date, $user_id);
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Report already finalized';
  }
  $dt = explode('-', $date);
  $start = mktime(0, 0, 0, $dt[1], $dt[2], $dt[0]);
  $end = mktime(23, 59, 59, $dt[1], $dt[2], $dt[0]);
  $dmenu = $cls_menu->get_all_daily_menu($start, $end);
  $items = $cls_product->get_issued_item_in_current_day($date);
  $purchases = $cls_product->get_issued_direct_purchase($date);
  $all_tiffin = $cls_family->get_all_tiffin_size($date);
  $charges = $cls_product->get_other_charges($date);
}

if (isset($_POST['search']) OR ( isset($_GET['date']) && $_GET['cmd'] == 'daily_menu')) {
  $post = TRUE;
  if (isset($_POST['search']))
    $date = $_POST['date'];
  if (isset($_GET['date']) && $_GET['cmd'] == 'daily_menu')
    $date = $_GET['date'];
  $dt = explode('-', $date);
  $start = mktime(0, 0, 0, $dt[1], $dt[2], $dt[0]);
  $end = mktime(23, 59, 59, $dt[1], $dt[2], $dt[0]);
  $items = $cls_product->get_issued_item_in_current_day($date);
  $dmenu = $cls_menu->get_all_daily_menu($start, $end);
  $purchases = $cls_product->get_issued_direct_purchase($date);
  $all_tiffin = $cls_family->get_all_tiffin_size($date);
  $finalize = $cls_product->is_date_finalized($date);
  $charges = $cls_product->get_other_charges($date);
  $_SESSION['items'] = $items;
  $_SESSION['direct_purchases'] = $purchases;
}

//print_r($items);

if (isset($_POST['csv_download'])) {
  csv_download($_SESSION['items'], $_SESSION['direct_purchases'], 'inventory_issue_report.csv');
  exit();
}

function csv_download($items, $purchases, $filename) {
  // output headers so that the file is downloaded rather than displayed
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=' . $filename);

// create a file pointer connected to the output stream
  $output = fopen('php://output', 'w');

// output the column headings
  fputcsv($output, array('No', 'Name', 'Quantity', 'Unit', 'Amount'));

// fetch the data
// loop over the rows, outputting them
  if ($items) {
    $c = 1;
    foreach ($items as $row) {
      fputcsv($output, array($c++, $row['name'], number_format($row['quantity'], 3), $row['unit'], 'Rs. ' . number_format($row['act_cost'], 2)));
    }
  }
  
  if ($purchases) {
    $p = 1;
    foreach ($purchases as $data) {
      $amount_direct = $data['quantity'] * $data['unit'];
      fputcsv($output, array($p++, $data['item_name'], number_format($data['quantity'], 3), number_format($data['unit'], 2), 'Rs. ' . number_format($amount_direct, 2)));
    }
  }
  
}

$title = 'Inventory issue report';
$active_page = 'report';

require_once 'includes/header.php';

$page_number = INVENTORY_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
<?php
include 'includes/inc_left.php';
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?php echo $title; ?></h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Reports</a></li>
      <li><a href="#">Inventory</a></li>
      <li class="active"><?php echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12">&nbsp;</div>

      <!-- Center Bar -->
      <div class="col-md-12 ">
        <form method="post" role="form" class="form-horizontal">
          <div></div>
          <div class="form-group">
            <label class="col-md-3 control-label">From</label>
            <div class="col-md-4">
              <input type="date" name="date" class="form-control" id="date" placeholder="Date of Birth" value="<?php echo (isset($_REQUEST) && !empty($_REQUEST)) ? $_REQUEST['date'] : FALSE; ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3">&nbsp;</label>
            <div class="col-md-4">
              <input type="submit" class="btn btn-success" name="search" id="search" value="Search">
              <a target="_blank" href="print_inventory_issue_report.php?date=<?php
              if (isset($_REQUEST['search']) OR isset($_GET['date']))
                echo $_REQUEST['date'];
              else
                echo '';
              ?>" class="btn btn-primary <?php echo!$post ? 'disabled' : ''; ?>">Print</a>
              <input type="submit" class="btn btn-success" name="csv_download" id="csv_download" value="CSV Download">
            </div>
          </div>
        </form>
        <script>
          $('#search, #print').click(function () {
            var from_date = $('#date').val();
            if (from_date == '')
            {
              alert('Please select date first..')
              return false;
            }
          });
        </script>
        <div class="col-md-12">&nbsp;</div>
        <?php
        if (isset($_REQUEST['search']) OR isset($_POST['finalize']) OR isset($_REQUEST['date'])) {
          ?>
          <div class="col-md-8 alert-info" style="padding: 6px; border-radius: 8px;border-color: #bce8f1;">
            <strong>Menu Name: </strong><?php echo $cls_menu->get_base_menu_name($dmenu[0]['menu_id']); ?>
          </div>
          <?php
          if (!$finalize) {
            ?>
            <form method="post">
              <div class="pull-right">
                <input type="hidden" name="date" value="<?php echo (isset($_REQUEST) && !empty($_REQUEST)) ? $_REQUEST['date'] : FALSE; ?>">
                <input type="submit" name="finalize" id="finalize" value="Finalize" class="btn btn-success">
              </div>
            </form>
          <?php } else { ?>
            <div class="col-md-4 alert-success text-center" style="padding: 6px; border-radius: 8px;border-color: #bce8f1;">
              <span><span class="glyphicon glyphicon-ok"></span>&nbsp;<strong>Report Finalized</strong></span>
            </div>
          <?php } ?>
          <div class="col-md-12">&nbsp;</div>
          <div class="clearfix"></div>
          <div class="col-md-12">

            <table class="table table-hover table-condensed table-bordered">
              <thead>
                <tr>
                  <th colspan="5">Inventory Issue</th>
                </tr>
                <tr>
                  <th>No.</th>
                  <th>Name</th>
                  <th class="text-right">Quantity</th>
                  <th class="text-right">Unit</th>
                  <th class="text-right">Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i = 1;
                if ($items) {
                  foreach ($items as $item) {
                    ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><a href="item_history.php?item=<?php echo $item['inventory_id']; ?>" target="_blank"><?php echo $item['name']; ?></a></td>
                      <td class="text-right"><?php echo number_format($item['quantity'], 3); ?></td>
                      <td class="text-right"><?php echo $item['unit']; ?></td>
                      <td class="text-right"><?php
                        echo 'Rs. ' . number_format($item['act_cost'], 2);
                        $gblIssue += $item['act_cost'];
                        ?></td>
                    </tr>
                  <?php }
                  ?>
                  <tr>
                    <td colspan="4"><strong>Total:</strong></td>
                    <td class="text-right"><?php echo 'Rs. ' . number_format($gblIssue, 2); ?></td>
                  </tr>
                <?php } else {
                  ?>
                  <tr>
                    <td colspan="5" class="alert-danger">No results found</td>
                  </tr>
  <?php } ?>
              </tbody>
            </table>
          </div>
          <div class="col-md-12">
            <table class="table table-hover table-condensed table-bordered">
              <thead>
                <tr>
                  <th colspan="6">Direct Purchase</th>
                </tr>
                <tr>
                  <th>Sr. No.</th>
                  <th>Id</th>
                  <th>Item Name</th>
                  <th class="text-right">Quantity</th>
                  <th class="text-right">Unit</th>
                  <th class="text-right">Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $k = 1;
                if ($purchases) {

                  foreach ($purchases as $purchase) {
                    ?>
                    <tr>
                      <td><?php echo $k++; ?></th>
                      <td><a target="_blank" href="upd_bill_details.php?cmd=show_direct&bid=<?php echo $purchase['id']; ?>"><?php echo $purchase['id']; ?></a></td>
                      <td><?php echo $purchase['item_name']; ?></td>
                      <td class="text-right"><?php echo number_format($purchase['quantity'], 3); ?></td>
                      <td class="text-right"><?php echo number_format($purchase['unit'], 2); ?></td>
                      <td class="text-right"><?php
                        $sum_direct = $purchase['quantity'] * $purchase['unit'];
                        echo 'Rs. ' . number_format($sum_direct, 2);
                        $gblDirect += $sum_direct;
                        ?></td>
                    </tr>
                  <?php }
                  ?>
                  <tr>
                    <td colspan="5"><font style="text-align: right;font-weight: bold">Total:</font></td>
                    <td class="text-right"><?php echo 'Rs. ' . number_format($gblDirect, 2); ?></td>
                  </tr>
                <?php } else {
                  ?>
                  <tr>
                    <td colspan="6" class="alert-danger">No results found</td>
                  </tr>
  <?php } ?>
              </tbody>
            </table>
          </div>
          <div class="col-md-12">
            <table class="table table-hover table-condensed table-bordered">
              <thead>
                <tr>
                  <td colspan="4">Other Charges</td>
                </tr>
                <tr>
                  <th>Sr. No</th>
                  <th>Id</th>
                  <th>Shop Name</th>
                  <th class="text-right">Other charges</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $number = 1;

                if ($charges) {
                  foreach ($charges as $charge) {
                    $gblCharges += $charge['other_charges'];
                    $shop_name = $cls_billbook->get_shops($charge['name']);
                    ?>
                    <tr>
                      <td><?php echo $number++; ?></td>
                      <td><a target="_blank" href="upd_bill_details.php?cmd=show_direct&bid=<?php echo $charge['id']; ?>"><?php echo $charge['id']; ?></a></td>
                      <td><?php echo $shop_name['ShopName']; ?></td>
                      <td class="text-right"><?php echo 'Rs. ' . $charge['other_charges']; ?></td>
                    </tr>
                  <?php } ?>
                <?php } else {
                  ?>
                  <tr>
                    <td colspan="4" class="alert-danger">No results found</td>
                  </tr>
  <?php } ?>
                <tr>
                  <td colspan="3"><strong>Grand Total (Inventory issue + Direct Purchase + Other charges):</strong></td>
                  <td class="text-right"><?php echo 'Rs. ' . number_format($gblDirect + $gblIssue + $gblCharges, 2); ?></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="col-md-12">
            <table class="table table-hover table-condensed table-bordered">
              <thead>
                <tr>
                  <th>Tiffin Size</th>
                  <th>Count</th>
                </tr>
              </thead>
              <tbody>
  <?php foreach ($all_tiffin as $key => $value) { ?>
                  <tr>
                    <td><?php echo $key; ?></td>
                    <td><?php echo $value; ?></td>
                  </tr>
  <?php } ?>
              </tbody>
            </table>
          </div>
<?php } ?>
      </div>
      <!-- /Center Bar -->
    </div>
    <!-- /Content -->
  </section>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>
