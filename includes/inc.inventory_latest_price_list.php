<table id="example1" class="table table-bordered table-striped">
  <thead>
  <th>No.</th>
  <th>Item Name</th>
  <th class="text-right">Unit Price</th>
  <th class="text-right">Unit</th>
  <th>Category</th>
</thead>
<tbody>
  <?php
  if ($result) {
    $i = 1;
    foreach ($result as $ingredient) {
      ?>
      <tr>
        <td><?php echo $i++; ?></td>
        <td><a href="item_history.php?item=<?php echo $ingredient['id']; ?>" target="_blank"><?php echo $ingredient['name'] ?></a></td>
        <td class="text-right"><?php echo $ingredient['unit_price']; ?></td>
        <td class="text-right"><?php echo $ingredient['unit'] ?></td>
        <td><?php echo $ingredient['category']; ?></td>
      </tr>
    <?php }
    ?>
      <?php
  } else {
    ?>
    <tr>
      <td colspan="5" class="alert-danger">No items found.</td>
    </tr>
<?php } ?>
</tbody>
</table>
