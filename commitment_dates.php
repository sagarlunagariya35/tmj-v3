<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.family.php');
$cls_family = new Mtx_family();
$user_id = $_SESSION[USER_ID];

if (isset($_POST['Add'])) {
  $year = $_POST['takhmeen_year_amount'];
  $txt_date = $_POST['txt_date'];
  $txt_lable = $_POST['txt_lable'];
  
  $takh_commitment = $cls_family->add_takhmeen_commitment_dates($year, $txt_date, $txt_lable);
  if ($takh_commitment) {
    $_SESSION[SUCCESS_MESSAGE] = 'Takhmeen commitment dates added successfully.';
    header('Location: commitment_dates.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing the request..';
    header('Location: commitment_dates.php');
    exit();
  }
}
$title = 'Takhmeen Commitment Dates';
$active_page = 'family';

include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <form method="post" role="form" class="form-horizontal">
            <div class="col-md-5">
              <div></div>
              <div class="form-group">
                <label class="control-label col-md-3"><?php echo YEAR; ?></label>
                <div class="col-md-9">
                  <select class="form-control get_record" name="takhmeen_year_amount" id="takhmeen_year_amount">
                    <option value ="">-- Select One --</option>
                    <?php
                    for ($i = 1; $i <= $no_years; $i++) {
                      $year = (($i == 1) ? $default_year : ($default_year + ($i - 1))) . '-' . (substr($default_year, -2) + $i);
                      ?>
                      <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">Date</label>
                <div class="col-md-9">
                  <input type="date" name="txt_date" id="txt_date" class="form-control">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3">Lable</label>
                <div class="col-md-9">
                  <input type="text" name="txt_lable" id="txt_lable" class="form-control">
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">&nbsp;</label>
                <div class="col-md-4">
                  <input type="submit" id="update" name="Add" value="Save" class="btn btn-success">
                </div>
              </div>
         </form>

          <div class="clearfix"></div>
        </div>
        <!-- /Center Bar -->
      <script>
        var exist = false;
        $('#update').click(function() {
          var year = $('#takhmeen_year_amount').val();
          var txt_date = $('#txt_date').val();
          var txt_lable = $('#txt_lable').val();
          var errors = [];
          var key = 0;
          if (year === '')
            errors[key++] = '\u221A Takhmeen year';
          if (txt_date === '')
            errors[key++] = '\u221A Takhmeen date';
          if (txt_lable === '')
            errors[key++] = '\u221A Takhmeen date lable';
          if (errors.length) {
            alert('Please correct these fields:\n' + errors.join('\n'));
            return false;
          }
        });
      </script>
    </div>
  <!-- /Content -->
  </section>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>
