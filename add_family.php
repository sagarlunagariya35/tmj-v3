<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.menu.php');
$cls_menu = new Mtx_Menu();
$cls_family = new Mtx_family();
$user_id = $_SESSION[USER_ID];
if (isset($_POST['add_family'])) {
  $data = $database->clean_data($_POST);
  $cls_family->setCard(ucfirst($data['card']));
  $cls_family->setSabil_id($data['sabilid']);
  $year = date('Y') - $data['age'];
  $cls_family->setAge($year);
  $cls_family->setPrefix($data['prefix']);
  $cls_family->setFirstName($data['first']);
  $cls_family->setFatherPrefix($data['father_prefix']);
  $cls_family->setFatherName($data['father_name']);
  $cls_family->setSurname($data['surname']);
  $cls_family->setGender($data['gender']);
  $cls_family->setMohallah($data['mohallah']);
  $cls_family->setMarital(ucfirst($data['marital']));
  $cls_family->setAddress(ucwords($data['address']));
  $cls_family->setEmail($data['email']);
  $cls_family->setFcCode((int) $data['code']);
  $cls_family->setOccupation(ucfirst($data['occupation']));
  $cls_family->setEjamaat((int) $data['ejamat']);
  $cls_family->setPhone($data['res_phone']);
  $cls_family->setOff_phone($data['off_phone']);
  $cls_family->setMobile($data['mobile']);
  $cls_family->setAdults((int) $data['adults']);
  $cls_family->setChild((int) $data['children']);
  $cls_family->setDiabetic((int) $data['diabetic']);
  $cls_family->setDiabeticRoti((int) $data['roti']);
  $cls_family->setTiffin($data['tiffin']);
  $cls_family->set_payment_term((int) $data['term']);
  if ($data['scanning'])
    $data['scanning'] = 1;
  else
    $data['scanning'] = 0;
  $cls_family->set_scanning($data['scanning']);
  $result = $cls_family->add_new_family($user_id);
  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = 'Family Added Successfully';
    header('location:list_family.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Try again.';
    header('location:list_family.php');
    exit();
  }
}
$tiffin = $cls_menu->get_all_tiffin_size();
$mohallah = $cls_family->get_all_tanzeem();
$title = 'Add a new family';
$active_page = 'family';

include('includes/header.php');

$page_number = PROFILE_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profiles</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <style>
        .form-group.required .control-label:after {
          content:" *";color:red;
        }
      </style>

      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div class="col-md-6"><div></div>
              <div class="form-group required" id="sabil">
                <label class="control-label col-md-5"><?php echo THALI_ID; ?></label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="sabilid" id="sabilid">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">Card Status</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="card" id="card">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">Age</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="age" id="age">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-5 ">Prefix</label>
                <div class="col-md-7">
                  <select name="prefix" id="prefix" class="form-control">
                    <option value="">None</option>
                    <option value="Mulla">Mulla</option>
                    <option value="Shaik">Shaik</option>
                  </select>
                </div>
              </div>
              <div class="form-group required">
                <label class="control-label col-md-5">First Name</label>
                <div class="col-md-7">
                  <select class="form-control" name="first" id="first">
                    <option value="">--Select First Name</option>
                    <?php include('includes/inc.male.php'); ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">Father's Prefix</label>
                <div class="col-md-7">
                  <select name="father_prefix" id="father_prefix" class="form-control">
                    <option value="">None</option>
                    <option value="Mulla">Mulla</option>
                    <option value="Shaik">Shaik</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">Father Name</label>
                <div class="col-md-7">
                  <select class="form-control" name="father_name" id="father_name">
                    <option value="">--Select Father Name</option>
                    <?php include('namelist.txt'); ?>
                  </select>
                </div>
              </div>
              <div class="form-group required">
                <label class="control-label col-md-5">Surname</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="surname" id="surname">
                </div>
              </div>

              <div class="form-group required">
                <label class="control-label col-md-5">Gender</label>
                <div class="col-md-7">
                  <select id="gender" name="gender" id="gender" class="form-control">
                    <option value="">Select Gender</option>
                    <option value="M">Male</option>
                    <option value="F">Female</option>
                  </select>
                </div>
              </div>
              <div class="form-group required">
                <label class="control-label col-md-5">Mohallah</label>
                <div class="col-md-7">
                  <select name="mohallah" id="mohallah" class="form-control">
                    <option value="0">Select one</option>
                    <?php foreach ($mohallah as $name) { ?>
                      <option value="<?php echo $name['name']; ?>"><?php echo $name['name']; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">Marital Status</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="marital" id="marital">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">Address</label>
                <div class="col-md-7">
                  <textarea class="form-control" rows="5" name="address" id="address"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-5 control-label">Payment in Kisht</label>
                <div class="col-md-7">
                  <input class="form-control" name="term" id="term">
                </div>
              </div>
            </div>
            <!-- another-->
            <div class="col-md-6"><div></div>
              <div class="form-group">
                <label class="control-label col-md-5 ">Fc Code</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="code" id="code">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">Occupation</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="occupation" id="occupation">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">Email</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="email" id="email">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">E-jamaat Id</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="ejamat" id="ejamat">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">Res Phone</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="res_phone" id="res_phone">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">Off Phone</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="off_phone" id="off_phone">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">Mobile</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="mobile" id="mobile">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">No. of adults</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="adults" id="adults">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">No. of kids</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="children" id="children">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">No. of diabetic</label>
                <div class="col-md-7">
                  <input type="text" name="diabetic" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5 ">Diabetic Roti</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" name="roti" id="roti">
                </div>
              </div>
              <div class="form-group required">
                <label class="control-label col-md-5">Tiffin size</label>
                <div class="col-md-7">
                  <select class="form-control" name="tiffin" id="tiffin">
                    <option value="0">Select One</option>
                    <?php foreach ($tiffin as $size) { ?>
                      <option value="<?php echo $size['size']; ?>"><?php echo $size['size']; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-5">Show in scanning</label>
                <div class="col-md-7">
                  <input type="checkbox" name="scanning" id="scanning" checked>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="pull-right">
              <button name="add_family" id="add_family" type="submit" class="btn btn-success">Add</button>
              <input type="hidden" value="" name="exist" id="exist">
              <a href="list_family.php" class="btn btn-info">Home</a>

            </div>
          </form>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>
<script>
  var opt = '<option value="0">--Select First Name--</option>';
  $('#gender').change(function() {
    var sex = $(this).val();
    var first = $('#first').val();
    if (sex == 'F') {
      $.ajax({
        url: 'ajax.php',
        type: 'GET',
        data: 'cmd=get_namelist_file&sex=' + sex,
        success: function(data) {
          if (first != '') {
            data += '<option value="' + first + '" selected>' + first + '</option>';
          }
          $('#first').html(opt + data);
        }
      });
    } else if (sex == 'M') {
      $.ajax({
        url: 'ajax.php',
        type: 'GET',
        data: 'cmd=get_namelist_file&sex=' + sex,
        success: function(data) {
          if (first != '') {
            data += '<option value="' + first + '" selected>' + first + '</option>';
          }
          $('#first').html(opt + data);
        }
      });
    }
  });
  $('#sabilid').keyup(function() {
    var sabilid = $(this).val().toUpperCase();
    $(this).val(sabilid);
  });
  $('#add_family').click(function() {
    //validate
    var sabil = $('#sabilid').val();
    var first = $('#first').val();
    var surname = $('#surname').val();
    var gender = $('#gender').val();
    var mohallah = $('#mohallah').val();
    var tiffin = $('#tiffin').val();
    var exist = $('#exist').val();
    var error = 'Following error(s) are occurred\n';
    var validate = true;

    if (sabil == '')
    {
      error += 'Please enter Family id\n';
      validate = false;
    }
    if (exist == '1')
    {
      error += 'Thali ID already exist\n';
      validate = false;
    }
    if (first == '')
    {
      error += 'Please select first name\n';
      validate = false;
    }
    if (surname == '')
    {
      error += 'Please enter surname\n';
      validate = false;
    }
    if (gender == '')
    {
      error += 'Please select gender\n';
      validate = false;
    }
    if (mohallah == '0')
    {
      error += 'Please enter mohallah\n';
      validate = false;
    }
    if (tiffin == '0')
    {
      error += 'Please select tiffin size\n';
      validate = false;
    }
    if (validate == false)
    {
      alert(error);
      return validate;
    }

  });
  $('#sabilid').change(function() {
    var thali = $(this).val();
    $.ajax({
      url: "ajax.php",
      type: "GET",
      data: 'thali=' + thali + '&cmd=check_thali_ID_exist',
      success: function(data)
      {
        if (data == '1') {
          alert('Thali ID already exists');
          $('#sabil').attr('class', 'form-group has-error');
          $('#sabilid').focus();
          $('#exist').val('1');
          return false;
        } else {
          $('#exist').val('0');
          $('#sabil').attr('class', 'form-group has-success');
          return true;
        }
      }
    });
  });

</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
  include 'includes/footer.php';
?>