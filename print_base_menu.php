<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.product.php');
require_once('classes/class.menu.php');
$cls_product = new Mtx_Product();

if (isset($_GET['mid']))
  $menu_id = (int) trim($_GET['mid']);
else
  $menu_id = FALSE;

$cls_menu = new Mtx_Menu();

$person_count = $cls_menu->get_base_menu_person_count($menu_id);
$menu_name = $cls_menu->get_base_menu_name($menu_id);
$category_id = $cls_menu->get_base_menu_category($menu_id);


if(isset($_POST['updateForm'])) {
  $data = $database->clean_data($_POST);
  
  $menuName = $data['menu_name'];
  $category_id = $data['category'];
  $person_count = $data['count'];
  $baseInvItem = (array) $data['base_inv'];
  $baseInvQty = (array) $data['baseInvQty'];
  $baseDirQty = (array) $data['baseDirQty'];
  $baseDirItem = (array) $data['directItems'];
  $isUpdated = $cls_menu->update_BaseMenuForm($menuName, $category_id, $person_count, $baseInvItem, $baseInvQty, $baseDirItem, $baseDirQty, $menu_id);
  if($isUpdated) {
    $_SESSION[SUCCESS_MESSAGE] = 'Base menu updated successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while updating the records.';
  }
}

$categories = $cls_menu->get_all_category();
$base_direct = $cls_menu->get_all_base_direct_items($menu_id, $person_count);
$base_inv = $cls_menu->get_all_base_inv_issue($menu_id, $person_count);
$direct_items = $cls_product->get_all_direct_items();
$items = $cls_product->get_all_ingredients();
$title = 'Base Menu';
$active_page = 'menu';

?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <style type="text/css">
      @media all {
        body { font-size: 16px; }
        table { page-break-inside:auto; }
        tr    { page-break-inside:avoid; page-break-after:auto; }
        thead { display:table-header-group; }
        tfoot { display:table-footer-group; }
      }
    </style>
  </head>
  <body onload="window.print()">
    <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->
    <!-- Content -->
  <!-- Center Bar -->
  <div class="col-md-8">
      <div class="col-md-12">
        <div class="col-md-4">
          <div class="form-group">
            <label class="control-label col-md-3">Menu</label>
            <div class="col-md-9">
              <span class="form-control-static"><?php echo $menu_name ? $menu_name : ''; ?></span>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="control-label col-md-4">Category</label>
            <div class="col-md-8">
              <span class="form-control-static"><?php echo $cls_menu->get_all_category($category_id); ?></span>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="control-label col-md-3">Person</label>
            <div class="col-md-4">
              <span class="form-control-static"><?php echo $person_count ? $person_count : ''; ?></span>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">&nbsp;</div>
      
        <div class="col-md-6">
          <span><h3>Inventory Issue</h3></span>
          
          
          <table class="table table-bordered table-condensed table-hover">
            <thead>
              <tr>
                <th>No.</th>
                <th>Item Name</th>
                <th class="text-right">Quantity</th>
              </tr>
            </thead>
            <tbody id="display_issue">
              <?php
              if ($base_inv) {
                $i = 1;
                foreach ($base_inv as $item) {
                  ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><span class="form-control-static"><?php
                    $query = "SELECT name FROM `inventory` WHERE `id` = '$item[item_id]'";
                    $result = $database->query_fetch_full_result($query);
                    echo ucwords(strtolower($result[0]['name']));
                    ?></span></td>
                    <td class="text-right"><span class="form-control-static"><?php echo number_format($item['qty'] * $person_count, 3); ?></span></td>
                  </tr>
                  <?php
                }
              } else { ?>
                <tr>
                  <td class="alert-danger" colspan="3">Sorry! No items found.</td>
                </tr>
              <?php } ?>
              <?php
//              echo $base_inv;
              ?>
            </tbody>
          </table>
        </div>
        <div class="col-md-6">
          <span><h3>Direct Purchase</h3></span>
          <table class="table table-bordered table-condensed table-hover">
            <thead>
              <tr>
                <th>No.</th>
                <th>Item Name</th>
                <th class="text-right">Quantity</th>
              </tr>
            </thead>
            <tbody id="display_direct">
              <?php
              if ($base_direct) {
                $no = 1;
                foreach ($base_direct as $dirItem) {
                  ?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><span class="form-control-static"><?php echo $dirItem['item']; ?>
                    </td>
                    <td class="text-right"><span class="form-control-static"><?php echo number_format($dirItem['qty'] * $person_count, 3); ?></td>
                  </tr>
                  <?php }
              } else { ?>
                <tr>
                  <td class="alert-danger" colspan="3">Sorry! No items found.</td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
  </div>

<!-- /Center Bar -->
</div>
<!-- /Content -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
