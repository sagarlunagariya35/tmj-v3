<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/class.family.php');
require_once('classes/hijri_cal.php');
require_once('classes/class.user.php');
$cls_receipt = new Mtx_Receipt();
$cls_family = new Mtx_family();
$cls_user = new Mtx_User();

$from_date = $to_date = date('Y-m-d');
$tanzeem = FALSE;
if (isset($_POST['search'])) {
  $from_date = $_POST['from_date'];
  $to_date = $_POST['to_date'];
  $tanzeem = $_POST['tanzeem'];
}

// Get income details by date
// Income tables
// 1. credit_voucher
// 2. fmb_receipt_hub
// 3. receipt
$credit_vouchers = $cls_receipt->get_by_date_credit_vouchers($from_date, $to_date, $tanzeem);
$hub_receipts = $cls_receipt->get_by_date_hub_receipts($from_date, $to_date, $tanzeem);
$voln_receipts = $cls_receipt->get_by_date_voln_receipts($from_date, $to_date, $tanzeem);

// Get expence details by date
// Expence tables
// 1. account_bills
// 2. debit_voucher
// 3. direct_purchase
$acct_bills = $cls_receipt->get_by_date_acct_bills($from_date, $to_date, $tanzeem);
$debit_vouchers = $cls_receipt->get_by_date_debit_vouchers($from_date, $to_date, $tanzeem);
$direct_purchases = $cls_receipt->get_by_date_direct_purchases($from_date, $to_date, $tanzeem);

$total_income = 0;
$total_expense = 0;

$mohallah = $cls_user->get_single_user($_SESSION[USERNAME]);
$mohallahs = $cls_family->get_all_tanzeem();
$title = 'Transactions by date';
$active_page = 'report';
include('includes/header.php');

$page_number = ACCOUNTS_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Hub</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <div class="col-md-12">
              <label class="col-md-1 control-label">From</label>
              <div class="col-md-2">
                <input type="date" name="from_date" class="form-control" id="from_date" value="<?php echo $from_date; ?>">
              </div>
              <label class="col-md-1 control-label">To</label>
              <div class="col-md-2">
                <input type="date" name="to_date" class="form-control" id="to_date" value="<?php echo $to_date; ?>">
              </div>
              <label class="col-md-2 control-label">Tanzeem</label>
              <div class="col-md-2">
                <?php if($mohallah['Mohallah'] != '') { ?>
                <p class="form-control static"><?php echo $mohallah['Mohallah']; ?></p>
                <input type="hidden" name="tanzeem" value="<?php echo $mohallah['Mohallah']; ?>">
                <?php }else { ?>
                <select class="form-control" name="tanzeem" id="tanzeem">
                  <option value="">All</option>
                  <?php
                  foreach ($mohallahs as $mh) {
                    ?>
                    <option value="<?php echo $mh['name'] ?>" <?php echo (isset($tanzeem) && $tanzeem == $mh['name']) ? 'selected' : ''; ?>><?php echo $mh['name'] ?></option>
                  <?php } ?>
                </select>
                <?php } ?>
              </div>

              <input type="submit" class="btn btn-success validate" name="search" id="search" value="Search">
              <a href="print_by_date_finance.php?from=<?php echo $from_date; ?>&to=<?php echo $to_date; ?>&tanzeem=<?php echo $tanzeem; ?>" target="blank" class="btn btn-primary validate" id="print">Print</a>
            </div>
          </form>
          <div class="col-md-12">&nbsp;</div>
          <?php
          // =================  Credits  ====================== //
          // Cerdit Vouchers
          if ($credit_vouchers['cash_credit_vouchers']) {
            ?>
            <h3>Credit Vouchers (Cash)</h3>
            <table class="table table-condensed table-bordered table-responsive">
              <thead>
                <tr>
                  <th>Rcpt. ID</th>
                  <th>Account</th>
                  <th>Desc.</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($credit_vouchers['cash_credit_vouchers'] as $cr_voucher) {
                  ?>
                  <tr>
                    <td><a href="update_voucher.php?cmd=credit&cid=<?php echo $cr_voucher['id']; ?>" target="_blank"><?php echo $cr_voucher['id']; ?></a></td>
                    <td><?php echo $cr_voucher['acct_heads']; ?></td>
                    <td><?php echo $cr_voucher['description']; ?></td>
                    <td><?php echo $cr_voucher['amount']; ?></td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <td colspan="3" class="text-right"><strong>Total:</strong></td>
                  <td><strong><?php echo $credit_vouchers['cash_credit_vouchers_total']; ?></strong></td>
                </tr>
              </tbody>
            </table>
            <?php
            $total_income += $credit_vouchers['cash_credit_vouchers_total'];
          }

          if ($credit_vouchers['cheque_credit_vouchers']) {
            ?>
            <h3>Credit Vouchers (Cheque)</h3>
            <table class="table table-condensed table-bordered table-responsive">
              <thead>
                <tr>
                  <th>Rcpt. ID</th>
                  <th>Account</th>
                  <th>Desc.</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($credit_vouchers['cheque_credit_vouchers'] as $cr_voucher) {
                  ?>
                  <tr>
                    <td><a href="update_voucher.php?cmd=credit&cid=<?php echo $cr_voucher['id']; ?>" target="_blank"><?php echo $cr_voucher['id']; ?></a></td>
                    <td><?php echo $cr_voucher['acct_heads']; ?></td>
                    <td><?php echo $cr_voucher['description']; ?></td>
                    <td><?php echo $cr_voucher['amount']; ?></td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <td colspan="3" class="text-right"><strong>Total:</strong></td>
                  <td><strong><?php echo $credit_vouchers['cheque_credit_vouchers_totals']; ?></strong></td>
                </tr>
              </tbody>
            </table>
            <?php
            $total_income += $credit_vouchers['cheque_credit_vouchers_totals'];
          }

          // =================  Credits  ====================== //
          // FMB Hub receipts
          if ($hub_receipts['cash_receipts']) {
            ?>
            <h3>Hub Receipts (Cash)</h3>
            <table class="table table-condensed table-bordered table-responsive">
              <thead>
                <tr>
                  <th>Rcpt. ID</th>
                  <th>Thaali ID</th>
                  <th>Name</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($hub_receipts['cash_receipts'] as $receipt) {
                  ?>
                  <tr>
                    <td><a href="print_hub_receipt.php?id=<?php echo $receipt['id']; ?>" target="_blank"><?php echo $receipt['id']; ?></a></td>
                    <td><?php echo $receipt['FileNo']; ?></td>
                    <td><?php echo $receipt['name']; ?></td>
                    <td><?php echo $receipt['amount']; ?></td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <td colspan="3" class="text-right"><strong>Total:</strong></td>
                  <td><strong><?php echo $hub_receipts['cash_receipts_total']; ?></strong></td>
                </tr>
              </tbody>
            </table>
            <?php
            $total_income += $hub_receipts['cash_receipts_total'];
          }

          if ($hub_receipts['cheque_receipts']) {
            ?>
            <h3>Hub Receipts (Cheque)</h3>
            <table class="table table-condensed table-bordered table-responsive">
              <thead>
                <tr>
                  <th>Rcpt. ID</th>
                  <th>Thaali ID</th>
                  <th>Name</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($hub_receipts['cheque_receipts'] as $receipt) {
                  ?>
                  <tr>
                    <td><a href="print_hub_receipt.php?id=<?php echo $receipt['id']; ?>" target="_blank"><?php echo $receipt['id']; ?></a></td>
                    <td><?php echo $receipt['FileNo']; ?></td>
                    <td><?php echo $receipt['name']; ?></td>
                    <td><?php echo $receipt['amount']; ?></td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <td colspan="3" class="text-right"><strong>Total:</strong></td>
                  <td><strong><?php echo $hub_receipts['cheque_receipts_total']; ?></strong></td>
                </tr>
              </tbody>
            </table>
            <?php
            $total_income += $hub_receipts['cheque_receipts_total'];
          }

          // =================  Credits  ====================== //
          // Vouluntary Contributions
          if ($voln_receipts['cash_voln_receipts']) {
            ?>
            <h3>Voln. Contributions (Cash)</h3>
            <table class="table table-condensed table-bordered table-responsive">
              <thead>
                <tr>
                  <th>Rcpt. ID</th>
                  <th>Thaali ID</th>
                  <th>Name</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($voln_receipts['cash_voln_receipts'] as $receipt) {
                  ?>
                  <tr>
                    <td><a href="print_voluntary_receipt.php?id=<?php echo $receipt['id']; ?>" target="_blank"><?php echo $receipt['id']; ?></a></td>
                    <td><?php echo $receipt['FileNo']; ?></td>
                    <td><?php echo $receipt['hubber_name']; ?></td>
                    <td><?php echo $receipt['amount']; ?></td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <td colspan="3" class="text-right"><strong>Total:</strong></td>
                  <td><strong><?php echo $voln_receipts['cash_voln_receipts_total']; ?></strong></td>
                </tr>
              </tbody>
            </table>
            <?php
            $total_income += $voln_receipts['cash_voln_receipts_total'];
          }

          if ($voln_receipts['cheque_voln_receipts']) {
            ?>
            <h3>Voln. Contributions (Cheque)</h3>
            <table class="table table-condensed table-bordered table-responsive">
              <thead>
                <tr>
                  <th>Rcpt. ID</th>
                  <th>Thaali ID</th>
                  <th>Name</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($voln_receipts['cheque_voln_receipts'] as $receipt) {
                  ?>
                  <tr>
                    <td><a href="print_voluntary_receipt.php?id=<?php echo $receipt['id']; ?>" target="_blank"><?php echo $receipt['id']; ?></a></td>
                    <td><?php echo $receipt['FileNo']; ?></td>
                    <td><?php echo $receipt['hubber_name']; ?></td>
                    <td><?php echo $receipt['amount']; ?></td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <td colspan="3" class="text-right"><strong>Total:</strong></td>
                  <td><strong><?php echo $voln_receipts['cheque_voln_receipts_total']; ?></strong></td>
                </tr>
              </tbody>
            </table>
            <?php
            $total_income += $voln_receipts['cheque_voln_receipts_total'];
          }

          // =================  Debits  ====================== //
          // Account Bills
          if ($acct_bills['cash_account_bills']) {
            ?>
            <h3>Bill Purchases (Cash)</h3>
            <table class="table table-condensed table-bordered table-responsive">
              <thead>
                <tr>
                  <th>Bill ID</th>
                  <th>Name</th>
                  <th>Desc.</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($acct_bills['cash_account_bills'] as $bill) {
                  ?>
                  <tr>
                    <td><a href="upd_bill_details.php?cmd=show_bills&bid=<?php echo $bill['id']; ?>" target="_blank"><?php echo $bill['id']; ?></a></td>
                    <td><?php echo $bill['ShopName']; ?></td>
                    <td><?php echo $bill['description']; ?></td>
                    <td><?php echo $bill['amount']; ?></td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <td colspan="3" class="text-right"><strong>Total:</strong></td>
                  <td><strong><?php echo $acct_bills['cash_account_bills_total']; ?></strong></td>
                </tr>
              </tbody>
            </table>
            <?php
            $total_expense += $acct_bills['cash_account_bills_total'];
          }

          if ($acct_bills['cheque_account_bills']) {
            ?>
            <h3>Bill Purchases (Cheque)</h3>
            <table class="table table-condensed table-bordered table-responsive">
              <thead>
                <tr>
                  <th>Bill ID</th>
                  <th>Name</th>
                  <th>Desc.</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($acct_bills['cheque_account_bills'] as $bill) {
                  ?>
                  <tr>
                    <td><a href="upd_bill_details.php?cmd=show_bills&bid=<?php echo $bill['id']; ?>" target="_blank"><?php echo $bill['id']; ?></a></td>
                    <td><?php echo $bill['ShopName']; ?></td>
                    <td><?php echo $bill['description']; ?></td>
                    <td><?php echo $bill['amount']; ?></td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <td colspan="3" class="text-right"><strong>Total:</strong></td>
                  <td><strong><?php echo $acct_bills['cheque_account_bills_totals']; ?></strong></td>
                </tr>
              </tbody>
            </table>
            <?php
            $total_expense += $acct_bills['cheque_account_bills_totals'];
          }

          // =================  Debits  ====================== //
          // Direct Purchases
          if ($direct_purchases['cash_direct_purchases']) {
            ?>
            <h3>Direct Purchases (Cash)</h3>
            <table class="table table-condensed table-bordered table-responsive">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Desc.</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($direct_purchases['cash_direct_purchases'] as $bill) {
                  ?>
                  <tr>
                    <td><a href="upd_bill_details.php?cmd=show_direct_bills&bid=<?php echo $bill['id']; ?>" target="_blank"><?php echo $bill['id']; ?></a></td>
                    <td><?php echo $bill['ShopName']; ?></td>
                    <td><?php echo $bill['description']; ?></td>
                    <td><?php echo $bill['amount']; ?></td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <td colspan="3" class="text-right"><strong>Total:</strong></td>
                  <td><strong><?php echo $direct_purchases['cash_direct_purchases_total']; ?></strong></td>
                </tr>
              </tbody>
            </table>
            <?php
            $total_expense += $direct_purchases['cash_direct_purchases_total'];
          }

          if ($direct_purchases['cheque_direct_purchases']) {
            ?>
            <h3>Direct Purchases (Cheque)</h3>
            <table class="table table-condensed table-bordered table-responsive">
              <thead>
                <tr>
                  <th>Bill ID</th>
                  <th>Name</th>
                  <th>Desc.</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($direct_purchases['cheque_direct_purchases'] as $bill) {
                  ?>
                  <tr>
                    <td><a href="upd_bill_details.php?cmd=show_direct_bills&bid=<?php echo $bill['id']; ?>" target="_blank"><?php echo $bill['id']; ?></a></td>
                    <td><?php echo $bill['ShopName']; ?></td>
                    <td><?php echo $bill['description']; ?></td>
                    <td><?php echo $bill['amount']; ?></td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <td colspan="3" class="text-right"><strong>Total:</strong></td>
                  <td><strong><?php echo $direct_purchases['cheque_direct_purchases_totals']; ?></strong></td>
                </tr>
              </tbody>
            </table>
            <?php
            $total_expense += $direct_purchases['cheque_direct_purchases_totals'];
          }

          // =================  Debits  ====================== //
          // Debit Vouchers
          if ($debit_vouchers['cash_debit_vouchers']) {
            ?>
            <h3>Debit Vouchers (Cash)</h3>
            <table class="table table-condensed table-bordered table-responsive">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Desc.</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($debit_vouchers['cash_debit_vouchers'] as $bill) {
                  ?>
                  <tr>
                    <td><a href="update_voucher.php?cmd=debit&did=<?php echo $bill['id']; ?>" target="_blank"><?php echo $bill['id']; ?></a></td>
                    <td><?php echo $bill['ShopName']; ?></td>
                    <td><?php echo $bill['description']; ?></td>
                    <td><?php echo $bill['amount']; ?></td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <td colspan="3" class="text-right"><strong>Total:</strong></td>
                  <td><strong><?php echo $debit_vouchers['cash_debit_vouchers_total']; ?></strong></td>
                </tr>
              </tbody>
            </table>
            <?php
            $total_expense += $debit_vouchers['cash_debit_vouchers_total'];
          }

          if ($debit_vouchers['cheque_debit_vouchers']) {
            ?>
            <h3>Debit Vouchers (Cheque)</h3>
            <table class="table table-condensed table-bordered table-responsive">
              <thead>
                <tr>
                  <th>Bill ID</th>
                  <th>Name</th>
                  <th>Desc.</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($debit_vouchers['cheque_debit_vouchers'] as $bill) {
                  ?>
                  <tr>
                    <td><a href="update_voucher.php?cmd=debit&did=<?php echo $bill['id']; ?>" target="_blank"><?php echo $bill['id']; ?></a></td>
                    <td><?php echo $bill['ShopName']; ?></td>
                    <td><?php echo $bill['description']; ?></td>
                    <td><?php echo $bill['amount']; ?></td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <td colspan="3" class="text-right"><strong>Total:</strong></td>
                  <td><strong><?php echo $debit_vouchers['cheque_debit_vouchers_totals']; ?></strong></td>
                </tr>
              </tbody>
            </table>
            <?php
            $total_expense += $debit_vouchers['cheque_debit_vouchers_totals'];
          }
          ?>
        </div>
        <div class="col-md-12">
            <strong>Total Income: Rs <?php echo number_format($total_income); ?></strong>
        </div>
        <div class="col-md-12">
            <strong>Total Expense: Rs <?php echo number_format($total_expense); ?></strong>
        </div>
        <div class="col-md-12">
            <strong>Balance: Rs <?php echo number_format($total_income - $total_expense); ?></strong>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
  
<script>
  $('.validate').click(function() {
    var from = $('#from_date').val();
    var to = $('#to_date').val();
    var errors = [];
    var key = 0;
    if (from === '')
      errors[key++] = 'from';
    if (to === '')
      errors[key++] = 'to';
    if (errors.length) {
      alert('Please select ' + errors.join(' & ') + ' date to proceed..');
      return false;
    }
  });
</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>