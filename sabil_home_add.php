<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.receipt.php");
require_once("classes/class.family.php");
$cls_receipt = new Mtx_Receipt();
$cls_family = new Mtx_family();

if (isset($_POST['add'])) {
  $result = $cls_receipt->add_last_year_month_sabil_home($_POST['file'], $_POST['dob'], $_POST['hub']);
}
if(isset($_GET['id']) && ($_GET['id'] != '')){
  $hub_raqam = $cls_family->get_last_year_month($_GET['id']);
}
$title = 'Add Sabil Home';
$active_page = 'account';

include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Credit</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <div class="col-md-3 pull-left">
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Search</h3></div>
            <div class="panel-body">
              <?php include('includes/search_bar.php'); ?>
            </div>
          </div>
        </div>
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-8">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <div class="form-group">
              <label class="control-label col-md-3">File No</label>
              <div class="col-md-4">
                <input type="text" class="form-control" name="file" id="file" value="<?php if(isset($_GET['id'])) echo $_GET['id'];?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Amount</label>
              <div class="col-md-4">
                <input type="text" class="form-control" name="hub" id="hub" value="<?php if(isset($_GET['id']) && ($hub_raqam['hub_raqam'] != 0)) echo $hub_raqam['hub_raqam'];  ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Last Date</label>
              <div class="col-md-4">
                <input type="date" name="dob" class="form-control" id="dob" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">&nbsp;</label>
              <button class="btn btn-success" type="submit" name="add" id="save">Save</button>
            </div>

          </form>
        </div>
        <!-- /Center Bar -->
      <script>
        $('#save').click(function() {
        var file = $('#file').val();
        var dob = $('#dob').val();
        var hub = $('#hub').val();
        var error = 'Whoops! following errors are occurred!\n';
        var validate = true;
        if (file == '')
        {
          error += 'Please enter thali ID\n';
          validate = false;
        }
        if (hub == '')
        {
          error += 'Please enter sabil home amount for 1 month\n';
          validate = false;
        }
        if(dob == '')
        {
          error += 'Please select last year and month';
          validate = false;
        }

        if (validate == false)
        {
          alert(error);
          return validate;
        }
        });
      </script>

      </div>
      <!-- /Content -->
    </section>
  </div>
  
<?php
include('includes/footer.php');
?>