<?php
include 'session.php';
require_once("classes/class.database.php");
require_once("classes/class.family.php");
$cls_family = new Mtx_family();

$year = $_GET['year'];
$tanzeem = $_GET['tanzeem'];
$cal = $cls_family->get_data_for_muasaat();
$data = $cls_family->get_FileNo('CLOSE_NOT_ALLOWED', $tanzeem);
$ary_File = array();
$i = 0;
foreach ($data as $f_id) {
  $thali_id = $f_id['FileNo'];
  $tfn_size = $f_id['tiffin_size'];
  $takh = $cls_family->get_takhmeen_record($thali_id, FALSE, " AND `year` = '$year'");
  $takh_amount = $takh[0]['amount'];
  $tfn_cost = $cls_family->get_tiffin_cost($year, $tfn_size);

  $cost = $tfn_cost['cost'] ? $tfn_cost['cost'] : 0;
  // Remove ids with tiffin size VC
  if (strtoupper($tfn_size) != 'VC') {
    $ary_File[$i]['FileNo'] = $thali_id;
    $ary_File[$i]['size'] = $tfn_size;
    $ary_File[$i]['takhmeen'] = $takh_amount;
    $ary_File[$i]['cost'] = $cost;
    $ary_File[$i++]['HOF'] = $f_id['HOF_NAME'];
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print calculated muasaat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8 ">

        <?php
        if (isset($ary_File) && (!empty($ary_File))) {

          $print = TRUE;
          include('includes/inc.cal_muasaat.php');
        }
        ?>
      </div>
      <!-- /Center Bar -->

    </div>
    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
