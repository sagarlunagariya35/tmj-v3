<?php

/*
 * =======================================================================
 * @script:          Database_Class
 * -----------------------------------------------------------------------
 * @author:          Mustafa Rampurawala
 * -----------------------------------------------------------------------
 * @email:           mustafa@matrixsoftwares.com
 * -----------------------------------------------------------------------
 * @website:         http://matrixsoftwares.com
 * -----------------------------------------------------------------------
 * @copyright:       Mustafa Rampurawala, Matrixsoftwares.com
 * -----------------------------------------------------------------------
 * @date_created:    2010-Sep-10
 * -----------------------------------------------------------------------
 * @date_lastupdated:2012-Jun-01
 * -----------------------------------------------------------------------
 * @version:         1.2
 * =======================================================================
 */
require_once("constants.php");

class MySQLDB {

  private $connection; //The MySQL database connection
  private $result = array();
  private $enum = 1;
  private $err = array();

  /* Class constructor */

  function MySQLDB() {
    /* Make connection to database */
    $this->connection = mysqli_connect(DB_API_SERVER, DB_API_USER, DB_API_PASS, DB_API_NAME) or die(mysqli_error());
    mysqli_set_charset($this->connection, 'utf8');
  }

  function __destruct() {
    foreach ($this->result as $val) {
      if (is_resource($val))
        mysqli_free_result($val);
    }
    unset($this->result);
    mysqli_close($this->connection);
    unset($this->connection);
  }

  /**
   * query - Performs the given query on the database and
   * returns the result, which may be false, true or a
   * resource identifier.
   */
  function query($query) {
    $this->result[$this->enum] = mysqli_query($this->connection, $query);
    if(mysqli_error($this->connection)){
      $fh = fopen('mtx_log.log', 'a');
      fwrite($fh, '[' . date('Y-m-d') . ']' . $query . "\n\n");
      fwrite($fh, mysqli_error($this->connection) . "\n\n");
      fclose($fh);
      die(mysqli_error($this->connection));
    }
    
    if ($this->result[$this->enum]) {
      $this->enum++;
      return $this->enum - 1;
    } else {
      //echo $query;
      $this->err[$this->enum] = mysqli_error($this->connection);
      unset($this->result[$this->enum]);
      return FALSE;
    }
  }

  function query_fetch_full_result($query) {
    $result = mysqli_query($this->connection, $query);
    if(mysqli_error($this->connection)){
      $fh = fopen('mtx_log.log', 'a');
      fwrite($fh, '[' . date('Y-m-d') . ']' . $query . "\n\n");
      fwrite($fh, mysqli_error($this->connection) . "\n\n");
      fclose($fh);
      die(mysqli_error($this->connection));
    }
    
    $rows = array();

    if ($result) {
      while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
      }
      mysqli_free_result($result);
      if (count($rows) < 1) {
        $rows = FALSE;
      }
      return $rows;
    } else {
      return FALSE;
    }
  }

  function fetch_array($enum) {
    if ($this->result[$enum])
      $row = mysqli_fetch_assoc($this->result[$enum]);
    else
      $row = FALSE;
    return $row;
  }

  function row_count($enum) {
    if ($this->result[$enum])
      $count = mysqli_num_rows($this->result[$enum]);
    else
      $count = FALSE;
    return $count;
  }

  function sanitize($data) {
    $data = stripcslashes(strip_tags($data));
    $result = mysqli_real_escape_string($this->connection, $data);
    return $result;
  }

  function free_result($enum) {
    if ($this->result[$enum] && is_resource($this->result[$enum])) {
      mysqli_free_result($this->result[$enum]);
      unset($this->result[$enum]);
    }
  }

  function clean_data($input) {
    if (is_array($input)) {
      foreach ($input as &$value) {
        $value = $this->clean_data($value);
      }
    } else {
      $input = $this->sanitize($input);
    }
    return $input;
  }

  function get_last_err() {
    return $this->err[$this->enum];
  }

  function get_last_insert_id() {
    return mysqli_insert_id($this->connection);
  }

  function clean_currency($amount) {
    $amount = str_replace(array(',', ' ', '/', '$'), '', $amount);
    return $amount;
  }

}

;
/* Create database connection */
$database = new MySQLDB;
?>