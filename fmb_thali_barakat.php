<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.family.php');
$cls_family = new Mtx_family();
$post = FALSE;
if (isset($_POST['Search'])) {
  $post = TRUE;
  $search = $cls_family->barakat_received($_POST['Mohallah']);
} else {
  $post = TRUE;
  $search = $cls_family->barakat_received();
}

$mohallah = $cls_family->get_all_Mohallah();

$title = 'List of Families receiving FMB Barakat';
$active_page = 'report';


require_once 'includes/header.php';

$page_number = REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <div class="col-md-12">
            <?php
            $cur_hijri_date = HijriCalendar::GregorianToHijri();
            echo $cur_hijri_date[1] . ', ' . HijriCalendar::monthName($cur_hijri_date[0]) . ', ' . $cur_hijri_date[2] . ' H ';
            echo date('d-m-Y H:i:s');
            ?>
          </div>
          <form method="post" class="form-horizontal">
            <div class="form-group">
              <label class="col-md-2 control-label">Mohallah</label>
              <div class="col-md-5">
                <select id="Mohallah" name="Mohallah" class="form-control">
                  <option value="all">All</option>
                  <?php
                  if ($mohallah) {
                    foreach ($mohallah as $mohalla) {
                      ?>
                      <option value="<?php echo $mohalla['Mohallah'] ?>" <?php
                      if (isset($_POST['Search'])) {
                        if ($mohalla['Mohallah'] == $_POST['Mohallah']) {
                          echo 'selected';
                        }
                      }
                      ?>><?php echo ucwords($mohalla['Mohallah']); ?></option>
                              <?php
                            }
                          }
                          ?>
                </select>
              </div>
              <div>
                <span class="">
                  <input type="submit" class="btn btn-success" value="Search" name="Search">
                  <input type="hidden" value="<?php
                  if (isset($_POST['Search']))
                    echo $_POST['Mohallah'];
                  else
                    echo '';
                  ?>" class="btn" name="mh">
                </span>
                <a href="print_fmb_thali_barakat.php?mh=<?php if (isset($_POST['Search']))
                           echo $_POST['Mohallah'];
                         else
                           'all'
                    ?>" target="blank" class="btn btn-primary <?php echo !$post ? 'disabled' : ''; ?>" name="print">Print</a>
              </div>
              
            </div>


            <div class="form-group">

            </div>
          </form>
          <div class="com-md-12">
          </div>
          <div class="col-md-12" id="report">
            <?php
              $print = FALSE;
              include 'includes/inc.fmb_thali_barakat.php';
            ?>

          </div>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>
  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
  include('includes/footer.php');
?>