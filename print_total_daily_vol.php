<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/hijri_cal.php');
$cls_receipt = new Mtx_Receipt();

$from_date = $_GET['from'];
$to_date = $_GET['to'];
$from_split = explode('-', $from_date);
$from_ts = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
$to_split = explode('-', $to_date);
$to_ts = mktime(23, 59, 59, $to_split[1], $to_split[2], $to_split[0]);  

$receipts = $cls_receipt->get_daily_vol_rcpt_between_days($from_ts, $to_ts);
$title = 'Total daily voluntary receipt';
$active_page = 'report';

?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body onload="window.print()">
<!-- Content -->
<div class="row">
  <div class="col-md-12"></div>

  <!-- Left Bar -->
  <div class="col-md-2 pull-left">
    &nbsp;
  </div>
  <!-- /Left Bar -->


  <!-- Center Bar -->
  <div class="col-md-8 ">
    <div>
      <strong>Total daily Voluntary receipt Between <?php echo $from_date . ' And ' . $to_date;?><span class="pull-right"><?php echo date('d F,Y'); ?></span></strong>
    </div>
    <?php
    if($receipts) echo $receipts;
    ?>
  </div>
  <!-- /Center Bar -->

  </div>
  <!-- /Content -->

  </body>
</html>
