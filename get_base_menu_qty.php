<?php
include('session.php');
require_once('classes/class.database.php');
require_once("classes/class.receipt.php");
require_once('classes/class.product.php');

$cls_receipt = new Mtx_Receipt();
$cls_product = new Mtx_Product();

$base_menu = $result = FALSE;

if (isset($_POST['get_base_menu'])) 
{
  $base_menu = $_POST['base_menu'];
  $quantity = (int) $_POST['quantity'];
  $result = $cls_product->get_all_items_by_base_menu($base_menu);
}

$base_menus = $cls_product->get_all_base_menu_der();

$title = 'Base Menu Quantity';
$active_page = 'menu';

require_once 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profiles</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-6">
          <form method="post" role="form" class="form-horizontal">
            <div></div>

            <div class="form-group">
              <label class="control-label col-md-3">Base Menu</label>
              <div class="col-md-6">
                <select class="form-control" id="base_menu" name="base_menu">
                  <option value="">--Select One--</option>
                  <?php
                  if ($base_menus) {
                    foreach ($base_menus as $bm) {
                      ?>
                      <option value="<?php echo $bm['id']; ?>" <?php if($base_menu == $bm['id']) echo 'selected'; ?>><?php echo $bm['Menu']; ?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Quantity</label>
              <div class="col-md-6">
                <input type="text" id="quantity" name="quantity" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">&nbsp;</label>
              <button class="btn btn-success" style="margin-left: 15px;" type="submit" name="get_base_menu" id="add">Search</button>
            </div>

          </form>
        </div>
        <div class="col-md-6">
          <?php
            if($_POST){
            ?>
            <table class="table table-hover table-condensed table-bordered">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Item Name</th>
                  <th>Quantity</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if ($result) {
                $i = 1;
                foreach ($result as $menu) {
                  ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $menu['Item']; ?></td>
                    <td><?php echo $quantity; ?></td>
                  </tr>
            <?php
                }
              }else {
                echo '<tr><td colspan="3">No Records..</td></tr>';
              }
            ?>
              </tbody>
            </table>
          <?php } ?>
        </div>
        <!-- /Center Bar -->
        <script>
          $('#add').click(function() {
            var base_menu = $('#base_menu').val();
            var qty = $('#quantity').val();
            var error = '';
            var validate = true;
            if (base_menu == '')
            {
              error += 'Please select Base Menu Name\n';
              validate = false;
            }
            if (qty == '')
            {
              error += 'Please enter Quantity\n';
              validate = false;
            }
            if (validate == false)
            {
              alert(error);
              return validate;
            }
          });
          
          jQuery(document).ready(function () {

            var textWeight = $("input[type='text']#quantity");
            textWeight.change(function () {

              var value = textWeight.val();
              
              if(value < 50 || value > 1000)
              {
                alert("Please enter Quantity between 50 to 1000.");
                textWeight.val(50);
              }
            });
          });
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>