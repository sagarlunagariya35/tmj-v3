<form class="form-horizontal" role="form" method="post" action="" id="FRM_CALCULATION">
  <div class="form-group">
    <div class="col-md-12">
      <input type="text" name="thali_id_takh" class="form-control" id="thali_id_takh" placeholder="<?php echo THALI_ID; ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-12">
      <input type="text" name="for_months_takh" class="form-control" id="for_months_takh" placeholder="Kisht">
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-12">
      <select id="takhmeen_year" id="takhmeen_year" class="form-control">
        <option value ="">-- Select One --</option>
          <?php
            for($i = 1; $i <= $no_years; $i++) {
            $year = (($i == 1) ? $default_year : ($default_year + ($i - 1))) . '-' . (substr($default_year, -2) + $i);
          ?>
            <option value="<?php echo $year;?>"><?php echo $year; ?></option>
          <?php } ?>
        </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-md-12">
      <button class="btn btn-success btn-block" id="frm_calculate_takh" data-toggle="modal" data-target="#takh_calculate">Calculate</button>
    </div>
  </div>
</form>
<script>
  $('#frm_calculate_takh').click(function(){
    var thali_id = $('#thali_id_takh').val();
    var for_months = $('#for_months_takh').val();
    var year = $('#takhmeen_year').val();
    if(thali_id === '' || for_months === ''){
      alert('Thali id or for months cant be empty');
      return false;
    } else {
      $.ajax({
        url     : 'ajax.php',
        type    : 'POST',
        data    : 'thali_id='+thali_id+'&months='+for_months+'&year='+year+'&cmd=TAKHMEEN_CALCULATION',
        success : function(data){
          if(data != 'Invalid thali id') {
            $('#takh_body').hide();
            $('#result_takh_body').html('<p class="form-control-static">'+data+'</p>').show();
          } else {
            $('#takh_body').hide();
            $('#result_takh_body').html('<p class="form-control-static">'+data+'</p>').show();
          }
        }
      });
      return false;
    }
  });
</script>