<?php
include 'session.php';
require_once("classes/class.database.php");
require_once("classes/class.family.php");
require_once("classes/class.user.php");
$cls_user = new Mtx_User();
$cls_family = new Mtx_family();

if(isset($_POST['add_labels'])) {
  $max = count($_POST['name']);
  $data = $database->clean_data($_POST);
}

$setting = $cls_user->get_general_settings();
$db_tanzeem_name = $setting[0]['tanzeem_name'];
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Id Cards</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
      @font-face
      {
        font-family: barcode;
        src: url(asset/fonts/IDAutomationHC39M.ttf);
      }
      .row{
        width: 900px;
        height: 380px;
        /*border: 1px dotted black;*/
        padding-left: 20px;
      }
      #outer-square{
        display: inline-table;
        height: 365px;
        width: 435px;
        border: 1px solid #999;
        background-color: #2929A3;
        -webkit-print-color-adjust:exact;
      }
      #inner-square{
        margin-left: 20px;
        margin-top: 25px;
        height: 320px;
        width: 395px;
        border: 1px solid #999;
        background-color: #FFFFFF;
      }
      .small-square{
        display: inline-block;
        font-size: 0px;
        width: 134px;
        top: 4px;
        text-align: center;
        vertical-align: top;
        border: 1px solid #999;
      }
      .logo{
        display: inline-block;
        font-size: 20px;
        width: 250px;
        text-align: right;
        color: #FF9900;
      }
      .details{
        display: block;
        clear: both;
        margin: 0px;
        padding-left: 20px;
      }
      .name{
        font-size: 22px;
        margin-top: 20px;
        color: #800000;
      }
      .ph{
        font-size: 22px;
        margin: 0px;
      }
      .barcode{
        display: inline-block;
        font-family: barcode;
        font-size: 14px;
        text-align: center;
      }
      #thaali-id{
        display: inline-block;
        text-align: left;
        font-size: 35px;
        font-weight: 900;
        width: 220px;
        color: #33CC33;
      }
    </style>
  </head>
  <body>
    <?php
    $count = 0;
    $files = $_FILES['Upload'];
    for($i = 0; $i < $max; $i++){
      $filename = 'temp/'.$files['name'][$i];
      move_uploaded_file($files["tmp_name"][$i], $filename);
      $personName = $data['name'][$i];
      $mobile = $data['mobile'][$i];
      $its = $data['its'][$i];
      $department = $data['department'][$i];
      if($count == 0){
        echo '<div class="row">';
      }
    ?>
      <div class="" id="outer-square">
        <div id="inner-square">
          <div class="col-md-2 small-square">
            <img src="<?php echo $filename; ?>" width="134px">
          </div>
          <div class="logo">
            <img src="includes/logo.jpg" width="200px">
            <br>
            <?php echo $db_tanzeem_name;?>
          </div>
          <div class="details">
            <div class="name"><strong>Name: </strong><?php echo $personName; ?></div>
            <div class="ph"><strong>Mobile: </strong><?php echo $mobile; ?></div>
            <div class="ph"><strong>Department: </strong><?php echo $department; ?></div>
          </div>
          <div id="thaali-id">
            <?php echo $its; ?>
          </div>
          <div class="barcode">
            *<?php echo $its; ?>*
          </div>
        </div>
      </div>
    <?php
    if($count == 1){
      echo '</div>';
      $count = 0;
    } else {
      $count++;
    }
    
    }
    ?>
  </body>
</html>
