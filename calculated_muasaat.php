<?php
include 'session.php';
require_once("classes/class.database.php");
require_once("classes/class.family.php");
require_once('classes/class.user.php');
$cls_family = new Mtx_family();
$cls_user = new Mtx_User();
$post = FALSE;
if (isset($_POST['search'])) {
  $post = TRUE;
  $takhmeen_year_muasaat = $_POST['takhmeen_year_muasaat'];
  $tanzeem = $_POST['tanzeem'];
  $ary_File = array();
  $data = $cls_family->get_FileNo('CLOSE_NOT_ALLOWED', $tanzeem);
  $i = 0;
  foreach ($data as $key => $f_id) {
    $thali_id = $f_id['FileNo'];
    $tfn_size = $f_id['tiffin_size'];
    $takh = $cls_family->get_takhmeen_record($thali_id, FALSE, " AND `year` = '$takhmeen_year_muasaat'");
    $takh_amount = $takh[0]['amount'];
    $tfn_cost = $cls_family->get_tiffin_cost($takhmeen_year_muasaat, $tfn_size);

    $cost = $tfn_cost['cost'] ? $tfn_cost['cost'] : 0;
    // Remove ids with tiffin size VC
    if (strtoupper($tfn_size) != 'VC') {
      $ary_File[$i]['FileNo'] = $thali_id;
      $ary_File[$i]['size'] = $tfn_size;
      $ary_File[$i]['takhmeen'] = $takh_amount;
      $ary_File[$i]['cost'] = $cost;
      $ary_File[$i++]['HOF'] = $f_id['HOF_NAME'];
    }
  }
}
if (isset($_POST['print'])) {
  $takhmeen_year_muasaat = $_POST['takhmeen_year_muasaat'];
  header("Location: print_cal_muasaat.php?&y=$takhmeen_year_muasaat&tanzeem=$_POST[tanzeem]");
  exit();
}
$mohallah = $cls_user->get_single_user($_SESSION[USERNAME]);
$mohallahs = $cls_family->get_all_tanzeem();
$title = 'Muasaat';
$active_page = 'report';

require_once 'includes/header.php';

$page_number = ACCOUNTS_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Hub</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <form method="post" role="form" class="form-horizontal">
            <div class="col-md-12">
              <label class="col-md-1 control-label"><?php echo YEAR; ?></label>
              <div class="col-md-3">
                <select name="takhmeen_year_muasaat" id="takhmeen_year_muasaat" class="form-control">
                  <?php if (isset($takhmeen_year_muasaat)) { ?>
                    <option value="" <?php echo ($takhmeen_year_muasaat == '') ? 'selected' : ''; ?>>--Select Year--</option>
                    <?php
                  }
                  for ($i = 1; $i <= $no_years; $i++) {
                    $takh_year = (($i == 1) ? $default_year : ($default_year + ($i - 1))) . '-' . (substr($default_year, -2) + $i);
                    if ($takh_year == $takhmeen_year_muasaat)
                      $selected = 'selected';
                    else
                      $selected = '';
                    ?>
                    <option value="<?php echo $takh_year; ?>" <?php echo $selected; ?>><?php echo $takh_year; ?></option>
      <?php } ?>
                </select>
              </div>
              <label class="col-md-1 control-label">Tanzeem</label>
              <div class="col-md-3">
                <?php if($mohallah['Mohallah'] != '') { ?>
                <p class="form-control static"><?php echo $mohallah['Mohallah']; ?></p>
                <input type="hidden" name="tanzeem" value="<?php echo $mohallah['Mohallah']; ?>">
                <?php }else { ?>
                <select class="form-control" name="tanzeem" id="tanzeem">
                  <option value="">All</option>
                  <?php foreach ($mohallahs as $mh) { ?>
                    <option value="<?php echo $mh['name']; ?>" <?php echo (isset($tanzeem) && $tanzeem == $mh['name']) ? 'selected' : ''; ?>><?php echo $mh['name']; ?></option>
      <?php } ?>
                </select>
                <?php } ?>
              </div>
              <input type="submit" name="search" id="search" value="Search" class="btn btn-success validate">
              <?php if (isset($takhmeen_year_muasaat)) { ?>
                <a target="_blank" href="print_cal_muasaat.php?&year=<?php echo $takhmeen_year_muasaat; ?>&tanzeem=<?php echo $tanzeem; ?>"class="btn btn-primary validate <?php echo!$post ? 'disabled' : ''; ?>">Print</a>
      <?php } ?>
            </div>
          </form>
          <div class="col-md-12">&nbsp;</div>
          <?php
          if (isset($ary_File) && (!empty($ary_File))) {
            $i = 1;
            $print = FALSE;
            include('includes/inc.cal_muasaat.php');
          }
          ?>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>
  
<script>
  $('.validate').click(function() {
    var year = $('#takhmeen_year_muasaat').val();
    var tanzeem = $('#tanzeem').val();
    var errors = [];
    var key = 0;
    if (year === '')
      errors[key++] = 'takhmeen year'
    if (tanzeem == )
      errors[key++] = 'tanzeem';
    if (errors.length) {
      alert('Please select ' + errors.join(', ') + ' to proceed...');
      return false;
    }
  });
  $('#print').click(function() {
    var month = $('#month').val();
    var year = $('#takhmeen_year_muasaat').val();
    var error = 'Following error(s) are occurred\n';
    var validate = true;
    if (month == '0')
    {
      error += 'Please select month name\n';
      validate = false;
    }
    if (year == '')
    {
      error += 'Please enter year\n';
      validate = false;
    }
    if (validate == false)
    {
      alert(error);
      return validate;
    }
  });
</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>