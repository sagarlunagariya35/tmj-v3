<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.receipt.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();
$cls_receipt = new Mtx_Receipt();
$mohallah = $_REQUEST['mh'];
$result = $cls_family->get_family_by_mohallah($mohallah);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print sabil home pending report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body onload="window.print()">
<!-- Content -->
<div class="row">
  <div class="col-md-12"></div>

  <!-- Left Bar -->
  <div class="col-md-2 pull-left">
    &nbsp;
  </div>
  <!-- /Left Bar -->

  <!-- Center Bar -->
  <div class="col-md-8 ">
          <table class="table table-hover table-condensed resizable">
            <thead>
              <tr>
                <th colspan="10" style="text-align: center">Sabil home pending report<span class="pull-right"><?php echo date('d F,Y');?></span></th>
              </tr>
            <th>Sr No</th>
            <th>Family No</th>
            <th>HOF Name</th>
            <th>Mobile No.</th>
            <th>Paid Till</th>
            <th>Close date</th>
            <th>Pending Amount</th>
            <th>On hand Cash</th>
            <th>Actual Amount</th>
            <th>Mohallah</th>
            </thead>
            <tbody>
              <?php
              if ($result) {
                $i = 1;
                $total_pending_amount = 0;
                $total_actual_amount = 0;
                $total_on_hand_amount = 0;
                foreach ($result as $family) {
                  $partial = $cls_receipt->get_particular_partial_receipt($family['FileNo']);
                  if ($family['paid_till'])
                    $hijri_date = HijriCalendar::GregorianToHijri($family['paid_till']);
                  ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $family['FileNo']; ?></td>
                    <td><?php $name = $cls_family->get_name($family['FileNo']); echo $name; ?></td>
                    <td><?php echo $family['MPh']; ?></td>
                    <?php if ($family['paid_till'] != NULL) { ?>
                      <td><?php echo HijriCalendar::monthName($hijri_date[0]) . ', ' . $hijri_date[2] . ' H'; ?></td>
                    <?php } else { ?>
                      <td>--</td>
                      <?php } ?>
                      <?php if ($family['close_date'] != 0) { 
                        $hijri_close_date = HijriCalendar::GregorianToHijri($family['close_date']);
                        ?>
                        <td style="background-color: #FF9999;"><?php echo HijriCalendar::monthName($hijri_close_date[0]) . ', ' . $hijri_date[2] . ' H'; ?></td>
                        <?php } else { ?>
                        <td>--</td>
                      <?php } ?>
                    <td>
                    <?php
                    if($family['paid_till'] > 0){
                    $hijri_date = HijriCalendar::GregorianToHijri($family['paid_till']);
                    $last_year = $hijri_date[2];
                    
                    $cur_ts = HijriCalendar::GregorianToHijri();
                    $cur_month = $cur_ts[0];
                    if($family['close_date'] != 0){
                      $cur_ts = HijriCalendar::GregorianToHijri($family['close_date']);
                      $cur_month = $cur_ts[0];
                    }
                    
                    if($last_year == $cur_ts[2]){
                      $diff_months = abs($hijri_date[0] - $cur_month);
                    } else if($last_year < $cur_ts[2]){
                      $diff_year = $cur_ts[2] - $last_year;
                      if($diff_year > 1){
                        $remaining_months_from_last = 12 -  $hijri_date[0];
                        $diff_months = abs((12 * $diff_year) + $remaining_months_from_last);
                      } else {
                        $remaining_months_from_last = 12 -  $hijri_date[0];
                        $diff_months = abs($remaining_months_from_last + $cur_month);
                      }
                    } else if($last_year > $cur_ts[2]){
                      $diff_months = 0;
                    }
                    
                    $home = $cls_receipt->get_single_sabil_home($family['FileNo']);
                    $pending_amount = $home['amount'] * $diff_months;

                    echo number_format($pending_amount).'/-';
                    $total_pending_amount += $pending_amount;
                    }
                    ?>
                    </td>
                    <td><?php
                    if ($partial) {
                      echo number_format($partial['TotalPayment']) . '/-';
                      $total_on_hand_amount += $partial['TotalPayment'];
                    } else echo '--';
                    ?>
                    </td>
                    <td><?php
                    if ($partial){
                      $on_hand = $partial['TotalPayment'];
                      $actual = $pending_amount - $on_hand;
                      $total_actual_amount += $actual;
                       echo number_format($actual) . '/-';
                    }
                    ?>
                    </td>
                    <td><?php echo $family['Mohallah']; ?></td>
                  </tr>
                  <?php
                }
              } else {
                ?>
                <tr>
                  <td colspan="3">No Record Found</td>
                </tr>
                <?php
              }
              if (isset($total_pending_amount) && $total_pending_amount > 0 && isset($total_on_hand_amount) && isset($total_actual_amount)) {
                ?>
                <tr>
                  <td colspan="5"></td>
                  <td class="pull-right">Total:</td>
                  <td><strong><?php echo number_format($total_pending_amount) . '/-' ?></strong></td>
                  <td><strong><?php echo number_format($total_on_hand_amount) . '/-' ?></strong></td>
                  <td><strong><?php echo number_format($total_actual_amount) . '/-' ?></strong></td>
                  <td>&nbsp;</td>
                </tr>
                <?php
              }
              ?>
            </tbody>
          </table> 
        </div>

  <!-- /Center Bar -->
  </div>
  <!-- /Content -->

  </body>
</html>
