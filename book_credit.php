<?php
include 'session.php';
$pg_link = 'book_credit';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
$cls_receipt = new Mtx_Receipt();

$title = 'Credit voucher book';
$active_page = 'account';
$from_date = $to_date = FALSE;
$btn_print_link = FALSE;
$page = 1;
if (isset($_GET['page']) && $_GET['page'] != '') {
  $page = $_GET['page'];
}

if (isset($_GET['search'])) {
  $from_date = $_GET['from_date'];
  $to_date = $_GET['to_date'];
  $btn_print_link = "print_vouchers.php?from_date=$from_date&to_date=$to_date&cmd=credit";
  $post = TRUE;
  $search = $cls_receipt->get_receipts($page, 20, 'credit', $from_date, $to_date);
  $voucher = $cls_receipt->get_number_voucher('credit', $from_date, $to_date);
  $amount = $cls_receipt->get_credit_total_between_dates($from_date, $to_date);
} else {
  $post = FALSE;
  $search = $cls_receipt->get_receipts($page, 20, 'credit');
  $voucher = $cls_receipt->get_number_voucher('credit');
  $amount = FALSE;
}

include('includes/header.php');

$page_number = ACCOUNTS_REPORTS;
require_once 'page_rights.php';
?>
<script src="asset/dist/js/bootstrap-tooltip.js"></script> 
<script src="asset/dist/js/bootstrap-popover.js"></script> 
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Receipt Books</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>        

        <!-- Center Bar -->
        <div class="col-md-12">
          <?php include 'includes/inc.dates.php'; ?>
          <?php if ($_GET) { ?>
            <div class="col-md-12">&nbsp;</div>
            <div class="alert-success">
              <strong>Total Amount: </strong><?php echo number_format($amount[0]['Amount'], 2); ?>
            </div>
          <?php } ?>
          <div class="col-md-12">&nbsp;</div>
          <table class="table table-hover table-condensed table-bordered">
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Account Heads</th>
                <th class="text-right">Amount</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if ($search) {
                $i = 1;
                foreach ($search as $credit) {
                  ?>
                  <tr class="<?php echo ($credit['cancel'] == 1) ? 'alert-danger' : ''; ?>">
                    <td><a href="update_voucher.php?cmd=credit&cid=<?php echo $credit['id']; ?>"><?php echo $credit['id']; ?></a></td>
                    <td class="example" rel="popover" data-content="<?php echo $credit['description']; ?>" data-placement="top" data-original-title="Description"><?php echo ucwords(strtolower($credit['name'])); ?></td>
                    <td><?php echo ucfirst($credit['acct_heads']); ?></td>
                    <td class="text-right"><?php echo number_format($credit['amount'], 2); ?></td>
                    <td><?php echo date('d F, Y', $credit['timestamp']); ?></td>
                  </tr>
                <?php }
              } else {
                ?>
                <tr>
                  <td colspan="6" class="alert-danger">Sorry! No voucher found.</td>
                </tr>
      <?php } ?>
            </tbody>
          </table>
          <?php
          require_once("pagination.php");
          $getData = ($from_date && $to_date) ? 'from_date=' . $from_date . '&to_date=' . $to_date . '&search=Search&' : '';
          echo pagination(20, $page, '?' . $getData . 'page=', $voucher);
          ?>
        </div>
        <!-- /Center Bar -->
        <script>
          $(function()
          {
            $(".example").popover();
          });
        </script>  

      </div>
      <!-- /Content -->
    </section>
  </div>
<?php
include('includes/footer.php');
?>