<?php
include 'session.php';
require_once("classes/class.database.php");
require_once("classes/class.family.php");
require_once("classes/class.receipt.php");
require_once('classes/class.user.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();
$cls_receipt = new Mtx_Receipt();
$cls_user = new Mtx_User();

$takhmeen_year_fmb = $key = FALSE;

if (isset($_POST['search'])) {
  $thali_id = $_POST['FileNo'];
  $takhmeen_year_fmb = $_POST['takhmeen_year_fmb'];
  $takh = $cls_family->get_takhmeen_record($thali_id, FALSE, " AND `year`='$takhmeen_year_fmb'");
  $receipts = $cls_receipt->get_all_for_FMB_DETAILS($thali_id, $takhmeen_year_fmb);
}
$setting = $cls_user->get_general_settings();
$default_year = $setting[0]['start_year'];
$no_years = $setting[0]['no_of_years'];
$hub = 0;
$title = 'FMB ID details';
$active_page = 'report';

include('includes/header.php');

$page_number = ACCOUNTS_REPORTS;
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Hub</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <form method="post" role="form" class="form-horizontal">

            <div class="form-group">
              <label class="control-label col-md-3"><?php echo THALI_ID; ?></label>
              <div class="col-md-4">
                <input type="text" name="FileNo" id="FileNo" value="<?php if (isset($_POST['FileNo'])) echo $_POST['FileNo']; ?>" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Name</label>
              <div class="col-md-4">
                <input type="text" name="name" id="name" value="<?php if (isset($_POST['FileNo'])) echo $_POST['name']; ?>" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3"><?php echo YEAR; ?></label>
              <div class="col-md-4">
                <select class="form-control get_record" name="takhmeen_year_fmb" id="takhmeen_year_fmb">
                  <option value ="" <?php echo ($takhmeen_year_fmb == '') ? 'selected' : '';?>>-- Select One --</option>
                  <?php
                      for($i = 1; $i <= $no_years; $i++) {
                      $takh_year = (($i == 1) ? $default_year : ($default_year + ($i - 1))) . '-' . (substr($default_year, -2) + $i);
                      if($takh_year == $takhmeen_year_fmb) $selected = 'selected';
                      else $selected = '';
                    ?>
                    <option value="<?php echo $takh_year;?>" <?php echo $selected; ?>><?php echo $takh_year; ?></option>
                    <?php } ?>
                </select>
              </div>
            </div>
            <input type="submit" name="search" id="search" value="Search" class="btn btn-success col-md-offset-3">
          </form>
          <div class="col-md-12">&nbsp;</div>
          <?php
          if (isset($_POST['search'])) {
            ?>
            <table class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th><?php echo THALI_ID; ?></th>
                  <th><?php echo YEAR; ?></th>
                  <th class="text-right">Takhmeen</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($takh as $value) {
                  $hub += $value['Hub_raqam'];
                  $sep = explode('-', $key);
                  ?>
                  <tr>
                    <td><?php echo $value['FileNo']; ?></td>
                    <td><?php echo $value['year']; ?></td>
                    <td class="text-right"><?php echo number_format($value['amount'], 2); ?></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php
          }
          if (isset($receipts) && (!empty($receipts))) {
            ?>
            <table class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th colspan="5">Hub Receipts</th>
                </tr>
                <tr>
                  <th>Receipt No</th>
                  <th>Receipt date</th>
                  <th><?php echo YEAR; ?></th>
                  <th class="text-right">Amount</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($receipts as $receipt) {
                  $red = '';
                  if($receipt['cancel'] == '1') $red = ' class="alert-danger"';
                  ?>
                  <tr<?php echo $red;?>>
                    <td><?php echo $receipt['id']; ?></td>
                    <td><?php
                    $spl = explode('-', $receipt['date']);
                    echo date('d F, Y', mktime(0, 0, 0, $spl[1], $spl[2], $spl[0])); ?></td>
                    <td><?php echo $takhmeen_year_fmb; ?></td>
                    <td class="text-right"><?php echo number_format($receipt['amount'], 2); ?></td>
                  </tr>
        <?php } ?>
              </tbody>
            </table>
            <?php
          } elseif(isset($ary_File) && (!empty($ary_File))) {
            ?>
            <table class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th>Receipt No</th>
                  <th>Receipt date</th>
                  <th>From</th>
                  <th>To</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td colspan="5" class="alert-danger">No receipts Found</td>
                </tr>
              </tbody>
            </table>
            <?php
          }
          ?>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>
  
<script>
  $('#FileNo').change(function() {
    var upper_case = $(this).val().toUpperCase();
    $(this).val(upper_case);
    var fId = $(this).val();
    $('#loader').html('<img src="asset/img/loader.gif">');
    $.ajax({
      url: "ajax.php",
      type: "POST",
      data: 'fId=' + fId + '&cmd=get_name',
      success: function(data)
      {
        $('#loader').text('');
        if (data != 'invalid') {
          $('#exist').val('0');
          $('#name').val(data);
          $('#File').attr('class', 'form-group has-success');
          $('#amount').focus();
        } else {
          alert('Invalid thali ID');
          $('#exist').val('1');
          $('#File').attr('class', 'form-group has-error');
          $('#FileNo').focus();
          return false;
        }
      }
    });
  });
  $('#search').click(function() {
    var thali_id = $('#FileNo').val();
    var year = $('#takhmeen_year_fmb').val();
    var error = 'Following error(s) are occurred\n';
    var validate = true;
    if (thali_id == '')
    {
      error += 'Please enter thali id\n';
      validate = false;
    }
    if (year == '')
    {
      error += 'Please enter year\n';
      validate = false;
    }
    if (validate == false)
    {
      alert(error);
      return validate;
    }
  });
</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>