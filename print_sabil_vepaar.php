<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.receipt.php');
require_once('classes/hijri_cal.php');

$cls_family = new Mtx_family();
$hijari = new HijriCalendar();
$cls_receipt = new Mtx_Receipt();
$user_id = $_SESSION[USER_ID];
//form
if (isset($_POST['print'])) {
  $fId = $_POST['FileNo'];
  $name = $_POST['name'];
  $months = $_POST['months'];
  $type = $_POST['type'];
  $bank = $_POST['bankname'];
  $cheque = $_POST['cheque'];
  $date = $_POST['date'];
  $original = $_POST['ori_amount'];
  $expected = $_POST['edit_amount'];
  $till = $_POST['paid_till1'];
  $upto = $_POST['paid_upto1'];
  $form_id = $_POST['form_id'];

  // error checking
  $ary_err = array();
  if($fId == '') $ary_err[] = '* ID not provided.';
  if($name == '') $ary_err[] = '* Name not provided.';
  if($months < 1) $ary_err[] = '* Months can not be less then 1.';
  if($till < 1) $ary_err[] = '* From date date could not be calculated.';
  if($upto < 1) $ary_err[] = '* Paid upto date could not be calculated.';
  if($form_id == '') $ary_err[] = '* The form seems to be incompletly posted.';
  
  if(count($ary_err) > 0) {
    $_SESSION[ERROR_MESSAGE] = "Following errors were encountered while processing the request.<br>\n".implode("<br>\n", $ary_err);
    header('location: sabil_vepaar.php');
    exit;
  } else {
    if ($original != $expected) {
      $_SESSION[ORIGINAL] = $original;
      $_SESSION[EXPECTED] = $expected;
      $_SESSION[FILE] = $fId;
      $_SESSION[NAME] = $name;
      $_SESSION[PAID_TILL] = $till;
      $_SESSION[PAID_UPTO] = $upto;
      $_SESSION[TYPE] = $type;
      $_SESSION[BANK] = $bank;
      $_SESSION[CHEQUE] = $cheque;
      $_SESSION[DATE] = $date;
      $_SESSION[MONTHS] = $months;
      $_SESSION[FORM_ID1] = $form_id;
      header('Location: sabil_vepaar_confirmation.php');
      exit();
    } else {
  //here we again compute the paid upto date and expected amount
    $result = $cls_family->get_last_year_month_vepaar_sabil($_POST['FileNo']);
    if ($result) {
      $for_months = $_POST['months'];

      // to get last date
      $hijri_date = $hijari->GregorianToHijri($result['paid_till']);
      $last_month = $hijri_date[0];
      $last_year = $hijri_date[2];
      
      $add_months = $hijri_date[0] + 1;
      if($add_months > 12){
        $add_month = 1;
        $add_year = $last_year + 1;
      } else {
        $add_month = $add_months;
        $add_year = $last_year;
      }
      // FIX: to solve round months error in from date
      If($last_month == 12) {
        $from_month = 1;
        $from_year = $last_year+1;
      } else {
        $from_month = $last_month+1;
        $from_year = $last_year;
      }
      
      $to_month = ($last_month+$for_months) % 12; 
      $to_year = floor(($last_month + $for_months) / 12) + $last_year;

      // FIX: to solve round months error in upto date
      If($to_month == 0) {
        $to_month = 12;
        $to_year = $to_year-1;
      }
      $paid_till = HijriCalendar::HijriToUnix($add_month, '01', $add_year);
      $paid_upto = HijriCalendar::HijriToUnix($to_month, '01', $to_year);
      $expected_amount = number_format($result['amount'] * $for_months).'/-';
     }
      $result = $cls_receipt->add_sabil_vepaar($_POST['FileNo'], $name, $expected_amount, $paid_till, $paid_upto, $_POST['months'], $type, $bank, $cheque, $date, $user_id, $form_id);
      if ($result) {
        header('Location: print_sabil_home.php?id=' . $result);
        exit;
      }
    }
  }
}
//////////////start
if(isset($_REQUEST['id'])) {
  $rcpt = $cls_receipt->get_total_amount_receipt_sabil_vepaar($_REQUEST['id']);
  $file_id = $rcpt['FileNo'];
  $hub_details = $cls_family->get_last_year_month_vepaar_sabil($file_id);
  $family = $cls_family->get_single_family($file_id);
  $name = $cls_family->get_name($file_id);
  $from_date = $hijari->GregorianToHijri($rcpt['from_date']);
  $to_date = $hijari->GregorianToHijri($rcpt['to_date']);

  $from_month = $hijari->monthName($from_date[0]);
  $from_year = $from_date[2];
  $to_month = $hijari->monthName($to_date[0]);
  $to_year = $to_date[2];
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print hub receipt</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="asset/dist/css/print.css" media="print">
    <style type="text/css">
      @media all {
        body{font-size: 14px; }
        .page-break  { display: none; }
      }

      @media print {
        .page-break  { display: block; page-break-before: always; }
      }
    </style>
    <script type="text/javascript" src="asset/dist/js/NumberToWord.js"></script>
  </head>
  <body onload="NumberToWord(<?php echo $rcpt['amount']; ?>);
      window.print();">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8 ">
        <div style="text-align: center;" class="col-md-12">
          <img src="includes/logo.jpg" width="400px">
        </div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">
          <span class="pull-left col-md-4"><strong>Date</strong> : <u><?php echo date('d/m/Y', $rcpt['date']); ?></u></span>
          <span class="col-md-3"><strong>Home Sabeel No.</strong> : <u><?php echo $rcpt['FileNo']; ?></u></span>
          <span class="pull-right col-md-4"><strong>Receipt No.:</strong> <u><?php echo $rcpt['receipt_no']; ?></u></span>
        </div>
        
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        
        <div class="col-md-12">
          <span class="pull-left col-md-4"><strong>Tanzeem</strong> : <u><?php echo ucfirst($family['Mohallah']); ?></u></span>
          <span class="col-md-4"><strong>HOF Mobile No.</strong> :<u><?php echo $family['MPh']; ?></u></span>
        </div>
        
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        
        <div class="col-md-12">
          <span class="pull-left col-md-8"><strong>Paid from</strong> : <u><?php echo $from_month . '-' . $from_year . 'H TO ' . $to_month . '-' . $to_year . 'H'; ?></u></span>
          <?php if ($rcpt['partial_ids'] != '') { ?>
            <span class="pull-right col-md-4"><strong>Partial IDS</strong> : <u><?php echo $rcpt['partial_ids']; ?></u></span>
          <?php } ?>
        </div>
        
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        
        <div class="col-md-12">
          <span class="pull-left col-md-7"><strong>HOF Name</strong> :<u><?php echo ucwords($name); ?></u></span>
          <span class="pull-right col-md-4"><strong>HOF ITS ID</strong> : <u><?php
              if ($family['ejamatID'] > '0')
                echo $family['ejamatID'];
              else
                echo '';
              ?></u></span>
        </div>
        
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        
        <div class="col-md-12">
          <span class="pull-left col-md-6">
            <strong>Monthly Hub</strong> : <u><?php echo number_format($hub_details['hub_raqam']) . '/-'; ?></u>
          </span>
        </div>
        
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        
        <div class="col-md-12">
          <span class="pull-left col-md-6">BAAD SALAAM,</span>
        </div>
        
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        
        <div class="col-md-12">
          <span class="pull-left col-md-12">RECEIVED CASH WITH THANKS RS. <u><?php echo '<strong>' . number_format($rcpt['amount']) . '/-</strong> '; ?><span id="amount"></span></u>
        </div>
        
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        
        <div class="col-md-12">
          <span class="pull-left col-md-7"><strong>Paid by(Name)</strong> : <u><?php echo ucfirst($family['HOF']); ?></u></span>
          <span class="pull-right col-md-4"><strong>Paid by (ITS ID)</strong> : <u></u></span>
        </div>
        
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        
        <div class="col-md-12">
          <span class="pull-left col-md-4">AS VOLUNTARY CONTRIBUTION.</span>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-5">E & O.E.</span>
          <span class="pull-right col-md-4">ABDE SYEDNA(T.U.S)</span>
        </div>

        <!--div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div style="text-align: center;" class="col-md-12">
          <img src="includes/logo.jpg" width="400px">
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-4"><strong>Date</strong> : <u><?php echo date('d/m/Y', $rcpt['date']); ?></u></span>
          <span class="col-md-3"><strong>Home Sabeel No.</strong> : <u><?php echo $rcpt['FileNo']; ?></u></span>
          <span class="pull-right col-md-4"><strong>Receipt No.:</strong> <u><?php echo $rcpt['receipt_no']; ?></u></span>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-4"><strong>Tanzeem</strong> : <u><?php echo ucfirst($family['Mohallah']); ?></u></span>
          <span class="col-md-4"><strong>HOF Mobile No.</strong> :<u><?php echo $family['MPh']; ?></u></span>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-8"><strong>Paid from</strong> : <u><?php echo $from_month . '-' . $from_year . 'H TO ' . $to_month . '-' . $to_year . 'H'; ?></u></span>
          <?php if ($rcpt['partial_ids'] != '') { ?>
            <span class="pull-right col-md-4"><strong>Partial IDS</strong> : <u><?php echo $rcpt['partial_ids']; ?></u></span>
          <?php } ?>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-7"><strong>HOF Name</strong> :<u><?php echo ucfirst($rcpt['name']); ?></u></span>
          <span class="pull-right col-md-4"><strong>HOF ITS ID</strong> : <u><?php
              if ($family['ejamatID'] > '0')
                echo $family['ejamatID'];
              else
                echo '';
              ?></u></span>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-6">
            <strong>Monthly Hub</strong> : <u><?php echo number_format($hub_details['hub_raqam']) . '/-'; ?></u>
          </span>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-6">BAAD SALAAM,</span>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-12">RECEIVED CASH WITH THANKS RS. <u><?php echo '<strong>' . number_format($rcpt['amount']) . '/-</strong> '; ?><span id="amount1"></span></u>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-7"><strong>Paid by(Name)</strong> : <u><?php echo ucfirst($family['HOF']); ?></u></span>
          <span class="pull-right col-md-4"><strong>Paid by (ITS ID)</strong> : <u></u></span>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-4">AS VOLUNTARY CONTRIBUTION.</span>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-5">E & O.E.</span>
          <span class="pull-right col-md-4">ABDE SYEDNA(T.U.S)</span>
        </div>

        <div class="col-md-12">&nbsp;</div-->
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <a href="index.php" class="btn btn-info pull-right">Back to Home</a><br><br>
        </div>

        <div class="col-md-12">
          <a href="hub_receipt.php" class="btn btn-info pull-right">Back to Receipt</a>
        </div>

      </div>

      <!-- /Center Bar -->

      <!-- Right Bar -->

      <!-- /Right Bar -->

    </div>
    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
