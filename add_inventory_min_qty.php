<?php
include('session.php');
require_once('classes/class.database.php');
require_once("classes/class.receipt.php");
require_once('classes/class.product.php');

$cls_receipt = new Mtx_Receipt();
$cls_product = new Mtx_Product();

if (isset($_POST['add_inventory'])) 
{
  $result = $cls_receipt->add_min_qty_of_item_in_inventory($_POST['item'], $_POST['quantity'], $_POST['unit']);
  
  if ($result){
    $_SESSION[SUCCESS_MESSAGE] = 'Min Quantity in Inventory added successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Something went wrong';
  }
}

$items = $cls_product->get_all_ingredients();

$title = 'Add Min Quantity of Inventory';
$active_page = 'menu';

require_once 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div></div>

            <div class="form-group">
              <label class="control-label col-md-4">Item Name</label>
              <div class="col-md-4">
                <select class="form-control" id="item" name="item">
                  <option value="">--Select One--</option>
                  <?php
                  if ($items) {
                    foreach ($items as $item) {
                      ?>
                      <option value="<?php echo $item['name']; ?>"><?php echo $item['name']; ?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Quantity</label>
              <div class="col-md-4">
                <input type="text" id="quantity" name="quantity" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Unit</label>
              <div class="col-md-4">
                <select id="unit" name="unit" class="form-control">
                  <option value="">Select Unit</option>
                  <option value="kgs">Kilogram</option>
                  <option value="ltrs">Litre</option>
                  <option value="pcs">Pieces</option>
                  <option value="pkts">Packet</option>
                  <option value="boxes">Box</option>
                  <option value="btls">Bottle</option>
                  <option value="carba">Carba</option>
                  <option value="tin">Tin</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4"></label>
              <button class="btn btn-success" type="submit" name="add_inventory" id="add">Add</button>
            </div>

          </form>
        </div>
        <!-- /Center Bar -->
        <script>
          $('#add').click(function() {
            var item = $('#item').val();
            var qty = $('#quantity').val();
            var unt = $('#unit').val();
            var error = '';
            var validate = true;
            if (item == '')
            {
              error += 'Please select Item Name\n';
              validate = false;
            }
            if (qty == '')
            {
              error += 'Please enter Quantity\n';
              validate = false;
            }
            if (unt == '')
            {
              error += 'Please select Unit\n';
              validate = false;
            }
            if (validate == false)
            {
              alert(error);
              return validate;
            }
          });
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>