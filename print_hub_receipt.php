<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.receipt.php');
require_once('classes/class.user.php');
require_once('classes/hijri_cal.php');
require_once('includes/inc.num2words.php');
$cls_user = new Mtx_User();
$cls_family = new Mtx_family();
$hijari = new HijriCalendar();
$cls_receipt = new Mtx_Receipt();
$user_id = $_SESSION[USER_ID];
//form
if (isset($_POST['print'])) {
  $thali_id = $_POST['FileNo'];
  $thali_name = $_POST['thali_name'];
  $year = $_POST['takhmeen_year_hub_rcpt'];
  $amount = $_POST['amount'];
  $ref_no = (int) $_POST['ref_no'];
  $timestamp = $_POST['timestamp'];
  $form_id = $_POST['form_id'];
  $payment_type = $_POST['type'];
  $bank_name = $_POST['bankname'];
  $chk_no = $_POST['chk_no'];
  $chk_date = (int) $_POST['chk_date'];
  $split =  explode('-', $chk_date);
  $chk_date = mktime(0, 0, 0, $split[1], $split[2], $split[0]);
  $insertion = $cls_receipt->add_hub_receipt_record($thali_id, $thali_name, $year, $amount, $ref_no, $timestamp, $form_id, $payment_type, $bank_name, $chk_no, $chk_date);
  $sms = isset($_POST['sms']) ? TRUE : FALSE;
  $mobileNumber = $_POST['mobile'];
  
  if($sms) {
    $query = "UPDATE `family` SET `MPh` = '$mobileNumber' WHERE `FileNo` LIKE '$thali_id'";
    $result = $database->query($query);
    
    $msg = <<<MSG
            RECEIVED CASH FROM {$thali_id} FOR YEAR {$year} RS. {$amount} AND Receipt No. {$insertion}.
MSG;
    if($mobileNumber != '' || $mobileNumber > 0) $send = $cls_family->send_sms($mobileNumber, trim($msg));
    else $send = FALSE;
  }
  if($insertion) {
    header('Location: print_hub_receipt.php?id=' . $insertion);
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing the request..';
    header('Location: hub_receipt.php');
    exit();
  }
}

//////////////start
if (isset($_GET['id'])) {
  $rcpt_id = $_GET['id'];
  $hub_rcd = $cls_family->get_fmb_hub_record($rcpt_id);
  $paid_amount = $hub_rcd['amount'];
  $thali_id = $hub_rcd['FileNo'];
  $year = $hub_rcd['year'];
  $date = $hub_rcd['date'];
  $record = $cls_family->get_takhmeen_record($thali_id, FALSE, "AND `year` = '$year'");
  $yearly_takhmeen = $record[0]['amount'];
  $paid = $cls_family->get_paid_amount($thali_id, $year);
  $pending = $yearly_takhmeen - $paid;
  $family = $cls_family->get_single_family($thali_id);
  $name = $cls_family->get_name($thali_id);
}
$setting = $cls_user->get_general_settings();
$db_tanzeem_name = $setting[0]['tanzeem_name'];

?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print hub receipt</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="asset/dist/css/print.css" media="print">
    <style type="text/css">
      @media all {
        body{font-size: 14px; }
        .page-break  { display: none; }
      }

      @media print {
        .page-break  { display: block; page-break-before: always; }
      }
    </style>
  </head>
  <body onload="window.print();">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8 ">
        <div style="text-align: center;" class="col-md-12">
          <img src="includes/logo.jpg" width="200px">
        </div>
        <div class="col-md-12" style="text-align: center;">
          <h2><strong><?php echo $db_tanzeem_name; ?></strong></h2>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12">
          <span class="pull-left col-md-4"><strong>Date</strong> : <u><?php echo str_replace(':', '/', $date); ?></u></span>
          <span class="col-md-3"><strong><?php echo THALI_ID; ?></strong> : <u><?php echo $thali_id; ?></u></span>
          <span class="pull-right col-md-4"><strong>Receipt No.:</strong> <u><?php echo $rcpt_id; ?></u></span>
        </div>

        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-4"><strong>Tanzeem</strong> : <u><?php echo $family['Mohallah']; ?></u></span>
          <span class="col-md-4"><strong>HOF Mobile No.</strong> :<u><?php echo $family['MPh']; ?></u></span>
        </div>

        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-12"><strong>For Years</strong> : <u><?php echo $year; ?></u></span>
        </div>

        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-7"><strong>HOF Name</strong> :<u><?php echo $hub_rcd['name']; ?></u></span>
          <span class="pull-right col-md-4"><strong>HOF ITS ID</strong> : <u><?php
              if ($family['ejamatID'] > 0)
                echo $family['ejamatID'];
              else
                echo '';
              ?></u></span>
        </div>

        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-7">
            <strong>Yearly Takhmeen</strong> : <u><?php echo number_format($yearly_takhmeen, 2, '.', ',');?></u>
          </span>
          <span class="pull-right col-md-4">
            <strong>Pending Amount</strong> : <u><?php echo number_format($pending, 2, '.', ',');?></u>
          </span>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-6">BAAD SALAAM,</span>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <?php if($hub_rcd['payment_type'] == '' || $hub_rcd['payment_type'] == 'cash') $type = 'CASH';?>
        <?php if($hub_rcd['payment_type'] == 'cheque') $type = 'CHEQUE';?>
        <?php if($hub_rcd['payment_type'] == 'neft') $type = 'NEFT';?>
        <div class="col-md-12">
          <span class="pull-left col-md-12">RECEIVED <?php echo $type; ?> WITH THANKS <strong>RS. <u><?php echo number_format($paid_amount, 2, '.', ',') . ' ' . num2words($paid_amount) . ' Only'; ?></u></strong>
        </div>
        
<?php
      switch ($hub_rcd['payment_type']) {
        case 'cheque': $card = 'Cheque'; break;
        case 'neft': $card = 'Neft'; break;
        default : $card = 'Cash'; break;
      }
      if($hub_rcd['payment_type'] == 'cheque' || $hub_rcd['payment_type'] == 'neft') {
  ?>
          <div class="col-md-12">
            <span class="col-md-4"><strong>Bank Name</strong> :<u><?php echo ucwords($hub_rcd['bank_name']); ?></u></span>
            <span class="pull-left col-md-4"><strong><?php echo $card;?> No.</strong> : <u><?php echo $hub_rcd['cheque_no']; ?></u></span>
            <span class="col-md-4"><strong><?php echo $card;?> Date</strong> :<u><?php echo date('d/m/Y', $hub_rcd['cheque_date']); ?></u></span>
          </div>
      <?php } ?>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-4">AS VOLUNTARY CONTRIBUTION.</span>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <span class="pull-left col-md-5">E & O.E.</span>
          <span class="pull-right col-md-4">ABDE SYEDNA(T.U.S)</span>
        </div>

        <div class="col-md-12">&nbsp;</div>
        <div class="col-md-12">&nbsp;</div>

        <div class="col-md-12">
          <a href="index.php" class="btn btn-info pull-right no-print">Back to Home</a><br><br>
        </div>

        <div class="col-md-12">
          <a href="hub_receipt.php" class="btn btn-info pull-right no-print">Back to Receipt</a><br><br>
        </div>
        <?php
        if($_SESSION[USER_TYPE] == 'A'){
          if($hub_rcd['cancel'] == 0){
        ?>
        <div class="col-md-12">
          <a href="cancel_receipt.php?rid=<?php echo $rcpt_id;?>" class="btn btn-danger pull-right no-print">Cancel Receipt</a><br><br>
        </div>
        <?php }} ?>
      </div>

      <!-- /Center Bar -->

      <!-- Right Bar -->

      <!-- /Right Bar -->

    </div>
    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
