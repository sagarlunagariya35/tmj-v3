<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.menu.php');
require_once('classes/class.receipt.php');

$title = 'Idarah Report';
$active_page = 'report';
require_once 'includes/header.php';

$cls_receipt = new Mtx_Receipt();
$cls_menu = new Mtx_Menu();

$fromDate = $from_date = $to_date = $toDate = FALSE;
$btn_print = TRUE;
$post = FALSE;

if (isset($_GET['search'])) {
  $post = TRUE;
  $from_date = $_GET['from_date'];
  $to_date = $_GET['to_date'];
  $fdate = explode('-', $from_date);
  $fromDate = mktime(0, 0, 0, $fdate[1], $fdate[2], $fdate[0]);
  $tdate = explode('-', $to_date);
  $toDate = mktime(23, 59, 59, $tdate[1], $tdate[2], $tdate[0]);
  $menus = $cls_menu->get_act_cost_between_dates_from_finalize($fromDate, $toDate);

  $opening_data = $cls_receipt->get_balance_by_date($from_date);
  $closing_data = $cls_receipt->get_balance_by_date($to_date);

  $opening_cash = $opening_data[0]['opening_cash'];
  $opening_bank = $opening_data[0]['opening_bank'];
  $closing_cash = $closing_data[0]['closing_cash'];
  $closing_bank = $closing_data[0]['closing_bank'];
}

$btn_print_link = "#print_costing_report.php?from_date=$from_date&to_date=$to_date";

$setting = $cls_user->get_general_settings();

//Income
$credit_zabihat_inayat = $cls_receipt->get_vol_rcpt_from_sub_heads($setting[0]['zabihat_inayat'], $fromDate, $toDate);
$credit_mumineen_contri = $cls_receipt->get_vol_rcpt_from_sub_heads($setting[0]['mumineen_contri'], $fromDate, $toDate);
$hub_amount = $cls_receipt->get_hub_sum($from_date, $to_date);
$credit_voucher_total = $cls_receipt->get_credit_total_between_dates($from_date, $to_date);
$receipts_bills = $cls_receipt->get_vol_rcpt_detail_from_not_in_sub_heads($setting[0]['zabihat_inayat'], $setting[0]['mumineen_contri'], $fromDate, $toDate);

//Expences
$bill_amount = $cls_receipt->get_unpaid_bills_amount($fromDate, $toDate);
$debit_amount = $cls_receipt->get_debit_total_between_dates($from_date, $to_date);
$direct_purchase_amount = $cls_receipt->get_unpaid_direct_purchase_amount($fromDate, $toDate);

if($from_date < '2015-09-15'){
  $total_fly = 402;
}else if($from_date > '2015-10-14') {
  $total_fly = $cls_family->get_total_family();
}else {
  $total_fly = 450;
}
$other_expences = 2400;

$page_number = IDARAH_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
<?php
include 'includes/inc_left.php';
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?php echo $title; ?></h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Reports</a></li>
      <li><a href="#">Thali</a></li>
      <li class="active"><?php echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12">&nbsp;</div>

      <!-- Center Bar -->
      <div class="col-md-12">
        <?php include 'includes/inc.dates.php'; ?>
        <div class="col-md-12">&nbsp;</div>
        <?php if (isset($_GET['search'])) { ?>

          <div class="col-md-12">
            <div class="col-md-6 alert-success">
              <label>Opening Cash : </label><span class="pull-right"><?php if ($opening_cash) echo number_format($opening_cash, 2);
              else echo 0; ?></span><br>
              <label>Opening Bank : </label><span class="pull-right"><?php if ($opening_bank) echo number_format($opening_bank, 2);
              else echo 0; ?></span><br>
            </div>
            <div class="col-md-6 alert-success">
              <label>Closing Cash : </label><span class="pull-right"><?php if ($closing_cash) echo number_format($closing_cash, 2);
              else echo 0; ?></span><br>
              <label>Closing Bank : </label><span class="pull-right"><?php if ($closing_bank) echo number_format($closing_bank, 2);
              else echo 0; ?></span><br>
            </div>
            <div class="col-md-6 alert-success">
              <label>Opening Balance : </label><span class="pull-right"><?php echo number_format(($opening_cash + $opening_bank), 2); ?></span>
            </div>
            <div class="col-md-6 alert-success">
              <label>Closing Balance : </label><span class="pull-right"><?php echo number_format(($closing_cash + $closing_bank), 2); ?></span>
            </div>
          </div>
        
          <div class="col-md-12">
            <div class="col-md-6 alert-info">
              <label>Zabihat Inayat : </label><span class="pull-right"><?php echo number_format($credit_zabihat_inayat['Amount'], 2); ?></span><br>
              <label>Mumineen Contri.(Zabihat) : </label><span class="pull-right"><?php echo number_format($credit_mumineen_contri['Amount'], 2); ?></span><br>
              <label>FMB Hub : </label><span class="pull-right"><?php echo number_format($hub_amount['total'], 2); ?></span><br>
              <label>Other Credits(Salawat, Fatema hub, etc.) : </label><span class="pull-right"><?php echo number_format(($credit_voucher_total[0]['Amount'] + $receipts_bills), 2); ?></span><br><br>
              <label>Total Incomes : </label><span class="pull-right"><?php echo number_format(($credit_zabihat_inayat['Amount'] + $credit_mumineen_contri['Amount'] + $hub_amount['total']), 2); ?></span>
            </div>
            
            <div class="col-md-6 alert-warning">
              <label>Account Bills : </label><span class="pull-right"><?php echo number_format($bill_amount, 2); ?></span><br>
              <label>Debit Vouchers : </label><span class="pull-right"><?php echo number_format($debit_amount[0]['Amount'], 2); ?></span><br>
              <label>Direct Purchases : </label><span class="pull-right"><?php echo number_format($direct_purchase_amount, 2); ?></span><br><br><br>
              <label>Total Expences : </label><span class="pull-right"><?php echo number_format(($bill_amount + $debit_amount[0]['Amount'] + $direct_purchase_amount), 2); ?></span>
            </div>
          </div>

          <div class="col-md-12"><h3>Thali Costing Reports</h3></div>
          <div class="col-md-12">
            <table class="table table-hover table-condensed table-bordered">
              <thead>
                <tr>
                  <th>Sr No.</th>
                  <th>Date</th>
                  <th class="text-right">Inventory Cost</th>
                  <th class="text-right">Other Expenses</th>
                  <th class="text-right">Total</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if ($menus) {
                  $i = 1;
                  $ary_avg_cost = array();
                  $total_expenses = 0;
                  foreach ($menus as $menu) {
                    $act_cost = $menu['act_cost'];
                    if ($act_cost > 0) {
                      $total = $act_cost + $other_expences;
                      array_push($ary_avg_cost, $total);
                      $total_expenses += $total;
                    }
                    ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo date('d, M Y', $menu['timestamp']); ?></td>
                      <td class="text-right"><?php echo number_format($act_cost, 2); ?></td>
                      <td class="text-right"><?php echo number_format($other_expences, 2); ?></td>
                      <td class="text-right"><?php echo number_format($total, 2); ?></td>
                    </tr>
    <?php }
    ?>
                  <tr>
                    <td colspan="5" class="alert-info"><strong>Total Expenses: <?php echo number_format($total_expenses, 2); ?></strong></td>
                  </tr>
                  <tr>
                    <td colspan="5" class="alert-warning"><strong>Average Per Day Cost: <?php echo number_format(($total_expenses / count($ary_avg_cost)), 2); ?></strong></td>
                  </tr>
                  <tr>
                    <td colspan="5" class="alert-info"><strong>Total Thali: <?php echo $total_fly; ?></strong></td>
                  </tr>
                  <tr>
                    <td colspan="5" class="alert-warning"><strong>Average Per Thali Cost: <?php echo number_format((($total_expenses / count($ary_avg_cost)) / $total_fly), 2); ?></strong></td>
                  </tr>

  <?php } else {
    ?>
                  <tr>
                    <td colspan="4" class="alert-danger">No results found.</td>
                  </tr>
  <?php } ?>
              </tbody>
            </table>
          </div>          
<?php } ?>
      </div>
      <!-- /Center Bar -->

    </div>
    <!-- /Content -->
  </section>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>