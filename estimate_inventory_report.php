<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.product.php');
require_once('classes/class.menu.php');
require_once('classes/hijri_cal.php');
$cls_product = new Mtx_Product();
$cls_menu = new Mtx_Menu();
$bool_available = 1;
$user_id = $_SESSION[USER_ID];
$cmd = '';
$post = FALSE;

if (isset($_POST['confirm'])) {
  $post = TRUE;
  // if bool_available is 0 thn there is or are quantity unavailable in inventory issue
  if ($_POST['Available'] == 0) {
    $_SESSION[ERROR_MESSAGE] = 'Sorry! You cant issue in inventory when available is NO';
    $items = $cls_product->get_estimated_issue_item_in_current_day($_POST['date']);
    (empty($items)) ? $finalized = TRUE : $finalized = FALSE;
    $purchases = $cls_product->get_estimated_issue_direct_purchase($_POST['date']);
  } else {
    $finalized = $cls_product->finalize_inventory_issue($_POST['date'], $user_id, 'estimate', $_POST['qty']);
    $items = $cls_product->get_estimated_issue_item_in_current_day($_POST['date']);
    $purchases = $cls_product->get_estimated_issue_direct_purchase($_POST['date']);
    $directItems = $cls_product->get_issued_direct_purchase($_POST['date']);
    $cmd = 'issued';
  }
}

if (isset($_REQUEST['date'])) {

  $post = TRUE;
  $items = $cls_product->get_estimated_issue_item_in_current_day($_REQUEST['date']);
  //(empty($items)) ? $confirmed = TRUE : $confirmed = FALSE;
  $purchases = $cls_product->get_estimated_issue_direct_purchase($_REQUEST['date']);
  $directItems = $cls_product->get_issued_direct_purchase($_REQUEST['date']);
  $finalized = $cls_product->check_est_item_exist_in_issue($_REQUEST['date']);
  if ($finalized) {
    $cmd = 'issued';
  } else {
    $cmd = 'not issued';
  }
}

$array = array();
if (isset($directItems) && $directItems != FALSE) {
  foreach ($directItems as $key => $val) {
    $array[$val['item_name']] = $val['quantity'];
  }
}

$condition = isset($_REQUEST['date']) && !empty($_REQUEST['date']);
if ($condition) {
  $dsp = explode('-', $_REQUEST['date']);
  $start = mktime(0, 0, 0, $dsp[1], $dsp[2], $dsp[0]);
  $end = mktime(23, 59, 59, $dsp[1], $dsp[2], $dsp[0]);
  $menu_ids = $cls_menu->get_menu_between_dates($start, $end);
  $ids = $menu_ids[0]['menu_id'];
  if ($menu_ids[0]['custom_menu'] != '')
    $menu_name = $menu_ids[0]['custom_menu'];
  else
    $menu_name = $cls_menu->get_base_menu_name($ids);
}

$title = 'Estimate inventory issue report';
$active_page = 'report';

require_once 'includes/header.php';

$page_number = REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Inventory</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <div class="form-group">
              <label class="col-md-3 control-label">From</label>
              <div class="col-md-4">
                <input type="date" name="date" class="form-control" id="date" placeholder="Date of Birth" value="<?php
                if ($condition)
                  echo $_REQUEST['date'];
                //else if(isset ($_GET['date'])) echo $_GET['date'];
                ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">&nbsp;</label>
              <div class="col-md-4">
                <input type="submit" class="btn btn-success" name="search" id="search" value="Search">
                <a target="_blank" id="print" href="print_estimate_inventory_report.php?date=<?php if ($condition) echo $_REQUEST['date']; ?>" class="btn btn-primary <?php echo!$post ? 'disabled' : ''; ?>">Print</a>
              </div>
            </div>
          </form>
          <script>
            $('#search, #print').click(function() {
              var from_date = $('#date').val();
              if (from_date == '')
              {
                alert('Please select date first..')
                return false;
              }
            });
          </script>
          <div class="col-md-12">&nbsp;</div>
          <?php if ($condition) { ?>
            <div class="col-md-8 alert-info" style="padding: 6px; border-radius: 8px;border-color: #bce8f1;">
              <span class="pull-left"><strong>Menu: </strong><?php echo $menu_name; ?></span>
              <span class="pull-right"><strong>Date: </strong><?php echo $_REQUEST['date']; ?></span>
            </div>
            <div class="col-md-4 <?php echo $finalized ? 'alert-success' : 'alert-danger'; ?> text-center" style="padding: 6px; border-radius: 8px;">
              <span><strong>Inventory: </strong><?php echo $finalized ? 'Issued.' : 'Not Issued.'; ?></span>
            </div>
            <div class="col-md-12">&nbsp;</div>
          <?php } ?>
          <?php
          if (isset($_REQUEST['date']) OR isset($_POST['confirm'])) {
            ?>
            <div class="col-md-12">

              <table class="table table-hover table-condensed table-bordered">
                <thead>
                  <tr>
                    <?php
                    if($cmd != 'issued') {
                    ?>
                    <th colspan="4">Inventory Issue</th>
                    <?php
                    } else {
                    ?>
                    <th colspan="5">Inventory Issue</th>
                    <?php
                    }
                    ?>
                  </tr>
                  <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th class="text-right">Quantity</th>
                    <th class="text-right">Unit</th>
                    <?php
                    if ($cmd != 'issued') {
                    ?>
                    <th>Available</th>
                    <?php
                    }
                    ?>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i = 1;
                  if ($items) {
                    echo '<form method="post" role="form" class="form-horizontal">';
                    foreach ($items as $item) {
                      $available = $cls_product->get_qty_available_in_inventory($item['inventory_id']);
                      if ($cmd != 'issued') {
                        if ($item['quantity'] > $available) {
                          $bool_available = 'N';
                        }
                      } else
                        $available = 'Y';
                      ?>
                      <tr <?php echo ($cmd == 'issed' || $available >= $item['quantity']) ? 'class="alert alert-success"' : 'class="alert alert-danger"'; ?>>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $item['item']; ?></td>
                        <td class="text-right">
                          <?php
                          if ($finalized) {
                            echo number_format($item['quantity'], 3);
                          } else {
                            ?>
                            <input type="text" value="<?php echo number_format($item['quantity'], 3); ?>" class="form-control" name="qty[]">
                          <?php } ?>
                        </td>
                        <td class="text-right"><?php echo $item['unit']; ?></td>
                        <?php
                        if ($cmd != 'issued') {
                          ?>
                          <td>(<?php echo number_format($available, 3) . ' ' . $item['unit']; ?>)</td>
                          <?php
                        }
                        ?>
                      </tr>
                    <?php } ?>
                    <?php
                    if (!$finalized) {
                      echo '<tr><td colspan="5"><input type="submit" name="confirm" id="confirm" value="Confirm Issue" onclick="return confirmed();" class="btn btn-success"></td></tr>';
                    }
                    echo '<input type="hidden" name="Available" value="' . $bool_available . '">';
                    echo '<input type="hidden" name="date" value="' . $_REQUEST['date'] . '">';
                    echo '</form>';
                  } else {
                    ?>
                    <tr>
                      <td colspan="5" class="alert-danger">No results found</td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
<!--            <div class="col-md-6">
              <table class="table table-hover table-condensed table-bordered">
                <thead>
                  <tr>
                    <th colspan="6">Direct Purchase</th>
                  </tr>
                  <tr>
                    <th>Sr. No.</th>
                    <th>Item Name</th>
                    <th class="text-right">Required Quantity</th>
                    <th class="text-right">Purchased Quantity</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $k = 1;
                  if ($purchases) {
                    foreach ($purchases as $purchase) {
                      if (array_key_exists($purchase['item'], $array))
                        $color = 'alert-success';
                      else
                        $color = 'alert-danger';
                      ?>
                      <tr class="<?php echo $color; ?>">
                        <td><?php echo $k++; ?></th>
                        <td><?php echo $purchase['item']; ?></td>
                        <td class="text-right"><?php echo number_format($purchase['qty'], 3); ?></td>
                        <td class="text-right"><?php echo (isset($array[$purchase['item']])) ? number_format($array[$purchase['item']], 3) : '0.000'; ?></td>
                      </tr>
                    <?php } ?>
                  <?php } else {
                    ?>
                    <tr>
                      <td colspan="4" class="alert-danger">No results found</td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>-->

          </div>
          <script>
            function confirmed() {
              var dt = $('#date').val();
              window.location.href = 'estimate_inventory_report.php?date=' + dt + '&available=<?php echo $bool_available; ?>';
            }
          </script>
        <?php } ?>
      </div>
      <!-- /Center Bar -->
    </section>
  </div>
  <!-- /Content -->
  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>