<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.product.php');
$cls_product = new Mtx_product();

$print = TRUE;
$sub = '';
$result = $cls_product->get_all_ingredients();
$title = 'Print List of Inventory';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="asset/dist/css/print.css" media="print">
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8 ">
        <div>
          <strong>Inventory<span class="pull-right"><?php echo date('d F,Y'); ?></span></strong>
        </div>
        <?php
        include('includes/inc.inventory.php');
        ?>
      </div>
    </div>
    <!-- /Content -->
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
