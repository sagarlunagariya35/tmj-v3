<div class="panel panel-default">
  <div class="panel-heading"><h3 class="panel-title">Links</h3></div>
  <div class="panel-body">
    <ul class="nav nav-pills nav-stacked">
      <li class="<?php ($pg_link == 'hub') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_HUB_RECEIPT, $rights) OR $rights[ARY_HUB_RECEIPT] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="hub_receipt.php">FMB Hub<?php ($pg_link == 'hub') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li>
      <li class="<?php ($pg_link == 'Volun. Contri.') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_VOLUNTARY_RECEIPT, $rights) OR $rights[ARY_VOLUNTARY_RECEIPT] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="voluntary_rcpt.php">Volun. Contri.<?php ($pg_link == 'Volun. Contri.') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li>
      <li class="<?php ($pg_link == 'credit_voucher') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_CREDIT_VOUCHER_ADD, $rights) OR $rights[ARY_CREDIT_VOUCHER_ADD] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="credit_voucher.php">Credit voucher<?php ($pg_link == 'credit_voucher') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li>
      <!--li class="<?php ($pg_link == 'Partial Payments') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_PARTIAL_RECEIPT, $rights) OR $rights[ARY_PARTIAL_RECEIPT] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="partial_rcpt.php">Partial Payments<?php ($pg_link == 'Partial Payments') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li-->
      <!--li class="<?php ($pg_link == 'Sabil Home') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_SABIL_HOME_RECEIPT, $rights) OR $rights[ARY_SABIL_HOME_RECEIPT] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="sabil_home.php">Sabil Home Receipt<?php ($pg_link == 'Sabil Home') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li-->
      <!--li class="<?php ($pg_link == 'Sabil Vepaar') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_SABIL_VEPAAR_RECEIPT, $rights) OR $rights[ARY_SABIL_VEPAAR_RECEIPT] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="sabil_vepaar.php">Sabil Vepaar Receipt<?php ($pg_link == 'Sabil Vepaar') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li-->
      <li class="<?php ($pg_link == 'hub_rcpt_book') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_HUB_BOOK, $rights) OR $rights[ARY_HUB_BOOK] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="hub_rcpt_book.php">Hub Receipt Book<?php ($pg_link == 'hub_rcpt_book') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li>
      <li class="<?php ($pg_link == 'voluntary_rcpt_book') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_VOLUNTARY_BOOK, $rights) OR $rights[ARY_VOLUNTARY_BOOK] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="voluntary_rcpt_book.php">Vol. Receipt Book<?php ($pg_link == 'voluntary_rcpt_book') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li>
      <li class="<?php ($pg_link == 'book_credit') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_CREDIT_VOUCHER_BOOK, $rights) OR $rights[ARY_CREDIT_VOUCHER_BOOK] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="book_credit.php">Credit Voucher Book<?php ($pg_link == 'book_credit') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li>
      <!--li class="<?php ($pg_link == 'partial_rcpt_book') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_PARTIAL_BOOK, $rights) OR $rights[ARY_PARTIAL_BOOK] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="partial_rcpt_book.php">Partial Receipt Book<?php ($pg_link == 'partial_rcpt_book') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li>
      <li class="<?php ($pg_link == 'sabil_home_rcpt_book') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_SABIL_HOME_BOOK, $rights) OR $rights[ARY_SABIL_HOME_BOOK] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="sabil_home_rcpt_book.php">Sabil Home Receipt Book<?php ($pg_link == 'sabil_home_rcpt_book') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li>
      <li class="<?php ($pg_link == 'sabil_vepaar_rcpt_book') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_SABIL_HOME_BOOK, $rights) OR $rights[ARY_SABIL_HOME_BOOK] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="sabil_vepaar_rcpt_book.php">Sabil Vepaar Receipt Book<?php ($pg_link == 'sabil_vepaar_rcpt_book') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li-->
      <li class="<?php ($pg_link == 'add_bill') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_BILL_PAYMENTS, $rights) OR $rights[ARY_BILL_PAYMENTS] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="add_bill.php">Bill Payment<?php ($pg_link == 'add_bill') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li>
      <li class="<?php ($pg_link == 'debit_voucher') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_DEBIT_VOUCHER_ADD, $rights) OR $rights[ARY_DEBIT_VOUCHER_ADD] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="debit_voucher.php">Debit Voucher<?php ($pg_link == 'debit_voucher') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li>
      <li class="<?php ($pg_link == 'grocery_bill_book') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_BILL_BOOK, $rights) OR $rights[ARY_BILL_BOOK] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="grocery_bill_book.php">Bill Book<?php ($pg_link == 'grocery_bill_book') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li>
      <li class="<?php ($pg_link == 'book_debit') ? $class = "active" : $class = '';
      if($_SESSION[USER_TYPE] != 'A'){
        if(!array_key_exists(ARY_DEBIT_VOUCHER_BOOK, $rights) OR $rights[ARY_DEBIT_VOUCHER_BOOK] != 1){
          $class .= ' disabled';
        }
      }
      echo $class;?>"><a href="book_debit.php">Debit Voucher Book<?php ($pg_link == 'book_debit') ? $pointer = '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : $pointer = ''; echo $pointer;?></a></li>
    </ul>
  </div>
</div>
