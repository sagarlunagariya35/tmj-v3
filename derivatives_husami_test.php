<?php
require_once 'classes/class.database.php';
require_once 'classes/class.quadratic_derivatives.php';
require_once 'classes/functions.php';

$ary_x = array(50, 100, 500);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
    <?php
    echo '<br>';
    $filename = '10th_muharram.csv';
    $result = csv_array($filename, ',');

    if (file_exists('wdata/10th_muharram.csv')) {
      unlink('wdata/10th_muharram.csv');
    }
    $fh = fopen('wdata/10th_muharram.csv', 'a');

    foreach ($result as $row) {
      if ($row[2] != 0) {
        $ary_y = array($row[2], $row[3], $row[4]);
        $step = $row[5];
        
        global $database;
        $query = "SELECT `id` FROM `inventory` WHERE `name` = '$row[0]'";
        $result = $database->query_fetch_full_result($query);
  
        $id = $result[0]['id'];
        $calc = new Mtx_Quad_derivative($ary_x, $ary_y, $step);

        $a = $calc->get_a();
        $b = $calc->get_b();
        $c = $calc->get_c();

        echo "Item: $row[0], Unit: $row[1]<br> a: $a<br>b: $b<br>c: $c<br>";
        fwrite($fh, "$row[0],$row[1],$a,$b,$c,$step,$id\n");
      }
    }

    function csv_array($filename = '', $delimiter = ',') {
      if (!file_exists($filename) || !is_readable($filename))
        return FALSE;

      $header = NULL;
      $data = array();
      if (($handle = fopen($filename, 'r')) !== FALSE) {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
          if (!$header)
            $header = $row;
          else
            $data[] = $row;
        }
        fclose($handle);
      }
      return $data;
    }
    ?>
  </body>
</html>