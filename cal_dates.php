<?php
include 'session.php';
$page_number = 59;
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.menu.php');
$cls_family = new Mtx_family();
$cls_menu = new Mtx_Menu();

if (isset($_GET['m'])) {
  $month = (int) $_GET['m'];
  $post_year = (int) $_GET['y'];
} else {
  $month = (int) date('m');
  $post_year = (int) date('Y');
}
// set prev and next month
if ($month == 1) {
  $prev_month = 12;
  $prev_year = $post_year - 1;
} else {
  $prev_month = $month - 1;
  $prev_year = $post_year;
}
if ($month == 12) {
  $next_month = 1;
  $next_year = $post_year + 1;
} else {
  $next_month = $month + 1;
  $next_year = $post_year;
}

$no_of_days = date('t', mktime(0, 0, 0, $month, 1, $post_year)); //number of days in a month

$mn = date('M', mktime(0, 0, 0, $month, 1, $post_year)); // Month name

$j = date('w', mktime(0, 0, 0, $month, 1, $post_year)); // This will calculate the week day of the first day of the month

$adj = '';
for ($k = 1; $k <= $j; $k++) { // Adjustment of date starting
  $adj .="<td>&nbsp;</td>";
}
$daily_menus = $cls_menu->get_daily_menu($month, $post_year, 'TIMESTAMP_KEY');

$title = 'Yearly FMB Takhmeen';
$active_page = 'family';

require_once 'includes/header.php';

$page_number = TAKHMEEN;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profiles</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <table class="table table-bordered table-condensed" style="border-color: #3C8DBC;">
            <tr>
              <td>&nbsp;</td>
              <td><a href='cal_dates.php?m=<?php echo $prev_month;?>&y=<?php echo $prev_year;?>' class='btn btn-primary col-md-12'>Prev</a></td>
              <td colspan="4" class="text-center"><strong><?php echo $mn . ', ' . $post_year; ?></strong></td>
              <td><a href='cal_dates.php?m=<?php echo $next_month;?>&y=<?php echo $next_year;?>' class='btn btn-primary col-md-12'>Next</a></td>
            </tr>
            <tr>
              <th style="width: 0px;" class="text-center">Sun</th>
              <th class="text-center">Mon</th>
              <th class="text-center">Tues</th>
              <th class="text-center">Wed</th>
              <th class="text-center">Thu</th>
              <th class="text-center">Fri</th>
              <th class="text-center">Sat</th>
            </tr>
            <tr>
              <?php
              for ($i = 1; $i <= $no_of_days; $i++) {
                $d = ($i < 10) ? '0' . $i : $i;
                $m = ($month < 10) ? '0' . $month : $month;
                $day = $post_year . '-' . $m . '-' . $d;
                $time = mktime(0, 0, 0, $m, $i, $post_year);
                $menu_name = '';
                if(array_key_exists($time, $daily_menus)) {
                  $menu_name = $cls_menu->get_base_menu_name($daily_menus[$time]['menu_id']);
                }
                $menu_name = '<p class="text-danger"><strong>' . $menu_name . '</strong></p>';
                $hijri = HijriCalendar::GregorianToHijri($time);
                $h_m = HijriCalendar::monthName($hijri[0]);
                $h_d = $hijri[1];
                $h_y = $hijri[2];
                $hijari_date = '<p>' . $h_d . ' ' . $h_m . ', ' . $h_y . '</p>';
                $ary_names = array();
                $qty = 0;
                $result = $cls_family->get_takhmeen_dates($day);
                if($result) {
                  foreach($result as $data) {
                    $qty += $data['niyaz_qty'];
                    $name = $cls_family->get_name($data['FileNo']);
                    $ary_names[] = '&#8226;&nbsp;' . ucfirst(trim($name)) . '<br>';
                  }
                }
                if($qty >= 1) $cls = 'alert-success';
                if($qty == 0) $cls = 'alert-danger';
                if($qty >= 0 && $qty < 1) $cls = 'alert-warning';
                if ($ary_names)
                  echo $adj . "<td class='$cls'><p>$i<span class='pull-right'>$qty</span></p>$hijari_date $menu_name<p class='text-info'>" . implode('', $ary_names) . "</p></td>";
                else
                  echo $adj . "<td><p>$i</p>$hijari_date $menu_name</td>";
                $adj = '';
                $j++;
                if ($j == 7) {
                  echo "</tr><tr>";
                  $j = 0;
                }
              }
              $week = 7;
              $rest_of_days = $j;
              $limit = $week - $rest_of_days;
              for($k = 1; $k <= $limit; $k++) {
                echo "<td>&nbsp;</td>";
              }
              ?>
            </tr>
          </table>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>
