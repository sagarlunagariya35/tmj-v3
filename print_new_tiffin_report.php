<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/class.family.php');
require_once('classes/hijri_cal.php');
$cls_receipt = new Mtx_Receipt();
$cls_family = new Mtx_family();

$from_date = $_GET['from_date'];
$date = explode('-', $from_date);
$fromDate = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
$to_date = $_GET['to_date'];
$date = explode('-', $to_date);
$toDate = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
$status = $_GET['status'];

if ($status == 'Added') {
  $tiffins = $cls_family->get_added_tiffin_between_dates($fromDate, $toDate);
} else {
  $tiffins = $cls_family->get_closed_tiffin_between_dates($fromDate, $toDate);
}


$title = 'New Tiffin Report';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->


      <!-- Center Bar -->
      <div class="col-md-8 ">
        <?php
        if ($status == 'Added')
          $status = 'New tiffin added between ';
        else
          $status = 'Tiffin closed between ';
        ?>
        <div>
          <strong> <?php echo $status . $from_date . ' And ' . $to_date; ?><span class="pull-right"><?php echo date('d F,Y'); ?></span></strong>
        </div>
        <?php
        if ($tiffins)
          echo $tiffins;
        ?>
      </div>
      <!-- /Center Bar -->
    </div>
    <!-- /Content -->

  </body>
</html>
