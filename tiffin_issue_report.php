<?php
include 'session.php';
$page_number = 21;
require_once('classes/class.database.php');
require_once('classes/class.family.php');

$cls_family = new Mtx_family();

$tfn_count = $date = $post = FALSE;
if (isset($_POST['submit'])) {
  $post = TRUE;
  $date = $_POST['entry_time'];
  $ary_remaining_family = $cls_family->get_tiffin_data($date, 'issue');
  $tfn_count = $cls_family->get_tiffin_data($date, FALSE, 'count');
}

if (isset($_POST['sms'])) {
  $return_success = array();
  $return_fail = array();
  $mobile = $_POST['mph'];
  $date = $_POST['rpt_date'];
  $mobileNumber = explode(',', str_replace("\r\n", ',', $mobile));
  $dt = explode('-', $date);
  $new_dt = $dt[2] . '/' . $dt[1] . '/' . $dt[0];
  $tfn_count = $cls_family->get_tiffin_data($date, 'issue', 'count');
  $message = 'Total no. of thali issued on ' . $new_dt . ' = ' . $tfn_count;
  foreach ($mobileNumber as $mob) {
    if ($mob != '' || $mob > 0)
      $sms_sent = $cls_family->send_sms($mob, $message);
    else
      $sms_sent = FALSE;
    if ($sms_sent) {
      $return_success[] = $family['FileNo'];
    } else {
      $return_fail[] = $family['FileNo'];
    }
  }
  if (count($return_success)) {
    $sent_msg .= "SMS To the following IDs sent successfully!<br>";
    $sent_msg .= join('<br>', $return_success) . '<br>';
  }

  if (count($return_fail)) {
    $sent_msg .= "SMS To the following IDs Could Not be Sent!<br>";
    $sent_msg .= join('<br>', $return_fail);
  }
}

$title = 'Thaali Issued Report';
$active_page = 'report';

include('includes/header.php');
include('page_rights.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Thali</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <div class="col-md-12">
              <label class="col-md-4 control-label">Select Date</label>
              <div class="col-md-4">
                <input type="date" name="entry_time" class="form-control" id="entry_time" placeholder="Date" value="<?php echo $date; ?>">
              </div>
              <input type="submit" class="btn btn-success validate" name="submit" id="" value="submit">
              <a href="print_tiffin_issue_report.php?entry_time=<?php echo $date; ?>" id="print" target="_blank" class="btn btn-primary validate <?php echo !$post ? 'disabled' : ''; ?>">Print</a>
            </div>
          </form>
          <br><br>
          <div class="col-md-12">&nbsp;</div>
          <form class="form-inline alert-info" role="form" method="post">
            <div class="col-md-12">
              <label class="col-md-2 pull-left control-label">Phone No.</label>
              <div class="col-md-3">
                <textarea class="form-control" rows="3" name="mph" id="mph" placeholder="Enter Mobile Number to received the tiffin count"></textarea>
              </div>
              <label class="col-md-2 control-label">Select Date</label>
              <div class="col-md-3">
                <input type="date" name="rpt_date" class="form-control" id="entry_time" placeholder="Date of Birth" value="">
              </div>
              <div class="col-md-2">
                <input type="submit" class="btn btn-success" name="sms" id="submit" value="Send Via SMS">
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
          </form>
          <script>
            var picker = false;
            $('.validate').click(function() {
              var entry = $('#entry_time').val();
              if (entry == '')
              {
                alert("Please select date to view report");
                picker = false;
                return false;
              } else {
                picker = true;
                return true;
              }
            });
            $('#submit').click(function() {
              var txt = $('#mph').val();
              var entry = $('#rpt_date').val();
              if (txt === '' || entry == '') {
                alert('Date or Mobile number not specified..');
                return false;
              } else {
                picker = true;
                return true;
              }
            });
          </script>
      <?php if (isset($_POST['submit'])) { ?>
            <div class="col-md-12" id="report">

        <?php
        if (isset($_POST['submit']))
          echo $ary_remaining_family;
        ?>
            </div>
          <?php } ?>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>