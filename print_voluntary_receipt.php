<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/class.family.php');
require_once('classes/class.user.php');
require_once('includes/inc.num2words.php');
$cls_receipt = new Mtx_Receipt();
$cls_family = new Mtx_family();
$cls_user = new Mtx_User();

$rcpt_id = $_GET['id'];
$receipt = $cls_receipt->print_normal_receipt($rcpt_id);
$family = $cls_family->get_single_family($receipt['FileNo']);
$setting = $cls_user->get_general_settings();
$db_tanzeem_name = $setting[0]['tanzeem_name'];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print voluntary receipt</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="asset/dist/css/print.css" media="print">
    <style type="text/css">
      @media all {
        body{font-size: 14px; }
        .page-break  { display: none; }
      }

      @media print {
        .page-break  { display: block; page-break-before: always; }
      }
    </style>
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8 ">
          <div class="col-md-12">&nbsp;</div>
          <div style="text-align: center;" class="col-md-12">
            <img src="includes/logo.jpg" width="200px">
          </div>
        <div class="col-md-12" style="text-align: center;">
          <h1><strong><?php echo $db_tanzeem_name;?></strong></h1>
        </div>
          
          <div class="col-md-12">
            <span class="col-md-3"><strong>Date</strong> : <u><?php echo date('d/m/Y', $receipt['timestamp']); ?></u></span>
            <span class="col-md-4"><strong><!-- Home Sabeel No. --> <?php echo THALI_ID; ?></strong>: <u><?php echo $receipt['FileNo'] ?></u></span>
            <span class="col-md-4"><strong>Receipt No.:</strong> <u><?php echo $receipt['id']; ?></u></span>
            <div class="clearfix"></div>
          </div>

          <div class="col-md-12">
            <span class="col-md-3"><strong>Tanzeem</strong> : <u><?php echo ucfirst($family['Mohallah']); ?></u></span>
            <span class="col-md-4"><strong>HOF Mobile No.</strong> :<u><?php echo $family['MPh']; ?></u></span>
            <span class="col-md-4"><strong>HOF ITS ID</strong> : <u><?php if ($family['ejamatID'] > '0') echo $family['ejamatID'];
else echo ''; ?></u></span>
            <div class="clearfix"></div>
          </div>

          <div class="col-md-12">
            <span class="pull-left col-md-10"><strong>HOF Name</strong> :<u><?php echo ucwords($receipt['hubber_name']); ?></u></span>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">
            <span class="pull-left col-md-3">BAAD SALAAM,</span>
          </div>
          <div class="col-md-12">&nbsp;</div>
        <?php if($receipt['type'] == '' || $receipt['type'] == 'cash') $type = 'CASH';?>
        <?php if($receipt['type'] == 'cheque') $type = 'CHEQUE';?>
        <?php if($receipt['type'] == 'neft') $type = 'NEFT';?>

          <div class="col-md-12">
            <span class="pull-left col-md-10">RECEIVED <?php echo $type; ?> WITH THANKS <strong><u>RS. <?php echo number_format($receipt['amount'],2) . ' ' . num2words($receipt['amount']);?> Only</u></strong></span>
          </div>

          <div class="col-md-12">
            <span class="pull-left col-md-8">AS <strong><u><?php echo $receipt['head'];?></u></strong>
          </div>

          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-4">AS VOLUNTARY CONTRIBUTION.</span>
          </div>

          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-3">E & O.E.</span>
            <span class="pull-right col-md-4">ABDE SYEDNA(T.U.S)</span>
          </div>

          <div class="col-md-12">&nbsp;</div>
          
      <div class="col-md-12">
        <a href="index.php" class="no-print btn btn-info pull-right">Back to Home</a><br><br>
      </div>
      <div class="col-md-12">
        <a href="voluntary_rcpt.php" class="no-print btn btn-info pull-right">Back to Receipt</a><br><br>
      </div>
        <?php
        if($_SESSION[USER_TYPE] == 'A'){
          if($receipt['cancel'] == 0){
        ?>
        <div class="col-md-12">
          <a href="cancel_vol_receipt.php?tid=<?php echo $receipt['FileNo']; ?>&rid=<?php echo $receipt['id']; ?>" class="btn btn-danger pull-right no-print">Cancel Receipt</a>
        </div>
        <?php }} ?>
      </div>

      <!-- /Center Bar -->

      <!-- Right Bar -->

      <!-- /Right Bar -->

    </div>
    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
