<table class="table table-hover table-condensed table-bordered">
        <thead>
          <?php if($print){?>
          <tr>
              <th colspan="5" style="text-align: center">Families Receiving FMB Thaali Barakat<span class="pull-right"><?php echo date('d M, Y'); ?></span></th>
            </tr>
          <?php } ?>
          <tr>
        <th class="text-right">Sr No</th>
        <th><?php echo THALI_ID; ?></th>
        <th>HOF Name</th>
        <th class="text-right">FC Code</th>
        <th>Takes Barakat</th>
          </tr>
        </thead>
        <tbody>
<?php
if (isset($search) && $search) {
  $i = 1;
  $receives = 0;
  $does_not_receive = 0;
  $bool_receives = FALSE;
  
  foreach ($search as $family) {
    if($family['close_date'] < 1) $bool_receives = TRUE;
    else $bool_receives = FALSE;
    if($bool_receives) $receives++;
    else $does_not_receive++;
    
    ?>
              <tr<?php if($bool_receives) echo ' class="alert alert-success"'; else echo ' class="alert alert-danger"'; ?>>
                <td class="text-right"><?php echo $i++; ?></td>
                <td><?php echo $family['FileNo']; ?></td>
                <td><?php echo ($family['first_name'] == '') ? $family['HOF'] : $family['HOF_NAME']; ?></td>
                <td class="text-right"><?php echo $family['fc_code']; ?></td>
                <td><?php if($bool_receives) echo 'Yes'; else echo 'No'; ?></td>
              </tr>

    <?php
  }
  ?>
              <tr>
                <td colspan="4"><strong>Count of Families receiving Barakat: <?php echo $receives; ?></strong></td>
              </tr>
              <tr>
                <td colspan="4"><strong>Count of Families NOT receiving Barakat: <?php echo $does_not_receive; ?></strong></td>
              </tr>
              <tr>
                <td colspan="4"><strong>Percent Receiving: <?php echo number_format(($receives*100)/($receives+$does_not_receive),2); ?> %</strong></td>
              </tr>
  <?php
} else {
  ?>
            <tr>
              <td colspan="4">No Record Found</td>
            </tr>
  <?php
}
?>
        </tbody></table>