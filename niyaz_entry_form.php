<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.family.php');
$cls_family = new Mtx_family();
$user_id = $_SESSION[USER_ID];
$id = $name = $price = $result = FALSE;
if(isset($_GET['id'])) $id = $_GET['id'];
if(isset($_POST['add_niyaz']))
{
  $name = $_POST['name'];
  $price = $_POST['price'];
  $result = $cls_family->add_niyaz($name, $price);
  if($result)
    $_SESSION[SUCCESS_MESSAGE] = "Niyaz added Successfully";
  else
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing...';
}
if(isset($_POST['update_niyaz'])) {
  $name = $_POST['name'];
  $price = $_POST['price'];
  $result = $cls_family->update_niyaz($id, $name, $price);
  if($result)
    $_SESSION[SUCCESS_MESSAGE] = "Niyaz updated Successfully";
  else
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing...';
}
if(isset($_GET['del_id'])){
  $delete = $cls_family->delete_Niyaz($_GET['del_id']);
  if($delete) $_SESSION[SUCCESS_MESSAGE] = 'Niyaz has been deleted successfully.';
  else $_SESSION[ERROR_MESSAGE] = 'Error encountered while deleting the records...';
  header('Location: niyaz_entry_form.php');
  exit;
}
$niyazes = $cls_family->get_all_niyaz();
if($id) $result = $cls_family->get_all_niyaz($id);
$title = 'Entry form for Niyaz';
$active_page = 'settings';
include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-6">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
              <div class="form-group">
                <label class="col-md-3 control-label">Name</label>
                <div class="col-md-8">
                  <input type="text" name="name" id="niyaz_name" class="form-control" value="<?php echo $result[0]['name']; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Price</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="price" id="price" value="<?php echo $result[0]['price']; ?>">
                </div>
              </div>
            <?php
            $btn_name = ($id) ? 'update' : 'add';
            ?>
              <div class="form-group">
                <label class="col-md-3 control-label">&nbsp;</label>
                <input type="submit" class="btn btn-success" name="<?php echo $btn_name;?>_niyaz" id="submit" value="Add">
                <a href="niyaz_entry_form.php" class="btn btn-danger">Cancel</a>
              </div>

          </form>
            <script>
              $('#submit').click(function() {
                var name = $('#niyaz_name').val();
                var price = $('#price').val();
                if(name === '' || price === '') return false;
              });
            </script>
          </div>
          <div class="col-md-6">
            <?php
            if($niyazes){
              $i = 1;
              ?>
            <table class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($niyazes as $niyaz) {?>
                <tr>
                  <td><?php echo $i++;?></td>
                  <td><?php echo $niyaz['name'];?></td>
                  <td><?php echo number_format($niyaz['price'], 2);?></td>
                  <td><a href="?id=<?php echo $niyaz['id'];?>" class="btn btn-success btn-xs">Update</a> | <input type="submit" class="btn btn-danger btn-xs" value="Delete" name="delete" id="delete" onclick="deleteNiyaz(<?php echo $niyaz['id']?>)"></td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
            <?php } ?>
          </div>
        </div>
        <!-- /Center Bar -->
      <script>
      function deleteNiyaz(val){
        window.location.href='niyaz_entry_form.php?del_id='+val;
      }
      </script>
    </div>
    <!-- /Content -->
  </section>
  </div>
  
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <?php
  include('includes/footer.php');
  ?>