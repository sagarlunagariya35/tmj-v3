<?php
include 'session.php';
require_once("classes/class.database.php");
require_once("classes/class.family.php");
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();

$mohallah = $cls_family->get_all_Mohallah();
$title = 'Thali Quantity';
$active_page = 'report';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body onload="window.print()">
<!-- Content -->
<div class="row">
  <div class="col-md-12"></div>

  <!-- Left Bar -->
  <div class="col-md-2 pull-left">
    &nbsp;
  </div>
  <!-- /Left Bar -->


  <!-- Center Bar -->
  <div class="col-md-8 ">
    <div class="col-md-12">&nbsp;</div>
    <table class="table table-condensed table-hover">
      <thead>
        <tr>
          <th colspan="4">Thali Quantity Report<span class="pull-right"><?php echo date('d F, Y');?></span></th>
        </tr>
        <tr>
          <th>No</th>
          <th>Tanzeem</th>
          <th>Tiffin Size</th>
          <th class="text-right">Count</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i = 1;
        foreach ($mohallah as $mh) {
        ?>
        <tr>
          <td><?php echo $i++;?></td>
          <td><?php echo ucfirst($mh['Mohallah']);?></td>
          <?php
          $size = $cls_family->get_tiffin_group($mh['Mohallah']);
          $k = 0;
          foreach ($size as $tfn => $cnt) {
            if($k != 0) echo '<tr><td>&nbsp;</td><td>&nbsp;</td>';
          ?>
          <td><?php echo $tfn;?></td>
          <td class="text-right"><?php echo $cnt;?></td>
          <?php
          if($k != 0) echo '</tr>';
          $k++;
          }
          ?>
        </tr>
        <?php } ?>
      </tbody>
        <tfoot>
        <tr>
          <td colspan="2" style="text-align: right"><strong>Grand Total :</strong></td>
          <?php
          $m = 0;
          $all_size = $cls_family->get_tiffin_size();
            $ary_sizes = array();
          foreach($all_size as $tiffin_size){
            $contain_x_words = $cls_family->get_X_sizes($tiffin_size['size']);
            $get_count = $cls_family->get_X_sizes($tiffin_size['size'], 'count');
            $key = '';
            $value = 0;
            $i = 1;
            foreach($contain_x_words as $size){
              $count = $cls_family->get_tiffin_group();
              if(empty($key)) $key .= $size['size'];
              else $key .= ' + '.$size['size'];
              $value += $count[$size['size']];
              if($i == $get_count) $ary_sizes[$key] = $value;
              $i++;
            }
          }
          $g_total = 0;
          foreach ($ary_sizes as $tfn => $cnt) {
            if($m != 0) echo '<tr><td>&nbsp;</td><td>&nbsp;</td>';
          ?>
          <td><?php echo $tfn;?></td>
          <td class="text-right"><?php echo $cnt; $g_total += $cnt; ?></td>
          <?php
          if($m != 0) echo '</tr>';
          $m++;
          } ?>
        </tr>
        <tr>
          <td colspan="2" class="text-right"><strong>All Total :</strong></td>
          <td></td>
          <td class="text-right"><?php echo $g_total; ?></td>
        </tr>
        </tfoot>
    </table>
  </div>
  <!-- /Center Bar -->

</div>
<!-- /Content -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
