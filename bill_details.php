<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
$cls_receipt = new Mtx_Receipt();
$ary_voucher = array();
$cmd = $_GET['cmd'];

$from = $to = FALSE;
  
  if(isset($_GET['from'])) $from = $_GET['from'];
  if(isset($_GET['to'])) $to = $_GET['to'];
  if(isset($_GET['sid'])) $sub_head = urldecode($_GET['sid']);
  if(isset($_GET['hd'])) $master_head = urldecode($_GET['hd']);
switch ($cmd) {
  case 'master_credit':
      $vouchers = $cls_receipt->get_credit_voucher_from_master($master_head, $from, $to);
      $vol_receipts = $cls_receipt->get_vol_rcpt_detail_from_master($master_head, $from, $to);
      if($vouchers){
        $ary_voucher[$vouchers[0]['id']] = $vouchers[0];
      }
    break;
  case 'credit':
      $vouchers = $cls_receipt->get_credit_voucher_from_sub_heads_id($sub_head, $from, $to);
      $vol_receipts = $cls_receipt->get_vol_rcpt_detail_from_sub_heads($sub_head, $from, $to);
      if($vouchers){
        foreach($vouchers as $voucher) $ary_voucher[$voucher['id']] = $voucher;
      }
      //echo $vouchers;
    break;
  case 'master_debit':
    $debit_vocuher = $cls_receipt->get_debit_vocuhers_from_master($master_head, $from, $to);
    $bill_book = $cls_receipt->get_bill_payment_bills_from_master($master_head, $from, $to);
    break;
  case 'debit':
    $debit_vocuher = $cls_receipt->get_debit_vocuhers($sub_head, $from, $to);
    $bill_book = $cls_receipt->get_bill_payment_bills($sub_head, $from, $to);
    //return $vouchers;
    break;
}
$title = 'Balance Sheet\'s bill details';
$active_page = 'report';
include('includes/header.php');

$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Balance Sheet</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <div class="col-md-3 pull-left">
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Search</h3></div>
            <div class="panel-body">
              <?php include('includes/search_bar.php'); ?>
            </div>
          </div>
        </div>
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-8">
          <table class="table table-bordered table-condensed table-hover">
            <thead>
              <tr>
                <th>Sr. No.</th>
                <th>Name</th>
                <th>Type of Payment</th>
                <th>Amount</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if($_GET['cmd'] == 'credit' OR $_GET['cmd'] == 'master_credit'){

                ?>
              <tr>
                <td colspan="5"><strong>Credit Vouchers</strong></td>
              </tr>
              <?php
              if(isset($ary_voucher) && !empty($ary_voucher)){
              $i = 1;
              foreach($ary_voucher as $key=>$value){
                ?>
              <tr>
                <td><?php echo $i++;?></td>
                <td><?php
                if($value['name'] != '')
                  echo $value['name'];
                else
                  echo '';?></td>
                <td><?php echo ucfirst($value['payment_type']);?></td>
                <td><span class="pull-right"><?php echo number_format($value['amount'], 2, '.', ',');?></span></td>
                <td><?php echo date('d F, Y',$value['timestamp']);?></td>
              </tr>
              <?php }} else { ?>
              <tr>
                <td colspan="5" class="alert-danger">No credit vouchers found.</td>
              </tr>
              <?php }} ?>
              <?php
              if($_GET['cmd'] == 'credit' OR $_GET['cmd'] == 'master_credit'){
                ?>
              <tr>
                <td colspan="5"><strong>Voluntary receipts</strong></td>
              </tr>
              <?php
              if(isset($vol_receipts) && !empty($vol_receipts)){
              $i = 1;
              foreach($vol_receipts as $key=>$value){
                ?>
              <tr>
                <td><?php echo $i++;?></td>
                <td><?php
                if($value['hubber_name'] != '')
                  echo $value['hubber_name'];
                else
                  echo '';?></td>
                <td><?php echo ucfirst($value['type']);?></td>
                <td><span class="pull-right"><?php echo number_format($value['amount'], 2, '.', ',');?></span></td>
                <td><?php echo date('d F, Y',$value['timestamp']);?></td>
              </tr>
              <?php }} else { ?>
              <tr>
                <td colspan="5" class="alert-danger">No voluntary receipts found.</td>
              </tr>
              <?php }} ?>
              <?php
              if($_GET['cmd'] == 'debit' OR $_GET['cmd'] == 'master_debit'){

                ?>
              <tr>
                <td colspan="5"><strong>Debit Vouchers</strong></td>
              </tr>
              <?php
              if(isset($debit_vocuher) && !empty($debit_vocuher)){
              $i = 1;
              foreach($debit_vocuher as $key=>$value){
              ?>
              <tr>
                <td><?php echo $i++;?></td>
                <td><?php
                if($value['name'] != '')
                  echo $value['name'];
                else
                  echo '';?></td>
                <td><?php echo ucfirst($value['payment_type']);?></td>
                <td><span class="pull-right"><?php echo number_format($value['amount'], 2, '.', ',');?></span></td>
                <td><?php echo date('d F, Y',$value['timestamp']);?></td>
              </tr>
              <?php }} else {?>
              <tr>
                <td colspan="5" class="alert-danger">No debit vouchers found.</td>
              </tr>
              <?php }} ?>
              <?php
              if($_GET['cmd'] == 'debit' OR $_GET['cmd'] == 'master_debit'){

                ?>
              <tr>
                <td colspan="5"><strong>Bill Book</strong></td>
              </tr>
              <?php
              if(isset($bill_book) && !empty($bill_book)){
              $i = 1;
              foreach($bill_book as $key=>$value){
              ?>
              <tr>
                <td><?php echo $i++;?></td>
                <td><?php echo $value['name'];?></td>
                <td><?php echo $value['payment_type'];?></td>
                <td><?php echo $value['amount'];?></td>
                <td><?php echo date('d F, Y',$value['timestamp']);?></td>
              </tr>
              <?php }} else {?>
              <tr>
                <td colspan="5" class="alert-danger">No bills found.</td>
              </tr>
              <?php }} ?>
            </tbody>
          </table>
          <div class="col-md-12">
            <a href="new_balancesheet.php" class="btn btn-info pull-right">Back to Balancesheet</a>
          </div>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>