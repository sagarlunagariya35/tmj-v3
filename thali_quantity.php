<?php
include 'session.php';
$page_number = 69;
require_once("classes/class.database.php");
require_once("classes/class.family.php");
require_once("classes/class.menu.php");
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();
$cls_menu = new Mtx_Menu();

$mohallah = $cls_family->get_all_Mohallah();
$title = 'Thali Quantity';
$active_page = 'report';
include('includes/header.php');
include('page_rights.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <div class="pull-right">
            <a href="print_thali_quantity.php" target="_blank" class="btn btn-primary btn-xs">Print</a>
          </div>
          <table class="table table-condensed table-hover">
            <thead>
              <tr>
                <th>No</th>
                <th>Tanzeem</th>
                <th>Tiffin Size</th>
                <th class="text-right">Count</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $i = 1;
              foreach ($mohallah as $mh) {
                ?>
                <tr>
                  <td><?php echo $i++; ?></td>
                  <td><?php echo ucfirst($mh['Mohallah']); ?></td>
                  <?php
                  $size = $cls_family->get_tiffin_group($mh['Mohallah']);
                  $k = 0;
                  foreach ($size as $tfn => $cnt) {
                    if ($k != 0)
                      echo '<tr><td>&nbsp;</td><td>&nbsp;</td>';
                    ?>
                    <td><?php echo $tfn; ?></td>
                    <td class="text-right"><?php echo $cnt; ?></td>
                    <?php
                    if ($k != 0)
                      echo '</tr>';
                    $k++;
                  }
                  ?>
                </tr>
              <?php } ?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="2" style="text-align: right"><strong>Grand Total :</strong></td>
                <?php
                $m = 0;
                $all_size = $cls_family->get_tiffin_size();
                $ary_sizes = array();
                foreach ($all_size as $tiffin_size) {
                  $contain_x_words = $cls_family->get_X_sizes($tiffin_size['size']);
                  $get_count = $cls_family->get_X_sizes($tiffin_size['size'], 'count');
                  $key = '';
                  $value = 0;
                  $i = 1;
                  foreach ($contain_x_words as $size) {
                    $count = $cls_family->get_tiffin_group();
                    if (empty($key))
                      $key .= $size['size'];
                    else
                      $key .= ' + ' . $size['size'];
                    $value += $count_value = (isset($count[$size['size']])) ? $count[$size['size']] : 0;
                    if ($i == $get_count)
                      $ary_sizes[$key] = $value;
                    $i++;
                  }
                }
                $g_total = 0;
                foreach ($ary_sizes as $tfn => $cnt) {
                  if ($m != 0)
                    echo '<tr><td>&nbsp;</td><td>&nbsp;</td>';
                  ?>
                  <td><?php echo $tfn; ?></td>
                  <td class="text-right"><?php echo $cnt;
                $g_total += $cnt;
                  ?></td>
                  <?php
                  if ($m != 0)
                    echo '</tr>';
                  $m++;
                }
                ?>
              </tr>
              <tr>
                <td colspan="2" class="text-right"><strong>All Total :</strong></td>
                <td></td>
                <td class="text-right"><?php echo $g_total; ?></td>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>
  
<script>
  $('#search').click(function() {
    var month = $('#month').val();
    var year = $('#year').val();
    var error = 'Following error(s) are occurred\n';
    var validate = true;
    if (month == '0')
    {
      error += 'Please select month name\n';
      validate = false;
    }
    if (year == '')
    {
      error += 'Please enter year\n';
      validate = false;
    }
    if (validate == false)
    {
      alert(error);
      return validate;
    }
  });
  $('#print').click(function() {
    var month = $('#month').val();
    var year = $('#year').val();
    var error = 'Following error(s) are occurred\n';
    var validate = true;
    if (month == '0')
    {
      error += 'Please select month name\n';
      validate = false;
    }
    if (year == '')
    {
      error += 'Please enter year\n';
      validate = false;
    }
    if (validate == false)
    {
      alert(error);
      return validate;
    }
  });
</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>