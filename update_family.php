<?php
ob_start();
include('session.php');
//require_once('classes/class.family.php');
require_once('classes/class.menu.php');
require_once('classes/hijri_cal.php');
//$cls_family = new Mtx_family();
$cmd = '';
$user_id = $_SESSION[USER_ID];
$hijri_cal = new HijriCalendar();

$title = 'Update Family';
$active_page = 'family';

include 'includes/header.php';

if (isset($_REQUEST['cmd']))
  $cmd = $_REQUEST['cmd'];

if (isset($_POST['reopen_acct'])) {
  $_SESSION[PREFIX] = $_POST['prefix'];
  $_SESSION[FIRST_NAME] = $_POST['first'];
  $_SESSION[FATHER_PREFIX] = $_POST['father_prefix'];
  $_SESSION[FATHER_NAME] = $_POST['father_name'];
  $_SESSION[SURNAME] = $_POST['surname'];
  $_SESSION[FILENO] = $_POST['sabilid'];
  header('location: reopen_family.php');
  exit;
}

if (isset($_POST['print_fam'])) {
  $_SESSION[FILE] = $id = $_POST['sabilid'];
  header('Location: print_family.php?family_id='.$id);
  exit;
}

if (isset($_POST['close_family'])) {
  $_SESSION[PREFIX] = $_POST['prefix'];
  $_SESSION[FIRST_NAME] = $_POST['first'];
  $_SESSION[FATHER_PREFIX] = $_POST['father_prefix'];
  $_SESSION[FATHER_NAME] = $_POST['father_name'];
  $_SESSION[SURNAME] = $_POST['surname'];
  $_SESSION[FILENO] = $_POST['sabilid'];
  header('location: close_family.php');
  exit;
}

if ($cmd == 'update_family') {
  $data = $database->clean_data($_POST);
  $cls_family->setSabil_id($data['sabilid']);
  if (strlen($data['age']) == 2)
    $year = date('Y') - $data['age'];
  else if(strlen($data['age'] != 4))
    $year = $data['age'];
  else $year = 0;
  $cls_family->setAge($year);
  $cls_family->setCard($data['card']);
  $cls_family->setPrefix($data['prefix']);
  $cls_family->setFirstName($data['first']);
  $cls_family->setFatherPrefix($data['father_prefix']);
  $cls_family->setFatherName($data['father_name']);
  $cls_family->setSurname(ucfirst($data['surname']));
  $cls_family->setGender($data['gender']);
  $cls_family->setMohallah($data['mohallah']);
  $cls_family->setMarital(ucfirst($data['marital']));
  $cls_family->setAddress(ucwords($data['address']));
  $cls_family->setEmail($data['email']);
  $cls_family->setFcCode($data['code']);
  $cls_family->setOccupation(ucfirst($data['occupation']));
  $cls_family->setEjamaat($data['ejamat']);
  $cls_family->setPhone($data['res_phone']);
  $cls_family->setOff_phone($data['off_phone']);
  $cls_family->setMobile($data['mobile']);
  $cls_family->setAdults($data['adults']);
  $cls_family->setChild($data['children']);
  $cls_family->setDiabetic($data['diabetic']);
  $cls_family->setDiabeticRoti((int) $data['roti']);
  $cls_family->set_payment_term($data['term']);
  if($data['scanning']) $data['scanning'] = 1;
  else $data['scanning'] = 0;
  $cls_family->set_scanning($data['scanning']);
  $cls_family->set_category($data['category']);

  $result = $cls_family->update_family();
  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = 'Family updated Successfully';
    header('location: list_family.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while updating Family Record';
    header('location: list_family.php');
    exit();
  }
}


function show_family($str) {
  global $hijri_cal;

  $cls_menu = new Mtx_Menu();
  $cls_fly = new Mtx_family();
  $mohallah = $cls_fly->get_all_tanzeem();
  $categories = $cls_fly->get_all_category();
  $tiffin = $cls_menu->get_all_tiffin_size();
  $latest = $cls_fly->latest_tiffin_size_year($_REQUEST['family_id']);
  $close = $cls_fly->check_close_family($_REQUEST['family_id']);
  //$acct_open = $cls_fly->get_family_open_date($_REQUEST['family_id']);
  $acct_open = $cls_fly->get_family_open_date_from_family($_REQUEST['family_id']);
  if ($acct_open) {
    switch (USE_CALENDAR) {
      case 'Hijri':
        $open = HijriCalendar::GregorianToHijri(strtotime($acct_open['open_date']));
        $open_date = $open[1] . ' ' . HijriCalendar::monthName($open[0]) . ', ' . $open[2] . ' H';
        break;
      case 'Greg':
        $open_date = date('d', strtotime($acct_open['open_date'])) . ' ' . date('F', mktime(0, 0, 0, date('m', strtotime($acct_open['open_date'])))) . ', ' . date('Y', strtotime($acct_open['open_date']));
        break;
    }
  }
  
  if ($close['close_date'] > 0) {
    $status = 'Closed';
    $h = (USE_CALENDAR == 'Hijri') ? 'H' : '';
    switch (USE_CALENDAR) {
      case 'Hijri':
        $close_date = HijriCalendar::GregorianToHijri($close['close_date']);
        $close_month = $hijri_cal->monthName($close_date[0]);
        $close_year = $close_date[2];
        break;
      case 'Greg':
        $close_month = date('F', mktime(0, 0, 0, date('m', $close['close_date'])));
        $close_year = date('Y', $close['close_date']);
        break;
    }
  } else {
    $status = 'Open';
  }

$cls_family = new Mtx_family($str);
$title = 'Update Family';
$active_page = 'family';
  
$page_number = PROFILE_ENTRY;
require_once 'page_rights.php';
?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profiles</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-md-12">&nbsp;</div>

          <!-- Center Bar -->
          <div class="col-md-12">
            <form method="post" role="form" class="form-horizontal">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label col-md-5">Account Open</label>
                  <div class="col-md-7">
                    <p class="form-control-static"><?php if($open_date) echo $open_date;?></p>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label col-md-5">Acct. Close</label>
                  <div class="col-md-7">
                    <p class="form-control-static"><?php if($status == 'Closed') echo $close_date[1].' '.$close_month . ', ' . $close_year . " $h"; else echo '---';?></p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 <?php echo ($status == 'Closed') ? 'alert-danger' : 'alert-success'; ?>">
                <label class="control-label col-md-2">Status</label>
                <div class="col-md-8">
                  <p class="form-control-static"><?php echo $status; ?></p>
                </div>
              </div>
              <div class="col-md-6"><br>
                <div class="form-group">
                  <label class="control-label col-md-5 "><?php echo THALI_ID; ?></label>
                  <div class="col-md-7">
                    <p class="form-control-static"><?php echo $cls_family->getSabil_id(); ?></p>
                    <input type="hidden" name="sabilid" id="sabilid" value="<?php echo $cls_family->getSabil_id(); ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Card Status</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="card" id="card" value="<?php
                    $card = ucfirst($cls_family->getCard());
                    if ($card == 'G')
                      echo 'Green';
                    else if ($card == 'Y')
                      echo 'Yellow';
                    else if ($card == 'R')
                      echo 'Red';
                    ?>">
                  </div>
                </div>
                <?php
      //if ($cls_family->getGender() == 'F')
      //                  $lines = file("includes/inc.female.php");
      //                else
      //                  $lines = file("includes/inc.male.php");
      //                $found = FALSE;
      //                print_r($lines);          ?>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Age</label>
                  <div class="col-md-7">
                    <?php
                    if ($cls_family->getAge() > 0) {
                      ?>
                      <p class="form-control-static"><?php echo $age = date('Y') - $cls_family->getAge(); ?></p>
                      <input class="form-control" type="hidden" name="age" value="<?php echo $cls_family->getAge() ?>" id="age">
                    <?php } else { ?>
                      <input class="form-control" type="text" name="age" value="" id="age">
        <?php } ?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-5 ">Prefix</label>
                  <div class="col-md-7">
                    <select name="prefix" id="prefix" class="form-control">
                      <option value="" <?php if ($cls_family->getPrefix() == '') echo 'selected' ?>>None</option>
                      <option value="Mulla"<?php if ($cls_family->getPrefix() == 'Mulla') echo 'selected' ?>>Mulla</option>
                      <option value="Shaik"<?php if ($cls_family->getPrefix() == 'Shaik') echo 'selected' ?>>Shaik</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">First Name</label>
                  <div class="col-md-7">
                    <select class="form-control" name="first" id="first">
                      <option value="">--Select First Name--</option>
                      <?php
                      if ($cls_family->getGender() == 'F')
                        $lines = file("includes/inc.female.php");
                      else
                        $lines = file("includes/inc.male.php");
                      $found = FALSE;
                      $option = '';
                      foreach ($lines as $line) {
                        $array = (array) explode('">', str_replace('<option value="', '', trim($line)));
                        $ListName = $array[0];
                        $name = $cls_family->getFirstName();
                        if ($found == FALSE) {
                          if ($name == trim($ListName)) {
                            $selected = "selected='selected'";
                            $found = TRUE;
                          } else {
                            $selected = '';
                            $found = FALSE;
                          }
                        } else {
                          $selected = '';
                        }

                        $option .= '<option value="' . trim($ListName) . '"' . $selected . '>' . trim($ListName) . '</option>';
                      }
                      if (!$found) {
                        $option .= '<option class="alert-danger" value="' . $cls_family->getFirstName() . '" selected>' . $cls_family->getFirstName() . '</option>';
                      }
                      echo $option;
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Father's Prefix</label>
                  <div class="col-md-7">
                    <select name="father_prefix" id="father_prefix" class="form-control">
                      <option value=""<?php if ($cls_family->getFatherPrefix() == '') echo 'selected' ?>>None</option>
                      <option value="Mulla"<?php if ($cls_family->getFatherPrefix() == 'Mulla') echo 'selected' ?>>Mulla</option>
                      <option value="Shaik"<?php if ($cls_family->getFatherPrefix() == 'Shaik') echo 'selected' ?>>Shaik</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Father Name</label>
                  <div class="col-md-7">
                    <select name="father_name" id="father_name" class="form-control">
                      <option value="">--Select Father Name--</option>
                      <?php
                      $option = '';
                      $father_found = FALSE;
                      $lines = file("name_list.txt");
                      foreach ($lines as $line) {
                        if ($father_found == FALSE) {
                          if ($cls_family->getFatherName() == trim($line)) {
                            $selected = "selected='selected'";
                            $father_found = TRUE;
                          } else {
                            $selected = '';
                            $father_found = FALSE;
                          }
                        } else {
                          $selected = '';
                        }
                        $option .= '<option value="' . trim($line) . '"' . $selected . '>' . trim($line) . '</option>';
                      }
                      if ($father_found == FALSE) {
                        $option .= '<option class="alert-danger" value="' . $cls_family->getFatherName() . '" selected>' . $cls_family->getFatherName() . '</option>';
                      }

                      echo $option;
                      ?>

                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Surname</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="surname" id="surname" value="<?php echo $cls_family->getSurname(); ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-5 ">Gender</label>
                  <div class="col-md-7">
                    <select id="gender" name="gender" class="form-control">
                      <option value="0">Select Gender</option>
                      <option value="M" <?php if (ucfirst($cls_family->getGender()) == 'M') echo 'selected'; ?>>Male</option>
                      <option value="F"  <?php if (ucfirst($cls_family->getGender()) == 'F') echo 'selected'; ?>>Female</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Mohallah</label>
                  <div class="col-md-7">
                    <select name="mohallah" id="mohallah" class="form-control">
                      <option value="0">Select one</option>
                      <?php foreach ($mohallah as $name) { ?>
                        <option value="<?php echo $name['name']; ?>" <?php if ($cls_family->getMohallah() == $name['name']) echo 'selected'; ?>><?php echo $name['name']; ?></option>
        <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Marital Status</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="marital" id="marital" value="<?php
                    $marital = ucfirst($cls_family->getMarital());
                    if ($marital == 'M')
                      echo 'Married';
                    else if ($marital == 'S')
                      echo 'Single';
                    else if ($marital == 'D')
                      echo 'Divorced';
                    ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Address</label>
                  <div class="col-md-7">
                    <textarea class="form-control" rows="5" name="address" id="address"><?php echo $cls_family->getAddress() ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-md-5 control-label">Payment in kisht</label>
                  <div class="col-md-7">
                    <input class="form-control" name="term" id="term" value="<?php echo $cls_family->get_payment_term(); ?>">
                  </div>
                </div>
              </div>
              <!-- another-->
              <div class="col-md-6"><div></div><br>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Fc Code</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="code" id="code" value="<?php echo ucfirst($cls_family->getFcCode()); ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Occupation</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="occupation" id="occupation" value="<?php echo ucfirst($cls_family->getOccupation()); ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Email</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="email" id="email" value="<?php echo $cls_family->getEmail() ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">E-jamaat Id</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="ejamat" id="ejamat" value="<?php echo $cls_family->getEjamaat(); ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Res Phone</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="res_phone" id="res_phone" value="<?php echo $cls_family->getPhone() ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Off Phone</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="off_phone" id="off_phone" value="<?php echo $cls_family->getOff_phone() ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Mobile</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="mobile" id="mobile" value="<?php echo $cls_family->getMobile(); ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">No. of adults</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="adults" id="adults" value="<?php echo $cls_family->getAdults() ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">No. of kids</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="children" id="children" value="<?php echo $cls_family->getChild(); ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">No. of diabetic</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="diabetic" id="diabetic" value="<?php echo $cls_family->getDiabetic(); ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Diabetic Roti</label>
                  <div class="col-md-7">
                    <input type="text" class="form-control" name="roti" id="roti">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Tiffin size</label>
                  <div class="col-md-7">
                    <p class="form-control-static"><?php echo $latest['new_tiffin_size']; ?></p>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5 ">Category</label>
                  <div class="col-md-7">
                    <select id="category" name="category" class="form-control">
                      <option value="0">Select Category</option>
                      <?php foreach ($categories as $c) { ?>
                      <option value="<?php echo $c['category']; ?>" <?php if (ucfirst($cls_family->get_category()) == $c['category']) echo 'selected'; ?>><?php echo $c['category']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-5">Show in scanning</label>
                  <div class="col-md-7">
                    <input type="checkbox" name="scanning" id="scanning" <?php echo ($cls_family->get_scanning() == 1) ? 'checked' : '';?>>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-12">&nbsp;</div>
              <div class="col-md-12">&nbsp;</div>
              <div class="pull-right">
              <button name="submit" type="submit" id="update_family" class="btn btn-success col-md-offset4">Update</button>
              <input  type="hidden" value="<?php echo $cls_family->getSabil_id(); ?>" name="id">
              <input  type="hidden" value="update_family" name="cmd" id="cmd">
              <?php 
                if($_SESSION[USER_TYPE] == 'A'){
                  if ($status != 'Closed') {
              ?>
                <button id="del_family" name="close_family" type="submit" class="btn btn-danger">Close Account</button>
              <?php } ?>
              <?php if ($status != 'Open') { ?>
                <button id="reopen_acct" name="reopen_acct" type="submit" class="btn btn-success">Reopen Account</button>
              <?php } } ?>
              <button id="print_fam" name="print_fam" type="submit" class="btn btn-primary">Print</button>
              <a href="list_family.php" class="btn btn-info">Home</a>
              </div>

            </form>
          </div>
          <!-- /Center Bar -->
          <script>
            $('#gender').change(function() {
              var sex = $(this).val();
              var first = $('#first').val();
              if (sex == 'F') {
                $.ajax({
                  url: 'ajax.php',
                  type: 'GET',
                  data: 'cmd=get_namelist_file&sex=' + sex,
                  success: function(data) {
                    if(first != '') {
                      //alert(first);
                      data += '<option value="'+first+'" selected>'+first+'</option>';
                    }
                    $('#first').html(data);
                  }
                });
              } else if (sex == 'M') {
                $.ajax({
                  url: 'ajax.php',
                  type: 'GET',
                  data: 'cmd=get_namelist_file&sex=' + sex,
                  success: function(data) {
                    if(first != '') {
                      //alert(first);
                      data += '<option value="'+first+'" selected>'+first+'</option>';
                    }
                    $('#first').html(data);
                  }
                });
              }
            });

            $('#update_family').click(function() {
              var first = $('#first').val();
              var surname = $('#surname').val();
              var gender = $('#gender').val();
              var mohallah = $('#mohallah').val();
              var error = 'Following error(s) are occurred\n';
              var validate = true;
              if (first == '0')
              {
                error += 'Please enter first name\n';
                validate = false;
              }
              if (surname == '')
              {
                error += 'Please enter surname\n';
                validate = false;
              }
              if (gender == '0')
              {
                error += 'Please select gender\n';
                validate = false;
              }
              if (mohallah == '0')
              {
                error += '\tPlease enter mohallah\n';
                validate = false;
              }
              if (validate == false)
              {
                alert(error);
                return validate;
              }
            });
          </script>


        </div>
        <!-- /Content -->
        <?php
      }

      if (isset($cmd)) {
        switch ($cmd) {
          case 'show':
            show_family($_REQUEST['family_id']);
            break;
          case 'update':
            break;
          case 'del':
            break;
        }
      }
      ?>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
<?php
  include 'includes/footer.php';
?>