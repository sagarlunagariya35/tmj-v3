<?php
include('session.php');

require_once("classes/class.database.php");
require_once("classes/class.menu.php");
require_once("classes/class.family.php");
$cls_menu = new Mtx_Menu();
$cls_family = new Mtx_family();

$date = $menu_details = $ary_thaali = $total_thali_count = $rows = $dataarray = FALSE;
$base_count = array();
$per_thali_cost = 0;

if (isset($_POST['date'])) {
  $date = $_POST['date'];

  $query = "SELECT * FROM cron_thaali_count WHERE `date` = '$date'";
  $rows = $database->query_fetch_full_result($query);
}

if ($rows) {
// convert the data into tabular format with thaali size as header
  foreach ($rows as $row) {
    $dataarray[$row['mohallah']][$row['tiffin_size']] = $row['count'];
  }
}

$date2 = strtotime($date);
$menu_details = $cls_menu->get_daily_menu_details($date2);

// get all thaali sizes from database with cost multiplier
$tiffin_size = $cls_family->get_tiffin_size();

foreach ($tiffin_size as $tfn) { // we use this array for size and its multiplier
  $ary_size[$tfn['size']] = $tfn['cost_multiplier'];
}

// get the actual thaali count
$ary_thaali = $dataarray['All'];

foreach ($ary_thaali as $size => $count) {
  $base_count[$size] = round($count * $ary_size[$size],2);
  $total_thali_count += $base_count[$size];
}
$per_thali_cost = round($menu_details['act_cost'] / $total_thali_count,2);
/*
echo '<pre>';
print_r($ary_thaali);
print_r($base_count);
print_r($total_thali_count.'<br>');
print_r($per_thali_cost);
echo '</pre>';
exit();
*/
$title = 'Thaali calculator';
$active_page = 'menu';

include 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
<?php
include 'includes/inc_left.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?php echo $title; ?></h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Maedat</a></li>
      <li><a href="#">Menu</a></li>
      <li class="active"><?php echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">&nbsp;</div>

      <!-- Center Bar -->
      <div class="col-md-12">
        <form method="post" role="form" class="form-horizontal">
          <div class="row"></div>
          <div class="form-group">
            <label class="col-md-3 control-label">From</label>
            <div class="col-md-4">
              <input type="date" name="date" class="form-control" value="<?php echo $date; ?>">
            </div>
            <div class="col-md-2">
              <input type="submit" class="btn btn-success" name="search" id="search" value="Search">
            </div>
          </div>
          <div class="row">&nbsp;</div>
          <?php
          if ($menu_details) {
            ?>
          <h4 class="box-title"><strong>Menu : </strong><?php echo $menu_details['custom_menu'] ?></h4>
          <h4 class="box-title"><strong>Menu Cost : </strong><?php echo number_format($menu_details['act_cost'], 2); ?></h4>

            <?php
            if ($dataarray) {
              foreach ($dataarray as $tanzeem => $tfn) {
                ?>
            <hr>
                <table class="table table-hover table-condensed">
                  <thead>
                    <tr>
                      <th><?php echo $tanzeem; ?></th>
                      <th>Thali Count</th>
                      <th class="text-right">Size Cost</th>
                      <th class="text-right">Per Thali Cost</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $total_size_cost = $total_thali = $thali_cost = $total_thali_count = $total_tanzeem_cost = 0;

                    foreach ($tfn as $tfn_size => $tfn_thali) {
                      $cost_multiplier = $ary_size[$tfn_size];

                      $base_count[$tfn_size] = $tfn_thali * $cost_multiplier;
                      $total_thali_count += $base_count[$tfn_size];

                      $thali_cost = round($per_thali_cost * $cost_multiplier, 2);
                      $size_cost = round($thali_cost * $tfn_thali, 2);
                      $total_tanzeem_cost += $size_cost;
                      ?>
                      <tr>
                        <td><?php echo $tfn_size; ?></td>
                        <td><?php echo $tfn_thali; ?></td>
                        <td class="text-right"><?php echo number_format($size_cost, 2); ?></td>
                        <td class="text-right"><?php echo number_format($thali_cost, 2); ?></td>
                      </tr>
                      <?php
                    }
                    ?>
                    <tr>
                      <td colspan="2">
                        <strong>Total Base Thali: </strong><?php echo $total_thali_count; ?>
                      </td>
                      <td>
                        <span class="pull-right"><strong>Total: </strong><?php echo number_format($total_tanzeem_cost,2); ?></span>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <?php
              }
            }
          }
          ?>
        </form>
      </div>
      <!-- /Center Bar -->
    </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php
include 'includes/footer.php';
?>