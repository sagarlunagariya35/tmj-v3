<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.receipt.php");
$cls_receipt = new Mtx_Receipt();

if (isset($_POST['submit_opening'])) {
    $open_cash = floatval($database->clean_currency($_POST['open_cash']));
    $open_bank = floatval($database->clean_currency($_POST['open_bank']));
    $addi_cash = floatval($database->clean_currency($_POST['addi_cash']));
    $addi_bank = floatval($database->clean_currency($_POST['addi_bank']));
    $withdraw = floatval($database->clean_currency($_POST['withdraw']));
    $result = $cls_receipt->add_opening_daily_cash($open_cash, $open_bank, $addi_cash, $addi_bank, $withdraw);
    if($result) $_SESSION[SUCCESS_MESSAGE] = 'Data submitted successfully.';
    else $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing the request...';
}
if (isset($_POST['submit_closing'])) {
    $close_cash = floatval($database->clean_currency($_POST['close_cash']));
    $close_bank = floatval($database->clean_currency($_POST['close_bank']));
    $deposit = floatval($database->clean_currency($_POST['deposit']));
    $result = $cls_receipt->add_closing_daily_cash($close_cash, $close_bank, $deposit);
    if($result) $_SESSION[SUCCESS_MESSAGE] = 'Data submitted successfully.';
    else $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing the request...';
}

// Get all settings if set for today
$date = date('Y-m-d');
$query = "SELECT * FROM daily_cash WHERE date LIKE '$date'";
$result = $database->query_fetch_full_result($query);
$today_details = $result[0];

// get current cash and bank details
$query = "SELECT (
	(
	  (SELECT SUM(amount) FROM balancesheet WHERE name LIKE 'opening_cash') + 
	  (SELECT IFNULL(SUM(amount),0) FROM credit_voucher WHERE payment_type = 'cash' AND cancel = 0) + 
	  (SELECT IFNULL(SUM(amount),0) FROM fmb_receipt_hub WHERE payment_type = 'cash' AND cancel = 0) + 
	  (SELECT IFNULL(SUM(amount),0) FROM receipt WHERE type = 'cash' AND cancel = 0) + 
	  (SELECT IFNULL(SUM(`from_bank`), 0) FROM `bank_transaction`)
	) - 
	(
	  (SELECT IFNULL(SUM(amount),0) FROM account_bill WHERE payment_type = 'cash' AND paid = 1 AND cancel = 0) + 
	  (SELECT IFNULL(SUM(amount),0) FROM debit_voucher WHERE payment_type = 'cash' AND cancel = 0) + 
	  (SELECT IFNULL(SUM(amount),0) FROM direct_purchase WHERE payment_type = 'cash' AND paid = 1 AND cancel = 0) + 
	  (SELECT IFNULL(SUM(`to_bank`), 0) DEPOSIT FROM `bank_transaction`)
	)
       ) cash, 
       (
	(
	  (SELECT SUM(amount) FROM balancesheet WHERE name LIKE 'opening_bank') + 
	  (SELECT IFNULL(SUM(amount),0) FROM credit_voucher WHERE (payment_type = 'cheque' OR payment_type = 'neft') AND cheque_clear = 1 AND cancel = 0) + 
	  (SELECT IFNULL(SUM(amount),0) FROM fmb_receipt_hub WHERE (payment_type = 'cheque' OR payment_type = 'neft') AND cheque_clear = 1 AND cancel = 0) + 
	  (SELECT IFNULL(SUM(amount),0) FROM receipt WHERE type = 'cheque' AND cheque_clear = 1 AND cancel = 0) + 
	  (SELECT IFNULL(SUM(`to_bank`), 0) DEPOSIT FROM `bank_transaction`)
	) - 
	(
	  (SELECT IFNULL(SUM(amount),0) FROM account_bill WHERE (payment_type = 'cheque' OR payment_type = 'neft') AND cheque_clear = 1 AND paid = 1 AND cancel = 0) + 
	  (SELECT IFNULL(SUM(amount),0) FROM debit_voucher WHERE (payment_type = 'cheque' OR payment_type = 'neft') AND cheque_clear = 1 AND cancel = 0) + 
	  (SELECT IFNULL(SUM(amount),0) FROM direct_purchase WHERE (payment_type = 'cheque' OR payment_type = 'neft') AND cheque_clear = 1 AND paid = 1 AND cancel = 0) + 
	  (SELECT IFNULL(SUM(`from_bank`), 0) FROM `bank_transaction`)
	)
       ) bank";
$result = $database->query_fetch_full_result($query);
$current_details = $result[0];


$title = 'Daily Cash';
$active_page = 'account';


require_once 'includes/header.php';

$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
          <div class="col-md-12">&nbsp;</div>

          <!-- Center Bar -->
          <div class="col-md-12">
              <p class="form-control-static text-center"><strong>Date:</strong> <?php echo date('d F, Y'); ?></p>

              <div class="col-md-6">
                  <form method="post" role="form" class="form-horizontal">
                      <div class="form-group">
                          <h3 class="text-center alert-info">Opening</h3>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3">Opening Cash</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control" name="open_cash" id="open_cash" value="<?php echo number_format($current_details['cash'], 2);?>">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3">Opening Bank</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control" name="open_bank" id="open_bank" value="<?php echo number_format($current_details['bank'], 2);?>">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3">Withdrawal from Bank</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control" name="withdraw" id="open_bank" value="<?php echo 0 > 0 ? number_format(0, 2) : '';?>">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3">Additional Cash</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="addi_cash" id="addi_cash">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3">Additional Bank</label>
                          <div class="col-md-6">
                              <input type="text" class="form-control" name="addi_bank" id="addi_bank">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3">&nbsp;</label>
                          <div class="col-md-6">
                              <button class="btn btn-success" type="submit" name="submit_opening" id="submit_opening">Submit</button>
                          </div>
                      </div>

                  </form>
              </div>
              <div class="col-md-6">
                  <form method="post" role="form" class="form-horizontal">
                      <div class="form-group">
                          <h3 class="text-center alert-info">Closing</h3>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3">Closing Cash</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control" name="close_cash" id="close_cash" value="<?php echo number_format($current_details['cash'], 2);?>">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3">Closing Bank</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control" name="close_bank" id="close_bank" value="<?php echo number_format($current_details['bank'], 2);?>">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3">Deposit Bank</label>
                          <div class="col-md-6">
                            <input type="text" class="form-control" name="deposit" id="deposit" value="<?php echo 0 > 0 ? number_format(0, 2) : ''; ?>">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3">&nbsp;</label>
                          <div class="col-md-6">
                              <button class="btn btn-success" type="submit" name="submit_closing" id="submit_closing">Submit</button>
                          </div>
                      </div>

                  </form>
              </div>
          </div>
          <!-- /Center Bar -->
          <script>

          </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>