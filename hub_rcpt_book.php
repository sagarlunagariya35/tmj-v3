<?php
include 'session.php';
$pg_link = 'hub_rcpt_book';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/class.family.php');
$cls_receipt = new Mtx_Receipt();
$cls_family = new Mtx_family();

$title = 'Hub Receipt Book';
$active_page = 'account';
$page = 1;
if (isset($_GET['page']) && $_GET['page'] != '') {
  $page = $_GET['page'];
}

$from_date = $to_date = FALSE;
$btn_csv_download_link = TRUE;
if (isset($_GET['search']) OR $_GET) {
  $from_date = $_GET['from_date'];
  $to_date = $_GET['to_date'];
  $result = $cls_receipt->get_all_receipt_for_hub($page, 20, $from_date, $to_date);
  $total_hub_receipt = $cls_receipt->get_total_hub_receipt($from_date, $to_date);
  $amount = $cls_receipt->get_total_between_dates($from_date, $to_date);
} else {
  $result = $cls_receipt->get_all_receipt_for_hub($page, 20);
  $total_hub_receipt = $cls_receipt->get_total_hub_receipt();
  $amount = FALSE;
}

if (isset($_GET['download_csv_file'])) {
  csv_download($result, 'hub_receipt_book.csv');
  exit();
}

function csv_download($ary_data, $filename) {
  // output headers so that the file is downloaded rather than displayed
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=' . $filename);

// create a file pointer connected to the output stream
  $output = fopen('php://output', 'w');

// output the column headings
  fputcsv($output, array('id', 'ref_no', 'FileNo', 'name', 'amount', 'year', 'date'));

// fetch the data
// loop over the rows, outputting them
  foreach ($ary_data as $row) {
    fputcsv($output, array($row['id'], $row['ref_no'], $row['FileNo'], '', 'Rs. ' . number_format($row['amount'], 2), $row['year'], date('d F, Y', strtotime($row['date']))));
  }
}

require_once 'includes/header.php';

$page_number = ACCOUNTS_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Receipt Books</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <?php include 'includes/inc.dates.php';?>
          <?php if ($_GET) { ?>
            <div class="col-md-12">&nbsp;</div>
            <br><br>
            <div class="alert-success">
              &nbsp;<strong>Total Amount: </strong><?php echo number_format($amount[0]['Amount'], 2); ?>
            </div>
          <?php } ?>

          <div class="col-md-12">&nbsp;</div>
          <table class="table table-hover table-bordered table-condensed">
            <thead>
            <th>Receipt No</th>
            <th>Ref. No.</th>
            <th>File No</th>
            <th>Name</th>
            <th class="text-right">Amount</th>
            <th>Year</th>
            <th>Date</th>
            </thead>
            <tbody>
              <?php
              if ($result) {
                foreach ($result as $receipt) {
                  ?>
              <tr class="<?php echo ($receipt['cancel'] == 1) ? 'alert-danger' : '';?>">
                    <td><a href="print_hub_receipt.php?id=<?php echo $receipt['id'] ?>" target="_blank" ><?php echo $receipt['id']; ?></a></td>
                    <td><?php echo $receipt['ref_no']; ?></td>
                    <td><?php echo $receipt['FileNo']; ?></td>
                    <td><?php echo $cls_family->get_name($receipt['FileNo']); ?></td>
                    <td class="text-right"><?php echo 'Rs. ' . number_format($receipt['amount'], 2); ?></td>
                    <td><?php echo $receipt['year']; ?></td>
                    <td><?php if ($receipt['date'] > 0) echo date('d F, Y', strtotime($receipt['date'])); ?></td>
                  </tr>
                <?php }
              } else {
                ?>
                <tr>
                  <td colspan="6" class="alert-danger">Sorry! No receipt added yet.</td>
                </tr>
      <?php } ?>
            </tbody>
          </table>
          <?php
          require_once("pagination.php");
          $link = ($from_date && $to_date) ? 'from_date=' . $from_date . '&to_date=' . $to_date . '&' : '';
            echo pagination(20, $page, '?'.$link.'page=', $total_hub_receipt);

          ?>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
<?php
include('includes/footer.php');
?>