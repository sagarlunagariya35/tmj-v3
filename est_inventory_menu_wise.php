<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.product.php');
require_once('classes/class.menu.php');
require_once('classes/hijri_cal.php');

$cls_product = new Mtx_Product();
$cls_menu = new Mtx_Menu();
$user_id = $_SESSION[USER_ID];

$post = FALSE;

$condition = isset($_REQUEST['date']) && !empty($_REQUEST['date']);
if ($condition) {
  $dsp = explode('-', $_REQUEST['date']);
  $start = mktime(0, 0, 0, $dsp[1], $dsp[2], $dsp[0]);
  $end = mktime(23, 59, 59, $dsp[1], $dsp[2], $dsp[0]);
  $menu_ids = $cls_menu->get_menu_between_dates($start, $end);
  $ids = $menu_ids[0]['menu_id'];
  $person_count = $menu_ids[0]['person_count'];
  $post = TRUE;
}

$title = 'Estimate inventory menu wise report';
$active_page = 'report';

require_once 'includes/header.php';

$page_number = REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Inventory</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <div class="form-group">
              <label class="col-md-3 control-label">From</label>
              <div class="col-md-4">
                <input type="date" name="date" class="form-control" id="date" placeholder="Date of Birth" value="<?php
                if ($condition)
                  echo $_REQUEST['date'];
                ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">&nbsp;</label>
              <div class="col-md-4">
                <input type="submit" class="btn btn-success" name="search" id="search" value="Search">
                <a target="_blank" id="print" href="#print_estimate_inventory_report.php?date=<?php echo $_REQUEST['date']; ?>" class="btn btn-primary <?php echo !$post ? 'disabled' : ''; ?>">Print</a>
              </div>
            </div>
          </form>
          <script>
            $('#search, #print').click(function() {
              var from_date = $('#date').val();
              if (from_date == '')
              {
                alert('Please select date first..')
                return false;
              }
            });
          </script>
          
          <?php
          if (isset($_REQUEST['date']) OR isset($_POST['confirm'])) {
            if ($ids) {
              $ids = explode(',', $ids);
              
              foreach ($ids as $data_id) {
                $menu_name = $cls_menu->get_base_menu_name($data_id);
                $items = $cls_menu->get_cook_est_inv_issue_items($data_id, $person_count, $start);
            ?>
          
            <div class="col-md-12">&nbsp;</div>
            <div class="col-md-12 alert-info" style="padding: 6px; border-radius: 8px;border-color: #bce8f1;">
              <span class="pull-left"><strong>Menu: </strong><?php echo $menu_name; ?></span>
            </div>
            <div class="col-md-12">&nbsp;</div>
          
            <div class="col-md-12">

              <table class="table table-hover table-condensed table-bordered">
                <thead>
                  <tr>
                    <th colspan="4">Inventory Issue</th>
                  </tr>
                  <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th class="text-right">Quantity</th>
                    <th class="text-right">Unit</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i = 1;
                  if ($items) {
                    foreach ($items as $key => $item) {
                      if($item['base_inv_issue'] != 0) {
                  ?>
                      <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $item['item_name']; ?></td>
                        <td class="text-right"><?php echo number_format($item['base_inv_issue'] * $person_count, 3, '.', ''); ?></td>
                        <td class="text-right"><?php echo $item['unit']; ?></td>
                      </tr>
                  <?php 
                      }
                    } 
                  } else {
                  ?>
                    <tr>
                      <td colspan="4" class="alert-danger">No results found</td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <?php
              }
            }
            ?>
          </div>
        <?php } ?>
      </div>
      <!-- /Center Bar -->
    </section>
  </div>
  <!-- /Content -->
  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>