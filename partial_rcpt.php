<?php
include 'session.php';
$page_number = 2;
$pg_link = 'Partial Payments';
require_once('classes/class.database.php');

require_once('classes/class.family.php');
require_once('classes/class.receipt.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();

$len = mt_rand(4, 10);
$form_id1 = '';
for ($i = 0; $i < $len; $i++) {
  $d = rand(1, 30) % 2;
  $form_id1 .= $d ? chr(rand(65, 90)) : chr(rand(48, 57));
}

$form_id1 = md5(time() . $form_id1);

$len = mt_rand(4, 10);
$form_id2 = '';
for ($i = 0; $i < $len; $i++) {
  $d = rand(1, 30) % 2;
  $form_id2 .= $d ? chr(rand(65, 90)) : chr(rand(48, 57));
}

$form_id2 = md5(time() . $form_id2);

if (isset($_POST['print'])) {

  $user_id = $_SESSION[USER_ID];
  $cls_receipt = new Mtx_Receipt();
  $fId = $_POST['FileNo'];
  $amount = $_POST['amount'];
  $name = $_POST['name'];
  $paid_till = $_POST['paid_till'];
  $form_id1 = $_POST['form_id1'];
  $form_id2 = $_POST['form_id2'];
  $monthly_hub = $hub_amt = str_replace(',', '', $_POST['monthly_hub']);
  $_SESSION[FILE] = $fId;
  $_SESSION[PARTIAL_AMOUNT] = $amount;
  $_SESSION[NAME] = $name;
  $_SESSION[MONTHLY_HUB] = $monthly_hub;
  $_SESSION[PAID_TILL] = $paid_till;
  $_SESSION[FORM_ID1] = $form_id1;
  $_SESSION[FORM_ID2] = $form_id2;

  $on_hand = $cls_receipt->receipt_amount_total_is_hub($fId);
  $_SESSION[ON_HAND] = $on_hand['TotalPayment'];
  if ($on_hand) {
    $total_parital_amount = $on_hand['TotalPayment'] + $amount;
    $_SESSION[TOTAL_PARTIAL_AMOUNT] = $total_parital_amount;
    if($monthly_hub == 'Default'){
      $result = $cls_family->get_last_year_month($fId);
      $till_dt = HijriCalendar::GregorianToHijri($result['paid_till']);
      $till_mon_plus_one = $till_dt[0] + 1;
      $till_year = $till_dt[2];
      if($till_mon_plus_one > 12){
        $till_mon_plus_one = 1;
        $till_year = $till_year + 1;
      }
      
      $key = $till_mon_plus_one.'-'.$till_year;
      $user_ts = HijriCalendar::HijriToUnix($till_mon_plus_one, '01', $till_year);
      $hub = $cls_family->get_hub_by_month($fId, $user_ts);
      
      $monthly_hub = $hub['Hub_raqam'];
      $_SESSION[MONTHLY_HUB] = $monthly_hub;
    }
    if ($total_parital_amount >= $monthly_hub) {
      header('Location: partial_payment_confirmation.php');
      exit;
    } else {
      $result = $cls_receipt->add_partial_payment($fId, $amount, $name, $user_id, $form_id1);
      if ($result) {
        header('Location: print_partial_receipt.php?id=' . $result . '&cmd=normal');
        exit;
      }
    }
  }
  else
    echo 'nodata';
}

$title = 'Partial payment';
$active_page = 'account';

include('includes/header.php');
include('page_rights.php');

?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Credit</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <div class="col-md-3 pull-left">
          <?php include 'links.php';?>
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Search</h3></div>
            <div class="panel-body">
              <?php include('includes/search_bar.php');?>
            </div>
          </div>
        </div>
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-8 ">
          <form method="post" role="form" class="form-horizontal">
            <div></div>

            <div class="form-group" id="File">
              <label class="control-label col-md-3">Thali ID</label>
              <div class="col-md-4">
                <input type="text" id="FileNo" name="FileNo" placeholder="Enter File No" class="form-control">
                <div class="col-md-1" id="loader">

                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Name</label>
              <div class="col-md-4">
                <input type="text" name="name" id="name" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Monthly FMB Hub</label>
              <div class="col-md-4">
                <p id="hub" class="form-control-static"></p>
                <input type="hidden" name="monthly_hub" id="monthly_hub">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">On hand amount</label>
              <div class="col-md-4">
                <p class="form-control-static" id="hand_amount"></p>
                <input type="hidden" name="" id="">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Amount</label>
              <div class="col-md-4">
                <input type="text" id="amount" name="amount" class="form-control">
                <input type="hidden" id="paid_till" name="paid_till">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">&nbsp;</label>
              <div class="col-md-4">
                <input type="hidden" id="exist">
                <input type="hidden" name="form_id1" value="<?php echo $form_id1; ?>">
                <input type="hidden" name="form_id2" value="<?php echo $form_id2; ?>">
                <input type="submit" id="print" name="print" value="Print" class="btn btn-success">
                 <a href="partial_rcpt_book.php" class="btn btn-danger"> Cancel</a>
              </div>
            </div>
          </form>
        </div>
        <!-- /Center Bar -->
        <script>
          $('#print').click(function() {
            var file = $('#FileNo').val();
            var amount = $('#amount').val();
            var ex = $('#exist').val();
            var error = 'Following error(s) are occurred\n\n';
            var validate = true;
            if (file == 0)
            {
              error += 'Please select File No\n';
              validate = false;
            }
            if (ex == '1')
            {
              error += 'Invalid thali ID\n';
              validate = false;
            }
            if (amount < 1)
            {
              error += 'Please specify amount\n';
              validate = false;
            }
            if (validate == false)
            {
              alert(error);
              return validate;
            }
          });

          $('#FileNo').change(function() {
            var FileNo = $(this).val().toUpperCase();
            $(this).val(FileNo);
            var fId = $('#FileNo').val();
            $('#loader').html('<img src="asset/img/loader.gif" height="20" width="20">');
            $.ajax({
              url: "ajax.php",
              type: "POST",
              data: 'fId=' + fId + '&cmd=get_name',
              success: function(data)
              {
                $('#loader').text('');
                if(data != 'invalid'){
                $('#File').attr('class', 'form-group has-success');
                $('#exist').val('0');  
                $('#name').val(data);
                $('#amount').focus();
                $.ajax({
                  url: "ajax.php",
                  type: "POST",
                  data: 'fId=' + fId + '&cmd=get_partial_amount',
                  success: function(data)
                  {
                    $('#amount').focus();
                    var obj = JSON.parse(data);
                    $('#hub').text(obj.MONTHLY_HUB);
                    $('#monthly_hub').val(obj.MONTHLY_HUB);
                    if(obj.PARTIAL_AMOUNT != null)
                      $('#hand_amount').text(obj.PARTIAL_AMOUNT);
                    $('#paid_till').val(obj.PAID_TILL);
                  }
                });
              } else {
                alert('Invalid thali ID');
                $('#exist').val('1');
                $('#File').attr('class', 'form-group has-error');
                $('#FileNo').focus();
                return false;
              }
              }
            });
          });
            $(document).ready(function(){
              $('#FileNo').focus(function(){
                $(this).select();
              });
            });

        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>