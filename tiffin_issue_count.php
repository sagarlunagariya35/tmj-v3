<?php
include 'session.php';
$page_number = 40;
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/class.family.php');
require_once('classes/class.barcode.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();
$cls_barcode = new Mtx_Barcode();
$title = 'Tiffin count report';
$active_page = 'report';
$from_date = $to_date = $post = $fromDate = $toDate = FALSE;
if (isset($_GET['search'])) {
  $post = TRUE;
  $from_date = $_GET['from_date'];
  $to_date = $_GET['to_date'];
  $sfrom = explode('-', $from_date);
  $fmonth = $sfrom[1];
  $fyear = $sfrom[0];
  $fday = $sfrom[2];
  $fromDate = mktime(0, 0, 0, $fmonth, 1, $fyear);
  $sto = explode('-', $to_date);
  $toDate = mktime(23, 59, 59, $sto[1], 30, $sto[0]);
  $filenos = $cls_family->get_FileNo_daily_entry($fromDate, $toDate);
}
$btn_print_link = "print_tiffin_issue_count.php?from=$fromDate&to=$toDate";

include('includes/header.php');
include('page_rights.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Thali</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <?php include 'includes/inc.dates.php';?>
          <div class="col-md-12">&nbsp;</div>
          <?php if (isset($_GET['search'])) { ?>
            <table class="table table-hover table-condensed table-bordered">
              <thead>
              <th>No</th>
              <th><?php echo THALI_ID; ?></th>
              <th>Total days</th>
              <th>Issued Count</th>
              <th>not issued days</th>
              </thead>
              <tbody>
                <?php
                $i = 1;
                if ($filenos) {
                  foreach ($filenos as $id) {
                    $issued_count = $cls_family->get_entry_from_daily_entry($id['family_id'], $fromDate, $toDate);
                    $days_in_month = cal_days_in_month(CAL_GREGORIAN, $fmonth, $fyear);
                    ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $id['family_id']; ?></td>
                      <td><?php echo $days_in_month; ?></td>
                      <td><?php echo $issued_count; ?></td>
                      <td><?php echo abs($issued_count - $days_in_month) . ' days'; ?></td>
                    </tr>
                    <?php
                  }
                } else {
                  echo '<tr><td colspan="5">No results found.</td></tr>';
                }
                ?>

              </tbody>
            </table>
          <?php } ?>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>
<?php
include('includes/footer.php');
?>