<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.family.php');
$cls_family = new Mtx_family();
$user_id = $_SESSION[USER_ID];
if (isset($_POST['update'])) {
  $hub = floatval($database->clean_currency($_POST['hub']));
  $user_ts = floatval($_POST['takhmeen_year_change']);

  $check_exist = $cls_family->check_month_exist($user_ts, $_POST['FileNo']);

  if ($check_exist == 0) {
    $result = $cls_family->update_tiffin_hub($_POST['FileNo'], $hub, $_POST['tiffin'], $user_ts, $user_id);
  } else {
    $result = $cls_family->update_tiffin_hub_on_exist($_POST['FileNo'], $hub, $_POST['tiffin'], $user_ts, $user_id, $to);
  }

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = 'Hub and TIffin has been changed successfully.';
    header('Location: list_family.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing...';
    header('Location: list_family.php');
    exit();
  }
}
$title = 'Update tiffin size and Hub';
$active_page = 'family';

require_once 'includes/header.php';

$page_number = PROFILE_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profiles</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-6">
            <form method="post" role="form" class="form-horizontal">
              <div class="form-group" id="File">
                <label class="col-md-3 control-label"><?php echo THALI_ID; ?></label>
                <div class="col-md-9">
                  <input type="text" id="FileNo" name="FileNo" placeholder="Enter Thali ID" class="form-control">
                </div>
                <div class="col-md-1" id="loader">

                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">Name</label>
                <div class="col-md-9">
                  <input type="text" name="name" id="name" class="form-control">
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">Takhmeen</label>
                <div class="col-md-9">
                  <input type="text" name="hub" id="hub" class="form-control">
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">Tiffin Size</label>
                <div class="col-md-9">
                  <select id="tiffin" name="tiffin" class="form-control">

                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3"><?php echo YEAR; ?></label>
                <div class="col-md-9">
                  <select class="form-control get_record" name="takhmeen_year_change" id="takhmeen_year_change">
                    <option value="">--Select year--</option>
                    <?php
                    for ($i = 1; $i <= $no_years; $i++) {
                      $takh_year = (($i == 1) ? $default_year : ($default_year + ($i - 1))) . '-' . (substr($default_year, -2) + $i);
                      ?>
                      <option value="<?php echo $takh_year; ?>"><?php echo $takh_year; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">&nbsp;</label>
                <div class="col-md-4">
                  <input type="submit" id="update" name="update" value="Update" class="btn btn-success">
                  <input type="hidden" id="exist">
                </div>
              </div>
            </form>
          </div>
          <div class="col-md-6" id="table">

          </div>
        </div>
        <!-- /Center Bar -->
        <script>
          var exist = false;
          $('#update').click(function() {
            var file = $('#FileNo').val();
            var month = $('#month').val();
            var year = $('#year').val();
            var error = 'Following error(s) are occurred\n\n';
            var validate = true;
            if (file == '')
            {
              error += 'Please enter Thali ID\n';
              validate = false;
            }
            if (month == '0')
            {
              error += 'Please select month\n';
              validate = false;
            }
            if (year == '')
            {
              error += 'Please enter year\n';
              validate = false;
            }
            if (exist == true)
            {
              error += "Invalid thali ID";
              validate = false;
            }
            if (validate == false)
            {
              alert(error);
              return validate;
            }
          });
          $('#FileNo').keyup(function() {
            var FileNo = $(this).val().toUpperCase();
            $(this).val(FileNo);
          });
          //when tab pressed from File no. and if file no is wrong then it will not let you to move down
          $("#FileNo").keydown(function(e) {
            if (e.keyCode === 9) {
              if (exist == true) {
                $('#FileNo').focus();
                return false;
              }
            }
          });

          $('#FileNo').change(function() {
            var fId = $('#FileNo').val();
            $('#loader').html('<img src="asset/img/loader.gif" height="20" width="20">');
            $.ajax({
              url: "ajax.php",
              type: "POST",
              data: 'fId=' + fId + '&cmd=tiffin_size_data',
              success: function(data)
              {
                $('#loader').text('');
                if (data != '0') {
                  $('#File').attr('class', 'form-group has-success');
                  exist = false;
                  var obj = JSON.parse(data);
                  $('#name').val(obj.NAME);
                  $('#hub').val(obj.HUB);
                  $('#tiffin').append(obj.OPTION);
                  $('#table').html(obj.PAST_RECORD);
                  $('#hub').focus();
                } else {
                  alert('Invalid thali ID');
                  exist = true;
                  $('#File').attr('class', 'form-group has-error');
                  $('#FileNo').focus();
                  return false;
                }
              }
            });
          });
          $(document).ready(function() {
            $('#FileNo').focus(function() {
              $(this).select();
            });
          });
        </script>
      </div>
      <!-- /Content -->
    </section>
  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>