<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
$cls_receipt = new Mtx_Receipt();

$title = 'Debit voucher book';
$active_page = 'account';
$pg_link = 'book_debit';

$from_date = $_GET['from_date'];
$to_date = $_GET['to_date'];
$tbl_name = $_GET['cmd'];
$post = TRUE;
$search = $cls_receipt->get_receipts(FALSE, FALSE, $tbl_name, $from_date, $to_date);

$voucher = $cls_receipt->get_number_voucher($tbl_name, $from_date, $to_date);
$func = 'get_'.$tbl_name.'_total_between_dates';
$amount = $cls_receipt->$func($from_date, $to_date);

?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print Vouchers</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="asset/dist/css/print.css" media="print">
    <style type="text/css">
      @media all {
        body { font-size: 16px; }
        table { page-break-inside:auto; }
        tr    { page-break-inside:avoid; page-break-after:auto; }
        thead { display:table-header-group; }
        tfoot { display:table-footer-group; }
      }
    </style>
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->
  <!-- Center Bar -->
  <div class="col-md-8 ">
    <table class="table table-hover table-condensed table-bordered">
      <thead>
        <tr>
          <th colspan="5"><?php echo ucfirst($tbl_name); ?> Voucher between <?php echo $from_date .' & '.$to_date; ?><span class="pull-right"><strong><?php echo date('l d F, Y');?></strong></span></th>
        </tr>
        <tr>
          <th>No</th>
          <th>Name</th>
          <th>Type of Payment</th>
          <th class="text-right">Amount</th>
          <th>Date</th>
        </tr>
      </thead>
      <tbody>
        <?php
        if ($search) {
          $grand_total = 0;
          foreach ($search as $debit) {
            $grand_total += $debit['amount'];
            ?>
            <tr>
              <td><?php echo $debit['id']; ?></td>
              <td><?php echo ($debit['name'] > 0) ? $debit['shopName'] : $debit['name']; ?></td>
              <td><?php echo ucfirst($debit['payment_type']); ?></td>
              <td class="text-right"><?php echo number_format($debit['amount'], 2); ?></td>
              <td><?php echo date('d F, Y', $debit['timestamp']); ?></td>
            </tr>
          <?php } ?>
            <tr>
              <td colspan="3"><span class="pull-right"><strong>Total</strong>:</span></td>
              <td class="text-right"><?php echo number_format($grand_total, 2); ?></td>
              <td>&nbsp;</td>
            </tr>
            <?php
        } else {
          ?>
          <tr>
            <td colspan="6" class="alert-danger">Sorry! No voucher found.</td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
    
  </div>
  <!-- /Center Bar -->
</div>
    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
