<?php
include 'session.php';
$page_number = 60;
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.user.php');
$cls_family = new Mtx_family();
$cls_user = new Mtx_User();
$post = FALSE;
$takhmeen_year_report = $show_all = $tanzeem = FALSE;
if(isset($_POST['search'])) {
  $post = TRUE;
  $tanzeem = $_POST['tanzeem'];
  $takhmeen_year_report = $_POST['takhmeen_year_report'];
  if(isset($_POST['show_all'])) $show_all = $_POST['show_all'];
  //$records = $cls_family->
  $records = $cls_family->get_takhmeen_report_record($takhmeen_year_report, $tanzeem, $show_all);
}
$mh = $cls_family->get_all_Mohallah();
$setting = $cls_user->get_general_settings();
$default_year = $setting[0]['start_year'];
$no_years = $setting[0]['no_of_years'];
$title = 'Takhmeen Report';
$active_page = 'report';

include('includes/header.php');
include('page_rights.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Thali</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <form method="post" role="form" class="form-horizontal">
            <div class="form-group">
              <label class="control-label col-md-1"><?php echo YEAR; ?></label>
              <div class="col-md-3">
                <select class="form-control get_record" name="takhmeen_year_report" id="takhmeen_year_report">
                  <option value ="" <?php echo ($takhmeen_year_report == '') ? 'selected' : '';?>>-- Select One --</option>
                  <?php
                    for($i = 1; $i <= $no_years; $i++) {
                    $takh_year = (($i == 1) ? $default_year : ($default_year + ($i - 1))) . '-' . (substr($default_year, -2) + $i);
                    if($takh_year == $takhmeen_year_report) $selected = 'selected';
                    else $selected = '';
                  ?>
                  <option value="<?php echo $takh_year;?>" <?php echo $selected; ?>><?php echo $takh_year; ?></option>
                  <?php } ?>
                </select>
              </div>
              <label class="col-md-1 control-label">Tanzeem</label>
              <div class="col-md-3">
                <select class="form-control get_record" name="tanzeem" id="tanzeem">
                  <option value ="">All</option>
                  <?php foreach($mh as $data) {?>
                  <?php if($tanzeem == $data['Mohallah']) echo 'selected'; ?>
                  <option value="<?php echo $data['Mohallah']; ?>" <?php echo ($tanzeem == $data['Mohallah']) ? 'selected' : '';?>><?php echo $data['Mohallah']; ?></option>
                  <?php } ?>
                </select>
              </div>

              <input type="checkbox" name="show_all" value="1" <?php echo ($show_all) ? 'checked' : '';?>> Show All
              <input type="submit" class="btn btn-success validate" name="search" id="search" value="Search">
              <a href="print_takhmeen_report.php?year=<?php echo $takhmeen_year_report;?>&tan=<?php echo $tanzeem;?>" target="blank" class="btn btn-primary validate <?php echo !$post ? 'disabled' : ''; ?>" id="print">Print</a>
            </div>
          </form>
          <script>
            $('.validate').click(function() {
              var takhmeen_year = $('#takhmeen_year_report').val();
              if(takhmeen_year === '') {
                alert('Please select takhmeen year to proceed..');
                return false;
              }
            });
          </script>
          <?php if(isset($_POST['search'])) { $print = FALSE;?>
          <div class="col-md-12">
            <?php include 'includes/inc.takhmeen_report.php';?>
          </div>
          <?php } ?>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>