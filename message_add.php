<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.user.php");
$cls_user = new Mtx_User();

$user_id = $_SESSION[USER_ID];
if (isset($_POST['msg'])) {
  $result = $cls_user->add_message($_POST['Message'], $user_id);
  if($result){
    $_SESSION[SUCCESS_MESSAGE] = 'Message added.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while processing new Message';
  }
}
$messages = $cls_user->get_messages();
$title = 'Message add';
$active_page = 'settings';

include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-6">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <div class="form-group">
              <label class="control-label col-md-3">Message</label>
              <div class="col-md-8">
                <input type="text" name="Message" class="form-control" />
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">&nbsp;</label>
              <button class="btn btn-success" type="submit" name="msg" id="msg">Add</button>
            </div>

          </form>
          </div>
          <div class="col-md-6">
          <?php if($messages){$i = 1;?>
            <table class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Message</th>
                  <th>Timestamp</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  foreach($messages as $message){
                    ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $message['message'];?></td>
                  <td><?php echo date('d M, Y', $message['timestamp']);?></td>
                  <td><input type="checkbox" name="" id="box<?php echo $message['id'];?>" onclick="check(<?php echo $message['id'];?>)" <?php if($message['active'] == 1){ $checked = 'checked';$value=1;}else {$checked='';$value=0;}?> value="0" <?php echo $checked;?>></td>
                </tr>
                <?php
                  $i++;
                  }
                ?>
              </tbody>
            </table>
          <?php } ?>
          </div>
        </div>
        <!-- /Center Bar -->
      <script>
        $('#msg').click(function() {
        var Message = $('#Message').val();
        if(Message === ''){
          alert('Please enter a message..');
          return false;
        }
        });
        function check(val){
          var doc = document.getElementById('box'+val).checked;

          if(doc == true) doc = 1;
          else doc = 0;
          window.location.href='msg_active.php?id='+val+'&status='+doc;
        }
      </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>