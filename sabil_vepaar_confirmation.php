<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/hijri_cal.php');
$cls_receipt = new Mtx_Receipt();
$hijari = new HijriCalendar();

$file = $_SESSION[FILE];
$name = $_SESSION[NAME];
$original = $_SESSION[ORIGINAL];
$expected = $_SESSION[EXPECTED];
$till = $_SESSION[PAID_TILL];
$upto = $_SESSION[PAID_UPTO];
$months = $_SESSION[MONTHS];
$type = $_SESSION[TYPE];
$bank = $_SESSION[BANK];
$cheque = $_SESSION[CHEQUE];
$date = $_SESSION[DATE];
$user_id = $_SESSION[USER_ID];
$form_id1 = $_SESSION[FORM_ID1];
    $till_paid = explode('-', $till);
    $till_month = $till_paid[0] + 1;
    $till_year = $till_paid[1];
    $d = '05';
    $upto_paid = explode('-', $upto);
    $upto_month = $upto_paid[0];
    $upto_year = $upto_paid[1];
    $paid_till = $hijari->HijriToUnix($till_month, $d, $till_year);
    $paid_upto = $hijari->HijriToUnix($upto_month, $d, $upto_year);

if(isset($_POST['continue']))
{
 $result = $cls_receipt->add_sabil_vepaar($file, $name, $expected, $paid_till, $paid_upto, $months, $type, $bank, $cheque, $date, $user_id, $form_id1);
if($result)
  header('Location: print_sabil_vepaar.php?id=' . $result);
}

$title = "Sabil vepaar confirmation";
$active_page = "receipt";

include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Credit</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <div class="col-md-3 pull-left">
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Menu This week</h3></div>
            <div class="panel-body">
              <?php include('includes/search_bar.php'); ?>
            </div>
          </div>
        </div>
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-8 ">
          <form method="post" role="form" class="form-horizontal">
              <div></div>

                    <p style="font-size: 20px">Hub amount calculated for File No <strong><?php echo $file;?></strong> is <span style="color: red;font-weight: 900"><?php echo "Rs: ".$original;?></span>.<br>
                      Edited amount is <span style="color: red;font-weight: 900"><?php echo "Rs: ".number_format($expected).'/-';?></span>. Do you want to proceed?</p>
                    <input type="submit" name="continue" value="Continue" class="btn btn-success">
                    <a class="btn btn-info" href="javascript:history.go(-1);">Edit</a>

            </form>
        </div>
        <!-- /Center Bar -->

        </div>
        <!-- /Content -->
    </section>
  </div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>