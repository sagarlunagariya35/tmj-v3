<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.family.php");
require_once("classes/class.user.php");
$cls_family = new Mtx_family();
$cls_user = new Mtx_User();
$setting = $cls_user->get_general_settings();

$sms_vendor = $result[0]['default_sms_vendor'];

$computek_senderID = $result[0]['sender_id'];
$computek_user_name = $result[0]['user_name'];
$computek_password = $result[0]['password'];

$masaar_sender_id = $result[0]['masaar_sender_id'];
$masaar_API_key = $result[0]['API_key'];
$masaar_route = $result[0]['route'];
$masaar_country_code = $result[0]['country_code'];

if ($sms_vendor == 0) {
  $_SESSION[ERROR_MESSAGE] = 'Please provide the <a href="general_settings.php">SMS API SETTINGS.</a>';
} elseif ($sms_vendor == 1) {
  if (empty($senderID) || empty($user_name) || empty($password)) {
    $_SESSION[ERROR_MESSAGE] = 'Please provide the <a href="general_settings.php">SMS API SETTINGS.</a>';
  }
} elseif ($sms_vendor == 2) {
  if (empty($masaar_sender_id) || empty($masaar_API_key) || empty($masaar_route) || empty($masaar_country_code)) {
    $_SESSION[ERROR_MESSAGE] = 'Please provide the <a href="general_settings.php">SMS API SETTINGS.</a>';
  }
}

$page = FALSE;
if (isset($_GET['page']))
  $page = $_GET['page'];

function tab_active($page_name) {
  global $page;
  echo ($page == $page_name) ? 'active' : '';
}

function isGlyphicon($page_name) {
  global $page;
  echo ($page == $page_name) ? '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' : '';
}

$title = 'Send SMS';
$active_page = 'settings';
include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
<?php
include 'includes/inc_left.php';
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?php echo $title; ?></h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Settings</a></li>
      <li class="active"><?php echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12">&nbsp;</div>
      <div class="col-md-3">
        <div class="panel panel-default">
          <div class="panel-heading"><h3 class="panel-title">Links</h3></div>
          <div class="panel-body">
            <ul class="nav nav-pills nav-stacked">
              <li class="<?php tab_active('hub_pending'); ?>"><a href="?page=hub_pending">Hub Pending<?php isGlyphicon('hub_pending'); ?></a></li>
              <li class="<?php tab_active('selected'); ?>"><a href="?page=selected">To Selected<?php isGlyphicon('selected'); ?></a></li>
              <li class="<?php tab_active('to_all'); ?>"><a href="?page=to_all">To all<?php isGlyphicon('to_all'); ?></a></li>
              <li class="<?php tab_active('to_close'); ?>"><a href="?page=to_close">To Close<?php echo isGlyphicon('to_close'); ?></a></li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Center Bar -->
      <div class="col-md-9">
        <?php
        switch ($page) {
          case 'hub_pending':
            include 'seg_hub_pending.php';
            break;
          case 'selected':
          case 'to_all':
          case 'to_close':
            include 'seg_send_sms.php';
            break;
          default :
            include 'seg_send_sms.php';
            break;
        }
        ?>
      </div>
      <!-- /Center Bar -->


    </div>
    <!-- /Content -->
  </section>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>



