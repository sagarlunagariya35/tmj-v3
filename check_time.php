<?php

echo date('Y-m-d h:i:s') . '<br>';
echo date_default_timezone_get() . '<br>';
date_default_timezone_set('Asia/Kolkata') . '<br>';
echo date('Y-m-d h:i:s') . '<br>';

if(class_exists('ZipArchive')) {
  echo 'Zip class exists';
} else {
  echo 'Zip NOT INSTALLED';
}