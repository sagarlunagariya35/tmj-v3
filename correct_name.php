<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.family.php");
$cls_family = new Mtx_family();

if(isset($_POST['update'])) {
  $prefix = $_POST['prefix'];
  $first = $_POST['first_name'];
  $father_prefix = $_POST['father_prefix'];
  $father = $_POST['father_name'];
  $surname = $_POST['surname'];
  $gender = $_POST['gender'];
  $thali_id = $_POST['thali_id'];
  $result = $cls_family->update_hof($prefix, $first, $father_prefix, $father, $surname, $gender, $thali_id);
  if($result) {
    $_SESSION[SUCCESS_MESSAGE] = 'HOF Name updated successfully.';
    header('Location: correct_name.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encounter while updating the hof name!';
    header('Location: correct_name.php');
    exit();
  }
}

$result = $cls_family->get_blank_first_name();

$title = 'Correct HOF Name';
$active_page = 'settings';

require_once 'includes/header.php';

$page_number = PROFILE_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div class="form-group">
              <label class="control-label col-md-3">HOF Name</label>
              <div class="col-md-5">
                <p class="form-control-static"><?php echo $result['HOF'];?></p>
                <input type="hidden" name="thali_id" value="<?php echo $result['FileNo'];?>">
              </div>
              <div class="col-md-4">
                <p><strong>Total Remaining Label: <?php echo $result['rows'];?></strong></p>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Prefix</label>
              <div class="col-md-5">
                <select class="form-control" name="prefix">
                  <option value="">None</option>
                  <option value="Mulla">Mulla</option>
                  <option value="Shaik">Shaik</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">First Name</label>
              <div class="col-md-5">
                <input type="text" name="first_name" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Father Prefix</label>
              <div class="col-md-5">
                <select class="form-control" name="father_prefix">
                  <option value="">None</option>
                  <option value="Mulla">Mulla</option>
                  <option value="Shaik">Shaik</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Father Name</label>
              <div class="col-md-5">
                <input type="text" name="father_name" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Surname</label>
              <div class="col-md-5">
                <input type="text" name="surname" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Gender</label>
              <div class="col-md-5">
                <select name="gender" class="form-control">
                  <option value="">--Select One--</option>
                  <option value="M">Male</option>
                  <option value="F">Female</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">&nbsp;</label>
              <div class="col-md-5">
                <input type="submit" name="update" class="btn btn-success" value="Update">
              </div>
            </div>
          </form>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
  
<script>
$('#update').on('click', function() {
  
});
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>