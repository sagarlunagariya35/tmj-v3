<?php
include('session.php');
$page_number =33;
$pg_link = 'Sabil Home';
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.receipt.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();
$cls_receipt = new Mtx_Receipt();
$hijari = new HijriCalendar();

// create a unique hash for the transaction
$len = mt_rand(4, 10);
$form_id = '';
for ($i = 0; $i < $len; $i++) {
  $d = rand(1, 30) % 2;
  $form_id .= $d ? chr(rand(65, 90)) : chr(rand(48, 57));
}

$form_id = md5(time().$form_id);

$title = 'Sabil Home Receipt';
$active_page = 'account';
include('includes/header.php');
include('page_rights.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Credit</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <div class="col-md-3 pull-left">
          <?php include 'links.php';?>
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Search</h3></div>
            <div class="panel-body">
              <?php include('includes/search_bar.php');?>
            </div>
          </div>
        </div>
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-8 ">
          <form method="post" role="form" class="form-horizontal" action="print_sabil_home.php">
              <div></div>
              <div class="form-group" id="File">
                <label class="control-label col-md-3">Thali ID</label>
                <div class="col-md-4">
                  <input type="text" id="FileNo" name="FileNo" placeholder="Enter Thali ID" class="form-control"> 
                </div>
                <div class="col-md-1" id="loader">

                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Name</label>
                <div class="col-md-4">
                  <input type="text" name="name" id="name" class="form-control">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" id="till">Paid till</label>
                <div class="col-md-4" id="paid_till_div">
                  <p id="paid_till" class="form-control-static"></p>
                  <input type="hidden" id="paid_till1" name="paid_till1">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Monthly Hub</label>
                <div class="col-md-4">
                  <p id="monthly_hub" class="form-control-static"></p>
                </div>
              </div>
              <div class="form-group alert-info" style="display: none" id="hand_amount">
                <label class="control-label col-md-3">On hand Amount</label>
                <div class="col-md-4">
                  <p id="on_hand" class="form-control-static"></p>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Pending Amount</label>
                <div class="col-md-4">
                  <p id="pending_amount" class="form-control-static"></p>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">For Months</label>
                <div class="col-md-4">
                  <input type="text" id="months" name="months" class="form-control">
                </div>
                <div id="loader_month" class="col-md-1">

                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Paying upto</label>
                <div class="col-md-4">
                  <p id="paid_upto" class="form-control-static"></p>
                  <input type="hidden" id="paid_upto1" name="paid_upto1">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Type</label>
                <div class="col-md-4">
                  <input type="radio" id="radio" name="type" value="cash" checked >&nbsp;Cash&emsp;
                  <input type="radio" id="radio" name="type" value="cheque">&nbsp;Cheque
                </div>
              </div>
              <div id="cheque_detail" style="display: none">
                <div></div>
                <div class="form-group" >
                  <label class="control-label col-md-3">Bank Name</label>
                  <div class="col-md-4">
                    <input type="text" name="bankname" id="bankname" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Cheque No.</label>
                  <div class="col-md-4">
                    <input type="text" name="cheque" id="cheque" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3">Date</label>
                  <div class="col-md-4">
                      <input type="date" name="dob" class="form-control" id="dob" placeholder="">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3">Expected Amount</label>
                <div class="col-md-4">
                  <input type="text" id="edit_amount" name="edit_amount" class="form-control">
                  <input type="hidden" id="ori_amount" name="ori_amount">
                </div>
              </div>
              <input type="hidden" id="exist">
                <input type="hidden" name="form_id" value="<?php echo $form_id; ?>">
                <input type="submit" id="print" name="print" value="Print" class="btn btn-success col-md-offset-3">
            </form>
        </div>
        <!-- /Center Bar -->
      <script>
        var enter = false; //if last paid_till = 0 than enter = true;
        //print button click
        $('#print').click(function() {
          var file = $('#FileNo').val();
          var ex = $('#exist').val();
          var months = $('#months').val();
          var error = 'Whoopis! Following error(s) are occurred\n\n';
          var validate = true;
          if (file == 0)
          {
            error += 'Please select Thali ID\n';
            validate = false;
          }
          if (ex == '1')
          {
            error += 'Invalid thali ID\n';
            validate = false;
          }
          if (months == '')
          {
            error += 'Please enter month(s)';
            validate = false;
          }

          if (validate == false)
          {
            alert(error);
            return validate;
          }
        });

        //for radio buttons
        $("input:radio[name=type]").click(function() {
          var radio = $(this).val();
          if (radio === 'cheque')
          {
            $('#cheque_detail').show();
          }
          else
          {
            $('#cheque_detail').hide();
          }
        });

        //change
        $('#FileNo').change(function() {
          var FileNo = $(this).val().toUpperCase();
          $(this).val(FileNo);
          var fId = $('#FileNo').val();
          $('#loader').html('<img src="asset/img/loader.gif" height="20" width="20">');
          $.ajax({
            url: "ajax.php",
            type: "POST",
            data: 'fId=' + fId + '&cmd=get_name',
            success: function(data)
            {
              if(data != 'invalid'){
                $('#exist').val('0');
                $('#File').attr('class', 'form-group has-success');
                $('#name').val(data);
                $('#months').focus();
                $.ajax({
                  url: "ajax.php",
                  type: "POST",
                  data: 'fId=' + fId + '&cmd=get_year_month_for_sabil_home',
                  success: function(data)
                  {
                    $('#loader').text('');
                    var link = '<a href="sabil_home_add.php?id='+fId+'">Click here to enter a last year, month and hub raqam</a>';
                    var ary_obj = JSON.parse(data);
                    $('#monthly_hub').text(ary_obj.MONTHLY_HUB);
                    if (data == '0') {
                      $('#till').attr('class','alert-danger col-md-3 control-label');
                      $('#paid_till_div').attr('class','col-md-8');
                      $('#paid_till').html(link);
                      enter = true;
                      $('#file').val(upper_case);
                      $('#on_hand').text('');
                      $('#print').attr('disabled','disabled');
                    }
                    else{
                      $('#paid_till').text(ary_obj.YEAR_MONTH);
                      if(ary_obj.ON_HAND_AMOUNT != '0'){
                        $('#hand_amount').show();
                        $('#on_hand').text(ary_obj.ON_HAND_AMOUNT+'/-');
                      } else {
                        $('#hand_amount').hide();
                      }
                      $('#paid_till1').val(ary_obj.EN_M_Y);
                    }

                    if (ary_obj.PENDING_AMOUNT != null)
                      $('#pending_amount').text(ary_obj.PENDING_AMOUNT);
                    else
                      $('#pending_amount').text('CLEAR');
                    } 

                });
              } else {
                alert('Invalid thali ID');
                $('#loader').text('');
                $('#exist').val('1');
                $('#File').attr('class', 'form-group has-error');
                $('#FileNo').focus();
                return false;
              }
            }
          });
        });
        $('#months').keyup(function() {
          var months = $('#months').val();
          var exist = $('#exist').val();
          var last_months = $('#paid_till1').val();
          var fId = $('#FileNo').val();
          if (fId == '0')
          {
            alert('Please select File No');
            $(this).val('');
            return false;
          } else if(exist == '1')
          {
            //alert('Please correct the thali ID to proceed..');
            return false;
          } else if(enter == true)
          {
            //alert('Please enter last paid till and hub amount');
            return false;
          }
          else {
            $('#loader_month').html('<img src="asset/img/loader.gif" height="20" width="20">');
            $.ajax({
              url: "ajax.php",
              type: "POST",
              data: 'months=' + months + '&cmd=get_hub_amount_sabil_home&fId=' + fId + '&last_months=' + last_months,
              success: function(data)
              {
                $('#loader_month').text('');
                var obj = JSON.parse(data);
                $('#ori_amount').val(obj.TOTAL);
                $('#edit_amount').val(obj.TOTAL);
                $('#paid_upto').text(obj.NEW_MONTH + '-' + obj.NEW_YEAR);
                $('#paid_upto1').val(obj.EN_M_Y);
              }
            });
          }
        });
      $("#FileNo").keydown(function(e) {
        var exist = $('#exist').val();
        if (e.keyCode === 9) {
          if(exist === 1){
            $('#FileNo').focus();
            return false;
          } else return true;
        }
      });

        $(document).ready(function(){
          $('#FileNo').focus(function(){
            $(this).select();
          });
        });
      </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>