<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.family.php');
$cls_family = new Mtx_family();
$user_id = $_SESSION[USER_ID];

if (isset($_POST['Add'])) {
  $thali_id = $_POST['FileNo'];
  $amount = floatval($_POST['takhmeen_amount']);
  $year = $_POST['takhmeen_year_amount'];
  $pay_term = (int) $_POST['payment_term'];
  $ary_days = $_POST['pay_day'];
  $ary_months = $_POST['pay_month'];
  $ary_year = $_POST['year'];
  $ary_niyaz_qty = $_POST['niyaz_qty'];
  $ary_pay_date = (array) $_POST['pay_date'];
  $ary_amount = (array) $_POST['amount'];

  $bool_takh_payment = $cls_family->add_takhmeen_amount($thali_id, $amount, $year, $pay_term, $ary_days, $ary_months, $ary_year, $ary_niyaz_qty, (int) $_POST['skip_kisht'], (int) $_POST['total_kisht'], $ary_pay_date, $ary_amount, $user_id);
  if ($bool_takh_payment) {
    $_SESSION[SUCCESS_MESSAGE] = 'Yearly FMB Takhmeen added successfully.';
    header('Location: takhmeen_amount.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing the request..';
    header('Location: takhmeen_amount.php');
    exit();
  }
}
$title = 'Yearly FMB Takhmeen';
$active_page = 'family';

$commitment_dates = $cls_family->get_commitment_dates_of_year();

include('includes/header.php');
$page_number = TAKHMEEN;
include('page_rights.php');
?>
<!-- Left side column. contains the logo and sidebar -->
<?php
include 'includes/inc_left.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?php echo $title; ?></h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Profiles</a></li>
      <li class="active"><?php echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12">&nbsp;</div>

      <!-- Center Bar -->
      <div class="col-md-12 ">
        <form method="post" role="form" class="form-horizontal">
          <div class="col-md-5">
            <div></div>
            <div class="form-group">
              <label class="control-label col-md-3"><?php echo YEAR; ?></label>
              <div class="col-md-9">
                <select class="form-control get_record" name="takhmeen_year_amount" id="takhmeen_year_amount">
                  <option value ="">-- Select One --</option>
                  <?php
                  for ($i = 1; $i <= $no_years; $i++) {
                    $year = (($i == 1) ? $default_year : ($default_year + ($i - 1))) . '-' . (substr($default_year, -2) + $i);
                    ?>
                    <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group" id="File">
              <label class="col-md-3 control-label"><?php echo THALI_ID; ?></label>
              <div class="col-md-9">
                <input type="text" id="FileNo" name="FileNo" placeholder="Enter Thali ID" class="form-control get_record">
              </div>
              <div class="col-md-1" id="loader">

              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Name</label>
              <div class="col-md-9">
                <input type="text" name="name" id="name" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Takhmeen</label>
              <div class="col-md-9">
                <input type="text" name="takhmeen_amount" id="takhmeen_amount" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Skip Kisht</label>
              <div class="col-md-9">
                <input type="text" name="skip_kisht" id="skip_kisht" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Count Till Kisht</label>
              <div class="col-md-9">
                <input type="text" name="total_kisht" id="total_kisht" class="form-control">
              </div>
            </div>


            <div class="form-group">
              <label class="control-label col-md-3">Payment Term</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="payment_term" id="payment_term">
                <!--select class="form-control" name="payment_term" id="payment_term">
                  <option value="0">--Select One--</option>
                  <option value="12">Monthly</option>
                  <option value="3">Quarterly</option>
                  <option value="2">Semi Annual</option>
                  <option value="1">Annual</option>
                </select-->
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">&nbsp;</label>
              <div class="col-md-4">
                <input type="submit" id="update" name="Add" value="Save" class="btn btn-success">
              </div>
            </div>

          </div>
          <div class="col-md-7">
            <h4>Takhmeen Date Given</h4>
            <div class="masterDiv">
              <div class="form-group">
                <div class="col-md-3">
                  <select name="pay_day[]" id="pay_day" class="form-control">
                    <option value="0">Day</option>
                    <?php for ($i = 1; $i < 32; $i++) { ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-3">
                  <select name="pay_month[]" id="pay_month" class="form-control">
                    <option value="0">Month</option>
                    <?php for ($i = 1; $i < 13; $i++) { ?>
                      <option value="<?php echo $i; ?>"><?php echo HijriCalendar::monthName($i); ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-3">
                  <input type="text" name="year[]" placeholder="Year" value="" class="form-control">
                </div>
                <div class="col-md-3">
                  <input type="text" name="niyaz_qty[]" placeholder="Qty" value="" class="form-control">
                </div>
              </div>
            </div>
            <div style="display: none;">
              <div></div>
              <div class="form-group copyDiv">
                <div class="col-md-3">
                  <select name="pay_day[]" id="pay_day" class="form-control">
                    <option value="0">Day</option>
                    <?php for ($i = 1; $i < 32; $i++) { ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-3">
                  <select name="pay_month[]" id="pay_month" class="form-control">
                    <option value="0">Month</option>
                    <?php for ($i = 1; $i < 13; $i++) { ?>
                      <option value="<?php echo $i; ?>"><?php echo HijriCalendar::monthName($i); ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-3">
                  <input type="text" name="year[]" placeholder="Year" value="" class="form-control">
                </div>
                <div class="col-md-3">
                  <input type="text" name="niyaz_qty[]" placeholder="Qty" value="" class="form-control">
                </div>

              </div>
            </div>
            <input type="submit" name="add" id="btnMagic" value="Add" class="btn btn-success btn-sm">
          </div>

          <div class="col-md-7">
            <br>
            <div class="masteramountDiv">
              <h4>Payment Commitments By Mumin</h4>
              <div class="form-group">
                <div class="col-md-3">
                  <select name="pay_date[]" id="pay_date" class="form-control pay_date">
                    <option value="0">Select Date</option>
                    <?php foreach ($commitment_dates as $cd) { ?>
                      <option value="<?php echo $cd['date']; ?>"><?php echo $cd['txt_lable']; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-3">
                  <input type="text" name="amount[]" placeholder="Amount" value="" class="form-control">
                </div>
              </div>
            </div>
            <div style="display: none;">
              <div></div>
              <div class="form-group copyamountDiv">
                <div class="col-md-3">
                  <select name="pay_date[]" id="pay_date" class="form-control pay_date">
                    <option value="0">Select Date</option>
                    <?php foreach ($commitment_dates as $cd) { ?>
                      <option value="<?php echo $cd['date']; ?>"><?php echo $cd['txt_lable']; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-3">
                  <input type="text" name="amount[]" placeholder="Amount" value="" class="form-control">
                </div>
              </div>
            </div>
            <input type="submit" name="add" id="btnamountMagic" value="Add" class="btn btn-success btn-sm">
          </div>

        </form>

        <div class="clearfix"></div>
        <div class="col-md-12" id="records">

        </div>
      </div>
      <!-- /Center Bar -->
      <script>
        var exist = false;
        $('#btnMagic').on('click', function(e) {
          e.preventDefault();
          $(".copyDiv").clone().appendTo(".masterDiv");
          $(".masterDiv>div.copyDiv").removeClass("copyDiv");
        });
        
        $('#btnamountMagic').on('click', function(e) {
          e.preventDefault();
          $(".copyamountDiv").clone().appendTo(".masteramountDiv");
          $(".masteramountDiv>div.copyamountDiv").removeClass("copyamountDiv");
        });
        
        $('#update').click(function() {
          var file = $('#FileNo').val();
          var amount = $('#takhmeen_amount').val();
          var year = $('#takhmeen_year_amount').val();
          var errors = [];
          var key = 0;
          if (year === '')
            errors[key++] = '\u221A Takhmeen year';
          if (file === '')
            errors[key++] = '\u221A Thaali ID';
          if (amount === '')
            errors[key++] = '\u221A Takhmeen amount';
          if (errors.length) {
            alert('Please correct these fields:\n' + errors.join('\n'));
            return false;
          }
        });
        $('#FileNo').keyup(function() {
          var FileNo = $(this).val().toUpperCase();
          $(this).val(FileNo);
        });
        //when tab pressed from File no. and if file no is wrong then it will not let you to move down
        $("#FileNo").keydown(function(e) {
          if (e.keyCode === 9) {
            if (exist == true) {
              $('#FileNo').focus();
              return false;
            }
          }
        });

        $('.get_record').change(function() {
          var fId = $('#FileNo').val();
          var year = $('#takhmeen_year_amount').val();
          if (year == '' || fId == '')
            return false;
          myApp.showPleaseWait();
          $.ajax({
            url: "ajax.php",
            type: "POST",
            data: 'fId=' + fId + '&cmd=get_name',
            success: function(data)
            {
              if (data != 'invalid') {
                $('#File').attr('class', 'form-group has-success');
                $('#name').val(data);
                $.ajax({
                  url: "ajax.php",
                  type: "POST",
                  data: 'fId=' + fId + '&cmd=get_yearly_takhmeen_record&year=' + year,
                  success: function(response) {
                    if (response) {
                      var stdObj = JSON.parse(response);
                      var obj = stdObj.RECORDS;
                      var tak_dates = stdObj.DATES;

                      $('.masterDiv').prepend(tak_dates);
                      var tbl = '<table class="table table-hover table-condensed table-bordered"><thead><th>Sr. No.</th><th class="text-right">Takhmeen</th><th>Year</th><th>Payment Term</th></thead><tbody>';
                      var no = 1;
                      var term = '';
                      $.each(obj, function() {
                        term = this.payment_term;
                        //                    switch (this.payment_term) {
                        //                      case '12':
                        //                        term = 'Monthly';
                        //                        break;
                        //                      case '3':
                        //                        term = 'Quarterly';
                        //                        break;
                        //                      case '2':
                        //                        term = 'Semi Annual';
                        //                        break;
                        //                      case '1':
                        //                        term = 'Annual';
                        //                        break;
                        //                    }
                        tbl += '<tr><td>' + no + '</td><td class="text-right">' + numeral(this.amount).format('0,0.00') + '</td><td>' + this.year + '</td><td>' + term + '</td></tr>';
                        no++;
                      });
                      tbl += '</tbody></table>';
                      $('#records').html(tbl);
                    }
                    myApp.hidePleaseWait();
                    return true;
                  }
                });
              } else {
                alert('Invalid thali ID');
                $('#File').attr('class', 'form-group has-error');
                $('#FileNo').focus();
                myApp.hidePleaseWait();
                return false;
              }
            }
          });
        });
        $(document).ready(function() {
          $('#FileNo').focus(function() {
            $(this).select();
          });
        });
        $('#takhmeen_year_amount').on("change", function() {
          var t_year_amount = $(this).val();
          $(".pay_date").empty();
          $.ajax({
            url: 'ajax.php',
            method: 'POST',
            data: 'cmd=get_takhmeen_commitment_dates&year=' + t_year_amount,
            success: function(data) {
              $(".pay_date").append('<option value="">Select Date</option>');
              var objdates = jQuery.parseJSON(data);
              $.each(objdates, function() {
                $(".pay_date").append('<option value="' + this.date + '">' + this.txt_lable + '</option>');
              });
            }
          });
        });
        var myApp;
        myApp = myApp || (function() {
          var pleaseWaitDiv = $('<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Processing . . .</h4></div><div class="modal-body"><img src="asset/img/loader.gif"></div></div></div></div>');
          return {
            showPleaseWait: function() {
              pleaseWaitDiv.modal();
            },
            hidePleaseWait: function() {
              pleaseWaitDiv.modal('hide');
            },
          };
        })();
      </script>
    </div>
    <!-- /Content -->
  </section>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>
