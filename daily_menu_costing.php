<?php
include('session.php');
require_once('classes/class.database.php');

//daily menu costing
$query = "UPDATE `daily_menu` SET `est_costing` = NULL";
$result = $database->query($query);

$query = "SELECT * FROM `daily_menu` WHERE est_costing IS NULL";
//$query = "SELECT * FROM `daily_menu` WHERE est_costing IS NULL AND `menu` = '79,82,70'";
$result = $database->query_fetch_full_result($query);
if ($result) {
  foreach ($result as $menu) {
    $est_inv_cost = TRUE;
    $est_direct_cost = TRUE;
    $menu_id = $menu['menu'];
    $timestamp = $menu['timestamp'];
    $sp_dt = explode('-', date('Y-m-d', $timestamp));
    $start = mktime(0, 0, 0, $sp_dt[1], $sp_dt[2], $sp_dt[0]);
    $end = mktime(23, 59, 59, $sp_dt[1], $sp_dt[2], $sp_dt[0]);
    $person_count = $menu['person_count'];
    $inv_cost = 0;
    $query = "SELECT item AS item_id, qty, (SELECT `unit_price` FROM `get_inventory_price_from_bills` ipb WHERE ipb.id LIKE est.item) cost FROM `est_inventory` est WHERE `date` BETWEEN '$start' AND '$end'";
    //echo $query.'<br>';
    $est_inv = $database->query_fetch_full_result($query);
    if ($est_inv) {
      foreach ($est_inv as $item) {
        $item_qty = $item['qty'];
        $item_id = $item['item_id'];
        $item_cost = $item['cost'];
        if ($item_cost > 0)
          $inv_cost += ($item_qty * $item_cost);
        else
          $est_inv_cost = FALSE;
      }
    }
    $dir_cost = 0;
    $query = "SELECT item AS item_id, qty, (SELECT `cost` FROM `lp_direct_costing` lp WHERE lp.inventory_id LIKE est.item) cost FROM `est_direct_pur` est WHERE `date` BETWEEN '$start' AND '$end'";
    //echo $query;exit;
    $est_inv = $database->query_fetch_full_result($query);
    if ($est_inv) {
      foreach ($est_inv as $item) {
        $item_qty = $item['qty'];
        $item_id = $item['item_id'];
        $item_cost = $item['cost'];
        if ($item_cost > 0)
          $dir_cost += $item_qty * $item_cost;
        else
          $est_direct_cost = FALSE;
      }
    }
    if ($est_inv_cost && $est_direct_cost) {
      $tot_cost = $inv_cost + $dir_cost;
      $costing_ts = time();
      $query = "UPDATE `daily_menu` SET `est_costing` = '$tot_cost', `est_costing_ts` = '$costing_ts' WHERE `menu` = '$menu_id' AND `timestamp` = '$timestamp'";
      $result = $database->query($query);
    }
  }
}

//base menu costing
$query = "UPDATE `base_menu` SET `est_costing` = NULL";
$result = $database->query($query);
$query = "SELECT * FROM `base_menu` WHERE est_costing IS NULL";
$result = $database->query_fetch_full_result($query);
if ($result) {
  foreach ($result as $base_menu) {
    $inv_cost = TRUE;
    $direct_cost = TRUE;
    $menu_name = $base_menu['name'];
    $base_menu_id = $base_menu['id'];
    $person_count = $base_menu['person_count'];

    //inventory issue
    $tot_inv_cost = 0;
    $query = "SELECT * FROM `base_inventory` bi left join lp_inventory_costing lp ON bi.item = lp.inventory_id where bi.base_menu_id = '$base_menu_id'";
    $base_inv = $database->query_fetch_full_result($query);
    if ($base_inv) {
      foreach ($base_inv as $item) {
        $item_qty = $item['qty'];
        $item_cost = $item['cost'];
        if ($item_cost > 0) {
          $tot_inv_cost += ($item_qty * $person_count) * $item_cost;
        } else
          $inv_cost = FALSE;
      }
    }
    //Direct Purchase
    $tot_dir_cost = 0;
    $query = "SELECT * FROM `base_direct_pur` bd left join lp_direct_costing lp ON bd.item = lp.inventory_id where bd.base_menu_id = '$base_menu_id'";
    $base_inv = $database->query_fetch_full_result($query);
    if ($base_inv) {
      foreach ($base_inv as $item) {
        $item_qty = $item['qty'];
        $item_cost = $item['cost'];
        if ($item_cost > 0)
          $tot_dir_cost += ($item_qty * $person_count) * $item_cost;
        else
          $direct_cost = FALSE;
      }
    }
    if ($inv_cost && $direct_cost) {
      $total = $tot_inv_cost + $tot_dir_cost;
      $costing_ts = time();
      $query = "UPDATE `base_menu` SET `est_costing` = '$total', `est_costing_ts` = '$costing_ts' WHERE `id` = '$base_menu_id'";
      $result = $database->query($query);
    }
  }
}
$_SESSION[SUCCESS_MESSAGE] = 'Database updated successfully';
$title = 'Update daily and base menu Costing';
$active_page = 'menu';

require_once 'includes/header.php';

$page_number = ACCOUNTS_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <div class="col-md-3 pull-left">
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Search</h3></div>
            <div class="panel-body">
              <?php include('includes/search_bar.php'); ?>
            </div>
          </div>
        </div>
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-8">
          <a href="daily_menu.php" class="btn btn-info">Daily Menu</a> | <a href="base_menus.php" class="btn btn-info">Base Menu</a>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>