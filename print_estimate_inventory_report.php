<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.product.php');
require_once('classes/class.family.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();
$cls_product = new Mtx_Product();
//echo date('d-m-y', 1393920457);
$user_id = $_SESSION[USER_ID];

if(isset($_GET['date'])) {
  $items = $cls_product->get_estimated_issue_item_in_current_day($_GET['date']);
  $purchases = $cls_product->get_estimated_issue_direct_purchase($_GET['date']);
  $all_tiffin = $cls_family->get_all_tiffin_size($_GET['date']);
}
$mohallah = $cls_family->get_all_tanzeem();

$title = 'Estimate inventory issue report';
$active_page = 'report';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

  <!-- Center Bar -->
  <div class="col-md-8">
    <?php
    if(isset($_POST['search']) OR isset($_GET['date'])) {
    ?>
    <div class="col-md-6">
      
      <table class="table table-hover table-condensed table-bordered">
        <thead>
          <tr>
            <th colspan="5">Inventory Issue</th>
          </tr>
        <tr>
          <th>No.</th>
          <th>Name</th>
          <th class="text-right">Quantity</th>
          <th class="text-right">unit</th>
          <th>Available</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i = 1;
        if($items) {
        foreach($items as $item) {
          $available = $cls_product->check_product_available_in_inventory($item['id'], $item['quantity']);
        ?>
        <tr>
          <td><?php echo $i++;?></td>
          <td><?php echo $item['item'];?></td>
          <td class="text-right"><?php echo number_format($item['quantity'], 3);?></td>
          <td><?php echo $item['unit'];?></td>
          <td><?php echo $available;?></td>
        </tr>
        <?php } 
        } else { ?>
        <tr>
          <td colspan="5" class="alert-danger">No results found</td>
        </tr>
        <?php } ?>
      </tbody>
      </table>
    </div>
    <div class="col-md-6">
      <table class="table table-hover table-condensed table-bordered">
        <thead>
          <tr>
            <th colspan="5">Direct Purchase</th>
          </tr>
        <tr>
          <th>No.</th>
          <th>Item Name</th>
          <th class="text-right">Quantity</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $k = 1;
        if($purchases) {
        foreach($purchases as $purchase) {
        ?>
        <tr>
          <td><?php echo $k++;?></th>
          <td><?php echo $purchase['item'];?></td>
          <td class="text-right"><?php echo number_format($purchase['qty'], 3);?></td>
        </tr>
        <?php } 
        } else { ?>
        <tr>
          <td colspan="4" class="alert-danger">No results found</td>
        </tr>
        <?php } ?>
      </tbody>
      </table>
    </div>
    <div class="col-md-12">
      <table class="table table-hover table-condensed table-bordered">
        <thead>
          <tr>
            <th>Tiffin Size</th>
            <th>Count</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($all_tiffin as $key => $value) {?>
          <tr>
            <td><?php echo $key;?></td>
            <td><?php echo $value;?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <?php } ?>
  </div>
  <!-- /Center Bar -->
</div>
<!-- /Content -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
