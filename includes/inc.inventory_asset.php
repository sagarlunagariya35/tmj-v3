    <table class="table table-hover table-condensed table-bordered">
      <thead>
        <?php if($print) {?>
        <tr>
          <td colspan="3" class="text-right"><?php echo date('d F, Y');?></td>
        </tr>
        <?php } ?>
        <tr>
          <th>Sr. No.</th>
          <th>Item Name</th>
          <th class="text-right">Cost</th>
        </tr>
      </thead>
      <tbody>
        <?php
        if ($items) {
          $i = 1;
          $tot_cost = 0;
          foreach ($items as $item) {
            $tot_cost += $item['item_cost'];
            ?>
            <tr>
              <td><?php echo $i++; ?></td>
              <td><?php echo ucwords($item['item_name']); ?></td>
              <td class="text-right"><?php echo number_format($item['item_cost'], 2); ?></td>
            </tr>
          <?php
          }
          ?>
            <tr>
              <td colspan="2" class="text-right"><strong>Total:</strong></td>
              <td class="text-right"><?php echo number_format($tot_cost, 2);?></td>
            </tr>
            <?php
        } else {
          ?>
          <tr>
            <td colspan="3" class="alert-danger">Sorry! no menus found.</td>
          </tr>
<?php } ?>
      </tbody>
    </table>
