<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.billbook.php");
$cls_billbook = new Mtx_BillBook();

$from_date = $to_date = FALSE;
if (isset($_GET['company'])) {
  $data = $database->clean_data($_GET);
  $companyID = $data['company'];
  if (isset($data['from_date']) && isset($data['to_date'])) {
    $from_date = $data['from_date'];
    $to_date = $data['to_date'];
    $date = explode('-', $from_date);
    $from_ts = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    $date = explode('-', $to_date);
    $to_ts = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
  }

  $query = "SELECT *, (SELECT ShopName FROM `companies` c WHERE c.id LIKE ab.name) ShopName   FROM `account_bill` ab WHERE `name` LIKE '$companyID'";
  if ($from_date && $to_date)
    $query .= " AND `timestamp` BETWEEN '$from_ts' AND '$to_ts'";
  $accountBills = $database->query_fetch_full_result($query);

  $query = "SELECT *, (SELECT ShopName FROM `companies` c WHERE c.id LIKE dp.name) ShopName   FROM `direct_purchase` dp WHERE `name` LIKE '$companyID'";
  if ($from_date && $to_date)
    $query .= " AND `timestamp` BETWEEN '$from_ts' AND '$to_ts'";
  $directBills = $database->query_fetch_full_result($query);
  
  $query = "SELECT *, (SELECT ShopName FROM `companies` c WHERE c.id LIKE dv.name) ShopName   FROM `debit_voucher` dv WHERE `name` LIKE '$companyID'";
  if ($from_date && $to_date)
    $query .= " AND `timestamp` BETWEEN '$from_ts' AND '$to_ts'";
  $debitBills = $database->query_fetch_full_result($query);
}

$shops = $cls_billbook->get_shops();
$title = 'Company Bill Details';
$active_page = 'settings';

$account_paid_total = 0;
$account_pending_total = 0;
$account_cancel_total = 0;

$direct_paid_total = 0;
$direct_pending_total = 0;
$direct_cancel_total = 0;

$debit_paid_total = 0;
$debit_pending_total = 0;
$debit_cancel_total = 0;

require_once 'includes/header.php';

$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="get" role="form" class="form-horizontal">
            <div class="col-md-12">
              <label class="col-md-1 control-label">From</label>
              <div class="col-md-2">
                <input type="date" name="from_date" class="form-control" id="from_date" value="<?php echo $from_date; ?>" placeholder="From Date">
              </div>
              <label class="col-md-1 control-label">To</label>
              <div class="col-md-2">
                <input type="date" name="to_date" class="form-control" id="to_date" value="<?php echo $to_date; ?>" placeholder="To Date">
              </div>

              <label class="col-md-1 control-label">Company</label>
              <div class="col-md-3">
                <select class="form-control" name="company">
                  <option value="">--Select Company--</option>
                  <?php
                  foreach ($shops as $shop) {
                    $selected = ($companyID == $shop['id']) ? 'selected' : '';
                    ?>
                    <option value="<?php echo $shop['id']; ?>" <?php echo $selected; ?>><?php echo $shop['ShopName']; ?></option>
                  <?php } ?>
                </select>
              </div>
              <input type="submit" class="btn btn-success" name="" value="Search">
            </div>
          </form>
          <br><br>
          <?php if ($from_date && $to_date && $companyID) { ?>
            <div class="col-md-12">&nbsp;</div>
            <table class="table table-bordered table-hover">
              <thead>
                <tr class="alert-info">
                  <th colspan="6">Account Bills</th>
                </tr>
                <tr>
                  <th>No.</th>
                  <th>Bill ID</th>
                  <th>Shop Name</th>
                  <th class="text-right">Amount</th>
                  <th>Paid/Unpaid</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i = 1;
                if ($accountBills) {
                  foreach ($accountBills as $a) {
                    if ($a['cancel'] == 1) {
                      $tr_cls = 'class="alert-danger"';
                      $account_cancel_total += $a['amount'];
                    } elseif ($a['paid'] == 1) {
                      $tr_cls = 'class="alert-success"';
                      $account_paid_total += $a['amount'];
                    } else {
                      $tr_cls = 'class="alert-warning"';
                      $account_pending_total += $a['amount'];
                    }
                    ?>
                    <tr <?php echo $tr_cls; ?>>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $a['BillNo']; ?></td>
                      <td><?php echo $a['ShopName']; ?></td>
                      <td class="text-right"><?php echo number_format($a['amount'], 2); ?></td>
                      <td><?php echo $a['paid'] > 0 ? 'Yes' : 'No'; ?></td>
                      <td><?php echo date('d F, Y', $a['timestamp']); ?></td>
                    </tr>
                    <?php
                  }
                  // show the sub total tr
                } else {
                  ?>
                  <tr>
                    <td colspan="6" class="alert-danger">No results found.</td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>

            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th colspan="6" class="alert-info">Direct Purchase Bills</th>
                </tr>
                <tr>
                  <th>No.</th>
                  <th>Bill ID</th>
                  <th>Shop Name</th>
                  <th class="text-right">Amount</th>
                  <th>Paid/Unpaid</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i = 1;
                if ($directBills) {
                  foreach ($directBills as $d) {
                    if ($d['cancel'] == 1) {
                      $tr_cls = 'class="alert-danger"';
                      $direct_cancel_total += $d['amount'];
                    } elseif ($d['paid'] == 1) {
                      $tr_cls = 'class="alert-success"';
                      $direct_paid_total += $d['amount'];
                    } else {
                      $tr_cls = 'class="alert-warning"';
                      $direct_pending_total += $d['amount'];
                    }
                    ?>
                    <tr <?php echo $tr_cls; ?>>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $d['bill_id']; ?></td>
                      <td><?php echo $d['ShopName']; ?></td>
                      <td class="text-right"><?php echo number_format($d['amount'], 2); ?></td>
                      <td><?php echo $d['paid'] > 0 ? 'Yes' : 'No'; ?></td>
                      <td><?php echo date('d F, Y', $d['timestamp']); ?></td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="6" class="alert-danger">No results found.</td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th colspan="6" class="alert-info">Debit Voucher Bills</th>
                </tr>
                <tr>
                  <th>No.</th>
                  <th>Bill ID</th>
                  <th>Shop Name</th>
                  <th class="text-right">Amount</th>
                  <th>Paid/Unpaid</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i = 1;
                if ($debitBills) {
                  foreach ($debitBills as $d) {
                    if ($d['cancel'] == 1) {
                      $tr_cls = 'class="alert-danger"';
                      $debit_cancel_total += $d['amount'];
                    } elseif ($d['payment_type'] != '') {
                      $tr_cls = 'class="alert-success"';
                      $debit_paid_total += $d['amount'];
                    } else {
                      $tr_cls = 'class="alert-warning"';
                      $debit_pending_total += $d['amount'];
                    }
                    ?>
                    <tr <?php echo $tr_cls; ?>>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $d['id']; ?></td>
                      <td><?php echo $d['ShopName']; ?></td>
                      <td class="text-right"><?php echo number_format($d['amount'], 2); ?></td>
                      <td><?php echo ($d['payment_type'] != '') ? 'Yes' : 'No'; ?></td>
                      <td><?php echo date('d F, Y', $d['timestamp']); ?></td>
                    </tr>
                    <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="6" class="alert-danger">No results found.</td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          <?php } ?>

            <div class="panel panel-info">
              <div class="panel-heading">
                <h3 class="panel-title">Totals</h3>
              </div>
              <div class="panel-body">

                <h5>Inventory Bill Totals</h5>
                <div class="col-md-4 alert alert-success"><strong>Paid Amount:</strong> <?php echo number_format($account_paid_total, 2); ?></div>
                <div class="col-md-4 alert alert-warning"><strong>Pending Amount:</strong> <?php echo number_format($account_pending_total, 2); ?></div>
                <div class="col-md-4 alert alert-danger"><strong>Canceled Bills:</strong> <?php echo number_format($account_cancel_total, 2); ?></div>

                <h5>Direct Purchases</h5>
                <div class="col-md-4 alert alert-success"><strong>Paid Amount:</strong> <?php echo number_format($direct_paid_total, 2); ?></div>
                <div class="col-md-4 alert alert-warning"><strong>Pending Amount:</strong> <?php echo number_format($direct_pending_total, 2); ?></div>
                <div class="col-md-4 alert alert-danger"><strong>Canceled Bills:</strong> <?php echo number_format($direct_cancel_total, 2); ?></div>
                
                <h5>Debit Vouchers</h5>
                <div class="col-md-4 alert alert-success"><strong>Paid Amount:</strong> <?php echo number_format($debit_paid_total, 2); ?></div>
                <div class="col-md-4 alert alert-warning"><strong>Pending Amount:</strong> <?php echo number_format($debit_pending_total, 2); ?></div>
                <div class="col-md-4 alert alert-danger"><strong>Canceled Bills:</strong> <?php echo number_format($debit_cancel_total, 2); ?></div>

                <h5>Grand Totals</h5>
                <div class="col-md-4 alert alert-success"><strong>Paid Amount:</strong> <?php echo number_format($account_paid_total + $direct_paid_total + $debit_paid_total, 2); ?></div>
                <div class="col-md-4 alert alert-warning"><strong>Pending Amount:</strong> <?php echo number_format($account_pending_total + $direct_pending_total + $debit_pending_total, 2); ?></div>
                <div class="col-md-4 alert alert-info"><strong>Grand Total:</strong> <?php echo number_format($account_paid_total + $direct_paid_total + $debit_paid_total + $account_pending_total + $direct_pending_total + $debit_pending_total, 2); ?></div>

              </div>
            </div>

            <div class="col-md-12">&nbsp;</div>


        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>