<?php

require_once('class.database.php');
require_once('hijri_cal.php');

class Mtx_family {

  private $file_no;
  private $card;
  private $age;
  private $prefix;
  private $first_name;
  private $father_prefix;
  private $father_name;
  private $surname;
  private $hof;
  private $gender;
  private $mohallah;
  private $marital;
  private $address;
  private $email;
  private $code;
  private $occupation;
  private $ejamaat;
  private $hijari;
  private $phone;
  private $off_phone;
  private $mobile;
  private $adults;
  private $child;
  private $diabetic;
  private $diabetic_roti;
  private $hub_amount;
  private $month;
  private $year;
  private $tiffin;
  private $pay_type;
  private $payment_date;
  private $niyaaz_date;
  private $payment_term;
  private $scanning;
  private $category;
  // fix for sms sending
  private $sms_vendor = NULL;
  private $computek_senderID = NULL;
  private $computek_user_name = NULL;
  private $computek_password = NULL;
  private $masaar_sender_id = NULL;
  private $masaar_API_key = NULL;
  private $masaar_route = NULL;
  private $masaar_country_code = NULL;

  public function Mtx_family($family_id = '') {
    if ($family_id != '') {
      if ($this->setSabil_id($family_id)) {
        if ($this->load_family())
          return TRUE;
        else
          return FALSE;
      } else
        return FALSE;
    }
  }

  public function __destruct() {
    // TODO: destructor code
  }

  private function load_family() {
    global $database;
    $query = "SELECT * FROM `family` WHERE `FileNo`='" . $this->file_no . "'";
    $result = $database->query_fetch_full_result($query);

    if (!$result)
      return FALSE;

    $result = $result[0];
    $this->file_no = $result['FileNo'];
    $this->code = $result['fc_code'];
    $this->prefix = $result['prefix'];
    $this->first_name = $result['first_name'];
    $this->father_prefix = $result['father_prefix'];
    $this->father_name = $result['father_name'];
    $this->surname = $result['surname'];
    $this->card = $result['Card'];
    $this->age = $result['Age'];
    $this->hof = $result['HOF'];
    $this->gender = $result['Sex'];
    $this->email = $result['email'];
    $this->marital = $result['MaritualStatus'];
    $this->mohallah = $result['Mohallah'];
    $this->address = $result['Address'];
    $this->occupation = $result['Occupation'];
    $this->ejamaat = $result['ejamatID'];
    $this->phone = $result['RPh'];
    $this->off_phone = $result['OPh'];
    $this->mobile = $result['MPh'];
    $this->adults = $result['adults'];
    $this->child = $result['Childrens'];
    $this->diabetic = $result['diabetics'];
    $this->tiffin = $result['tiffin_size'];
    $this->pay_type = $result['pay_type'];
    $this->payment_date = $result['payment_date'];
    $this->niyaaz_date = $result['niyaaz_date'];
    $this->payment_term = $result['payment_term'];
    $this->scanning = $result['show_in_scanning'];
    $this->category = $result['category'];

    $query = "SELECT * FROM `fmb_hub` WHERE `FileNo`='" . $this->file_no . "'";
    $result = $database->query_fetch_full_result($query);
    $result = $result[0];
    $this->hub_amount = $result['hub_raqam'];
    if ($result['paid_till'] && $result['paid_till'] > 0) {
      $hijri_date = HijriCalendar::GregorianToHijri($result['paid_till']);
      $this->month = $hijri_date[0];
      $this->year = $hijri_date[2];
    } else {
      $this->month = '';
      $this->year = '';
    }

    return TRUE;
  }

  public function delete_family() {
    global $database;
    $query = "DELETE FROM `family` WHERE `FileNo`='" . $this->file_no . "'";
    $result = $database->query($query);

    if ($result) {
      $query = "DELETE FROM `fmb_hub` WHERE `FileNo` = '" . $this->file_no . "'";
      $result = $database->query($query);
    }
    if ($result)
      return TRUE;
    else
      return FALSE;
  }

  public function add_new_family($user_id) {
    global $database;
    $open_date = date('Y-m-d');
    $query = "INSERT INTO family(`FileNo`, `fc_code`,`prefix`,`first_name`,`father_prefix`,`father_name`,`surname`, `Mohallah`, `Address`,`email`, `ejamatID`, `RPh`, `OPh`, `MPh`, `Card`, `Age`, `Sex`, `MaritualStatus`, `Occupation`, `adults`, `Childrens`, `diabetics`, `diabetics_roti`, `tiffin_size`, `payment_term`, `show_in_scanning`, `category`, `password`, `open_date`) VALUES('" . $this->file_no . "','" . $this->code . "','" . $this->prefix . "','" . $this->first_name . "','" . $this->father_prefix . "','" . $this->father_name . "','" . $this->surname . "','" . $this->mohallah . "','" . $this->address . "','" . $this->email . "','" . $this->ejamaat . "','" . $this->phone . "','" . $this->off_phone . "','" . $this->mobile . "','" . $this->card . "','" . $this->age . "','" . $this->gender . "','" . $this->marital . "','" . $this->occupation . "','" . $this->adults . "','" . $this->child . "','" . $this->diabetic . "','" . $this->diabetic_roti . "','" . $this->tiffin . "', '" . $this->payment_term . "', '" . $this->scanning . "', '" . $this->category . "', '" . md5($this->file_no) . "', '$open_date')";

    $result = $database->query($query) or die(mysql_error());
    $time = time();
    $query = "INSERT INTO `tiffin_size`(`FileNo`,`new_tiffin_size`,`user_id`,`timestamp`) VALUES('" . $this->file_no . "','" . $this->tiffin . "','$user_id','$time')";

    $result = $database->query($query) or die(mysql_error());

    return $result;
  }

  public function update_family() {
    global $database;
    $hof = trim($this->prefix . ' ' . $this->first_name . ' ' . $this->father_prefix . ' ' . $this->father_name . ' ' . $this->surname);
    $hof = preg_replace('!\s+!', ' ', $hof);
    $query = "UPDATE `family` SET
             `fc_code`='" . $this->code . "',
             `prefix`='" . $this->prefix . "',
             `first_name`='" . $this->first_name . "',
             `father_prefix`='" . $this->father_prefix . "',
             `father_name`='" . $this->father_name . "',
             `surname`='" . $this->surname . "',
               `HOF` = '" . $hof . "',
            `Mohallah`='" . $this->mohallah . "',
               `Address`='" . $this->address . "',
               `email`='" . $this->email . "',
                 `ejamatID`='" . $this->ejamaat . "',
                 `RPh`='" . $this->phone . "',
                 `OPh`='" . $this->off_phone . "',
                 `MPh`='" . $this->mobile . "',
                 `Card`='" . $this->card . "',
                 `Age`='" . $this->age . "',
                 `Sex`='" . $this->gender . "',
                 `MaritualStatus`='" . $this->marital . "',
                 `Occupation`='" . $this->occupation . "',
                 `adults`='" . $this->adults . "',
                 `Childrens`='" . $this->child . "',
                 `diabetics`='" . $this->diabetic . "',
                 `diabetics_roti`='" . $this->diabetic_roti . "',
                 `payment_term` = '" . $this->payment_term . "',
                 `show_in_scanning` = '" . $this->scanning . "',
                 `category` = '" . $this->category . "'
            WHERE `FileNo`='" . $this->file_no . "'";
    $result = $database->query($query);
    return $result;
  }

  function get_blank_first_name() {
    global $database;
    $query = "SELECT *, COUNT(*) AS rows  FROM `family` WHERE `first_name` = '' LIMIT 0, 1";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  function update_hof($prefix, $first, $father_prefix, $father, $surname, $gender, $thali_id) {
    global $database;
    $query = "UPDATE `family` SET `prefix` = '$prefix', `first_name` = '$first', `father_prefix` = '$father_prefix', `father_name` = '$father', `surname` = '$surname', `Sex` = '$gender' WHERE `FileNo` = '$thali_id'";
    $result = $database->query($query);
    return $result;
  }

  public function reopen_closed_account($fileNo, $ts, $user_id) {
    global $database;
    $query = "SELECT `open_date`,`close_date` FROM `family` WHERE `FileNo` = '$fileNo'";
    $result = $database->query_fetch_full_result($query);
    $close_date = $result[0]['close_date'];
    if ($close_date > 0) {
      $dt = HijriCalendar::GregorianToHijri($close_date);
      $from_dt = $dt[0] . '-' . $dt[2];
      $tdt = HijriCalendar::GregorianToHijri();
      $to_dt = $tdt[0] . '-' . $tdt[2];
      $to = time();
      echo $from_dt;
      $query = "INSERT INTO `fmb_hub_exception`(`FileNo`, `from_date`, `to_date`, `user_id`, `from_ts`, `to_ts`) VALUES('$fileNo', '$from_dt', '$to_dt', '$user_id', '$close_date', '$to')";
      $database->query($query);
      //$exceptions = $this->fmb_hub_exception($fileNo, $from_dt, $to_dt, $user_id, $close_date, $to);
      $prv_date = $result[0]['open_date'];
      $close_ts = date('Y-m-d', $close_date);
      $reopen_date = $ts;
      $query = "INSERT INTO `family_account_log`(`FileNo`, `previous_ts`, `close_ts`, `reopen_ts`, `reopen_user_id`) VALUES('$fileNo', '$prv_date', '$close_ts', '$reopen_date', '$user_id') ON DUPLICATE KEY UPDATE `previous_ts` = '$prv_date', `close_ts` = '$close_ts', `reopen_ts` = '$reopen_date', `reopen_user_id` = '$user_id'";
      $database->query($query);
      
      $query = "UPDATE `family` SET `close_date` = '0', `close_months` = '0', `open_date` = '$reopen_date' WHERE `FileNo` = '$fileNo'";
      $result = $database->query($query);
      return $result;
    } else {
      return 'active';
    }
  }

  public function get_all_familyfor_report($mh = FALSE, $show_all = TRUE, $ignore_vc = FALSE, $check_show_scanning = FALSE) {
    global $database;
    $query = "SELECT f.FileNo, f.prefix, f.first_name, f.father_prefix, f.father_name, f.surname, f.MPh, f.tiffin_size, f.close_date, f.Mohallah, f.HOF, f.fc_code, (SELECT amount FROM `takhmeen_amount` ta WHERE ta.FileNo LIKE f.FileNo LIMIT 0, 1) takhmeen_amount FROM family f";

    $condition = array();
    if ($mh && $mh != 'all')
      $condition[] = " f.`Mohallah` LIKE '$mh'";
    if (!$show_all)
      $condition[] = " f.`close_date` = '0'";
    if ($ignore_vc)
      $condition[] = " f.`tiffin_SIZE` != 'vc'";
    if ($check_show_scanning)
      $condition[] = " f.`show_in_scanning` = '1'";
    if (count($condition) > 0)
      $update = " WHERE " . implode(" AND ", $condition);
    else
      $update = "";
    $query .= $update;
    //echo $query;
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_family($page_num, $row_count) {
    global $database;
    $limit = ($page_num - 1) * $row_count;
    $query = "SELECT * FROM family WHERE close_date = '0' LIMIT $limit,$row_count";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_diabetics_roti($mh = FALSE) {
    global $database;
    $query = "SELECT SUM(`diabetics_roti`) AS `Roti` FROM `family` WHERE `close_date` = '0'";
    if ($mh)
      $query = "SELECT SUM(`diabetics_roti`) AS `Roti` FROM `family` WHERE `close_date` = '0' AND `Mohallah` LIKE '$mh'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_mohallah_family($page_num, $row_count, $mohallah = FALSE) {
    global $database;
    $limit = ($page_num - 1) * $row_count;
    $query = "SELECT * FROM family WHERE `close_date` = '0' LIMIT $limit,$row_count";
    if ($mohallah)
      $query = "SELECT * FROM family WHERE `close_date` = '0' AND `Mohallah` LIKE '$mohallah' LIMIT $limit,$row_count";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_count_tiffin_size() {
    global $database;
    $query = "SELECT * FROM tiffin_details";
    $result = $database->query_fetch_full_result($query);
    $sum = 0;
    foreach ($result as $tiffin) {
      $tiffin_details[$tiffin['size']] = $tiffin['person'];
    }
    $query = "Select FileNo, (SELECT new_tiffin_size FROM tiffin_size WHERE FileNo LIKE f.FileNo ORDER BY id DESC LIMIT 0,1) tiffin_size from family f WHERE close_date='0' AND show_in_scanning = '1'";
    $result = $database->query_fetch_full_result($query);
    foreach ($result as $row) {
      if (isset($tiffin_details[$row['tiffin_size']])) {
        $sum += $tiffin_details[$row['tiffin_size']];
      }
    }

    return $sum;
  }

  public function get_all_tiffin_size($date) {
    global $database;
    $query = "SELECT * FROM tiffin_details";
    $result = $database->query_fetch_full_result($query);
    $dateval = explode("-", $date);
    $day = $dateval[2];
    $month = $dateval[1];
    $year = $dateval[0];
    $start_time = mktime(0, 0, 0, $month, $day, $year);
    $end_time = mktime(23, 59, 59, $month, $day, $year);
    $ary_tiffin = array();
    foreach ($result as $size) {
      $query = "select count(*) as count_size from daily_entry de inner join family f on f.FileNo = de.family_id where f.tiffin_size = '$size[size]' AND de.`date` between '$start_time' AND '$end_time'";
      //echo $query;
      $result = $database->query_fetch_full_result($query);
      $ary_tiffin[$size['size']] = $result[0]['count_size'];
    }
    return $ary_tiffin;
  }

  public function get_tiffin_group($mohallah = FALSE) {
    global $database;

    if ($mohallah)
      $query = "SELECT FileNo, close_date, (SELECT new_tiffin_size FROM tiffin_size WHERE FileNo LIKE f.FileNo ORDER BY id DESC LIMIT 0,1) tiffin_size FROM family f WHERE close_date = '0' AND Mohallah LIKE '$mohallah'";
    else
      $query = "SELECT FileNo, close_date, (SELECT new_tiffin_size FROM tiffin_size WHERE FileNo LIKE f.FileNo ORDER BY id DESC LIMIT 0,1) tiffin_size FROM family f WHERE close_date = '0'";
    $result = $database->query_fetch_full_result($query);
    $exceptions = $this->get_all_hub_exceptions();
    $ary_tiffin = array();
//    $str = array();
    foreach ($result as $row) {
      if (!in_array($row['FileNo'], $exceptions)) {
//        if($row['tiffin_size'] == '') $str[] = $row['FileNo'];
        if (isset($ary_tiffin[$row['tiffin_size']])) {
          $ary_tiffin[$row['tiffin_size']] += 1;
        } else {

          $ary_tiffin[$row['tiffin_size']] = 1;
        }
      }
    }
//    echo '<pre>';
//    if(count($str) > 0) print_r($str);
//    //echo implode(', ', $str);
//    //exit;
    return $ary_tiffin;
  }

  public function get_tiffin_size() {
    global $database;
    $query = "SELECT * FROM `tiffin_details` WHERE `size` NOT LIKE '%X'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_X_sizes($tfn_size, $count = FALSE) {
    global $database;
    $query = "SELECT * FROM `tiffin_details` WHERE `size` LIKE '$tfn_size%'";
    if ($count) {
      $result = mysql_num_rows(mysql_query($query));
      return $result;
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_hub_exceptions() {
    global $database;
    $month = date('m');
    $day = date('d');
    $year = date('Y');
    $ary_ret = array();
    $timestamp = mktime(0, 0, 0, $month, $day, $year);
    $query = "SELECT `FileNo` FROM `fmb_hub_exception` WHERE `from_ts` <= '$timestamp' AND `to_ts` >= '$timestamp'";
    $result = $database->query_fetch_full_result($query);
    if ($result) {
      foreach ($result as $row) {
        $ary_ret[] = $row['FileNo'];
      }
    }
    return $ary_ret;
  }

  public function get_total_family($mohallah = FALSE) {
    global $database;
    $ts = time();
    $query = "SELECT COUNT(f.`FileNo`) c FROM `family` f WHERE close_date = '0' AND f.FileNo NOT IN (SELECT e.FileNo FROM fmb_hub_exception e WHERE e.from_ts <= $ts AND e.to_ts >= $ts)";
    if ($mohallah) {
      if ($mohallah != 'all')
        $query = "SELECT COUNT(`FileNo`) c FROM `family` f WHERE close_date = '0' AND `Mohallah` LIKE '$mohallah' AND f.FileNo NOT IN (SELECT e.FileNo FROM fmb_hub_exception e WHERE e.from_ts <= $ts AND e.to_ts >= $ts)";
    }
    $result = $database->query_fetch_full_result($query);

    return $result[0]['c'];
  }

  public function get_FileNo($command = FALSE, $tanzeem = FALSE) {
    global $database;
    $query = "SELECT FileNo FROM family";
    if ($command)
      $query = "SELECT DISTINCT(`FileNo`), tiffin_size, CONCAT(`prefix`, ' ', `first_name`, ' ', `father_prefix`, ' ', `father_name`, ' ', `surname`) HOF_NAME FROM family WHERE `close_date` = '0'";
    if ($tanzeem && $tanzeem != '')
      $query .= " AND `Mohallah` LIKE '$tanzeem'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function scanning_tot_family($from, $to, $tanzeem) {
    global $database;
    $datediff = strtotime($to) - strtotime($from);
    $month_days = floor($datediff / (60 * 60 * 24));
    $sfrom = explode('-', $from);
    $from_month = $sfrom[1];
    $from_year = $sfrom[0];
    $from_day = $sfrom[2];
    $fromDate = mktime(0, 0, 0, $from_month, 1, $from_year);
    $sto = explode('-', $to);
    $to_day = $sto[2];
    $to_month = $sto[1];
    $to_year = $sto[0];
    $toDate = mktime(23, 59, 59, $to_month, $to_day, $to_year);
    $gbl_condition = ($tanzeem == '') ? '' : "family_id IN ( SELECT FileNo FROM family WHERE Mohallah LIKE '$tanzeem' ) AND";
    $condition = ($tanzeem == '') ? '' : " WHERE `Mohallah` LIKE '$tanzeem'";
    $query = "SELECT COUNT(*) TOT_FAMILY FROM  `family` $condition";
    $result = $database->query_fetch_full_result($query);
    $tot_family = $result[0]['TOT_FAMILY'];

    $condition = ($tanzeem == '') ? '' : "`Mohallah` LIKE '$tanzeem' AND ";
    $query = "SELECT COUNT(`FileNo`) OPEN_ACCT FROM `family` WHERE $condition `close_date` =  '0'";
    $result = $database->query_fetch_full_result($query);
    $open_acct = $result[0]['OPEN_ACCT'];
    $day = $from_day;
    $ary_count = array();
    while ($month_days >= 0) {
      $from = mktime(0, 0, 0, $from_month, $day, $from_year);
      $to = mktime(23, 59, 59, $from_month, $day, $from_year);
      $query = "SELECT COUNT(*) ISSUE_COUNT FROM  `daily_entry` WHERE $gbl_condition `date` BETWEEN '$from' AND '$to'";

      $result = $database->query_fetch_full_result($query);
      $issue_count = $result[0]['ISSUE_COUNT'];

      $condition = ($tanzeem == '') ? '' : "FileNo IN ( SELECT FileNo FROM family WHERE Mohallah LIKE '$tanzeem' ) AND";
      $query = "SELECT COUNT(*) STOP_REQUEST FROM `fmb_hub_exception` WHERE $condition `from_ts` >= '$from' AND `to_ts` <= '$to'";
      $result = $database->query_fetch_full_result($query);
      $stop_request = $result[0]['STOP_REQUEST'];
      $date = date('D d F, Y', $from);
      $ary_count[$date]['ISSUE_COUNT'] = $issue_count;
      $ary_count[$date]['STOP_REQUEST'] = $stop_request;
      $ary_count[$date]['TIMESTAMP'] = $from;
      $day++;
      $month_days -= 1;
    }
    return array('TOT_FAMILY' => $tot_family, 'OPEN_ACCT' => $open_acct, 'RESULTS' => $ary_count);
  }

  public function get_FileNo_daily_entry($from = FALSE, $to = FALSE) {
    global $database;
    $query = "SELECT DISTINCT(`family_id`) FROM `daily_entry`";
    if (($from != '') && ($to != '')) {
      $query = "SELECT DISTINCT(`family_id`) FROM `daily_entry` WHERE `date` BETWEEN '$from' AND '$to'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_entry_from_daily_entry($FileNo, $from = FALSE, $to = FALSE) {
    global $database;
    $query = "SELECT * FROM `daily_entry` WHERE `family_id` = '$FileNo' ";
    if (($from != '') && ($to != '')) {
      $query = "SELECT * FROM `daily_entry` WHERE `family_id` = '$FileNo' AND `date` BETWEEN '$from' AND '$to'";
    }
    return mysql_num_rows(mysql_query($query));
  }

  public function get_date_from_daily_entry($FileNo) {
    global $database;
    $query = "SELECT * FROM `daily_entry` WHERE `family_id` = '$FileNo'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function month_day_count($month) {
    global $database;
    $query = "SELECT * FROM `month_day_count` WHERE `month` = '$month'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function add_month_day_count($month, $year, $count) {
    global $database;
    $mon = $month . '-' . $year;
    $query = "INSERT INTO `month_day_count`(`month`,`day_count`) VALUES('$mon','$count')";
    $result = $database->query($query);
    return $result;
  }

  public function get_single_family($fId) {
    global $database;
    $query = "Select FileNo, HOF, CONCAT(`prefix`, ' ', `first_name`, ' ', `father_prefix`, ' ', `father_name`, ' ', `surname`) HOF_NAME, ejamatID, tiffin_size, Mohallah, MPh from `family` where FileNo LIKE '$fId'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function check_past_record_hub_exception($fId) {
    global $database;
    $query = "Select * from `fmb_hub_exception` where FileNo = '$fId'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function add_muasaat($fileNo, $cost, $size_id) {
    global $database;
    $time = time();
    $query = "INSERT INTO muasaat(`fmb_id`,`from_month`,`cost`,`size_id`) VALUES('$fileNo','$time','$cost','$size_id')";
    $result = $database->query($query);
    return $result;
  }

  public function get_data_for_muasaat() {
    global $database;
    $query = "SELECT FileNo FROM `tiffin_size` WHERE `new_fmb_hub` IS NOT NULL GROUP BY FileNo ORDER BY `id`";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_data_for_muasaat_ts($year, $file) {
    global $database;
    $query = "SELECT * FROM `tiffin_size` WHERE `user_ts` LIKE '$year' AND `FileNo` LIKE '$file' ORDER BY `id` DESC LIMIT 0,1";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_tiffin_cost($year, $size) {
    global $database;
    $query = "SELECT * FROM `tiffin_cost` tc INNER JOIN `tiffin_details` td ON td.id = tc.size_id WHERE `year` LIKE '$year' AND `size` = '$size'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function check_close_family($fId) {
    global $database;
    $query = "SELECT * FROM `family` WHERE FileNo LIKE '$fId'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function close_reopen_difference($fId) {
    global $database;
    $query = "SELECT * FROM `fmb_hub_exception` WHERE FileNo = '$fId'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_last_year_month($fId) {
    global $database;
    $query = "SELECT h.FileNo, h.paid_till, (select new_fmb_hub from tiffin_size where FileNo = h.FileNo order by id DESC LIMIT 0,1) hub_raqam, (select new_tiffin_size from tiffin_size where FileNo = h.FileNo order by id DESC LIMIT 0,1) tiffin_size FROM `fmb_hub` h INNER JOIN `tiffin_size` ts ON h.FileNo = ts.FileNo WHERE h.FileNo = '$fId'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_last_year_month_home_sabil($fId) {
    global $database;
    $query = "SELECT * FROM `sabil_home` WHERE `FileNo` = '$fId'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_last_year_month_vepaar_sabil($fId) {
    global $database;
    $query = "SELECT * FROM `sabil_vepaar` WHERE `FileNo` = '$fId'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_partial_amount($fId) {
    global $database;
    $query = "SELECT SUM(`amount`) AS PartialAmount FROM  `partial_payments` WHERE FileNo = '$fId' AND `close_date` = '0'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function close_account($fId, $ts = FALSE) {
    global $database;
    if (!$ts)
      $ts = time();
    $query = "UPDATE `family` SET close_date = '$ts' WHERE `FileNo` = '$fId'";
    $result = $database->query($query);
    return $result;
  }

  public function get_all_family_by_date($start_time, $end_time) {
    global $database;
    $query = "SELECT * FROM `daily_entry` WHERE `date` BETWEEN '$start_time' AND '$end_time'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_Mohallah() {
    global $database;
    $query = "SELECT DISTINCT(Mohallah) FROM family";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_category() {
    global $database;
    $query = "SELECT DISTINCT(category) FROM category_lookup";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function family_by_mohallah($mohallah) {
    global $database;
    if ($mohallah) {
      if ($mohallah == 'all')
        $query = "SELECT * FROM family WHERE `close_date`= '0'";
      else
        $query = "SELECT * FROM family WHERE `Mohallah` LIKE '$mohallah' AND `close_date` = '0'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function barakat_received($mohallah = NULL) {
    global $database;

    $query = "SELECT *, CONCAT(`prefix`, ' ', `first_name`, ' ', `father_prefix`, ' ', `father_name`, ' ', `surname`) HOF_NAME FROM `family`";
    if ($mohallah && strtoupper($mohallah) != 'ALL')
      $query .= " WHERE `Mohallah` = '$mohallah'";
    $query .= " GROUP BY `fc_code`";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  function update_person_count($tfn, $timestamp) {
    global $database;
    $query = "UPDATE `daily_menu` SET `person_count` = '$tfn' WHERE `timestamp` = '$timestamp'";
    $result = $database->query($query);
    return $result;
  }

  public function search_by_fc_code($code) {
    global $database;
    $query = "SELECT *, IF(`close_date` = 0, 'Open', 'Close') receives_barakat, CONCAT(`prefix`, ' ', `first_name`, ' ', `father_prefix`, ' ', `father_name`, ' ', `surname`) HOF_NAME FROM `family` WHERE `fc_code` = '$code'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_family_by_mohallah($mohallah = FALSE, $year = FALSE, $category = FALSE) {
    global $database;

    $hijri_date = HijriCalendar::GregorianToHijri();
    $cur_hijri_month = $hijri_date[0];
    $cur_hijri_year = $hijri_date[2];
    $ary_conditions = array();
    $query = "SELECT f.FileNo,f.close_date, f.MPh, f.Mohallah, f.category, (SELECT ts.new_tiffin_size FROM tiffin_size ts WHERE ts.FileNo = f.FileNo ORDER BY ts.id DESC LIMIT 0,1) tiffin_size FROM `family` f";

    if (($mohallah && $mohallah != '') || ($year && $year) || ($category && $category != ''))
      $query .= ' WHERE';
    if ($mohallah && $mohallah != '')
      $ary_conditions[] = " f.Mohallah LIKE '$mohallah'";
    if ($year)
      $ary_conditions[] = " rh.year LIKE '$year'";
    if ($category)
      $ary_conditions[] = " f.category LIKE '$category'";
    $query .= implode('AND', $ary_conditions);
    $query .= ' ORDER BY f.FileNo ASC';

    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_hub_pending() {
    global $database;
    $hijri_date = HijriCalendar::GregorianToHijri();
    $cur_hijri_month = $hijri_date[0];
    $cur_hijri_year = $hijri_date[2];
    $time_stamp = HijriCalendar::HijriToUnix($cur_hijri_month, 01, $cur_hijri_year);
    $query = "SELECT f.FileNo,f.close_date, f.prefix, f.first_name, f.father_prefix, f.father_name, f.surname, f.MPh, f.Mohallah, (SELECT rh.year FROM fmb_receipt_hub rh WHERE rh.FileNo = f.FileNo ORDER BY rh.year DESC LIMIT 0,1) year, (SELECT ts.new_tiffin_size FROM tiffin_size ts WHERE ts.FileNo = f.FileNo ORDER BY ts.id DESC LIMIT 0,1) tiffin_size FROM family f ORDER BY f.FileNo ASC";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_sabil_home_pending() {
    global $database;
    $hijri_date = HijriCalendar::GregorianToHijri();
    $cur_hijri_month = $hijri_date[0];
    $cur_hijri_year = $hijri_date[2];
    $time_stamp = HijriCalendar::HijriToUnix($cur_hijri_month, 01, $cur_hijri_year);
    $query = "SELECT f.FileNo,f.close_date, f.prefix, f.first_name, f.father_prefix, f.father_name, f.surname, f.MPh, f.Mohallah, h.paid_till, h.amount FROM family f INNER JOIN `sabil_home` h ON f.FileNo = h.FileNo WHERE h.paid_till < $time_stamp OR h.paid_till IS NULL";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_sabil_vepaar_pending() {
    global $database;
    $hijri_date = HijriCalendar::GregorianToHijri();
    $cur_hijri_month = $hijri_date[0];
    $cur_hijri_year = $hijri_date[2];
    $time_stamp = HijriCalendar::HijriToUnix($cur_hijri_month, 01, $cur_hijri_year);
    $query = "SELECT f.FileNo, f.close_date, f.prefix, f.first_name, f.father_prefix, f.father_name, f.surname, f.MPh, f.Mohallah, h.paid_till, h.amount FROM family f INNER JOIN `sabil_vepaar` h ON f.FileNo = h.FileNo WHERE h.paid_till < $time_stamp OR h.paid_till IS NULL";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function search_family_byname($name) {
    global $database;
    $query = "Select * from family where HOF='$name'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function check_month_exist($user_ts, $fId) {
    $query = "SELECT *  FROM `tiffin_size` WHERE `FileNo` LIKE '$fId' AND `user_ts` LIKE '$user_ts'";
    return mysql_num_rows(mysql_query($query));
  }

  public function update_tiffin_hub($fileNo, $new_hub, $new_tiffin, $user_ts, $user_id) {
    global $database;
    $time = time();

    //to get old tiffin size
    $query = "SELECT * FROM `family` WHERE `FileNo` = '$fileNo'";
    $tiffin = $database->query_fetch_full_result($query);
    $old_size = floatval($tiffin[0]['tiffin_size']);

    //to get old hub raqam
    $query = "SELECT `hub_raqam` FROM `fmb_hub` WHERE `FileNo` = '$fileNo'";
    $hub = $database->query_fetch_full_result($query);
    $old_hub = floatval($hub[0]['hub_raqam']);

    if ($new_hub != '')
      $query = "INSERT INTO `tiffin_size`(`FileNo`,`old_tiffin_size`,`new_tiffin_size`,`old_fmb_hub`,`new_fmb_hub`,`user_id`, `user_ts`,`timestamp`) VALUES('$fileNo','$old_size','$new_tiffin','$old_hub','$new_hub','$user_id','$user_ts', '$time')";
    else
      $query = "INSERT INTO `tiffin_size`(`FileNo`,`old_tiffin_size`,`new_tiffin_size`,`old_fmb_hub`,`user_id`, `user_ts`,`timestamp`) VALUES('$fileNo','$old_size','$new_tiffin','$old_hub','$user_id','$user_ts', '$time')";

    $result = $database->query($query) or die(mysql_error());


    //update new tiffin size in family tbl
    $query = "UPDATE `family` SET `tiffin_size`='$new_tiffin' WHERE `FileNo` LIKE '$fileNo'";
    $database->query($query);

    //update new hub raqam in fmb_hub tbl
    $query = "UPDATE `fmb_hub` SET `hub_raqam`='$new_hub' WHERE `FileNo` LIKE '$fileNo'";
    $database->query($query);
    return $result;
  }

  public function update_tiffin_hub_on_exist($fileNo, $new_hub, $new_tiffin, $user_ts, $user_id, $to_ts) {
    global $database;

    $query = "SELECT `id`  FROM `tiffin_size` WHERE `FileNo` LIKE '$fileNo' AND `user_ts` LIKE '$user_ts'";
    $result = $database->query_fetch_full_result($query);
    $id = $result[0]['id'];
    $timestamp = time();
    if (!empty($new_hub))
      $query = "UPDATE `tiffin_size` SET `new_tiffin_size`='$new_tiffin', `new_fmb_hub`='$new_hub', `user_ts`='$user_ts', `timestamp`='$timestamp' WHERE `id` = '$id'";
    else
      $query = "UPDATE `tiffin_size` SET `new_tiffin_size`='$new_tiffin', `user_ts`='$user_ts', `timestamp`='$timestamp' WHERE `id` = '$id'";

    $result = $database->query($query);

    //update new tiffin size in family tbl
    $query = "UPDATE `family` SET `tiffin_size`='$new_tiffin' WHERE `FileNo` LIKE '$fileNo'";
    $database->query($query);

    //update new hub raqam in fmb_hub tbl
    $query = "UPDATE `fmb_hub` SET `hub_raqam`='$new_hub' WHERE `FileNo` LIKE '$fileNo'";
    $database->query($query);
    return $result;
  }

  public function get_count_of_selected_date($date) {
    global $database;
    $dateval = explode("-", $date);
    $day = $dateval[2];
    $month = $dateval[1];
    $year = $dateval[0];
    $start_time = mktime(0, 0, 0, $month, $day, $year);
    $end_time = mktime(23, 59, 59, $month, $day, $year);
    $query = "SELECT * FROM `daily_entry` de INNER JOIN family f ON f.FileNo = de.family_id WHERE date between '$start_time' AND '$end_time'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function add_niyaz($name, $price) {
    global $database;
    $query = "INSERT INTO `niyaz`(`name`,`price`) VALUES('$name','$price')";
    $result = $database->query($query);
    return $result;
  }

  public function update_niyaz($id, $name, $price) {
    global $database;
    $query = "UPDATE `niyaz` SET `name` = '$name', `price` = '$price' WHERE `id` = '$id'";
    $result = $database->query($query);
    return $result;
  }

  public function delete_Niyaz($id) {
    global $database;
    $query = "DELETE FROM `niyaz` WHERE `id` = '$id'";
    $result = $database->query($query);
    return $result;
  }

  public function get_all_niyaz($id = FALSE) {
    global $database;
    $query = "SELECT * FROM `niyaz`";
    if ($id)
      $query .= " WHERE `id` = '$id'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_searched_data_by_id($id) {
    global $database;
    $query = "SELECT * FROM `family` WHERE `FileNo` LIKE '$id'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_searched_data_by_name($name) {
    global $database;
    $query = "SELECT * FROM `family` WHERE `first_name` LIKE '%$name%' OR `father_name` LIKE '%$name%' OR `surname` LIKE '%$name%' OR `HOF` LIKE '%$name%'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_family_open_date($fId) {
    global $database;
    $query = "SELECT timestamp FROM `tiffin_size` WHERE FileNo LIKE '$fId' ORDER BY `FileNo` ASC LIMIT 0,1";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }
  
  public function get_family_open_date_from_family($fId) {
    global $database;
    $query = "SELECT open_date FROM `family` WHERE FileNo LIKE '$fId'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_family_close_date($fId) {
    global $database;
    $query = "SELECT *  FROM `family` WHERE `FileNo` LIKE '$fId' LIMIT 0,1";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_count_of_selected_month($from, $to, $fileno = FALSE) {
    global $database;
    $query = "SELECT COUNT(*) AS Total, `family_id` FROM `daily_entry` WHERE `date` BETWEEN '$from' AND '$to' GROUP BY `family_id`";
    if ($fileno) {
      $query = "SELECT COUNT(*) AS Total FROM `daily_entry` WHERE `date` BETWEEN '$from' AND '$to' AND `family_id` LIKE '$fileno'";
      $result = $database->query_fetch_full_result($query);
      return $result[0]['Total'];
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_name($fId) {
    global $database;
    $query = "Select FileNo, prefix, first_name, father_prefix, father_name, surname, HOF, Sex from `family` where FileNo='$fId'";
    $result = $database->query_fetch_full_result($query);

    $name = '';
    $fname = '';
    if ($result[0]['first_name'] != '') {
      if ($result[0]['father_name'] != '')
        $fname = trim($result[0]['father_prefix'] . ' ' . $result[0]['father_name'] . 'bhai');

      if ($result[0]['Sex'] == 'M') {
        if ($result[0]['first_name'] != '')
          $name = trim($result[0]['prefix'] . ' ' . $result[0]['first_name'] . 'bhai');
      } else if ($result[0]['Sex'] == 'F') {
        if ($result[0]['first_name'] != '')
          $name = trim($result[0]['prefix'] . ' ' . $result[0]['first_name'] . 'ben');
      } else {
        $name = trim($result[0]['prefix'] . ' ' . $result[0]['first_name']);
      }
      $full_name = ucwords(trim($name . ' ' . $fname . ' ' . $result[0]['surname']));
    } else
      $full_name = trim($result[0]['HOF']);
    return $full_name;
  }

  public function get_name_and_mobile($fId) {
    global $database;
    $query = "Select FileNo, prefix, first_name, father_prefix, father_name, surname, HOF, Sex, MPh from `family` where FileNo='$fId'";
    $result = $database->query_fetch_full_result($query);

    $name = '';
    $fname = '';
    if ($result[0]['first_name'] != '') {
      if ($result[0]['father_name'] != '')
        $fname = trim($result[0]['father_prefix'] . ' ' . $result[0]['father_name'] . 'bhai');

      if ($result[0]['Sex'] == 'M') {
        if ($result[0]['first_name'] != '')
          $name = trim($result[0]['prefix'] . ' ' . $result[0]['first_name'] . 'bhai');
      } else if ($result[0]['Sex'] == 'F') {
        if ($result[0]['first_name'] != '')
          $name = trim($result[0]['prefix'] . ' ' . $result[0]['first_name'] . 'ben');
      } else {
        $name = trim($result[0]['prefix'] . ' ' . $result[0]['first_name']);
      }
      $full_name = ucwords(trim($name . ' ' . $fname . ' ' . $result[0]['surname']));
    } else
      $full_name = trim($result[0]['HOF']);

    $ret['full_name'] = $full_name;
    $ret['mobile'] = $result[0]['MPh'];
    return $ret;
  }

  public function check_thali_ID_exist($thaliID) {
    global $database;
    $query = "SELECT `FileNo` FROM `family` WHERE `FileNo` LIKE '$thaliID' LIMIT 0, 1";
    $result = $database->query_fetch_full_result($query);

    if ($result) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  public function add_tanzeem($tanzeem) {
    global $database;
    $query = "INSERT INTO `tanzeem`(`name`) VALUES('$tanzeem')";
    $result = $database->query($query);
    return $result;
  }

  public function get_added_tiffin_between_dates($from, $to) {
    global $database;
    $query = "SELECT DISTINCT(`FileNo`) FileNo, (select ts.new_tiffin_size from tiffin_size ts where ts.FileNo = tss.FileNo LIMIT 0,1) new_tiffin_size, timestamp FROM tiffin_size tss WHERE timestamp between '$from' AND '$to'";

    $result = $database->query_fetch_full_result($query);
    $table = '<table class="table table-bordered table-condensed table-hover"><thead><tr><th>No.</th><th>' . THALI_ID . '</th><th>HOF Name</th><th>Tiffin size</th><th>Date</th></tr></thead><tbody>';
    if ($result) {
      $i = 1;
      foreach ($result as $record) {
        $name = ucwords(strtolower($this->get_name($record['FileNo'])));
        $table .= '<tr><td>' . $i++ . '</td><td>' . $record['FileNo'] . '</td><td>' . $name . '</td><td>' . $record['new_tiffin_size'] . '</td><td>' . date('d F, Y', $record['timestamp']) . '</td></tr>';
      }
    } else {
      $table .= '<tr><td colspan="6" class="alert-danger">Sorry! there is no tiffin added.</td></tr>';
    }
    $table .= '</tbody></table>';
    return $table;
  }

  public function get_closed_tiffin_between_dates($from, $to) {
    global $database;
    $query = "SELECT FileNo, (select ts.new_tiffin_size from tiffin_size ts where ts.FileNo = f.FileNo AND `timestamp` BETWEEN '$from' AND '$to' LIMIT 0,1) new_tiffin_size FROM family f WHERE f.close_date between '$from' AND '$to'";
    $result = $database->query_fetch_full_result($query);
    $table = '<table class="table table-bordered table-condensed table-hover"><thead><tr><th>No.</th><th>' . THALI_ID . '</th><th>HOF Name</th></tr></thead><tbody>';
    if ($result) {
      $i = 1;
      foreach ($result as $record) {
        $name = ucwords(strtolower($this->get_name($record['FileNo'])));
        $table .= '<tr><td>' . $i++ . '</td><td>' . $record['FileNo'] . '</td><td>' . $name . '</td></tr>';
      }
    } else {
      $table .= '<tr><td colspan="6" class="alert-danger">Sorry! there is no tiffin closed.</td></tr>';
    }
    $table .= '</tbody></table>';
    return $table;
  }

  public function get_all_tanzeem($prefix = FALSE) {
    global $database;
    $query = "SELECT * FROM `tanzeem`";
    if ($prefix) {
      $query = "SELECT name FROM tanzeem WHERE prefix Like '$prefix'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_distribution_center($id = FALSE) {
    global $database;
    $query = "SELECT * FROM `location`";
    if ($id) {
      $query = "SELECT * FROM `location` WHERE `id` = '$id'";
      $result = $database->query_fetch_full_result($query);
      return $result[0];
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function add_distribution_center($title, $address, $latitude, $longitude) {
    global $database;
    $query = "INSERT INTO `location`(`title`, `address`, `latitude`, `longitude`) VALUES('$title', '$address', '$latitude', '$longitude')";
    $result = $database->query($query);
    return $result;
  }

  public function update_distribution_center($id, $title, $address, $latitude, $longitude) {
    global $database;
    $query = "UPDATE `location` SET `title`= '$title',`address`= '$address',`latitude`= '$latitude',`longitude`= '$longitude' WHERE `id` = '$id'";
    $result = $database->query($query);
    return $result;
  }

  public function insert_fmb_hub_exception($file, $from, $to, $user, $from_ts, $to_ts) {
    global $database;
    $query = "INSERT INTO `fmb_hub_exception`(`FileNo`,`from_date`,`to_date`,`user_id`, `from_ts`, `to_ts`) VALUES('$file','$from','$to','$user', '$from_ts', '$to_ts')";
    $result = $database->query($query);
    return $result;
  }

  public function delete_hub_exception($eid) {
    global $database;
    $query = "DELETE FROM `fmb_hub_exception` WHERE `id` = '$eid'";
    $result = $database->query($query);
    return $result;
  }

  public function get_all_fmb_hub_exception() {
    global $database;
    $query = "SELECT * FROM `fmb_hub_exception`";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function latest_tiffin_size_year($fId) {
    global $database;
    $query = "SELECT ts.`new_tiffin_size`, ts.`new_fmb_hub`, (SELECT `paid_till` FROM `fmb_hub` h WHERE h.`FileNo` = ts.FileNo) paid_till FROM `tiffin_size` ts WHERE `FileNo` LIKE '$fId' ORDER BY `id` DESC LIMIT 0,1";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function tiffin_issued_count($month, $year, $count, $userid) {
    global $database;
    $timestamp = HijriCalendar::HijriToUnix($month, '01', $year);
    $query = "INSERT INTO `tiffin_issued_count`(`month`, `count`) VALUES('$timestamp','$count','$userid')";
    $result = $database->query($query);
    return $result;
  }

  public function get_all_month_days() {
    global $database;
    $query = "SELECT * FROM `month_day_count` ORDER BY `id` DESC";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_data_id_given_tiffin_size($fId, $timestamp) {
    global $database;
    $query = "SELECT `FileNo`,`new_tiffin_size`,`new_fmb_hub`,`timestamp` FROM `tiffin_size` WHERE `FileNo` LIKE '$fId' AND `user_ts` <= $timestamp ORDER BY id DESC LIMIT 0 , 1";
    $result = $database->query_fetch_full_result($query);
    //if any exception set for the given user and month thn make hub = 0
    $query = "SELECT * FROM `fmb_hub_exception` WHERE FileNo = '$fId' AND `from_ts` <= '$timestamp' AND `to_ts` >= '$timestamp'";
    $exceptions = $database->query_fetch_full_result($query);
    if ($exceptions) {
      $result[0]['new_fmb_hub'] = 0;
    }
    return $result;
  }

  function check_payment_mode($file) {
    global $database;
    $query = "SELECT `pay_type`, `payment_term` FROM `family` WHERE `FileNo` = '$file'";
    $result = $database->query_fetch_full_result($query);
    if ($result[0]['pay_type'] == 1)
      return $result[0]['payment_term'];
    else
      return 0;
  }

  function get_payment_method_value($file) {
    global $database;
    $query = "SELECT `pay_type`, `payment_term` FROM `family` WHERE `FileNo` = '$file'";
    $result = $database->query_fetch_full_result($query);
    $multiplier = 1;
    $increment = 0;
    if ($result[0]['pay_type'] == 1) {
      switch ($result[0]['payment_term']) {
        case 12:
          $multiplier = 1;
          $increment = 0;
          break;
        case 3:
          $multiplier = 3;
          $increment = 2;
          break;
        case 2:
          $multiplier = 6;
          $increment = 5;
          break;
        case 1:
          $multiplier = 12;
          $increment = 11;
          break;
      }
    } else {
      $multiplier = 1;
    }
    return array('MULTIPLIER' => $multiplier, 'INCREMENT' => $increment);
  }

  public function get_array_of_months($fId, $last_month, $last_year, $months = FALSE, $use_ts = FALSE, $cmd = FALSE) {

    if (!$use_ts)
      $use_ts = time();
    if (!$months) {
      $check_close = $this->check_close_family($fId);
      switch (USE_CALENDAR) {
        case 'Hijri':
          $cur_ts = HijriCalendar::GregorianToHijri($use_ts);
          $cur_month = $cur_ts[0];
          $cur_year = $cur_ts[2];
          break;
        case 'Greg':
          $cur_ts = $use_ts;
          $cur_month = date('m');
          $cur_year = date('Y');
          break;
      }
      if ($check_close) {
        if ($check_close['close_date'] != 0) {
          switch (USE_CALENDAR) {
            case 'Hijri':
              $cur_ts = HijriCalendar::GregorianToHijri($check_close['close_date']);
              $cur_month = $cur_ts[0];
              $cur_year = $cur_ts[2];
              break;
            case 'Greg':
              $cur_ts = $check_close['close_date'];
              $cur_month = date('m', $check_close['close_date']);
              $cur_year = date('Y', $check_close['close_date']);
              break;
          }
        }
      }

      if ($last_year == $cur_year) {
        $diff_months = $cur_month - $last_month;
      } else if ($last_year < $cur_year) {
        $diff_year = $cur_year - $last_year;
        if ($diff_year > 1) {
          $remaining_months_from_last = 12 - $last_month;
          $diff_months = (12 * $diff_year) + $remaining_months_from_last;
        } else {
          $remaining_months_from_last = 12 - $last_month;
          $diff_months = $remaining_months_from_last + $cur_month;
        }
      } else if ($last_year > $cur_year) {
        $diff_months = 0;
      }
    }
    if ($months)
      $months = $months;
    else
      $months = $diff_months;

    if ($cmd) {
      switch ($cmd) {
        case 'Monthly':
          $months = $months;
          break;
        case 'Quarterly':
          $months *= 2;
          break;
        case 'Semi':
          $months *= 5;
          break;
        case 'Annual':
          $months *= 11;
          break;
      }
    }

    //if($cmd && $cmd == 'MONTHS_DOUBLE') $months *= 2;
    $ary_collection = array();
    for ($i = 1; $i <= $months; $i++) {
      $frm_months = $last_month + $i;
      $pay_multiplier = $this->get_payment_method_value($fId);
      if ($frm_months > 12) {
        $fr_months = $frm_months % 12;
        if ($fr_months > 0) {
          $fr_year = floor($frm_months / 12) + $last_year;
        } else {
          $fr_year = floor(($frm_months - 1) / 12) + $last_year;
        }

        // FIX: solve month 0 error due to modulus 
        ($fr_months == 0) ? $fr_months = 12 : $fr_months = $fr_months;
      } else {
        $fr_months = $frm_months;
        $fr_year = $last_year;
      }
      $ary_collection[$fr_months . '-' . $fr_year]['MULTI'] = $pay_multiplier['MULTIPLIER'];
      $i += $pay_multiplier['INCREMENT'];
    }
    return $ary_collection;
  }

  public function get_hub_by_month($fId, $user_ts, $monthly = TRUE) {
    global $database;
    switch (USE_CALENDAR) {
      case 'Hijri':
        $ts = HijriCalendar::GregorianToHijri($user_ts);
        $month = $ts[0] . '-' . $ts[2];
        break;
      case 'Greg':
        $ts = $user_ts;
        $month = date('m', $user_ts) . '-' . date('Y', $user_ts);
        break;
    }

    //we will get ttn size & hub raqam from tfn_size tbl
    $query = "SELECT *  FROM `tiffin_size` WHERE `FileNo` LIKE '$fId' AND `user_ts` <= '$user_ts' ORDER BY id DESC LIMIT 0, 1";
    $result = $database->query_fetch_full_result($query);
    if (!$result) {
      $query = "SELECT *  FROM `tiffin_size` WHERE `FileNo` LIKE '$fId' ORDER BY id ASC LIMIT 0,1";
      $result = $database->query_fetch_full_result($query);
    }
    if ($result) {
      $tfn_size = $result[0]['new_tiffin_size'];

      $hub = $result[0]['new_fmb_hub'];

      //if hub raqam = null thn we will fetch d data from tfn cost tbl else
      // if hub raqam != null thn we will check for d month
      if ($hub == NULL) {
        $query = "SELECT * FROM `tiffin_cost` INNER JOIN `tiffin_details` ON tiffin_details.id = tiffin_cost.size_id WHERE `month` = '$month' AND `size` = '$tfn_size'";
        $tfn_cost = $database->query_fetch_full_result($query);
        if ($tfn_cost) {
          $hub = $tfn_cost[0]['cost'];
        }
      } else {
        if (DIFF_MONTH_CALC && ($monthly == 0)) {
          if ($ts[0] == 1)
            $hub = $hub / 2;
          else if ($ts[0] == 9)
            $hub = 0;
          else
            $hub = $hub;
        }
      }
      //if any exception set for the given user and month thn make hub = 0
      $query = "SELECT * FROM `fmb_hub_exception` WHERE FileNo = '$fId' AND `from_ts` <= '$user_ts' AND `to_ts` > '$user_ts'";
      $exceptions = $database->query_fetch_full_result($query);
      if ($exceptions) {
        $hub = 0;
      }
      return array("Hub_raqam" => $hub, "Tiffin_size" => $tfn_size);
    }
  }

  function get_count_issue_tiffin($date) {
    global $database;
    $dateval = explode("-", $date);
    $day = $dateval[2];
    $month = $dateval[1];
    $year = $dateval[0];
    $start_time = mktime(0, 0, 0, $month, $day, $year);
    $end_time = mktime(23, 59, 59, $month, $day, $year);
    $query = "SELECT * FROM `daily_entry` WHERE `date` BETWEEN '$start_time' AND '$end_time'";
  }

  public function get_tiffin_data($date, $cmd, $count = FALSE) {
    $ary_remaining_family = array();
    $ary_barcode_family = array();
    $dateval = explode("-", $date);
    $day = $dateval[2];
    $month = $dateval[1];
    $year = $dateval[0];
    $start_time = mktime(0, 0, 0, $month, $day, $year);
    $end_time = mktime(23, 59, 59, $month, $day, $year);
    $reports_res = $this->get_all_family_by_date($start_time, $end_time);
    if (is_array($reports_res)) {
      foreach ($reports_res as $family_res) {
        $ary_barcode_family[$family_res['family_id']] = $family_res['family_id'];
      }
    }
    $result = $this->get_all_familyfor_report(FALSE, FALSE, TRUE, TRUE);
    foreach ($result as $family) {
      $name = $this->get_name($family['FileNo']);
      $ary_all_family[$family['FileNo']] = $name;
    }
    if (count($ary_barcode_family) != 0) {
      foreach ($ary_all_family as $key => $value) {
        //if cmd = issue thn it means we want to get data for tiffin issue page
        //else for not issue page
        if ($cmd == 'issue') {
          if (array_key_exists($key, $ary_barcode_family))
            $ary_remaining_family[$key] = $value;
        } else {
          if (!array_key_exists($key, $ary_barcode_family))
            $ary_remaining_family[$key] = $value;
        }
      }
    }

    // get the exceptions
    $exceptions = $this->get_all_hub_exceptions();
    foreach ($exceptions as $fam) {
      if (array_key_exists($fam, $ary_remaining_family))
        unset($ary_remaining_family[$fam]);
    }
    if ($count)
      return count($ary_remaining_family);

    $data = '<table class="table table-hover table-condensed"><thead><th>Sr No</th><th>' . THALI_ID . '</th><th>HOF Name</th></thead><tbody>';
    if (is_array($ary_remaining_family) && !empty($ary_remaining_family)) {
      $i = 1;
      foreach ($ary_remaining_family as $key => $family) {
        $data .= '<tr><td>' . $i++ . '</td><td>' . $key . '</td><td>' . $family . '</td>';
      }
    } else {
      $data .= '<tr><td colspan="3" class="alert-danger">No Record Found</td></tr>';
    }

    $data .= '</tbody></table>';
    return $data;
  }

  public function add_takhmeen_amount($thali_id, $amount, $year, $pay_term, $ary_days, $ary_months, $ary_year, $ary_niyaz_qty, $skipKisht, $totalKisht, $ary_pay_date, $ary_amount, $user_id) {
    global $database;
    $query = "SELECT * FROM `takhmeen_amount` WHERE `FileNo` LIKE '$thali_id' AND `year` = '$year'";
    $result = $database->query_fetch_full_result($query);
    if (!$result)
      $query = "INSERT INTO `takhmeen_amount`(`FileNo`, `amount`, `year`, `payment_term`, `skip_kisht`, `total_kisht`) VALUES ('$thali_id', '$amount', '$year', '$pay_term', '$skipKisht', '$totalKisht')";
    else
      $query = "UPDATE `takhmeen_amount` SET `amount` = '$amount', `payment_term` = '$pay_term', `skip_kisht` = '$skipKisht', `total_kisht` = '$totalKisht' WHERE `FileNo` LIKE '$thali_id' AND `year` LIKE '$year'";
    $result = $database->query($query);
    $query = "DELETE FROM `takhmeen_dates` WHERE `FileNo` LIKE '$thali_id'";
    $database->query($query);
    $max = count($ary_days);
    for ($i = 0; $i < $max; $i++) {
      if ($ary_months[$i] != 0) {
        $greg_date = HijriCalendar::HijriToUnix($ary_months[$i], $ary_days[$i] - 1, $ary_year[$i]);
        $date = date('Y-m-d', $greg_date);

        $query = "INSERT INTO `takhmeen_dates`(`FileNo`, `date`, `niyaz_qty`) VALUES ('$thali_id', '$date', '$ary_niyaz_qty[$i]')";
        $result = $database->query($query);
      }
    }

    $timestamp = date('Y-m-d');
    if ($ary_pay_date) {
      foreach ($ary_pay_date as $key => $val) {
        if ($val != 0) {
          $query = "INSERT INTO `takhmeen_commitment`(`FileNo`, `takhmeen_date`, `amount`, `year`, `user_id`, `timestamp`) VALUES ( '$thali_id','$val','$ary_amount[$key]','$year','$user_id','$timestamp' )";
          $result = $database->query($query);
        }
      }
    }

    return $result;
  }

  public function add_takhmeen_commitment_dates($year, $txt_date, $txt_lable) {
    global $database;
    $query = "INSERT INTO `takhmeen_commitment_dates` (`year`, `date`, `txt_lable`) VALUES ('$year', '$txt_date', '$txt_lable')";
    $result = $database->query($query);
    return $result;
  }

  public function get_commitment_dates_of_year($year = false) {
    global $database;
    $query = "SELECT * FROM `takhmeen_commitment_dates`";
    if ($year) {
      $query .= " WHERE `year` = '$year'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_fmb_hub_record($insert_id) {
    global $database;
    $query = "SELECT * FROM `fmb_receipt_hub` WHERE `id` = '$insert_id'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_past_five_transactions($thali_id, $year) {
    global $database;
    $query = "SELECT * FROM `fmb_receipt_hub` WHERE `FileNo` LIKE '$thali_id' AND `year` LIKE '$year' ORDER BY `id` DESC LIMIT 0, 5";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_paid_amount($thali_id, $year) {
    global $database;
    $query = "SELECT SUM(`amount`) AS amount FROM `fmb_receipt_hub` WHERE `FileNo` LIKE '$thali_id' AND `year` LIKE '$year' AND `cancel` = '0'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['amount'];
  }

  public function update_takhmeen_record($thali_id, $amount, $year) {
    global $database;
    $query = "UPDATE `takhmeen_amount` SET `amount` = '$amount' WHERE `year` LIKE '$year' AND `FileNo` LIKE '$thali_id'";
    $result = $database->query($query);
    return $result;
  }

  public function get_takhmeen_record($thali_id, $limit = FALSE, $year = FALSE) {
    global $database;
    if (!$limit)
      $limit = '';
    if (!$year)
      $year = '';
    $query = "SELECT *  FROM `takhmeen_amount` WHERE `FileNo` LIKE '$thali_id' $year ORDER BY `year` DESC $limit";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function transfer_FileNo($oldFileNo, $tanzeemPrefix) {
    global $database;

    $query = "SELECT MAX(FileNo) AS FileNo FROM `family` WHERE FileNo Like '$tanzeemPrefix%'";
    $result = $database->query_fetch_full_result($query);
    $newFileNo = substr($result[0]['FileNo'], -3) + 1;
    while (strlen($newFileNo) < 3) {
      $newFileNo = '0' . $newFileNo;
    }
    $newFileNo = $tanzeemPrefix . $newFileNo;
    $mh = $this->get_all_tanzeem($tanzeemPrefix);
    $mohallah = $mh[0]['name'];
    // Daily entry
    $query = "UPDATE `daily_entry` SET `family_id` = '$newFileNo' WHERE `family_id` = '$oldFileNo'";
    $result = $database->query($query);

    // family
    $query = "UPDATE `family` SET `FileNo` = '$newFileNo', `Mohallah` = '$mohallah' WHERE `FileNo` = '$oldFileNo'";
    $result = $database->query($query);

    // fmb_hub
    $query = "UPDATE `fmb_hub` SET `FileNo` = '$newFileNo' WHERE `FileNo` = '$oldFileNo'";
    $result = $database->query($query);

    // fmb_hub_exception
    $query = "UPDATE `fmb_hub_exception` SET `FileNo` = '$newFileNo' WHERE `FileNo` = '$oldFileNo'";
    $result = $database->query($query);

    // fmb_receipt_hub
    $query = "UPDATE `fmb_receipt_hub` SET `FileNo` = '$newFileNo' WHERE `FileNo` = '$oldFileNo'";
    $result = $database->query($query);

    // vol receipt
    $query = "UPDATE `receipt` SET `FileNo` = '$newFileNo' WHERE `FileNo` = '$oldFileNo'";
    $result = $database->query($query);

    // partial_payments
    $query = "UPDATE `partial_payments` SET `FileNo` = '$newFileNo' WHERE `FileNo` = '$oldFileNo'";
    $result = $database->query($query);

    // tiffin_size
    $query = "UPDATE `tiffin_size` SET `FileNo` = '$newFileNo' WHERE `FileNo` = '$oldFileNo'";
    $result = $database->query($query);

    return $result;
  }

  function get_takhmeen_report_record($year, $tanzeem, $show_all = FALSE) {
    global $database;
    $mohallah = '';
    $join = ($show_all) ? 'RIGHT' : 'INNER';
    $condition = ($show_all) ? 'OR  `year` IS NULL' : '';
    if ($tanzeem && $tanzeem != '')
      $mohallah = "AND `Mohallah` = '$tanzeem'";
    $query = "SELECT f.FileNo, t.`amount` FROM `takhmeen_amount` t $join JOIN `family` f ON f.FileNo = t.FileNo WHERE `year` = '$year' $condition $mohallah ORDER BY f.`FileNo` ASC";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  function get_clear_amount($thali_id, $year) {
    global $database;
    $query = "SELECT SUM(`amount`) AS CLEAR_AMOUNT FROM `fmb_receipt_hub` WHERE `FileNo` LIKE '$thali_id' AND `year` LIKE '$year' AND `cancel` = '0'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['CLEAR_AMOUNT'];
  }

  function get_takhmeen_dates($day, $thali_id = FALSE) {
    global $database;
    if ($thali_id)
      $query = "SELECT `date`, `niyaz_qty` FROM `takhmeen_dates` WHERE `FileNo` LIKE '$thali_id'";
    else
      $query = "SELECT DISTINCT(`FileNo`), niyaz_qty FROM `takhmeen_dates` WHERE `date` LIKE '$day'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  function get_sms_general_settings() {
    
  }

  function send_sms($mobile, $msg) {
    global $database;

    // verify if sms settings are fetched, else fetch them
    if ($this->sms_vendor === NULL) {
      $query = "SELECT * FROM `general_settings`";
      $result = $database->query_fetch_full_result($query);
      
      $this->sms_vendor = $result[0]['default_sms_vendor'];

      $this->computek_senderID = $result[0]['sender_id'];
      $this->computek_user_name = $result[0]['user_name'];
      $this->computek_password = $result[0]['password'];

      $this->masaar_sender_id = $result[0]['masaar_sender_id'];
      $this->masaar_API_key = $result[0]['API_key'];
      $this->masaar_route = $result[0]['route'];
      $this->masaar_country_code = $result[0]['country_code'];
    }

    if ($sms_vendor == 1) {
      $url = "http://route2.computeksms.in/gosms.aspx?user=$computek_user_name&pass=$computek_password&mobs=$mobile&sender=$computek_senderID&msg=" . urlencode($msg);
    } elseif ($sms_vendor == 2) {
      $url = "http://sms.myn2p.com/api/sendhttp.php?authkey=$masaar_API_key&mobiles=$mobile&sender=$masaar_sender_id&route=$masaar_route&country=$masaar_country_code&message=" . urlencode($msg);
    }

    $ch = curl_init();
    curl_setopt_array($ch, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
    ));

    $output = curl_exec($ch);
    curl_close($ch);

    if (strpos($output, 'sent successful')) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  function get_thali_records($from_date, $to_date, $tanzeem = FALSE) {
    global $database;
    $query = "SELECT * FROM `cron_thaali_count` WHERE `date` BETWEEN '$from_date' AND '$to_date'";

    if ($tanzeem) {
      $query .= " AND `mohallah` = '$tanzeem'";
    } else {
      $query .= " AND `mohallah` = 'All'";
    }
    $result = $database->query_fetch_full_result($query);

    if ($tanzeem) {
      $items = array();
      if ($result) {
        foreach ($result as $item) {
          $items[$item['date']]['mohallah'][] = $item['mohallah'];
          $items[$item['date']]['tiffin_size'][] = $item['tiffin_size'];
          $items[$item['date']]['count'][] = $item['count'];
        }
      }
      return $items;
    } else {
      return $result;
    }
  }

  function get_cost_multiplier_of_tiffin_size($tiffin_size) {
    global $database;
    $query = "SELECT `cost_multiplier` FROM `tiffin_details` WHERE `size` = '$tiffin_size'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['cost_multiplier'];
  }

  function get_act_cost_of_confirm_menu($start, $end) {
    global $database;
    $query = "SELECT `act_cost` FROM `inventory_finalize` WHERE `timestamp` BETWEEN '$start' AND '$end'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['act_cost'];
  }

  /* getter and setter function */

  public function getSabil_id() {
    return $this->file_no;
  }

  public function setSabil_id($file_no) {
    $this->file_no = $file_no;
    $this->load_family();
  }

  public function getCard() {
    return $this->card;
  }

  public function setCard($card) {
    $this->card = $card;
  }

  public function getAge() {
    return $this->age;
  }

  public function setAge($age) {
    $this->age = (int) $age;
  }

  public function getPrefix() {
    return $this->prefix;
  }

  public function setPrefix($prefix) {
    $this->prefix = $prefix;
  }

  public function getFirstName() {
    return $this->first_name;
  }

  public function setFirstName($first_name) {
    $this->first_name = $first_name;
  }

  public function getFatherPrefix() {
    return $this->father_prefix;
  }

  public function setFatherPrefix($father_prefix) {
    $this->father_prefix = $father_prefix;
  }

  public function getFatherName() {
    return $this->father_name;
  }

  public function setFatherName($father_name) {
    $this->father_name = $father_name;
  }

  public function getSurname() {
    return $this->surname;
  }

  public function setSurname($surname) {
    $this->surname = $surname;
  }

  public function getHof() {
    return $this->hof;
  }

  public function setHof($hof) {
    $this->hof = $hof;
  }

  public function getGender() {
    return $this->gender;
  }

  public function setGender($gender) {
    $this->gender = $gender;
  }

  public function getMohallah() {
    return $this->mohallah;
  }

  public function setMohallah($mohallah) {
    $this->mohallah = $mohallah;
  }

  public function getMarital() {
    return $this->marital;
  }

  public function setMarital($marital) {
    $this->marital = $marital;
  }

  public function getAddress() {
    return $this->address;
  }

  public function setAddress($address) {
    $this->address = $address;
  }

  public function getOccupation() {
    return $this->occupation;
  }

  public function setOccupation($occupation) {
    $this->occupation = $occupation;
  }

  public function getEjamaat() {
    return $this->ejamaat;
  }

  public function setEjamaat($ejamaat) {
    $this->ejamaat = $ejamaat;
  }

  public function getHijari() {
    return $this->hijari;
  }

  public function setHijari($hijari) {
    $this->hijari = $hijari;
  }

  public function getPhone() {
    return $this->phone;
  }

  public function setPhone($phone) {
    $this->phone = $phone;
  }

  public function getOff_phone() {
    return $this->off_phone;
  }

  public function setOff_phone($off_phone) {
    $this->off_phone = $off_phone;
  }

  public function getMobile() {
    return $this->mobile;
  }

  public function setMobile($mobile) {
    $this->mobile = $mobile;
  }

  public function getAdults() {
    return $this->adults;
  }

  public function setAdults($adults) {
    $this->adults = $adults;
  }

  public function getChild() {
    return $this->child;
  }

  public function setChild($child) {
    $this->child = $child;
  }

  public function getDiabetic() {
    return $this->diabetic;
  }

  public function setDiabetic($diabetic) {
    $this->diabetic = $diabetic;
  }

  public function getDiabeticRoti() {
    return $this->diabetic_roti;
  }

  public function setDiabeticRoti($diabetic_roti) {
    $this->diabetic_roti = $diabetic_roti;
  }

  public function getHub_amount() {
    return $this->hub_amount;
  }

  public function setHub_amount($hub_amount) {
    $this->hub_amount = $hub_amount;
  }

  public function getMonth() {
    return $this->month;
  }

  public function setMonth($month) {
    $this->month = $month;
  }

  public function getYear() {
    return $this->year;
  }

  public function setYear($year) {
    $this->year = $year;
  }

  public function getEmail() {
    return $this->email;
  }

  public function setEmail($email) {
    $this->email = $email;
  }

  public function getTiffin() {
    return $this->tiffin;
  }

  public function setTiffin($tiffin) {
    $this->tiffin = $tiffin;
  }

  public function get_pay_type() {
    return $this->pay_type;
  }

  public function set_pay_type($pay_type) {
    $this->pay_type = $pay_type;
  }

  public function get_payment_date() {
    return $this->payment_date;
  }

  public function set_payment_date($payment_date) {
    $this->payment_date = $payment_date;
  }

  public function get_niyaaz_date() {
    return $this->niyaaz_date;
  }

  public function set_niyaaz_date($niyaaz_date) {
    $this->niyaaz_date = $niyaaz_date;
  }

  public function get_payment_term() {
    return $this->payment_term;
  }

  public function set_payment_term($payment_term) {
    $this->payment_term = $payment_term;
  }

  public function get_scanning() {
    return $this->scanning;
  }

  public function set_scanning($scanning) {
    $this->scanning = $scanning;
  }

  public function get_category() {
    return $this->category;
  }

  public function set_category($category) {
    $this->category = $category;
  }

  public function getFcCode() {
    return $this->code;
  }

  public function get_hub_details($year) {
    global $database;

    $query = "SELECT `ta`.`amount` takhmeen_amount, SUM(`frh`.`amount`) paid_amount, COALESCE(`ta`.`amount`, 0) - COALESCE(SUM(`frh`.`amount`), 0) pending_amount FROM `fmb_receipt_hub` frh INNER JOIN `takhmeen_amount` ta ON `frh`.`FileNo` = `ta`.`FileNo` AND `frh`.`year` = `ta`.`year` WHERE `frh`.`FileNo` = '$this->file_no' AND `frh`.`year` = '$year' AND `frh`.`cancel` = '0'";

    $result = $database->query_fetch_full_result($query);

    return $result;
  }

  public function setFcCode($code) {
    $this->code = (int) $code;
  }

}

?>
