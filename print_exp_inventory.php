<?php
include 'session.php';
$pg_link = 'voluntary_rcpt_book';
require_once('classes/class.database.php');

require_once('classes/class.product.php');
$cls_product = new Mtx_Product();

$title = 'Expected inventory issue';
$active_page = 'report';

if(isset($_GET['default_pricing']) && $_GET['default_pricing'] > 0) $default_pricing = TRUE;
else $default_pricing = FALSE;
$result = $cls_product->get_all_estimate_inventory_between_dates($_GET['from_date'], $_GET['to_date']);
$bills = $cls_product->get_all_estimate_direct_bills_between_dates($_GET['from_date'], $_GET['to_date']);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Center Bar -->
      <div class="col-md-12 ">
        <div class="col-md-6">
          <table class="table table-hover table-condensed table-bordered">
            <thead>
              <tr>
                <th colspan="7">Expected Inventory issue</th>
              </tr>
              <tr>
                <th>Sr No.</th>
                <th>Item Name</th>
                <th class="text-right alert-info">Expected Quantity</th>
                <th class="text-right">Current Inventory</th>
                <th class="text-right alert-warning">Purchase Quantity</th>
                <th class="text-right alert-success">Estimated Cost</th>
                <th>Category</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if ($result) {
                $i = 1;
                $total_cost = 0;
                foreach ($result as $item) {
                  $purchase_qty = $item['qty'] > $item['current_quantity'] ? ($item['qty'] - $item['current_quantity']) : 0;
                  $cost = 0;
                  if ($purchase_qty > 0) {
                  if($default_pricing) $est_cost = $cls_product->get_estimate_cost($item['inventory_id'], 'USE_DEFAULT_COSTING');
                  else $est_cost = $cls_product->get_estimate_cost($item['inventory_id']);
                    $cost = $purchase_qty * $est_cost;
                    $total_cost += $cost;
                  }
                  ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $item['item_name']; ?></td>
                    <td class="text-right alert-info"><?php echo number_format($item['qty'], 3) . ' ' . $item['unit']; ?></td>
                    <td class="text-right"><?php echo number_format($item['current_quantity'], 3) . ' ' . $item['unit']; ?></td>
                    <td class="text-right alert-warning"><?php echo number_format($purchase_qty, 3) . ' ' . $item['unit']; ?></td>
                    <td class="text-right alert-success"><?php echo 'Rs. ' . number_format($cost, 2) ?></td>
                    <td><?php echo $item['category'] ?></td>
                  </tr>
                <?php }
                ?>
                <tr>
                  <td colspan="4"></td>
                  <td class="text-right"><strong>Total:</strong></td>
                  <td class="text-right"><?php echo 'Rs. ' . number_format($total_cost, 2); ?></td>
                  <td></td>
                </tr>

                <?php
              } else {
                ?>
                <tr>
                  <td colspan="6" class="alert-danger">No results found.</td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <div class="col-md-6">
          <table class="table table-hover table-condensed table-bordered">
            <thead>
              <tr>
                <th colspan="5">Direct Bills</th>
              </tr>
              <tr>
                <th>Sr No.</th>
                <th>Item Name</th>
                <th class="text-right alert-warning">Quantity</th>
                <th class="text-right alert-success">Estimated Cost</th>
                <th>Category</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if ($bills) {
                $i = 1;
                $direct_cost = 0;
                foreach ($bills as $bill) {
                  if($default_pricing)
                  $est_cost = $cls_product->get_estimate_cost_direct($bill['item'], 'USE_DEFAULT_COSTING');
                  else $est_cost = $cls_product->get_estimate_cost_direct($bill['item']);
                  $cost = $bill['qty'] * $est_cost;
                  $direct_cost += $cost;
                  ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $bill['item']; ?></td>
                    <td class="text-right alert-warning"><?php echo number_format($bill['qty'], 3); ?></td>
                    <td class="text-right alert-success"><?php echo 'Rs. ' . number_format($cost); ?></td>
                    <td><?php echo $bill['category'];?></td>
                  </tr>
                <?php }
                ?>
                <tr>
                  <td colspan="2"></td>
                  <td class="text-right"><strong>Total:</strong></td>
                  <td class="text-right"><?php echo 'Rs. ' . number_format($direct_cost, 2); ?></td>
                  <td></td>
                </tr>
                <?php
              } else {
                ?>
                <tr>
                  <td colspan="6" class="alert-danger">No results found.</td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /Center Bar -->

    </div>
    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
