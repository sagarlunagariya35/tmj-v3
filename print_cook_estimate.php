<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.product.php');
require_once('classes/class.billbook.php');
require_once('classes/class.menu.php');
require_once('classes/class.family.php');
$cls_product = new Mtx_Product();
$cls_billbook = new Mtx_BillBook();
$cls_menu = new Mtx_Menu();
$cls_family = new Mtx_family();

if (isset($_GET['date'])) {
  $date = trim($_GET['date']);
  $dt = explode('-', $date);
  $start = mktime(0, 0, 0, $dt[1], $dt[2], $dt[0]);
  $end = mktime(23, 59, 59, $dt[1], $dt[2], $dt[0]);
  $dmenu = $cls_menu->get_all_daily_menu($start, $end);
  $all_tiffin = $cls_family->get_count_tiffin_size();
  if ($dmenu[0]['person_count'] == 0) {
    $person_count = $all_tiffin;
  } else {
    $person_count = $dmenu[0]['person_count'];
  }

  $ary_issued_items = $cls_menu->get_cook_est_inv_issue_items($dmenu[0]['menu_id'], $person_count, $start, 'print');
  $ary_direct_items = $cls_menu->get_cook_est_direct_issue_items($dmenu[0]['menu_id'], $person_count, $start, 'print');
}
$title = 'Print Cook Estimate details';
$active_page = 'menu';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="asset/dist/css/print.css" media="print">
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->
      <!-- Center Bar -->
      <div class="col-md-8">
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label class="control-label col-md-4">Menu Name</label>
                <div class="col-md-4">
                  <p class="form-control-static"><?php echo isset($date) ? $cls_menu->get_base_menu_name($dmenu[0]['menu_id']) : ''; ?></p>
                  <input type="hidden" name="menu_name" value="<?php echo isset($dmenu[0]['menu_id']) ? $dmenu[0]['menu_id'] : ''; ?>">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-4">Date</label>
                <div class="col-md-4">
                  <p class="form-control-static"><?php echo isset($date) ? date('d F, Y', $dmenu[0]['timestamp']) : ''; ?></p>
                  <input type="hidden" name="date" value="<?php echo isset($date) ? date('m-d-Y', $dmenu[0]['timestamp']) : ''; ?>">
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label col-md-7">Person Count</label>
                <div class="col-md-3">
                  <p class="form-control-static"><?php echo $person_count; ?></p>
                </div>
              </div>
            </div>
          </div>
        <div class="col-md-12">
          <div class="col-md-6">
<?php if (isset($_POST['inventory_issue']) || isset($_GET['cmd']) || isset($_POST['direct_bill']) || isset($date)) { ?>
                <table class="table table-bordered table-condensed table-hover">
                  <thead>
                    <tr>
                      <th colspan="3">Inventory Issue</th>
                    </tr>
                    <tr>
                      <th>Sr. No</th>
                      <th>Item Name (Issued Qty)</th>
                      <th class="text-right">Quantity</th>
                    </tr>
                  </thead>
                  <tbody id="display_issue">
                    <?php
                    if (isset($ary_issued_items) && $ary_issued_items) {
                      $i = 1;
                      $button = '';
                      foreach ($ary_issued_items as $key => $item) {
                        if ($item['inv_issue']) {
                          $bg_clr = ' class="alert alert-success"';
                          $est_qty = " ($item[inv_issue])";
                        } else {
                          $bg_clr = '';
                          $est_qty = "";
                        }
                        if (!$item['base_inv_issue'])
                          $bg_clr = ' class="alert alert-warning"';
                        ?>
                        <tr<?php echo $bg_clr; ?>>
                          <td><?php echo $i++; ?></td>
                          <td><?php echo $item['item_name'] . $est_qty; ?></td>
                          <td class="text-right"><?php echo number_format($item['base_inv_issue'] * $person_count, 3); ?></td>
                        </tr>
                      <?php
                    }
                  } else {
                    echo '<tr><td class="alert-danger" colspan="4">Sorry! No items found.</td></tr>';
                  }
                  ?>
                  </tbody>
                </table>
            <?php } ?>
          </div>
          <div class="col-md-6">
<?php if (isset($_POST['direct_bill']) || isset($_POST['inventory_issue']) || isset($_GET['cmd']) || isset($date)) { ?>
                <table class="table table-bordered table-condensed table-hover">
                  <thead>
                    <tr>
                      <th colspan="3">Direct Purchase</th>
                    </tr>
                    <tr>
                      <th>Sr. No</th>
                      <th>Item Name (Issued Qty)</th>
                      <th class="text-right">Quantity</th>
                    </tr>
                  </thead>
                  <tbody id="display_direct">
                    <?php
                    if (isset($ary_direct_items) && $ary_direct_items) {
                      $i = 1;
                      $button = '';
                      foreach ($ary_direct_items as $key => $bill) {
                        if ($bill['direct_issue']) {
                          $bg_clr = ' class="alert alert-success"';
                          $est_qty = " ($bill[direct_issue])";
                        } else {
                          $bg_clr = '';
                          $est_qty = "";
                        }
                        if (!$bill['base_inv_issue'])
                          $bg_clr = ' class="alert alert-warning"';
                        ?>
                        <tr<?php echo $bg_clr; ?>>
                          <td><?php echo $i++; ?></td>
                          <td><?php echo $key . $est_qty; ?></td>
                          <td class="text-right"><?php echo number_format($bill['base_inv_issue'] * $person_count, 3); ?></td>
                        </tr>
                        <?php
                      }
                    } else {
                      echo '<tr><td class="alert-danger" colspan="4">Sorry! No items found.</td></tr>';
                    }
                    ?>
                  </tbody>
                </table>
<?php } ?>
          </div>
        </div>


      </div>
    </div>
    <!-- /Center Bar -->

    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
