<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.receipt.php");
require_once('classes/class.billbook.php');
$cls_receipt = new Mtx_Receipt();
$cls_billbook = new Mtx_BillBook();

$cheque_no = FALSE;
$account_bill = $direct_purchase = $credit_voucher = $debit_voucher = $receipt = $fmb_receipt_hub = FALSE;

if(isset($_POST['cheque_as_unclear'])) {
    $cheque_no = $database->clean_data($_POST['cheque_no']);
    $date = date('Y-m-d');
    $update_values = (array) $database->clean_data($_POST['update_value']);
    
    foreach ($update_values as $upd_vl){
      $v = explode('-', $upd_vl);
      $table = $v[0];
      $id = $v[1];
      
      $result = $cls_receipt->update_unclear_cheque_data($cheque_no,$date,$table,$id);
    }
    
    $_SESSION[SUCCESS_MESSAGE] = 'Cheque Un-Clear successfully.';
}

if(isset($_POST['cancel_receipt'])) {
    $cheque_no = $database->clean_data($_POST['cheque_no']);
    
    $date = explode('-', date('Y-m-d'));
    $cheque_date = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
      
    $update_values = (array) $database->clean_data($_POST['update_value']);
    
    foreach ($update_values as $upd_vl){
      $v = explode('-', $upd_vl);
      $table = $v[0];
      $id = $v[1];
      
      $result = $cls_receipt->update_cancel_receipt_data($cheque_no,$cheque_date,$table,$id);
    }
    
    $_SESSION[SUCCESS_MESSAGE] = 'Receipt Cancelled successfully.';
}

if(isset($_POST['search'])) {
    $cheque_no = $database->clean_data($_POST['cheque_no']);
    
    $account_bill = $cls_receipt->get_unclear_cheque_and_cancel_receipt_data($cheque_no,'account_bill');
    $direct_purchase = $cls_receipt->get_unclear_cheque_and_cancel_receipt_data($cheque_no,'direct_purchase');
    $credit_voucher = $cls_receipt->get_unclear_cheque_and_cancel_receipt_data($cheque_no,'credit_voucher');
    $debit_voucher = $cls_receipt->get_unclear_cheque_and_cancel_receipt_data($cheque_no,'debit_voucher');
    $receipt = $cls_receipt->get_unclear_cheque_and_cancel_receipt_data($cheque_no,'receipt');
    $fmb_receipt_hub = $cls_receipt->get_unclear_cheque_and_cancel_receipt_data($cheque_no,'fmb_receipt_hub');
      
}


$title = 'Un-clear Cheque AND Cancel Receipt';
$active_page = 'account';

require_once 'includes/header.php';

$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
          <div class="col-md-12">&nbsp;</div>

          <!-- Center Bar -->
          <div class="col-md-12">
            <p class="form-control-static text-center alert-info"><strong>Date:</strong> <?php echo date('d F, Y'); ?></p><br>



              <div class="col-md-12">
                  <form method="post" role="form" class="form-horizontal">
                      <div class="form-group">
                          <label class="control-label col-md-3">Cheque No.</label>
                          <div class="col-md-4">
                            <input type="text" class="form-control" name="cheque_no" id="cheque_no" value="<?php echo $cheque_no; ?>" required>
                          </div>
                          <div class="col-md-3">
                              <button class="btn btn-success" type="submit" name="search">Search</button>
                          </div>
                      </div>
                  </form>
              </div><br><br>

            <form method="post" role="form" class="form-horizontal">
              <?php if($account_bill) { ?>
              <div class="panel panel-info">
                <div class="panel-heading">Bill Cheque Book and Cancel Recept</div>
                <table class="table table-hover table-condensed table-bordered">
                  <thead>
                    <tr>
                      <th>Bill No</th>
                      <th>Name</th>
                      <th>Bank Name</th>
                      <th>Cheque No.</th>
                      <th class='text-right'>Amount</th>
                      <th class='text-center'>Status</th>
                      <th class='text-center'>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      foreach ($account_bill as $ab){
                        $shop_name = $cls_billbook->get_shops($ab['name']);
                    ?>
                    <tr>
                      <td><?php echo $ab['BillNo']; ?></td>
                      <td><?php echo $shop_name['ShopName']; ?></td>
                      <td><?php echo $ab['bankname']; ?></td>
                      <td><?php echo $ab['cheque_no']; ?></td>
                      <td class='text-right'><?php echo $ab['amount']; ?></td>
                      <td class='text-center'><?php if($ab['cheque_clear'] == '1'){ echo '<span title="Cheque Clear" class="glyphicon glyphicon-thumbs-up"></span>'; }else { echo '<span title="Cheque Un-clear" class="glyphicon glyphicon-thumbs-down"></span>'; } ?>&nbsp;&nbsp;<?php if($ab['cancel'] == '1'){ echo '<span title="Cheque Cancel" class="glyphicon glyphicon-ban-circle"></span>'; }else { echo '<span title="Cheque Not Cancel" class="glyphicon glyphicon-ok-circle"></span>'; } ?></td>
                      <td class='text-center'><input type='checkbox' name='update_value[]' value='<?php echo 'account_bill-'.$ab['id']; ?>'></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php } ?>

              <?php if($direct_purchase) { ?>
              <div class="panel panel-info">
                <div class="panel-heading">Direct Purchase Cheque Book and Cancel Recept</div>
                <table class="table table-hover table-condensed table-bordered">
                  <thead>
                    <tr>
                      <th>Bill No</th>
                      <th>Name</th>
                      <th>Bank Name</th>
                      <th>Cheque No.</th>
                      <th class='text-right'>Amount</th>
                      <th class='text-center'>Status</th>
                      <th class='text-center'>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      foreach ($direct_purchase as $dp){
                        $shop_name = $cls_billbook->get_shops($dp['name']);
                    ?>
                    <tr>
                      <td><?php echo $dp['bill_id']; ?></td>
                      <td><?php echo $shop_name['ShopName']; ?></td>
                      <td><?php echo $dp['bankname']; ?></td>
                      <td><?php echo $dp['cheque_no']; ?></td>
                      <td class='text-right'><?php echo $dp['amount']; ?></td>
                      <td class='text-center'><?php if($dp['cheque_clear'] == '1'){ echo '<span title="Cheque Clear" class="glyphicon glyphicon-thumbs-up"></span>'; }else { echo '<span title="Cheque Un-clear" class="glyphicon glyphicon-thumbs-down"></span>'; } ?>&nbsp;&nbsp;<?php if($dp['cancel'] == '1'){ echo '<span title="Cheque Cancel" class="glyphicon glyphicon-ban-circle"></span>'; }else { echo '<span title="Cheque Not Cancel" class="glyphicon glyphicon-ok-circle"></span>'; } ?></td>
                      <td class='text-center'><input type='checkbox' name='update_value[]' value='<?php echo 'direct_purchase-'.$dp['id']; ?>'></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php } ?>

              <?php if($credit_voucher) { ?>
              <div class="panel panel-info">
                <div class="panel-heading">Credit Voucher Book and Cancel Recept</div>
                <table class="table table-hover table-condensed table-bordered">
                  <thead>
                    <tr>
                      <th>Bill No</th>
                      <th>Name</th>
                      <th>Bank Name</th>
                      <th>Cheque No.</th>
                      <th class='text-right'>Amount</th>
                      <th class='text-center'>Status</th>
                      <th class='text-center'>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      foreach ($credit_voucher as $cv){
                    ?>
                    <tr>
                      <td><?php echo $cv['id']; ?></td>
                      <td><?php echo $cv['name']; ?></td>
                      <td><?php echo $cv['bank_name']; ?></td>
                      <td><?php echo $cv['cheque_no']; ?></td>
                      <td class='text-right'><?php echo $cv['amount']; ?></td>
                      <td class='text-center'><?php if($cv['cheque_clear'] == '1'){ echo '<span title="Cheque Clear" class="glyphicon glyphicon-thumbs-up"></span>'; }else { echo '<span title="Cheque Un-clear" class="glyphicon glyphicon-thumbs-down"></span>'; } ?>&nbsp;&nbsp;<?php if($cv['cancel'] == '1'){ echo '<span title="Cheque Cancel" class="glyphicon glyphicon-ban-circle"></span>'; }else { echo '<span title="Cheque Not Cancel" class="glyphicon glyphicon-ok-circle"></span>'; } ?></td>
                      <td class='text-center'><input type='checkbox' name='update_value[]' value='<?php echo 'credit_voucher-'.$cv['id']; ?>'></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php } ?>

              <?php if($debit_voucher) { ?>
              <div class="panel panel-info">
                <div class="panel-heading">Debit Voucher Book and Cancel Recept</div>
                <table class="table table-hover table-condensed table-bordered">
                  <thead>
                    <tr>
                      <th>Bill No</th>
                      <th>Name</th>
                      <th>Bank Name</th>
                      <th>Cheque No.</th>
                      <th class='text-right'>Amount</th>
                      <th class='text-center'>Status</th>
                      <th class='text-center'>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      foreach ($debit_voucher as $dv){
                        if(is_numeric($dv['name'])){
                          $shop_name = $cls_billbook->get_shops($dv['name']);
                          $name = $shop_name['ShopName'];
                        }else {
                          $name = $dv['name'];
                        }
                    ?>
                    <tr>
                      <td><?php echo $dv['id']; ?></td>
                      <td><?php echo $name; ?></td>
                      <td><?php echo $dv['bank_name']; ?></td>
                      <td><?php echo $dv['cheque_no']; ?></td>
                      <td class='text-right'><?php echo $dv['amount']; ?></td>
                      <td class='text-center'><?php if($dv['cheque_clear'] == '1'){ echo '<span title="Cheque Clear" class="glyphicon glyphicon-thumbs-up"></span>'; }else { echo '<span title="Cheque Un-clear" class="glyphicon glyphicon-thumbs-down"></span>'; } ?>&nbsp;&nbsp;<?php if($dv['cancel'] == '1'){ echo '<span title="Cheque Cancel" class="glyphicon glyphicon-ban-circle"></span>'; }else { echo '<span title="Cheque Not Cancel" class="glyphicon glyphicon-ok-circle"></span>'; } ?></td>
                      <td class='text-center'><input type='checkbox' name='update_value[]' value='<?php echo 'debit_voucher-'.$dv['id']; ?>'></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php } ?>

              <?php if($receipt) { ?>
              <div class="panel panel-info">
                <div class="panel-heading">Receipt Cheque Book and Cancel Recept</div>
                <table class="table table-hover table-condensed table-bordered">
                  <thead>
                    <tr>
                      <th>Bill No</th>
                      <th>Name</th>
                      <th>Bank Name</th>
                      <th>Cheque No.</th>
                      <th class='text-right'>Amount</th>
                      <th class='text-center'>Status</th>
                      <th class='text-center'>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      foreach ($receipt as $r){
                    ?>
                    <tr>
                      <td><?php echo $r['id']; ?></td>
                      <td><?php echo $r['hubber_name']; ?></td>
                      <td><?php echo $r['bankname']; ?></td>
                      <td><?php echo $r['cheque_no']; ?></td>
                      <td class='text-right'><?php echo $r['amount']; ?></td>
                      <td class='text-center'><?php if($r['cheque_clear'] == '1'){ echo '<span title="Cheque Clear" class="glyphicon glyphicon-thumbs-up"></span>'; }else { echo '<span title="Cheque Un-clear" class="glyphicon glyphicon-thumbs-down"></span>'; } ?>&nbsp;&nbsp;<?php if($r['cancel'] == '1'){ echo '<span title="Cheque Cancel" class="glyphicon glyphicon-ban-circle"></span>'; }else { echo '<span title="Cheque Not Cancel" class="glyphicon glyphicon-ok-circle"></span>'; } ?></td>
                      <td class='text-center'><input type='checkbox' name='update_value[]' value='<?php echo 'receipt-'.$r['id']; ?>'></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php } ?>

              <?php if($fmb_receipt_hub) { ?>
              <div class="panel panel-info">
                <div class="panel-heading">FMB Hub Receipt Cheque Book and Cancel Recept</div>
                <table class="table table-hover table-condensed table-bordered">
                  <thead>
                    <tr>
                      <th>Bill No</th>
                      <th>Name</th>
                      <th>Bank Name</th>
                      <th>Cheque No.</th>
                      <th class='text-right'>Amount</th>
                      <th class='text-center'>Status</th>
                      <th class='text-center'>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      foreach ($fmb_receipt_hub as $frh){
                    ?>
                    <tr>
                      <td><?php echo $frh['id']; ?></td>
                      <td><?php echo $frh['name']; ?></td>
                      <td><?php echo $frh['bank_name']; ?></td>
                      <td><?php echo $frh['cheque_no']; ?></td>
                      <td class='text-right'><?php echo $frh['amount']; ?></td>
                      <td class='text-center'><?php if($frh['cheque_clear'] == '1'){ echo '<span title="Cheque Clear" class="glyphicon glyphicon-thumbs-up"></span>'; }else { echo '<span title="Cheque Un-clear" class="glyphicon glyphicon-thumbs-down"></span>'; } ?>&nbsp;&nbsp;<?php if($frh['cancel'] == '1'){ echo '<span title="Cheque Cancel" class="glyphicon glyphicon-ban-circle"></span>'; }else { echo '<span title="Cheque Not Cancel" class="glyphicon glyphicon-ok-circle"></span>'; } ?></td>
                      <td class='text-center'><input type='checkbox' name='update_value[]' value='<?php echo 'fmb_receipt_hub-'.$frh['id']; ?>'></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <?php } ?>

              <?php
                if($_SESSION[USER_TYPE] == 'A'){
                 if($cheque_no) {
              ?>

              <input type="hidden" name="cheque_no" value="<?php echo $cheque_no; ?>">

              <div class="col-md-4 pull-right">
                  <button class="btn btn-success" type="submit" name="cheque_as_unclear">Cheque AS Unclear</button>
                  <button class="btn btn-success" type="submit" name="cancel_receipt">Cancel Receipt</button>
                </div>
              </div>
          
              <?php } } ?>
          <!-- /Center Bar -->
            </form>
            <br><br><br>
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>