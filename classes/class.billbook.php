<?php

class Mtx_BillBook {

  private $id;
  private $productname;
  private $shop_ownername;
  private $quantity;
  private $unit_price;
  private $receivedate;
  private $amount;
  private $bankname;
  private $type;

  public function Mtx_BillBook($id = '') {
    if ($id) {
      if ($this->set_id($id))
        return TRUE;
      else
        return FALSE;
    }
  }

  public function __destruct() {
    // TODO: destructor code
  }

  //function
  public function add_billbook($BillNo, $acct_heads, $shop, $bdate, $description, $ary_product, $ary_quantity, $ary_unit, $ary_price, $vat, $discount, $other_charges, $amount, $type, $bankname, $cheque_no, $cheque_date, $user_id, $paid, $contactPerson) {
    global $database;

    //$timestamp = time();
    if ($cheque_date != '') {
      $date = explode('-', $cheque_date);
      $cheque_date2 = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    } else
      $cheque_date2 = '0';
    
    $bdate = explode('-', $bdate);
    $bdate = mktime(0, 0, 0, $bdate[1], $bdate[2], $bdate[0]);

    $query = "INSERT INTO `account_bill`(`BillNo`, `name`, `description`, `amount`,`payment_type`,`bankname`, `cheque_no`, `cheque_date`, `timestamp`, `user_id`, `acct_heads`, `VAT`, `Discount`,`other_charges`, `paid`, `contact_person`) VALUES('$BillNo','$shop','$description','$amount','$type','$bankname','$cheque_no','$cheque_date2', '$bdate', '$user_id', '$acct_heads','$vat','$discount', '$other_charges', '$paid', '$contactPerson')";
    $result = $database->query($query) or die(mysql_error());
    $id = $database->get_last_insert_id();
    
    if($type == 'neft') {
      $clear_date = date('Y-m-d');
      $query = "UPDATE `account_bill` SET `cheque_clear` = '1', `cheque_clear_date` = '$clear_date' WHERE `id` = '$id'";
      $database->query($query);
    }
    
    if($paid == '1') {
      $clear_date = date('Y-m-d');
      $query = "UPDATE `account_bill` SET `cheque_clear_date` = '$clear_date' WHERE `id` = '$id'";
      $database->query($query);
    }

    $max = count($ary_product);
    for ($i = 0; $i < $max; $i++) {
      if ($ary_product[$i] != '0') {
        $query = "INSERT INTO bill_details(`bill_id`, `item_name`, `quantity`, `unit`, `price`) VALUES('$id','" . $ary_product[$i] . "','" . $ary_quantity[$i] . "','" . $ary_unit[$i] . "','" . ($ary_quantity[$i] * $ary_unit[$i]) . "')";
        $result = $database->query($query) or die(mysql_error());
        $bill_id = $database->get_last_insert_id();

        $query = "INSERT INTO `inventory_bills`(`bill_id`, `quantity`, `unit_price`, `timestamp`, `inventory_id`, `user_id`) VALUES('$id','" . $ary_quantity[$i] . "', '".$ary_unit[$i]."','$bdate','" . $ary_product[$i] . "','$user_id')";
        $result = $database->query($query) or die(mysql_error());
      }
    }
    return $result;
  }

  public function update_bill($acct_heads, $vat, $discount, $other_charges, $amount, $paid, $type, $bankname, $cheque_no, $cheque_date, $description, $bill_id, $contactPerson, $shopID) {
    global $database;
    if ($cheque_date != '') {
      $date = explode('-', $cheque_date);
      $cheque_date2 = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    } else
      $cheque_date2 = (int)'';
    
    if ($type == 'cash') {
      $bankname = '';
      $cheque_no = '';
      $cheque_date2 = '0';
    }

    $query = "UPDATE `account_bill` SET `acct_heads` = '$acct_heads', `VAT` = '$vat', Discount = '$discount', `other_charges` = '$other_charges', `amount` = '$amount', `paid` = '$paid', `payment_type` = '$type', `bankname` = '$bankname', `cheque_no` = '$cheque_no', `cheque_date` = '$cheque_date2', `description` = '$description', `contact_person` = '$contactPerson', `name` = '$shopID' WHERE id = '$bill_id'";
    $result = $database->query($query);
    
    if($type == 'neft') {
      $clear_date = date('Y-m-d');
      $query = "UPDATE `account_bill` SET `cheque_clear` = '1', `cheque_clear_date` = '$clear_date' WHERE `id` = '$bill_id'";
      $database->query($query);
    }
    
    if($paid == '1') {
      $clear_date = date('Y-m-d');
      $query = "UPDATE `account_bill` SET `cheque_clear_date` = '$clear_date' WHERE `id` = '$bill_id'";
      $database->query($query);
    }
    
    return $result;
  }

  public function update_directPurchase($acct_heads, $vat, $discount, $other_charges, $amount, $make_payment, $type, $bankname, $cheque_no, $cheque_date, $description, $bill_id, $billDate, $menuDate, $contactPerson, $shopID) {
    global $database;
    
    if ($cheque_date != '') {
      $date = explode('-', $cheque_date);
      $cheque_date2 = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    } else
      $cheque_date2 = '0';
    
    if ($type == 'cash') {
      $bankname = '';
      $cheque_no = '';
      $cheque_date2 = '';
    }
      $date = explode('-', $billDate);
      $billDate = mktime(0, 0, 0, $date[1], $date[2], $date[0]);

    $query = "UPDATE `direct_purchase` SET `acct_heads` = '$acct_heads', `VAT` = '$vat', Discount = '$discount', `other_charges` = '$other_charges', `amount` = '$amount', `payment_type` = '$type', `bankname` = '$bankname', `cheque_no` = '$cheque_no', `cheque_date` = '$cheque_date2', `description` = '$description', `paid` = '$make_payment', `bill_date` = '$billDate', `menu_date` = '$menuDate', `contact_person` = '$contactPerson', `name` = '$shopID' WHERE id = '$bill_id'";
    $result = $database->query($query);

    if($type == 'neft') {
      $clear_date = date('Y-m-d');
      $query = "UPDATE `direct_purchase` SET `cheque_clear` = '1', `cheque_clear_date` = '$clear_date' WHERE `id` = '$bill_id'";
      $database->query($query);
    }

    if($make_payment == '1') {
      $clear_date = date('Y-m-d');
      $query = "UPDATE `direct_purchase` SET `cheque_clear_date` = '$clear_date' WHERE `id` = '$id'";
      $database->query($query);
    }
    return $result;
  }

  public function add_estimate_direct_purchase($menu_id, $itemName, $quantity, $date) {
    global $database;

    $date = explode('-', $date);
    $date = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    $query = "INSERT INTO `est_direct_pur`(`item`, `qty`, `date`) VALUES('$itemName', '$quantity', '$date')";
    $result = $database->query($query);

    return $result;
  }

  public function add_directPurchase($bill_id, $acct_heads, $shop, $description, $ary_product, $ary_quantity, $ary_unit, $ary_price, $vat, $discount, $other_charges, $amount, $type, $bankname, $cheque_no, $cheque_date, $direct_bill_date, $user_id, $make_payment, $menu_date, $contactPerson) {
    global $database;

    $timestamp = time();
    if ($cheque_date != '') {
      $date = explode('-', $cheque_date);
      $cheque_date2 = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    } else
      $cheque_date2 = '0';
    
    if (!$menu_date)
      $menu_date = date('Y-m-d');
    $sp = explode('-', $direct_bill_date);
    //print_r($sp);
    $direct_bill_date = mktime(0, 0, 0, $sp[1], $sp[2], $sp[0]);
    $query = "INSERT INTO `direct_purchase`(`bill_id`, `name`, `description`, `amount`,`payment_type`,`bankname`, `cheque_no`, `cheque_date`, `timestamp`, `user_id`, `acct_heads`, `VAT`, `Discount`,`other_charges`, `bill_date`, `paid`, `menu_date`, `contact_person`) VALUES('$bill_id', '$shop','$description','$amount','$type','$bankname','$cheque_no','$cheque_date2', '$timestamp', '$user_id', '$acct_heads','$vat','$discount', '$other_charges', '$direct_bill_date', '$make_payment', '$menu_date', '$contactPerson')";
    //echo $query;exit;
    $result = $database->query($query) or die(mysql_error());
    $id = $database->get_last_insert_id();
    
    if($type == 'neft') {
      $clear_date = date('Y-m-d');
      $query = "UPDATE `direct_purchase` SET `cheque_clear` = '1', `cheque_clear_date` = '$clear_date' WHERE `id` = '$id'";
      $database->query($query);
    }

    if($make_payment == '1') {
      $clear_date = date('Y-m-d');
      $query = "UPDATE `direct_purchase` SET `cheque_clear_date` = '$clear_date' WHERE `id` = '$id'";
      $database->query($query);
    }

    $max = count($ary_product);
    for ($i = 0; $i < $max; $i++) {
      if ($ary_product[$i] != '' && !empty($ary_product[$i])) {
        $query = "INSERT INTO direct_bill_details(`bill_id`, `item_name`, `quantity`, `unit`, `price`) VALUES('$id','" . ucfirst($ary_product[$i]) . "','" . $ary_quantity[$i] . "','" . $ary_unit[$i] . "','" . $ary_price[$i] . "')";
        $result = $database->query($query) or die(mysql_error());
        $bill_id = $database->get_last_insert_id();
      }
    }
    return $result;
  }

  public function get_direct_bills_current_day() {
    global $database;
    $start = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
    $end = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
    $query = "SELECT * FROM `direct_purchase` WHERE `timestamp` BETWEEN '$start' AND '$end'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_estimate_direct_bills_current_day() {
    global $database;
    $start = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
    $end = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
    $query = "SELECT * FROM `est_direct_pur` WHERE `date` BETWEEN '$start' AND '$end'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_account_bill($BillNo = FALSE, $cmd = FALSE) {
    global $database;
    $query = "SELECT *, (SELECT ShopName FROM `companies` c WHERE c.id LIKE ab.name) ShopName FROM account_bill ab";
    if ($BillNo) {
      $query .= " WHERE ab.id = '$BillNo'";
      $result = $database->query_fetch_full_result($query);
      return $result[0];
    }
    if($cmd && $cmd == 'UNPAID') $query .= " WHERE `paid` = '0' AND `cancel` = 0";
    $query .= " ORDER BY id DESC";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_direct_bills($BillNo = FALSE, $cmd = FALSE) {
    global $database;
    $query = "SELECT *, (SELECT ShopName FROM `companies` c WHERE c.id LIKE dp.name) ShopName FROM direct_purchase dp";
    if ($BillNo) {
      $query = "SELECT `id`, bill_id AS BillNo, `name`, (SELECT ShopName FROM `companies` c WHERE c.id LIKE dp.name) ShopName, `description`, `amount`, `payment_type`, `bankname`, `cheque_no`, `cheque_date`, `timestamp`, `bill_date`, `user_id`, `acct_heads`, `VAT`, `Discount`, `other_charges`, `cheque_clear`, `paid`, `menu_date`, `contact_person` FROM direct_purchase dp WHERE id = '$BillNo'";
      $result = $database->query_fetch_full_result($query);
      return $result[0];
    }
    if($cmd && $cmd == 'UNPAID') $query .= " WHERE `paid` = '0' AND `cancel` = 0";
    $query .= " ORDER BY id DESC";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_bills_from_bill_details($billID) {
    global $database;
    $query = "SELECT *, (SELECT name FROM `inventory` i WHERE i.id = bd.item_name LIMIT 0,1) item_name FROM `bill_details` bd WHERE `bill_id` = '$billID'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_direct_purchase_bills_from_bill_details($billID) {
    global $database;
    $query = "SELECT * FROM `direct_bill_details` db WHERE `bill_id` = '$billID'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  function shop_exist($ShopName) {
    global $database;
    $query = "SELECT COUNT(*) rows FROM `companies` WHERE `ShopName` LIKE '$ShopName' LIMIT 0, 1";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['rows'];
  }

  function add_shop_name($ary_post) {
    global $database;
    $query = "INSERT INTO `companies`(`ShopName`, `ContactPerson`, `ContactNo`, `address`) VALUES ('" . implode("','", $ary_post) . "')";
    $result = $database->query($query);
    return $result;
  }

  function update_shop($ary_post, $shop_id) {
    global $database;
    $query = "UPDATE `companies` SET `ShopName` = '$ary_post[0]', `ContactPerson` = '$ary_post[1]', `ContactNo` = '$ary_post[2]', `address` = '$ary_post[3]' WHERE `id` = '$shop_id'";
    $result = $database->query($query);
    return $result;
  }

  function delete_shop($shop_id) {
    global $database;
    $query = "DELETE FROM `companies` WHERE `id` = '$shop_id'";
    $result = $database->query($query);
    return $result;
  }

  function get_shops($shop_id = FALSE) {
    global $database;
    $query = "SELECT * FROM `companies`";
    if ($shop_id)
      $query .= " WHERE `id` = '$shop_id'";
    $result = $database->query_fetch_full_result($query);
    return ($shop_id) ? $result[0] : $result;
  }

  //getter and setter
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getProductname() {
    return $this->productname;
  }

  public function setProductname($productname) {
    $this->productname = $productname;
  }

  public function getShop_ownername() {
    return $this->shop_ownername;
  }

  public function setShop_ownername($shop_ownername) {
    $this->shop_ownername = $shop_ownername;
  }

  public function getQuantity() {
    return $this->quantity;
  }

  public function setQuantity($quantity) {
    $this->quantity = $quantity;
  }

  public function getReceivedate() {
    return $this->receivedate;
  }

  public function setReceivedate($receivedate) {
    $this->receivedate = $receivedate;
  }

  public function getAmount() {
    return $this->amount;
  }

  public function setAmount($amount) {
    $this->amount = $amount;
  }

  public function getBankname() {
    return $this->bankname;
  }

  public function setBankname($bankname) {
    $this->bankname = $bankname;
  }

  public function getType() {
    return $this->type;
  }

  public function setType($type) {
    $this->type = $type;
  }

  public function getUnit_price() {
    return $this->unit_price;
  }

  public function setUnit_price($unit_price) {
    $this->unit_price = $unit_price;
  }

}

?>
