<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.menu.php');
require_once('classes/class.receipt.php');

$title = 'Quarter Idarah Report';
$active_page = 'report';
require_once 'includes/header.php';

$cls_receipt = new Mtx_Receipt();
$cls_menu = new Mtx_Menu();

$from_date_1 = $to_date_1 = $from_date_2 = $to_date_2 = $from_date_3 = $to_date_3 = FALSE;
$fromDate_1 = $toDate_1 = $fromDate_2 = $toDate_2 = $fromDate_3 = $toDate_3 = FALSE;

$btn_print = TRUE;
$post = FALSE;

if (isset($_GET['search'])) {
  $post = TRUE;
  $from_date_1 = $_GET['1_from_date'];
  $to_date_1 = $_GET['1_to_date'];
  $from_date_2 = $_GET['2_from_date'];
  $to_date_2 = $_GET['2_to_date'];
  $from_date_3 = $_GET['3_from_date'];
  $to_date_3 = $_GET['3_to_date'];
  
  $fdate_1 = explode('-', $from_date_1);
  $fromDate_1 = mktime(0, 0, 0, $fdate_1[1], $fdate_1[2], $fdate_1[0]);
  $tdate_1 = explode('-', $to_date_1);
  $toDate_1 = mktime(23, 59, 59, $tdate_1[1], $tdate_1[2], $tdate_1[0]);
  $fdate_2 = explode('-', $from_date_2);
  $fromDate_2 = mktime(0, 0, 0, $fdate_2[1], $fdate_2[2], $fdate_2[0]);
  $tdate_2 = explode('-', $to_date_2);
  $toDate_2 = mktime(23, 59, 59, $tdate_2[1], $tdate_2[2], $tdate_2[0]);
  $fdate_3 = explode('-', $from_date_3);
  $fromDate_3 = mktime(0, 0, 0, $fdate_3[1], $fdate_3[2], $fdate_3[0]);
  $tdate_3 = explode('-', $to_date_3);
  $toDate_3 = mktime(23, 59, 59, $tdate_3[1], $tdate_3[2], $tdate_3[0]);
  //$menus = $cls_menu->get_act_cost_between_dates_from_finalize($fromDate, $toDate);

  $opening_data = $cls_receipt->get_balance_by_date($from_date_1);
  $closing_data = $cls_receipt->get_balance_by_date($to_date_3);

  $opening_cash = $opening_data[0]['opening_cash'];
  $opening_bank = $opening_data[0]['opening_bank'];
  $opening_balance = $opening_cash + $opening_bank;
  
  $closing_cash = $closing_data[0]['closing_cash'];
  $closing_bank = $closing_data[0]['closing_bank'];
  $closing_balance = $closing_cash + $closing_bank;
}

$btn_print_link = "#print_costing_report.php?from_date=$from_date_1&to_date=$to_date_1";

$hub_amount_1 = $cls_receipt->get_hub_sum($from_date_1, $to_date_1);
$hub_amount_2 = $cls_receipt->get_hub_sum($from_date_2, $to_date_2);
$hub_amount_3 = $cls_receipt->get_hub_sum($from_date_3, $to_date_3);
$total_enayat = $hub_amount_1['total'] + $hub_amount_2['total'] + $hub_amount_3['total'];

$vol_amount_1 = $cls_receipt->get_vol_total_between_dates($from_date_1,$to_date_1);
$vol_amount_2 = $cls_receipt->get_vol_total_between_dates($from_date_2,$to_date_2);
$vol_amount_3 = $cls_receipt->get_vol_total_between_dates($from_date_3,$to_date_3);
$total_mumineen = $vol_amount_1[0]['Amount'] + $vol_amount_2[0]['Amount'] + $vol_amount_3[0]['Amount'];

//Expences
$bill_amount_1 = $cls_receipt->get_unpaid_bills_amount($fromDate_1, $toDate_1);
$debit_amount_1 = $cls_receipt->get_debit_total_between_dates($from_date_1, $to_date_1);
$direct_purchase_amount_1 = $cls_receipt->get_unpaid_direct_purchase_amount($fromDate_1, $toDate_1);
$total_expence_1 = $bill_amount_1 + $debit_amount_1[0]['Amount'] + $direct_purchase_amount_1;

$bill_amount_2 = $cls_receipt->get_unpaid_bills_amount($fromDate_2, $toDate_2);
$debit_amount_2 = $cls_receipt->get_debit_total_between_dates($from_date_2, $to_date_2);
$direct_purchase_amount_2 = $cls_receipt->get_unpaid_direct_purchase_amount($fromDate_2, $toDate_2);
$total_expence_2 = $bill_amount_2 + $debit_amount_2[0]['Amount'] + $direct_purchase_amount_2;

$bill_amount_3 = $cls_receipt->get_unpaid_bills_amount($fromDate_3, $toDate_3);
$debit_amount_3 = $cls_receipt->get_debit_total_between_dates($from_date_3, $to_date_3);
$direct_purchase_amount_3 = $cls_receipt->get_unpaid_direct_purchase_amount($fromDate_3, $toDate_3);
$total_expence_3 = $bill_amount_3 + $debit_amount_3[0]['Amount'] + $direct_purchase_amount_3;

$total_fly = $cls_family->get_total_family();

$page_number = IDARAH_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
<?php
include 'includes/inc_left.php';
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?php echo $title; ?></h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Reports</a></li>
      <li><a href="#">Thali</a></li>
      <li class="active"><?php echo $title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12">&nbsp;</div>

      <!-- Center Bar -->
      <div class="col-md-12">
        <form method="GET" role="form" class="form-horizontal">
          <div></div>
          <div class="col-md-12">
              <label class="col-md-2 control-label">Month 1:</label>
              <label class="col-md-2 control-label">From Date</label>
              <div class="col-md-3">
                  <input type="date" name="1_from_date" class="form-control" id="1_from_date" value="<?php echo $from_date_1; ?>" placeholder="From Date">
              </div>
              <label class="col-md-2 control-label">To Date</label>
              <div class="col-md-3">
                  <input type="date" name="1_to_date" class="form-control" id="1_to_date" value="<?php echo $to_date_1; ?>" placeholder="To Date">
              </div>
          </div>
          <div class="clearfix"></div>
          <div class="clearfix"><br></div>
          
          <div class="col-md-12">
              <label class="col-md-2 control-label">Month 2:</label>
              <label class="col-md-2 control-label">From Date</label>
              <div class="col-md-3">
                  <input type="date" name="2_from_date" class="form-control" id="2_from_date" value="<?php echo $from_date_2; ?>" placeholder="From Date">
              </div>
              <label class="col-md-2 control-label">To Date</label>
              <div class="col-md-3">
                  <input type="date" name="2_to_date" class="form-control" id="2_to_date" value="<?php echo $to_date_2; ?>" placeholder="To Date">
              </div>
          </div>
          <div class="clearfix"></div>
          <div class="clearfix"><br></div>
          
          <div class="col-md-12">
              <label class="col-md-2 control-label">Month 3:</label>
              <label class="col-md-2 control-label">From Date</label>
              <div class="col-md-3">
                  <input type="date" name="3_from_date" class="form-control" id="3_from_date" value="<?php echo $from_date_3; ?>" placeholder="From Date">
              </div>
              <label class="col-md-2 control-label">To Date</label>
              <div class="col-md-3">
                  <input type="date" name="3_to_date" class="form-control" id="3_to_date" value="<?php echo $to_date_3; ?>" placeholder="To Date">
              </div>
          </div>
          <div class="clearfix"></div>
          <div class="clearfix"><br></div>
          
          <div class="col-md-12">
              <label class="col-md-3 control-label"></label>
              <div class="col-md-7">
                <input type="submit" class="btn btn-success col-md-3" name="search" id="search" value="Search" style="margin-right: 5px;">
                  <?php if(isset($btn_print_link)) {?>
                  <a href="<?php echo $btn_print_link; ?>" target="blank" class="btn btn-primary col-md-3 validate <?php echo !$post ? 'disabled' : ''; ?>" id="print">Print</a>
                  <?php } ?>
              </div>
          </div>
        </form>

        <div class="col-md-12">&nbsp;</div>
        <?php if (isset($_GET['search'])) { ?>
        <div class="clearfix"></div>
        <p class="form-control-static text-center alert-info">FUND REPORT - QUARTER</p><br>
          <div class="col-md-12">
            <p class="form-control-static"><strong>Enter Total Thaali days in this Quarter:</strong> <?php //echo number_format($opening_data[0]['opening_cash'], 2); ?></p>
          </div>
          <?php
            switch (USE_CALENDAR) {
              case 'Hijri':
                $open = HijriCalendar::GregorianToHijri(strtotime($from_date_1));
                $open_date = $open[1] . ' ' . HijriCalendar::monthName($open[0]) . ', ' . $open[2] . ' H';
                $close = HijriCalendar::GregorianToHijri(strtotime($to_date_3));
                $close_date = $close[1] . ' ' . HijriCalendar::monthName($close[0]) . ', ' . $close[2] . ' H';

                break;
              case 'Greg':
                $open_date = date('d', strtotime($from_date_1)) . ' ' . date('F', mktime(0, 0, 0, date('m', strtotime($from_date_1)))) . ', ' . date('Y', strtotime($from_date_1));
                $close_date = date('d', strtotime($to_date_3)) . ' ' . date('F', mktime(0, 0, 0, date('m', strtotime($to_date_3)))) . ', ' . date('Y', strtotime($to_date_3));
                break;
            }
          ?>
          
          <div class="col-md-12">
            <p class="form-control-static"><strong>A. Opening Cash and Bank Balance as on:</strong> <?php echo date('d F, Y', strtotime($from_date_1)); ?> (<?php echo $open_date; ?>)</p><br>
            
            <div class="col-md-3 alert-info row">
              <p class="form-control-static"><strong>Cash:</strong> <?php echo number_format($opening_cash, 2); ?></p>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"><br></div>
            
            <div class="col-md-3 alert-info row">
              <p class="form-control-static"><strong>Bank:</strong> <?php echo number_format($opening_bank, 2); ?></p>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"><br></div>
            
            <div class="col-md-3 alert-success row">
              <p class="form-control-static"><strong>Total:</strong> <?php echo number_format($opening_balance, 2); ?></p>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"><br></div>
            
            <p class="form-control-static"><strong>B. Receipts:</strong></p>
            <p class="form-control-static"><strong>1. Receipts (Dawat Enayat Including, Moulana Hamza (A.S.) Salawat Enayat)</strong></p>
            
            <p class="form-control-static"><strong>(<?php echo date('d F',strtotime($from_date_1)) . ' to ' . date('d F Y',strtotime($to_date_1)); ?>)</strong></p>
            
            <div class="col-md-3 alert-info row">
              <p class="form-control-static"><?php echo number_format($hub_amount_1['total'], 2); ?></p>
            </div>
            <div class="clearfix"></div>
            
            <p class="form-control-static"><strong>(<?php echo date('d F',strtotime($from_date_2)) . ' to ' . date('d F Y',strtotime($to_date_2)); ?>)</strong></p>
            
            <div class="col-md-3 alert-info row">
              <p class="form-control-static"><?php echo number_format($hub_amount_2['total'], 2); ?></p>
            </div>
            <div class="clearfix"></div>
            
            <p class="form-control-static"><strong>(<?php echo date('d F',strtotime($from_date_3)) . ' to ' . date('d F Y',strtotime($to_date_3)); ?>)</strong></p>
            
            <div class="col-md-3 alert-info row">
              <p class="form-control-static"><?php echo number_format($hub_amount_3['total'], 2); ?></p>
            </div>
            <div class="clearfix"></div>
            
            <p class="form-control-static"><strong>Total Enayat</strong></p>
            
            <div class="col-md-3 alert-success row">
              <p class="form-control-static"><?php echo number_format($total_enayat, 2); ?></p>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"><br></div>
            
            <p class="form-control-static"><strong>2. Voluntary Contribution from Mumineen (Niyaz, Hoob, Salawat, etc.)</strong></p>
            
            <p class="form-control-static"><strong>(<?php echo date('d F',strtotime($from_date_1)) . ' to ' . date('d F Y',strtotime($to_date_1)); ?>)</strong></p>
            
            <div class="col-md-3 alert-info row">
              <p class="form-control-static"><?php echo number_format($vol_amount_1[0]['Amount'], 2); ?></p>
            </div>
            <div class="clearfix"></div>
            
            <p class="form-control-static"><strong>(<?php echo date('d F',strtotime($from_date_2)) . ' to ' . date('d F Y',strtotime($to_date_2)); ?>)</strong></p>
            
            <div class="col-md-3 alert-info row">
              <p class="form-control-static"><?php echo number_format($vol_amount_2[0]['Amount'], 2); ?></p>
            </div>
            <div class="clearfix"></div>
            
            <p class="form-control-static"><strong>(<?php echo date('d F',strtotime($from_date_3)) . ' to ' . date('d F Y',strtotime($to_date_3)); ?>)</strong></p>
            
            <div class="col-md-3 alert-info row">
              <p class="form-control-static"><?php echo number_format($vol_amount_3[0]['Amount'], 2); ?></p>
            </div>
            <div class="clearfix"></div>
            
            <p class="form-control-static"><strong>Total Mumineen Contribution</strong></p>
            
            <div class="col-md-3 alert-success row">
              <p class="form-control-static"><?php echo number_format($total_mumineen, 2); ?></p>
            </div>
            <div class="clearfix"></div>
            
            <p class="form-control-static"><strong>Total Enayat and Mumineen Contribution</strong></p>
            
            <div class="col-md-3 alert-warning row">
              <p class="form-control-static"><?php echo number_format($total_enayat + $total_mumineen, 2); ?></p>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"><br></div>
            
            <p class="form-control-static"><strong>C. Payments for Expenses:</strong> <?php date('d, F Y', strtotime($from_date_1)); ?></p>
            
            <p class="form-control-static"><strong>(<?php echo date('d F',strtotime($from_date_1)) . ' to ' . date('d F Y',strtotime($to_date_1)); ?>)</strong></p>
            
            <div class="col-md-3 alert-info row">
              <p class="form-control-static"><?php echo number_format($total_expence_1, 2); ?></p>
            </div>
            <div class="clearfix"></div>
            
            <p class="form-control-static"><strong>(<?php echo date('d F',strtotime($from_date_2)) . ' to ' . date('d F Y',strtotime($to_date_2)); ?>)</strong></p>
            
            <div class="col-md-3 alert-info row">
              <p class="form-control-static"><?php echo number_format($total_expence_2, 2); ?></p>
            </div>
            <div class="clearfix"></div>
            
            <p class="form-control-static"><strong>(<?php echo date('d F',strtotime($from_date_3)) . ' to ' . date('d F Y',strtotime($to_date_3)); ?>)</strong></p>
            
            <div class="col-md-3 alert-info row">
              <p class="form-control-static"><?php echo number_format($total_expence_3, 2); ?></p>
            </div>
            <div class="clearfix"></div>
            
            <p class="form-control-static"><strong>Total Expenses</strong></p>
            
            <div class="col-md-3 alert-success row">
              <p class="form-control-static"><?php echo number_format($total_expence_1 + $total_expence_2 + $total_expence_3, 2); ?></p>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"><br></div>
            
            <p class="form-control-static"><strong>D. Closing Cash and Bank Balance as on:</strong> <?php echo date('d F, Y', strtotime($to_date_3)); ?> (<?php echo $close_date; ?>)</p><br>
            
            <div class="col-md-3 alert-info row">
              <p class="form-control-static"><strong>Cash: </strong><?php echo number_format($closing_cash, 2); ?></p>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"><br></div>
            
            <div class="col-md-3 alert-info row">
              <p class="form-control-static"><strong>Bank: </strong><?php echo number_format($closing_bank, 2); ?></p>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"><br></div>
            
            <div class="col-md-3 alert-success row">
              <p class="form-control-static"><strong>Total: </strong><?php echo number_format($closing_balance, 2); ?></p>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"><br></div>
            
            <div class="col-md-3 alert-warning row">
              <p class="form-control-static"><strong>Differences: </strong><?php echo number_format($opening_balance - $closing_balance, 2); ?></p>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"><br></div>            
          </div>
                    
        <?php } ?>
      </div>
      <!-- /Center Bar -->
    </div>
    <!-- /Content -->
  </section>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>