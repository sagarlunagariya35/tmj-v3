<?php
include 'session.php';
$pg_link = 'voluntary_rcpt_book';
require_once('classes/class.database.php');
require_once('classes/class.product.php');
require_once('classes/class.menu.php');
$cls_menu = new Mtx_Menu();
$cls_product = new Mtx_Product();

$from_date = $to_date = FALSE;
if (isset($_GET) && $_GET) {
  $data = $database->clean_data($_GET);
  $from_date = $data['from_date'];
  $to_date = $data['to_date'];
  $dt = explode('-', $from_date);
  $start = mktime(0, 0, 0, $dt[1], $dt[2], $dt[0]);
  $dt = explode('-', $to_date);
  $end = mktime(23, 59, 59, $dt[1], $dt[2], $dt[0]);
  $query = "SELECT *, (SELECT menu FROM `daily_menu` dm WHERE dm.timestamp = inf.timestamp) menu_ids FROM `inventory_finalize` inf WHERE `timestamp` BETWEEN '$start' AND '$end'";
  $confirm_menus = $database->query_fetch_full_result($query);
} else
  $confirm_menus = FALSE;

$title = 'Confirm Menus';
$active_page = 'report';

require_once 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Inventory</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <form method="get" role="form" class="form-horizontal">
            <div></div>
            <div class="col-md-12">
              <label class="col-md-1 control-label">From</label>
              <div class="col-md-5">
                <input type="date" name="from_date" class="form-control" id="from_date" value="<?php echo $from_date; ?>">
              </div>
              <label class="col-md-1 control-label">To</label>
              <div class="col-md-4">
                <input type="date" name="to_date" class="form-control" id="to_date" value="<?php echo $to_date; ?>">
              </div>

              <input type="submit" class="btn btn-success validate" name="" id="search" value="Search">

            </div>
          </form>
          <script>
            $('.validate').click(function() {
              var from_date = $('#from_date').val();
              var to_date = $('#to_date').val();
              var error = '';
              var validate = true;
              if (from_date == '')
              {
                error += 'Please select From date\n';
                validate = false;
              }
              if (to_date == '')
              {
                error += 'Please select To date\n';
                validate = false;
              }
              if (validate == false) {
                alert(error);
                return validate;
              }
            });
          </script>
          <div class="col-md-12">&nbsp;</div>
          <?php if ($confirm_menus) { ?>
            <div class="col-md-12">
              <table class="table table-hover table-condensed table-bordered">
                <thead>
                  <tr>
                    <th>Sr No.</th>
                    <th>Date</th>
                    <th>Menu Name</th>
                    <th class="text-right">Cost</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if ($confirm_menus) {
                    $sr = 1;
                    $total_cost = 0;
                    $holidays = 0;
                    foreach ($confirm_menus as $m) {
                      // remove the 0 costing days
                      if ($m['act_cost'] == 0) {
                        $holidays++;
                        $tr_cls = 'text-warning alert-warning';
                      } else {
                        $tr_cls = 'text-info alert-info';
                      }
                      $total_cost += $m['act_cost'];
                      ?>
                      <tr class="<?php echo $tr_cls; ?>">
                        <td><?php echo $sr++; ?></td>
                        <td><?php echo date('d M, Y', $m['timestamp']); ?></td>
                        <td><?php
                          $menu_ids = $m['menu_ids'];
                          $ids = explode(',', $menu_ids);
                          $query = "SELECT GROUP_CONCAT(name) name FROM base_menu WHERE id IN ('" . implode("', '", $ids) . "')";
                          $result = $database->query_fetch_full_result($query);
                          echo $result[0]['name'];
                          ?></td>
                        <td class="text-right"><?php echo number_format($m['act_cost'], 2); ?></td>
                      </tr>
                    <?php }
                    ?>
                    <tr class="alert-info">
                      <td colspan="3" class="text-right"><strong>Total Cost:</strong></td>
                      <td class="text-right"><?php echo '<b>Rs.</b> ' . number_format($total_cost, 2); ?></td>
                    </tr>
                    <tr class="alert-info">
                      <td colspan="2" class="text-center"><strong>Total Niyaz: </strong><?php echo count($confirm_menus) - $holidays; ?></td>
                      <td colspan="2" class="text-right"><strong>Average Cost: Rs. </strong><?php echo number_format(($total_cost / (count($confirm_menus) - $holidays)), 2); ?></td>
                    </tr>

                  <?php } else {
                    ?>
                    <tr>
                      <td colspan="6" class="alert-danger">No results found.</td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          <?php } ?>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>