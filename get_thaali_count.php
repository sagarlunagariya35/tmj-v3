<?php
include('session.php');
require_once('classes/class.database.php');

// Get the date from user
$start_date = '2016-04-01';
$end_date = '2016-04-19';

// get the records from database between selected date range
$query = "SELECT * FROM cron_thaali_count WHERE `date` BETWEEN '$start_date' AND '$end_date'";
$rows = $database->query_fetch_full_result($query);

if ($rows) {
// convert the data into tabular format with thaali size as header
  foreach ($rows as $row) {
    $data[$row['date']][$row['mohallah']][$row['tiffin_size']] = $row['count'];
    $th[] = $row['tiffin_size'];
  }
}

$th = array_unique($th);

?>
<style>
  table, th, td {
    border: 1px solid black;
  }
</style>
<?php
foreach ($data as $date => $sub_data) {
  ?>
  <h1><?php echo $date; ?></h1>
  <table>
    <thead>
      <tr>
        <th>Tanzeem</th>
        <?php
        foreach ($th as $h) {
          ?>
        <th><?php echo $h; ?></th>
          <?php
        }
        ?>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach ($sub_data as $tanzeem => $vals) {
        ?>
          <tr>
            <td><?php echo $tanzeem; ?></td>
      <?php
        foreach ($th as $dh) {
          ?>
            <td><?php echo isset($vals[$dh]) ? $vals[$dh] : ''; ?></td>
          <?php
        }
        ?>
          </tr>
      <?php
      }
      ?>
    </tbody>
  </table>
  <?php
}