<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.menu.php");
$cls_menu = new Mtx_Menu();

if (isset($_POST['submit'])) {
  $item = $_POST['item_name'];
  $cost = $_POST['cost'];
  $result = $cls_menu->add_inventory_asset($item, $cost);
  if($result) {
    $_SESSION[SUCCESS_MESSAGE] = 'Asset has been added successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encounter while processing the request..';
  }
}

$title = 'Asset of inventory';
$active_page = 'account';

require_once 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Credit</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <div class="form-group">
              <label class="control-label col-md-3">Item Name</label>
              <div class="col-md-4">
                <input type="text" class="form-control" name="item_name" id="item_name">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Cost</label>
              <div class="col-md-4">
                <input type="text" class="form-control" name="cost" id="cost">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">&nbsp;</label>
              <button class="btn btn-success" type="submit" name="submit" id="add">Add</button>
            </div>

          </form>
        </div>
        <!-- /Center Bar -->
      <script>
        $('#add').click(function() {
        var name = $('#item_name').val();
        var cost = $('#cost').val();
        var error = [];
        var key = 0;
        if(name === '') error[key++] = 'item name';
        if(cost === '') error[key++] = 'item cost';
        if(error.length) {
          alert('Please enter ' + error.join(' & ') + ' to proceed..');
          return false;
        }
        });
      </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>