<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.product.php');
require_once('classes/class.billbook.php');
require_once('classes/class.menu.php');
require_once('classes/class.family.php');
$cls_product = new Mtx_Product();
$cls_billbook = new Mtx_BillBook();
$cls_menu = new Mtx_Menu();
$cls_family = new Mtx_family();

if (isset($_POST['inventory_issue'])) {

  $result = $cls_product->add_estimate_inventory_issue($_POST['menu_name'], $_POST['issue_item'], $_POST['issue_quantity'], $_POST['date']);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = 'Item issued in inventory is successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while processing Inventory Issue';
  }
}

if (isset($_POST['direct_bill'])) {

  $result = $cls_billbook->add_estimate_direct_purchase($_POST['menu_name'], ucfirst($_POST['direct_item']), $_POST['direct_quantity'], $_POST['date']);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = 'Direct bill added successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while processing Direct Purchase Bill';
  }
}

if(isset($_POST['delete_selected_inv'])) {
  $ary_inv_id = $_POST['delete_inv'];
  $date = $_GET['date'];
  $menu_id = $_POST['MENU_ID'];
  $ret = $cls_product->delete_estimated_inventory_issue($ary_inv_id, $date);
  if($ret) $_SESSION[SUCCESS_MESSAGE] = 'An item has been deleted.';
  else $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing...';
  header('Location: cook_estimate2.php?date='.$date);
  exit;
}

if(isset($_POST['delete_selected_direct'])) {
  $ary_inv_id = $_POST['delete_inv'];
  $date = $_GET['date'];
  $menu_id = $_POST['MENU_ID'];
  $ret = $cls_product->delete_estimated_direct_pur($ary_inv_id, $date);
  if($ret) $_SESSION[SUCCESS_MESSAGE] = 'An item has been deleted.';
  else $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing...';
  header('Location: cook_estimate2.php?date='.$date);
  exit;
}

if (isset($_POST['Update_qty'])) {
  $update_qty = $cls_product->insert_cook_est_inv_qty($_POST['inv_qty'], $_GET['date'], $_POST['person_cnt']);
  if($update_qty) $_SESSION[SUCCESS_MESSAGE] = "Estimated Inventory Issue Processed.";
  else $_SESSION[ERROR_MESSAGE] = "Errors encountered while processing Inventory Issue";
}

if (isset($_POST['update_direct_qty'])) {
  $update_direct_qty = $cls_product->insert_cook_est_direct_qty($_POST['direct_qty'], $_GET['date']);
  if($update_direct_qty) $_SESSION[SUCCESS_MESSAGE] = "Estimated Direct Purchase Processed.";
  else $_SESSION[ERROR_MESSAGE] = "Errors encountered while processing Direct Purchase Issue";
}

if (isset($_GET['cmd']) && $_GET['cmd'] == 'delete_bills') {
  $sql = $cls_product->delete_estimated_direct_pur($_GET['id']);
}
if (isset($_GET['cmd']) && ($_GET['cmd'] == 'delete')) {
  $sql = $cls_product->delete_estimated_inventory_issue($_GET['id'], $_GET['date']);
}
if (isset($_GET['date'])) {
  $date = trim($_GET['date']);
  $dt = explode('-', $date);
  $start = mktime(0, 0, 0, $dt[1], $dt[2], $dt[0]);
  $end = mktime(23, 59, 59, $dt[1], $dt[2], $dt[0]);
  $dmenu = $cls_menu->get_all_daily_menu($start, $end);
  $all_tiffin = $cls_family->get_count_tiffin_size();
  $est_cost = $cls_menu->get_daily_menu_cost($dmenu[0]['menu_id'], $start, $end);
  if ($dmenu[0]['person_count'] == 0) { $person_count = $all_tiffin; }
  else { $person_count = $dmenu[0]['person_count']; }

  $ary_issued_items = $cls_menu->get_cook_est_inv_issue_items($dmenu[0]['menu_id'], $person_count, $start);
  $ary_direct_items = $cls_menu->get_cook_est_direct_issue_items($dmenu[0]['menu_id'], $person_count, $start);
}
$direct_items = $cls_product->get_all_direct_items();
$items = $cls_product->get_all_ingredients();
$title = 'Inventory Issue';
$active_page = 'menu';

require_once 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profiles</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <label class="control-label col-md-4">Menu Name</label>
                  <div class="col-md-4">
                    <p class="form-control-static"><?php echo isset($date) ? $cls_menu->get_base_menu_name($dmenu[0]['menu_id']) : ''; ?></p>
                    <input type="hidden" name="menu_name" value="<?php echo isset($dmenu[0]['menu_id']) ? $dmenu[0]['menu_id'] : ''; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-4">Date</label>
                  <div class="col-md-4">
                    <p class="form-control-static"><?php echo isset($date) ? date('d F, Y', $dmenu[0]['timestamp']) : ''; ?></p>
                    <input type="hidden" name="date" value="<?php echo isset($date) ? date('m-d-Y', $dmenu[0]['timestamp']) : ''; ?>">
                  </div>
                </div>
                  <div class="form-group">
                    <label class="control-label col-md-4">Est Cost</label>
                    <div class="col-md-4">
                        <p class="form-control-static"><?php echo number_format($est_cost, 2);?></p>
                    </div>
                </div>
              </div>

              <div class="col-md-4">
                <span class="pull-right"><b><a class="btn btn-primary btn-xs" href="print_cook_estimate.php?date=<?php echo $date; ?>" target="_blank">Print</a></b></span>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label col-md-4">Person Count</label>
                  <div class="col-md-8">
                    <input value="<?php echo $person_count; ?>" name="tfn_cnt" id="tfn_cnt" class="form-control">
                  </div>
                  <div class="col-md-8">
                    <input type="button" name="upd_tfn" id="upd_tfn" class="btn btn-success" value="Update">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <span><b>Inventory Issue</b></span>
              <div class="form-group">
                <label class="control-label col-md-4">Item Name</label>
                <div class="col-md-5">
                  <select class="form-control" name="issue_item" id="item_issue">
                    <option value="0">--select one--</option>
                    <?php
                    if ($items) {
                      foreach ($items as $item) {
                        ?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></option>
                        <?php
                      }
                    }
                    ?>

                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-4">Quantity</label>
                <div class="col-md-5">
                  <input type="text" id="issue_quantity" name="issue_quantity" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-4">&nbsp;</label>
                <div class="col-md-5">
                  <input type="submit" value="Add" id="inventory_issue" name="inventory_issue" class="btn btn-success">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <span><b>Direct Purchase</b></span>
              <div class="form-group">
                <label class="control-label col-md-4">Item Name</label>
                <div class="col-md-5">
                  <select class="form-control" name="direct_item" id="direct_item">
                    <option value="0">--Select One--</option>
                    <?php foreach ($direct_items as $name) { ?>
                      <option value="<?php echo $name['name']; ?>"><?php echo $name['name']; ?></option>
      <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-4">Quantity</label>
                <div class="col-md-5">
                  <input type="text" id="direct_quantity" name="direct_quantity" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-4">&nbsp;</label>
                <div class="col-md-5">
                  <input type="submit" value="Add" id="direct_bill" name="direct_bill" class="btn btn-success">
                </div>
              </div>
            </div>
          </form>
          <div class="col-md-12">
          <div class="col-md-6">
      <?php if (isset($_POST['inventory_issue']) || isset($_GET['cmd']) || isset($_POST['direct_bill']) || isset($date)) { ?>
              <form method="post">
                <table class="table table-bordered table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>Sr. No</th>
                      <th>Item Name (Issued Qty)</th>
                      <th class="text-right">Quantity</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="display_issue">
                  <?php
                  if (isset($ary_issued_items) && $ary_issued_items) {
            $i = 1;
            $button = '';
            foreach ($ary_issued_items as $key => $item) {
                if ($item['inv_issue']) {
                  $bg_clr = ' class="alert alert-success"';
                  $est_qty = " ($item[inv_issue])";
                } else {
                  $bg_clr = '';
                  $est_qty = "";
                }
                if (!$item['base_inv_issue'])
                  $bg_clr = ' class="alert alert-warning"';

                  $button = '<button id="item-' . $key . '" class="btn btn-danger btn-xs del_issue_item">Delete</button>';
                  $chkbox = "<input type='checkbox' name='delete_inv[$key]' id='delete_inv-$key' value='$key'>";
                $txt_qty = '<input type="text" class="form-control text-right" value="' . number_format($item['base_inv_issue'] * $person_count, 3, '.', '') . '" name="inv_qty[' . $key . ']" id="inv_qty' . $i . '">';
                ?>
                    <tr<?php echo $bg_clr;?>>
                      <td><?php echo $i++;?></td>
                      <td><?php echo $item['item_name'] . $est_qty;?></td>
                      <td class="text-right"><?php echo $txt_qty;?></td>
                      <td><?php echo $chkbox;?></td>
                    </tr>
                    <input type="hidden" name="person_cnt" value="<?php echo $person_count;?>">
                    <input type="hidden" name="MENU_ID" value="<?php echo $dmenu[0]['menu_id'];?>">
                    <?php
            }

              echo '<tr><td colspan=4><input type="submit" class="btn btn-success" name="Update_qty" value="Confirm Inventory Issue"> <input type="submit" class="btn btn-danger" value="Delete Selected" name="delete_selected_inv" id="delete_selected_inv"></tr>';
            $gbl_item = $i - 1;
          } else {
            echo '<tr><td class="alert-danger" colspan="4">Sorry! No items found.</td></tr>';
            $gbl_item = 0;
          }
                  ?>
                  </tbody>
                </table>
              </form>
            <?php } ?>
          </div>
          <div class="col-md-6">
      <?php if (isset($_POST['direct_bill']) || isset($_POST['inventory_issue']) || isset($_GET['cmd']) || isset($date)) { ?>
              <form method="post">
                <table class="table table-bordered table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>Sr. No</th>
                      <th>Item Name (Issued Qty)</th>
                      <th class="text-right">Quantity</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="display_direct">
                  <?php
                  if (isset($ary_direct_items) && $ary_direct_items) {
            $i = 1;
            $button = '';//print_r($ary_direct_items);
            foreach ($ary_direct_items as $key => $bill) {

              if ($bill['direct_issue']) {
                $bg_clr = ' class="alert alert-success"';
                $est_qty = " ($bill[direct_issue])";
              } else {
                $bg_clr = '';
                $est_qty = "";
              }
              if (!$bill['base_inv_issue'])
                $bg_clr = ' class="alert alert-warning"';

              $txt_qty = '<input type="text" class="form-control text-right" value="' . number_format($bill['base_inv_issue'] * $person_count, 3, '.', '') . '" name="direct_qty[' . $key . ']" id="direct_qty' . $i . '">';
                $button = '<button id="direct-' . $key . '" class="btn btn-danger btn-xs del_issue_item">Delete</button>';
                $chkbox = "<input type='checkbox' name='delete_inv[$key]' id='delete_inv-$key' value='$key'>";
              //$tbody .= '<tr' . $bg_clr . '><td>' . $i++ . '</td><td>' . $key . $est_qty . '</td><td class="text-right">' . $txt_qty . '</td>' . $button . '</tr>';
              ?>
                    <tr<?php echo $bg_clr;?>>
                      <td><?php echo $i++;?></td>
                      <td><?php echo $key . $est_qty;?></td>
                      <td class="text-right"><?php echo $txt_qty;?></td>
                      <td><?php echo $chkbox;?></td>
                    </tr>
                    <?php
            }
              echo '<tr><td colspan=4><input type="submit" class="btn btn-success" name="update_direct_qty" value="Confirm Direct Purchase"> <input type="submit" class="btn btn-danger" value="Delete Selected" name="delete_selected_direct" id="delete_selected_inv"></tr>';
            $gbl_bills = $i - 1;
          } else {
            echo '<tr><td class="alert-danger" colspan="4">Sorry! No items found.</td></tr>';
            $gbl_bills = 0;
          }
                  ?>
                  </tbody>
                </table>
              </form>
      <?php } ?>
          </div>
        </div>


          </div>
        </div>
        <!-- /Center Bar -->
        <script>
          var gbl_tfn_cnt = '<?php echo $person_count; ?>';
          $('#upd_tfn').click(function() {
            var menu_id = '<?php echo $dmenu[0]['timestamp']; ?>';
            var tfn_cnt = $('#tfn_cnt').val();
            var limit = '<?php echo $gbl_item; ?>';
            var lim_bills = '<?php echo $gbl_bills; ?>';
            for (var i = 1; i <= limit; i++) {
              var qty = $('#inv_qty' + i).val().replace(',', '');
              var ori_qty = qty / gbl_tfn_cnt;
              var new_qty = ori_qty * tfn_cnt;
              $('#inv_qty' + i).val(new_qty.toFixed(3).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
            }
            for (var i = 1; i <= lim_bills; i++) {
              var qty = $('#direct_qty' + i).val().replace(',', '');
              var ori_qty = qty / gbl_tfn_cnt;
              var new_qty = ori_qty * tfn_cnt;
              $('#direct_qty' + i).val(new_qty.toFixed(3).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
            }
            gbl_tfn_cnt = tfn_cnt;
            $.ajax({
              url: 'ajax.php',
              type: "GET",
              async: true,
              data: 'cmd=update_person_count&tfn_cnt=' + tfn_cnt + '&timestamp=' + menu_id,
              success: function(data) {
              }
            });
          });
          $('#inventory_issue').click(function() {
            var menu = $('#menu_name').val();
            var item = $('#issue_item').val();
            var qty = $('#issue_quantity').val();
            var error = '';
            var validate = true;
            if (item == '0')
            {
              error += 'Please select menu Name\n';
              validate = false;
            }
            if (item == '0')
            {
              error += 'Please select issue item Name\n';
              validate = false;
            }
            if (qty == '')
            {
              error += 'Please enter issue quantity\n';
              validate = false;
            }
            if (validate === false)
            {
              alert(error);
              return validate;
            }
          });
          $('#direct_bill').click(function() {
            var menu = $('#menu_name').val();
            var item = $('#direct_item').val();
            var qty = $('#direct_quantity').val();
            var error = '';
            var validate = true;
            if (menu == '0')
            {
              error += 'Please select menu Name\n';
              validate = false;
            }
            if (item == '0')
            {
              error += 'Please select item Name\n';
              validate = false;
            }
            if (qty == '')
            {
              error += 'Please enter direct purchased quantity\n';
              validate = false;
            }
            if (validate === false)
            {
              alert(error);
              return validate;
            }
          });

          $('.del_issue_item').click(function(e){
            e.preventDefault();

            var answer = confirm('Are you sure you want to delete this?');
            if (answer) {
              myApp.showPleaseWait();
              var itemId = this.id.split('-');
              var choice = '';
              if(itemId[0] == 'direct') choice = 'DIRECT';
              else choice = 'INVENTORY';
              jQuery.ajax({
                type: "POST", // HTTP method POST or GET
                url: "ajax.php", //Where to make Ajax calls
                data: "cmd=del_inv_issue_item&id=" + itemId[1] + "&dt=<?php echo $date;?>&person_cnt=<?php echo $person_count;?>&menu_id=<?php echo $dmenu[0]['menu_id'];?>&choice=" + choice, //Form variables
                success:function(data,status){
                  //on success, show new data.
                  //$('#inv-' + itemId[1]).fadeOut();
                  var items = $.parseJSON(data);
                  var i = 1;
                  var est_qty = '';
                  var tbody = '';
                  var item_name;
                  var quantity;
                  var txt_qty;
                  var button;
                  $.each(items, function(key, value){
                    if(choice == 'DIRECT'){
                      item_name = key;
                      quantity = (this.base_inv_issue * <?php echo $person_count;?>).toFixed(3);
                      if (this.direct_issue) est_qty = ' (' +this.direct_issue + ')';
                      txt_qty = '<input type="text" class="form-control text-right" value="' + quantity + '" name="inv_qty[' + key + ']" id="inv_qty' + i + '">';
                      button = '<button id="item-' + key + '" class="btn btn-danger btn-xs del_issue_item">Delete</button>';
                    } else {
                      item_name = this.item_name;
                      quantity = (this.base_inv_issue * <?php echo $person_count;?>).toFixed(3);
                      if (this.inv_issue) est_qty = ' (' +this.inv_issue + ')';
                      txt_qty = '<input type="text" class="form-control text-right" value="' + quantity + '" name="inv_qty[' + key + ']" id="inv_qty' + i + '">';
                      button = '<button id="item-' + key + '" class="btn btn-danger btn-xs del_issue_item">Delete</button>';
                    }
                    tbody += '<tr' + this.BG_COLOR + '><td>' + i + '</td><td>' + item_name + est_qty + '</td><td class="text-right">' + txt_qty + '</td><td>' + button + '</td></tr><input type="hidden" name="person_cnt" value="' + <?php echo $person_count;?> + '">';
                    i++;
                  });
                  var display = '';
                  if(choice == 'DIRECT') {
                    display = 'display_direct';
                    var confirm_name = 'update_direct_qty';
                    var confirm_val = 'Confirm Direct Purchase';
                  } else {
                    display = 'display_issue';
                    var confirm_name = 'Update_qty';
                    var confirm_val = 'Confirm Inventory Issue';
                  }
                  tbody += '<tr><td colspan=4><input type="submit" class="btn btn-success" name="'+confirm_name+'" value="'+confirm_val+'"></tr>';
                  $('#'+display).html(tbody);
                  //alert(data);
                },
                error:function (xhr, ajaxOptions, thrownError){
                  //On error, we alert user
                  alert(thrownError);
                }
              });
              myApp.hidePleaseWait();
            } else {
              alert('Operation Canceled!');
            }
           });

          var myApp;
          myApp = myApp || (function () {
          var pleaseWaitDiv = $('<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Processing . . .</h4></div><div class="modal-body"><img src="asset/img/loader.gif"></div></div></div></div>');
          return {
              showPleaseWait: function() {
                  pleaseWaitDiv.modal();
              },
              hidePleaseWait: function () {
                  pleaseWaitDiv.modal('hide');
              },

          };
      })();

        </script>
    </section>
  </div>
<!-- /Content -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>
