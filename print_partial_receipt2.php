<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.receipt.php');
require_once('classes/hijri_cal.php');
require_once('includes/inc.num2words.php');

$cls_family = new Mtx_family();
$cls_receipt = new Mtx_Receipt();

$receipt_details = $cls_receipt->get_partial_receipt_details($_REQUEST['id']);
$name = $cls_family->get_name($receipt_details['FileNo']);
$family = $cls_family->get_single_family($receipt_details['FileNo']);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print partial receipt</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="asset/dist/css/print.css" media="print">
    <script type="text/javascript" src="asset/dist/js/NumberToWord.js"></script>
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8 ">
        <div style="text-align: center;" class="col-md-12">
          <img src="includes/logo.jpg" width="200px">
        </div>
        <div class="col-md-12" style="text-align: center;">
          <h2><strong><?php echo TANZEEM_NAME;?></strong></h2>
        </div>
  
        <div class="clearfix"></div>

          <div class="col-md-12">
            <span class="pull-left col-md-4"><strong>Date</strong> : <u><?php echo date('d/m/Y', $receipt_details['creat_date']); ?></u></span>
            <span class="col-md-3"><strong>FMB No.</strong>: <u><?php echo $receipt_details['FileNo'] ?></u></span>
            <span class="pull-right col-md-4"><strong>Receipt No.:</strong> <u><?php echo $receipt_details['id']; ?></u></span>
          </div>

          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-4"><strong>Tanzeem</strong> : <u><?php echo ucfirst($family['Mohallah']); ?></u></span>
            <span class="col-md-4"><strong>HOF Mobile No.</strong> :<u><?php echo $family['MPh']; ?></u></span>
          </div>

          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-8"><strong>HOF Name</strong> :<u><?php echo ucwords($name); ?></u></span>
            <span class="pull-right col-md-4"><strong>HOF ITS ID</strong> : <u><?php if ($family['ejamatID'] > '0')
  echo $family['ejamatID'];
else
  echo '';
?></u></span>
          </div>

          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-3">BAAD SALAAM,</span>
          </div>

          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-10">RECEIVED CASH WITH THANKS <strong>RS. <u><?php echo number_format($receipt_details['amount'],2,'.',',') . ' ' . num2words($receipt_details['amount']) . ' Only'; ?></u></strong>
          </div>

          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-4">AS amaanat.</span>
          </div>

          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-3">E & O.E.</span>
            <span class="pull-right col-md-4">ABDE SYEDNA(T.U.S)</span>
          </div>

          <!--div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>


          <div class="col-md-12">&nbsp;</div>
          <div style="text-align: center;" class="col-md-12">
            <img src="includes/logo.jpg" width="400px">
            <br>
            <br>
          </div>

          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-4"><strong>Date</strong> : <u><?php echo date('d/m/Y', $receipt_details['creat_date']); ?></u></span>
            <span class="col-md-3"><strong>FMB No.</strong>: <u><?php echo $receipt_details['FileNo'] ?></u></span>
            <span class="pull-right col-md-4"><strong>Receipt No.:</strong> <u><?php echo $receipt_details['id']; ?></u></span>
          </div>

          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-4"><strong>Tanzeem</strong> : <u><?php echo ucfirst($family['Mohallah']); ?></u></span>
            <span class="col-md-4"><strong>HOF Mobile No.</strong> :<u><?php echo $family['MPh']; ?></u></span>
          </div>

          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-7"><strong>HOF Name</strong> :<u><?php echo ucfirst($receipt_details['name']); ?></u></span>
            <span class="pull-right col-md-4"><strong>HOF ITS ID</strong> : <u><?php if ($family['ejamatID'] > '0')
  echo $family['ejamatID'];
else
  echo '';
?></u></span>
          </div>

          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-3">BAAD SALAAM,</span>
          </div>

          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-10">RECEIVED CASH WITH THANKS RS. <u><?php echo '<strong>' . number_format($receipt_details['amount']) . '/-</strong> '; ?><span id="amount1"></span></u>
          </div>

          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-7"><strong>Paid by(Name)</strong> : <u><?php echo ucfirst($receipt_details['name']); ?></u></span>
            <span class="pull-right col-md-4"><strong>Paid by (ITS ID)</strong> : <u></u></span>
          </div>

          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-4">AS amaanat.</span>
          </div>

          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">&nbsp;</div>

          <div class="col-md-12">
            <span class="pull-left col-md-3">E & O.E.</span>
            <span class="pull-right col-md-4">ABDE SYEDNA(T.U.S)</span>
          </div>
          <div class="col-md-12">&nbsp;</div-->
          <div class="col-md-12">&nbsp;</div>
<?php $cmd = $_REQUEST['cmd'];
if ($cmd == 'hub') {
  ?>
        <div class="col-md-12">
          <a id="continue_hub" href="print_hub_receipt.php?id=<?php echo $_REQUEST['hub_id'] ?>" class="no-print btn btn-success pull-right">Continue for hub</a><br><br>
        </div>
<?php } ?>
      <div class="col-md-12">
        <a href="index.php" class="no-print btn btn-info pull-right">Back to Home</a><br><br>
      </div>
      <div class="col-md-12">
        <a href="partial_rcpt.php" class="no-print btn btn-info pull-right">Back to Receipt</a>
      </div>

  </div>

      <!-- /Center Bar -->

      <!-- Right Bar -->

      <!-- /Right Bar -->

    </div>
    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
