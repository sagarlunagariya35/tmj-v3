<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.menu.php');
$cls_menu = new Mtx_Menu();
$cls_family = new Mtx_family();
$user_id = $_SESSION[USER_ID];


$title = 'Labels';
$active_page = 'settings';

require_once 'includes/header.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" target="blank" class="form-horizontal" action="print_labels.php">
            <div class="form-group">
              <label class="control-label col-md-4"><?php echo THALI_ID; ?>(s)</label>
              <div class="col-md-5">
                <textarea class="form-control" rows="8" name="tid" id="tid"></textarea>
              </div>
            </div>
            <div class="col-md-offset-4 col-md-4">
              <button name="add_labels" id="add_labels" type="submit" class="btn btn-success">Print</button>
              <a href="index.php" class="btn btn-info">Home</a>

            </div>
          </form>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>

<script>
  $('#add_labels').click(function() {
    var sid = $('#tid').val();
    if (sid == '') {
      alert('Please enter thali id(s)');
      return false;
    }
  });
</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>