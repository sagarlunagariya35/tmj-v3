<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/class.family.php');
require_once('classes/hijri_cal.php');
require_once('classes/class.user.php');
require_once('classes/class.menu.php');

$cls_receipt = new Mtx_Receipt();
$cls_family = new Mtx_family();
$cls_user = new Mtx_User();
$cls_menu = new Mtx_Menu();

$from_date = $to_date = date('Y-m-d');
$tanzeem = $selected_tanzeem_thali = FALSE;
$total_size_cost = 0;
$total_thali_cost = 0;
$grand_total = 0;
$per_thali_cost = 0;

if (isset($_POST['search'])) {
  $from_date = $_POST['from_date'];
  $to_date = $_POST['to_date'];
  $dt = explode('-', $from_date);
  $start = mktime(0, 0, 0, $dt[1], $dt[2], $dt[0]);
  $dt = explode('-', $to_date);
  $end = mktime(23, 59, 59, $dt[1], $dt[2], $dt[0]);
  
  $tanzeem = $_POST['tanzeem'];
  if($tanzeem == ''){
    $tanzeem = 'All';
  }
    
  $all_tanzeem_thali = $cls_family->get_thali_records($from_date, $to_date);
  $selected_tanzeem_thali = $cls_family->get_thali_records($from_date, $to_date, $tanzeem);
  
  if($all_tanzeem_thali){
    foreach ($all_tanzeem_thali as $att){
      $cost_multiplier = $cls_family->get_cost_multiplier_of_tiffin_size($att['tiffin_size']);

      $att['tiffin_size'] = $att['count'] * $cost_multiplier;
      $total_size_cost += $att['tiffin_size'];
    }

    $act_cost_of_menu = $cls_family->get_act_cost_of_confirm_menu($start,$end);
    $per_thali_cost = $act_cost_of_menu / $total_size_cost;
  }
}

$mohallah = $cls_user->get_single_user($_SESSION[USERNAME]);
$mohallahs = $cls_family->get_all_tanzeem();
$tiffin_size = $cls_menu->get_all_tiffin_size();

$title = 'Thali Records';
$active_page = 'report';
include('includes/header.php');

$page_number = ACCOUNTS_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Hub</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <div class="col-md-12">
              <label class="col-md-1 control-label">From</label>
              <div class="col-md-2">
                <input type="date" name="from_date" class="form-control" id="from_date" value="<?php echo $from_date; ?>">
              </div>
              <label class="col-md-1 control-label">To</label>
              <div class="col-md-2">
                <input type="date" name="to_date" class="form-control" id="to_date" value="<?php echo $to_date; ?>">
              </div>
              <label class="col-md-2 control-label">Tanzeem</label>
              <div class="col-md-2">
                <?php if($mohallah['Mohallah'] != '') { ?>
                <p class="form-control static"><?php echo $mohallah['Mohallah']; ?></p>
                <input type="hidden" name="tanzeem" value="<?php echo $mohallah['Mohallah']; ?>">
                <?php }else { ?>
                <select class="form-control" name="tanzeem" id="tanzeem">
                  <option value="">All</option>
                  <?php
                  foreach ($mohallahs as $mh) {
                    ?>
                    <option value="<?php echo $mh['name'] ?>" <?php echo (isset($tanzeem) && $tanzeem == $mh['name']) ? 'selected' : ''; ?>><?php echo $mh['name'] ?></option>
                  <?php } ?>
                </select>
                <?php } ?>
              </div>

              <input type="submit" class="btn btn-success validate" name="search" id="search" value="Search">
              <a href="#print_by_date_finance.php?from=<?php echo $from_date; ?>&to=<?php echo $to_date; ?>&tanzeem=<?php echo $tanzeem; ?>" target="blank" class="btn btn-primary validate" id="print">Print</a>
            </div>
          </form>
          <div class="col-md-12">&nbsp;</div>
          <?php
            if($selected_tanzeem_thali) {
          ?>
            <table class="table table-condensed table-bordered table-responsive">
              <thead>
                <tr>
                  <th>Date</th>
                  <?php
                    foreach ($selected_tanzeem_thali as $key=>$val) {
                      foreach ($val['tiffin_size'] as $index=>$value){
                        echo '<th>' . $value . '</th>';
                      }
                      foreach ($val['tiffin_size'] as $index=>$value){
                        echo '<th class="text-right">' . $value . '</th>';
                      }
                    }
                  ?>
                  <th class="text-right">Total Thali Cost</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  foreach ($selected_tanzeem_thali as $key=>$val) {
                ?>
                <tr>
                  <td><?php echo $key; ?></td>
                  <?php
                    foreach ($val['tiffin_size'] as $index=>$value){
                      $cost_multiplier2 = $cls_family->get_cost_multiplier_of_tiffin_size($value);
                      
                      $thali_cost_per_size[] = $val['count'][$index] * $cost_multiplier2 * $per_thali_cost;
                      $total_thali_cost = array_sum($thali_cost_per_size);
                      echo '<td>'. $val['count'][$index] .'</td>';
                    }
                  
                    foreach ($thali_cost_per_size as $tcps){
                      echo '<td class="text-right"><b>Rs.</b> ' . number_format($tcps, 2) . '</td>';
                    }
                  ?>
                  <td class="text-right"><?php echo '<b>Rs.</b> ' . number_format($total_thali_cost, 2); ?></td>
                </tr>
                <?php
                    $grand_total += $total_thali_cost;
                  }
                ?>
                <tr class="alert-info">
                  <td colspan="11" class="text-right"><strong>Grand Total:</strong></td>
                  <td class="text-right"><?php echo '<b>Rs.</b> ' . number_format($grand_total, 2); ?></td>
                </tr>
              </tbody>
            </table>
          <?php
            }
          ?>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
  
<script>
  $('.validate').click(function() {
    var from = $('#from_date').val();
    var to = $('#to_date').val();
    var errors = [];
    var key = 0;
    if (from === '')
      errors[key++] = 'from';
    if (to === '')
      errors[key++] = 'to';
    if (errors.length) {
      alert('Please select ' + errors.join(' & ') + ' date to proceed..');
      return false;
    }
  });
</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>