<?php
//include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.family.php");
require_once("classes/class.user.php");
$cls_family = new Mtx_family();
$cls_user = new Mtx_User();

$page = FALSE;
if (isset($_GET['page'])){
  $page = $_GET['page'];
}

if (isset($_POST['close_account'])) {
  $message = trim($_POST['msg']);
  $query = "SELECT *  FROM `family` WHERE `MPh` != '' AND `close_date` > '0'";
  $result = $database->query_fetch_full_result($query);
  if ($result){
    parse_result($result, $message);
  }
}

function parse_result($result, $message) {
  global $cls_family;
  if ($result) {
    // get all mobile numbers
    $ary_send_sms_to = get_mobile_numbers_in_group($result);
    
    foreach ($ary_send_sms_to as $sms_group) {
      $cls_family->send_sms($sms_group, $message);
    }
  }
}

function get_mobile_numbers_in_group($result){
    $mobile_numbers = array();
    $ary_send_sms_to = array();
    $counter = 0;
    foreach ($result as $family) {
      // group 20 mobile numbers in each set comma delimated
      if ($counter < 10) {
        $mobile_numbers[] = '91' . substr(preg_replace('/\D/', '', $family['MPh']), -10);
        $counter++;
      } else {
        $ary_send_sms_to[] = join(',', $mobile_numbers);
        $mobile_numbers = array(); // reset the array for next set
        $mobile_numbers[] = '91' . substr(preg_replace('/\D/', '', $family['MPh']), -10);
        $counter = 1;
      }
    }
    // add the remaining entries to the master array if any
    if (count($mobile_numbers)) {
      $ary_send_sms_to[] = join(',', $mobile_numbers);
    }
    
    return $ary_send_sms_to;
}

if (isset($_POST['send_to_above'])) {
  $message = trim($_POST['msg']);
  $FileNo = trim($_POST['tid']);
  $FileNo = explode(',', str_replace("\r\n", ',', $FileNo));
  $query = "SELECT *  FROM `family` WHERE `FileNo` IN ('" . implode("','", $FileNo) . "')";
  $result = $database->query_fetch_full_result($query);
  parse_result($result, $message);
}

if (isset($_POST['send_to_all'])) {
  $message = trim($_POST['msg']);
  $query = "SELECT *  FROM `family` WHERE `MPh` != '' AND `close_date` = '0'";
  $result = $database->query_fetch_full_result($query);
  parse_result($result, $message);
}

$title = 'Send SMS';
$active_page = 'settings';
//include('includes/header.php');
?>
<!-- Content -->
<div class="row">
  <div class="col-md-12">&nbsp;</div>

  <!-- Center Bar -->
  <div class="col-md-12">
    <form method="post" role="form" class="form-horizontal">
      <div></div>
      <div class="form-group">
        <label class="control-label col-md-3">Message</label>
        <div class="col-md-4">
          <textarea type="text" maxlength="160" onkeyup="textCounter(this, 160);" class="form-control" name="msg" id="msg"></textarea>
        </div>
        <div class="col-md-4">
          <span id="how_many"></span>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3"><?php echo THALI_ID; ?>(s)</label>
        <div class="col-md-4">
          <textarea class="form-control" rows="8" name="tid" id="tid"></textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">&nbsp;</label>
        <div class="col-md-8">
          <button class="btn btn-success" type="submit" name="send_to_all" id="send_to_all">Send To All</button>
          <button class="btn btn-success" type="submit" name="send_to_above" id="send_to_above">Send To above</button>
          <button class="btn btn-success" type="submit" name="close_account" id="above">To Close Account</button>
          <!--button class="btn btn-success" type="submit" name="hub_pending_msg" id="hub_pending_msg">Hub Pending</button-->
        </div>
      </div>

    </form>
  </div>
  <!-- /Center Bar -->
  <script>
    $('#send_to_all').click(function () {
      var msg = $('#msg').val();
      if (msg == '') {
        alert('To send the all family, please enter the message');
        return false;
      }
    });
    $('#send_to_above').click(function () {
      var msg = $('#msg').val();
      var thali_id = $('#tid').val();
      if (msg == '' || thali_id == '') {
        alert('To send the above, please enter the message and thali id(s)...');
        return false;
      }
    });
    $('#hub_pending_msg').click(function () {
      var question = confirm('This message will not send to above file id but this button will check for the file no which is having hub in pending so do you still want to continue?');
      if (question)
        return true;
      else
        return false;
    });
    function textCounter(field, maxlimit)
    {
      var len = field.value.length;
      if (field.value.length < maxlimit) {
        $('#how_many').addClass('alert alert-success').text((maxlimit - len) + ' Characters remaining');
      } else {
        $('#how_many').addClass('alert alert-danger').text('0 Character remaining');
        field.blur();
        field.focus();
        return false;
      }
    }
  </script>

</div>
<!-- /Content -->