<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.product.php');
require_once('classes/class.menu.php');
$cls_product = new Mtx_Product();

$i = $no = FALSE;
if (isset($_GET['mid']))
  $menu_id = (int) trim($_GET['mid']);
else
  $menu_id = FALSE;

$cls_menu = new Mtx_Menu();

$person_count = $cls_menu->get_base_menu_person_count($menu_id);
$menu_name = $cls_menu->get_base_menu_name($menu_id);
$category_id = $cls_menu->get_base_menu_category($menu_id);


if(isset($_POST['updateForm'])) {
  $data = $database->clean_data($_POST);
  
  $menuName = $data['menu_name'];
  $category_id = $data['category'];
  $person_count = $data['count'];
  $baseInvItem = (array) $data['base_inv'];
  $baseInv50Qty = (array) $data['baseInv50Qty'];
  $baseInv100Qty = (array) $data['baseInv100Qty'];
  $baseInv500Qty = (array) $data['baseInv500Qty'];
  $baseDirQty = (array) $data['baseDirQty'];
  $baseDirItem = (array) $data['directItems'];
  $isUpdated = $cls_menu->update_BaseMenuForm_Der($menuName, $category_id, $person_count, $baseInvItem, $baseInv50Qty, $baseInv100Qty, $baseInv500Qty, $baseDirItem, $baseDirQty, $menu_id);
  if($isUpdated) {
    $_SESSION[SUCCESS_MESSAGE] = 'Base menu updated successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while updating the records.';
  }
}

if (isset($_POST['add_menu'])) {
  $result = $cls_menu->add_base_menu(ucfirst($_POST['menu_name']), $_POST['category'], $_POST['count']);
  if ($result['success']) {
    $base_direct = $cls_menu->get_all_base_direct_items($result['insert_id'], $person_count);
    $base_inv = $cls_menu->get_all_base_inv_issue($result['insert_id'], $person_count);
    $_SESSION[SUCCESS_MESSAGE] = "Menu added successfully";
    header('Location: base_menu.php?mid=' . $result['insert_id']);
    exit;
  } else {
    $_SESSION[ERROR_MESSAGE] = "Errors encountered while processing Base Menu";
  }
}
if (isset($_POST['Add_base_inv'])) {
  $result = $cls_menu->add_base_inventory($_POST['issue_item'], $_POST['issue_quantity'] / $person_count, $menu_id);
  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "";
  } else {
    $_SESSION[ERROR_MESSAGE] = "Errors encountered while processing Base Inventory";
  }
}
if (isset($_POST['add_direct_bill'])) {
  $result = $cls_menu->add_base_direct($_POST['direct_item'], $_POST['direct_quantity'] / $person_count, $menu_id);
  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "";
  } else {
    $_SESSION[ERROR_MESSAGE] = "Errors encountered while processing Base Direct Purchase Bill";
  }
}

if (isset($_POST['DELETE_BASE_INV'])) {
  $item_id = $_POST['delete'];
  $result = $cls_menu->delete_base_menu_item_der($item_id, $menu_id);
  header('Location: base_menu.php?mid=' . $menu_id);
  exit();
}

if (isset($_POST['DELETE_BASE_DIRECT'])) {
  $item_id = $_POST['delete_direct'];
  $result = $cls_menu->delete_base_menu_direct_item($item_id, $menu_id);
  header('Location: base_menu.php?mid=' . $menu_id);
  exit();
}

$categories = $cls_menu->get_all_category();
$base_direct = $cls_menu->get_all_base_direct_items($menu_id, $person_count);
$base_inv = $cls_menu->get_all_base_inv_issue_der($menu_id, $person_count);
$direct_items = $cls_product->get_all_direct_items();
$items = $cls_product->get_all_ingredients();
//print_r($items);
$title = 'Base Menu';
$active_page = 'menu';

require_once 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';

?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-12">
            <a href="print_base_menu.php?mid=<?php echo $menu_id; ?>" class="btn btn-primary btn-xs pull-right">Print</a>
          </div>
          <div class="clearfix"></div>
          <form method="post">
            <div class="col-md-12">
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label col-md-4">Menu Name</label>
                  <div class="col-md-8">
                    <input type="text" name="menu_name" id="menu_name" class="form-control" value="<?php echo $menu_name ? $menu_name : ''; ?>">
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label col-md-4">Category</label>
                  <div class="col-md-8">
                    <select name="category" id="category" class="form-control">
                      <option value="0">--Select Category--</option>
                      <?php
                      foreach ($categories as $category) {
                        $sel = $category['id'] == $category_id ? 'selected' : '';
                        ?>
                        <option value="<?php echo $category['id']; ?>" <?php echo $sel; ?>><?php echo $category['name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="control-label col-md-3">Person</label>
                  <div class="col-md-4">
                    <input type="text" name="count" class="form-control" id="count" value="<?php echo $person_count ? $person_count : ''; ?>">
                  </div>
                </div>
              </div>
            </div>

            <?php if ($menu_id) { ?>
              <div class="col-md-12">
                <span><h3>Inventory Issue</h3></span>
                <!--          <div class="form-group">
                            <label class="control-label col-md-4">Item Name</label>
                            <div class="col-md-5">
                              <select class="form-control" name="issue_item" id="item_issue">
                                <option value="0">--select one--</option>
                <?php
                if ($items) {
                  foreach ($items as $item) {
                    ?>
                                            <option value="<?php //echo $item['id']; ?>"><?php //echo $item['name'] . ' - ' . $item['unit']; ?></option>
                    <?php
                  }
                }
                ?>

                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-4">Quantity</label>
                            <div class="col-md-5">
                              <input type="text" id="issue_quantity" name="issue_quantity" class="form-control">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-4">&nbsp;</label>
                            <div class="col-md-5">
                              <input type="submit" value="Submit" id="Add_base_inv" name="Add_base_inv" class="btn btn-success">
                            </div>
                          </div>-->
                <?php
                function AddMore($itemType) {
                  echo '<tr>
                          <td colspan="6" class="text-center"><button type="button" class="add_more btn-link btn" id="'.$itemType.'"><span class="glyphicon glyphicon-plus-sign"></span> Add Item</button></td>
                        </tr>';
                }
                ?>
                <table class="table table-bordered table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Item Name</th>
                      <th class="text-right">25 Thaal Qty</th>
                      <th class="text-right">100 Thaal Qty</th>
                      <th class="text-right">500 Thaal Qty</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="display_issue">
                    <tr style="display: none;">
                      <td class="inventorySerial"></td>
                          <td>
                            <select class="form-control" name="base_inv[]">
                              <option value="">--Select Item--</option>
                              <?php
                              if ($items) {
                                foreach ($items as $data) {
                                  ?>
                                  <option value="<?php echo $data['id']; ?>"><?php echo $data['name']; ?></option>
                                <?php }
                              }
                              ?>
                            </select>
                          </td>
                          <td><input type="text" name="baseInv50Qty[]" class="form-control" value=""></td>
                          <td><input type="text" name="baseInv100Qty[]" class="form-control" value=""></td>
                          <td><input type="text" name="baseInv500Qty[]" class="form-control" value=""></td>
                          <td><span class="glyphicon glyphicon-trash alert-danger" title="Delete"></span></td>
                        </tr>

                    <?php
                    if ($base_inv) {
                      $i = 1;
                      foreach ($base_inv as $item) {
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td>
                            <select class="form-control" name="base_inv[]">
                              <?php
                              if ($items) {
                                foreach ($items as $data) {
                                  $sel = $item['item_id'] == $data['id'] ? 'selected' : '';
                                  ?>
                                  <option value="<?php echo $data['id']; ?>" <?php echo $sel; ?>><?php echo $data['name']; ?></option>
                                <?php }
                              }
                              ?>
                            </select>
                          </td>
                          <td><input type="text" name="baseInv50Qty[]" class="form-control" value="<?php echo number_format($item['50_qty'], 3); ?>"></td>
                          <td><input type="text" name="baseInv100Qty[]" class="form-control" value="<?php echo number_format($item['100_qty'], 3); ?>"></td>
                          <td><input type="text" name="baseInv500Qty[]" class="form-control" value="<?php echo number_format($item['500_qty'], 3); ?>"></td>
                          <td><span class="glyphicon glyphicon-trash alert-danger deleteInv" title="Delete" id="inventory-<?php echo $item['id']; ?>"></span></td>
                        </tr>
                        <?php
                      } AddMore('inventory');
                    } else {
                      AddMore('inventory');
                      ?>
                      <tr>
                        <td class="alert-danger" colspan="6">Sorry! No items found.</td>
                      </tr>
                    <?php } ?>
                    <?php
      //              echo $base_inv;
                    ?>
                  </tbody>
                </table>
              </div>
            <?php } ?>
            <input type="submit" name="updateForm" class="btn btn-success btn-block" value="Update">
            <div class="col-md-12">&nbsp;</div>
          </form>
        </div>
      </div>
      <!-- /Center Bar -->
      <script>
        var DirectSerial = '<?php echo $no; ?>';
        var InventorySerial = '<?php echo $i; ?>';

        $('.deleteInv, .deleteDirect').click(function() {
          $(this).closest('tr').fadeOut('slow', function() { 
            $(this).remove();
          });
        });

        $('.add_more').click(function(){
          var btnId = $(this).attr('id');
          if(btnId === 'direct') {
            //addNewTr('display_direct', DirectSerial, 'directSerial');
            var data = $('#display_direct').children().eq(0);
            var element = data.clone().show();
            $(this).closest('tr').before(element);
            $('.directSerial').last().text(DirectSerial++);
          } else {
            //addNewTr('display_issue', InventorySerial, 'directSerial');
            var data = $('#display_issue').children().eq(0);
            var element = data.clone().show();
            $(this).closest('tr').before(element);
            $('.inventorySerial').last().text(InventorySerial++);
          }
        });

        function addNewTr(selector, serial, type) {
            var data = $('#'+selector).children().eq(0);
            var element = data.clone().show();
            $(this).closest('tr').before(element);
            $('.'+type).last().text(serial++);
        }

        $('#add_menu').click(function() {
          var menu = $('#menu_name').val();
          if (menu == '') {
            alert('Please enter menu name');
            return false;
          }
        });
        $('.del_issue_item').click(function(e) {
          e.preventDefault();

          var answer = confirm('Are you sure you want to delete this?');
          if (answer) {
            myApp.showPleaseWait();
            var itemId = this.id.split('-');
            var choice = '';
            if (itemId[0] == 'direct')
              choice = 'base_direct';
            else
              choice = 'base_inventory';
            jQuery.ajax({
              type: "POST", // HTTP method POST or GET
              url: "ajax.php", //Where to make Ajax calls
              data: "cmd=del_base_issue_item&id=" + itemId[1] + "&menu_id=<?php echo $menu_id; ?>&choice=" + choice + "&person_count=<?php echo $person_count; ?>", //Form variables
              success: function(data, status) {
                //on success, show new data.
                var display = '';
                if (choice == 'base_direct')
                  display = 'display_direct';
                else
                  display = 'display_issue';
                $('#' + display).html(data);
                myApp.hidePleaseWait();
              },
              error: function(xhr, ajaxOptions, thrownError) {
                //On error, we alert user
                alert(thrownError);
              }
            });
          } else {
            alert('Operation Canceled!');
            myApp.hidePleaseWait();
            return false;
          }
        });

        var myApp;
        myApp = myApp || (function() {
          var pleaseWaitDiv = $('<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Processing . . .</h4></div><div class="modal-body"><img src="asset/img/loader.gif"></div></div></div></div>');
          return {
            showPleaseWait: function() {
              pleaseWaitDiv.modal();
            },
            hidePleaseWait: function() {
              pleaseWaitDiv.modal('hide');
            },
          };
        })();
      </script>
      
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>
