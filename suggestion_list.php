<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/functions.php");

$page = 1;
if (isset($_GET['page']) && $_GET['page'] != '') {
  $page = $_GET['page'];
}

if(isset($_POST['submit'])){
  $data = $database->clean_data($_POST);
  $id = $data['msg_id'];
  $reply = $data['reply'];
  $email = $data['email'];
  $result = reply_suggestion($id, $reply, $email);
  
  if($result) $_SESSION[SUCCESS_MESSAGE] = 'Your Reply Sent Successfully.';
  else $_SESSION[ERROR_MESSAGE] = 'Error In Submit Data.';
}

if(isset($_POST['act']))
{
    $data = $database->clean_data($_POST);
    $act = $data['act'];
    if($act  == 'updt_read')
    {
      $sug_id = $data['sug_id'];
      $result = read_suggestion($sug_id);
      header('location : suggestion_list.php');
    }
} 

$suggestions = get_all_suggestions($page, 20);
$total_suggestions = get_total_suggestions();
    
$title = 'List of Suggestions';
$active_page = 'report';

include('includes/header.php');

$page_number = ACCOUNTS_REPORTS;
include('page_rights.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <table class="table table-hover table-condensed table-bordered">
            <thead>
              <tr>
                <th>No.</th>
                <th>ITS</th>
                <th>Name</th>
                <th>Message</th>
                <th>Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if($suggestions){
                $i = 1;
                foreach($suggestions as $sg){
                  $family_data = get_family_data_by_its($sg['its']);
              ?>
              <tr class="<?php if($sg['read'] == '0') { echo 'alert-info'; } ?>">
                <td><?php echo $i++;?></td>
                <td><?php echo $sg['its'];?></td>
                <td><?php echo $family_data['first_name'].' '.$family_data['surname'];?></td>

                <td><?php echo substr($sg['message'], 0, 90);?>&nbsp;&nbsp;<a href="#" style="color: #ad6704" id="<?php echo $sg['id']; ?>" class="rate-btn" data-toggle="modal" data-target="#myModal<?php echo $i; ?>">View More..</a></td>

                <td><?php echo $sg['timestamp'];?></td>

                <td class="text-center"><a href="#" data-toggle="modal" data-target="#myModal<?php echo $i; ?>">Reply</a></td>

                <div class="modal fade" id="myModal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <form method="post" action="">  
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel"><?php echo $family_data['first_name'].' '.$family_data['surname'];?>'s Suggestions</h4>
                        </div>
                        <div class="modal-body">
                          <?php echo $sg['message']; ?>
                          <hr>
                          <h5>Reply : </h5><textarea name="reply" class="form-control" rows="4"></textarea>
                          <input type="hidden" name="msg_id" value="<?php echo $sg['id']; ?>">
                          <?php
                            if($family_data['email'] != '')
                            { $family_email = $family_data['email']; }else { $family_email = '1'; }
                          ?>
                          <input type="hidden" name="email" value="<?php echo $family_email; ?>">
                        </div>
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
                          <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                        </div>
                      </div>
                    </div>
                </form>
              </div>
              </tr>
              <?php
                  }
              } else {
                echo '<tr><td colspan="6" class="alert-danger">Sorry! no activity to do today.</td></tr>';
              }
              ?>
            </tbody>
          </table>
          <?php
          require_once("pagination.php");
          echo pagination(20, $page, 'suggestion_list.php?page=', $total_suggestions);

          ?>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
  
<script>
  // rating script
  $(function(){ 
      $('.rate-btn').click(function(){    
          var therate = $(this).attr('id');
          var dataRate = 'act=updt_read&sug_id='+therate; //
          $.ajax({
              type : "POST",
              url : "suggestion_list.php",
              data: dataRate,
              success:function(){}
          });

      });
  });
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>