<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.menu.php');
$cls_menu = new Mtx_Menu();

$fromDate = $from_date = $to_date = $toDate = FALSE;
$btn_print = TRUE;
$post = FALSE;
if (isset($_GET['search'])) {
  $post = TRUE;
  $from_date = $_GET['from_date'];
  $to_date = $_GET['to_date'];
  $fdate = explode('-', $from_date);
  $fromDate = mktime(0, 0, 0, $fdate[1], $fdate[2], $fdate[0]);
  $tdate = explode('-', $to_date);
  $toDate = mktime(23, 59, 59, $tdate[1], $tdate[2], $tdate[0]);
  $menus = $cls_menu->get_menu_between_dates($fromDate, $toDate);
}
$btn_print_link = "print_costing_report.php?from_date=$from_date&to_date=$to_date";
$title = 'Costing Report';
$active_page = 'report';

require_once 'includes/header.php';

$page_number = ACCOUNTS_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Thali</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <?php include 'includes/inc.dates.php';?>
          <div class="col-md-12">&nbsp;</div>
          <?php if (isset($_GET['search'])) { ?>
            <div class="col-md-12">
              <table class="table table-hover table-condensed table-bordered">
                <thead>
                  <tr>
                    <th>Sr No.</th>
                    <th>Hijri Date</th>
                    <th>Menu</th>
                    <th class="text-right">Estimated Cost</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if ($menus) {
                    $i = 1;
                    $ary_avg_cost = array();
                    $total_est_cost = 0;
                    foreach ($menus as $menu) {
                      $menu_name = $cls_menu->get_base_menu_name($menu['menu_id']);
                      $est_cost = $menu['est_costing'];
                      if($est_cost > 0) {
                        array_push($ary_avg_cost, $est_cost);
                        $total_est_cost += $est_cost;
                      }
                      $hijari = HijriCalendar::GregorianToHijri($menu['timestamp']);
                      $hday = $hijari[1];
                      $month = HijriCalendar::monthName($hijari[0]);
                      $hyear = $hijari[2];
                      ?>
                      <tr>
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $hday . ' ' . $month . ', ' . $hyear . ' H';?></td>
                        <td><?php echo $menu_name; ?></td>
                        <td class="text-right"><?php echo number_format($est_cost, 2); ?></td>
                      </tr>
                    <?php }
                    ?>
                    <tr>
                      <td colspan="4" class="alert-info"><strong>Total Estimated Cost: <?php echo number_format($total_est_cost, 2);?></strong></td>
                    </tr>
                    <tr>
                      <td colspan="4" class="alert-warning"><strong>Average Per Day Cost: <?php echo number_format(($total_est_cost / count($ary_avg_cost)), 2);?></strong></td>
                    </tr>

                    <?php } else {
                    ?>
                    <tr>
                      <td colspan="4" class="alert-danger">No results found.</td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
              <div class="col-md-12">
                <p class="form-control-static"></p>
              </div>
            </div>
          <?php } ?>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>
  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>