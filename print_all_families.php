<?php
include('session.php');
require_once('classes/class.database.php');
$gblTotal = 0;

require_once('classes/class.family.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();
$mohallah = $_GET['mh'];
$search = $cls_family->get_all_familyfor_report($mohallah, FALSE);
$h = HijriCalendar::GregorianToHijri();
$date = $h[1] . ' ' . HijriCalendar::monthName($h[0]) . ', ' . $h[2];
$print = TRUE;
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print all families</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="asset/dist/css/print.css" media="print">
    <style type="text/css">
      @media all {
        body { font-size: 16px; }
        table { page-break-inside:auto; }
        tr    { page-break-inside:avoid; page-break-after:auto; }
        thead { display:table-header-group; }
        tfoot { display:table-footer-group; }
      }
    </style>
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8 ">
        <?php
        include 'includes/inc.list_families.php';
        ?>
      </div>

      <!-- /Center Bar -->

      <!-- Right Bar -->

      <!-- /Right Bar -->

    </div>
    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
