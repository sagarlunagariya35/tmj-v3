<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();

$tanzeem = $_GET['tan'];
$takhmeen_year_report = $_GET['year'];
$records = $cls_family->get_takhmeen_report_record($takhmeen_year_report, $tanzeem);
$mh = $cls_family->get_all_Mohallah();
$h = HijriCalendar::GregorianToHijri();
$date = $h[1] . ' ' . HijriCalendar::monthName($h[0]) . ', ' . $h[2];
$title = 'Takhmeen Report';
$active_page = 'report';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <style type="text/css">
      @media all {
        body { font-size: 16px; }
        table { page-break-inside:auto; }
        tr    { page-break-inside:avoid; page-break-after:auto; }
        thead { display:table-header-group; }
        tfoot { display:table-footer-group; }
      }
    </style>
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8">
        <?php
        $print = TRUE;
        include 'includes/inc.takhmeen_report.php';
        ?>
      </div>
      <!-- /Center Bar -->

    </div>
    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
