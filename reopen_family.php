<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.receipt.php');
require_once('classes/hijri_cal.php');
$cls_receipt = new Mtx_Receipt();
$cls_family = new Mtx_family();
$hijari = new HijriCalendar();

$user_id = $_SESSION[USER_ID];
$file = $_SESSION[FILENO];

if (isset($_POST['continue'])) {
  unset($_SESSION[PREFIX]);
  unset($_SESSION[FIRST_NAME]);
  unset($_SESSION[FATHER_PREFIX]);
  unset($_SESSION[FATHER_NAME]);
  unset($_SESSION[SURNAME]);
  $month = $_POST['month'];
  $year = $_POST['year'];
  $ts = $hijari->HijriToGregorian($month, '1', $year);
  $ts_date = date('Y-m-d',strtotime($ts)); 
  
  $result = $cls_family->reopen_closed_account($file, $ts_date, $user_id);
  if ($result && ($result != 'active')) {
    $_SESSION[SUCCESS_MESSAGE] = 'Account has been re-opened successfully.';
    header('Location: list_family.php');
    exit;
  } else if ($result == 'active') {
    $_SESSION[ERROR_MESSAGE] = 'Requested account is already active.';
    header('Location: list_family.php');
    exit;
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while processing Account Re-activation';
    header('Location: list_family.php');
    exit;
  }
}

$receipt = $cls_receipt->get_pending_amount_for_hub($file);
$total_pending_amount = 0;
$pending_amount = 0;
if ($receipt) {
  $hijri_date = HijriCalendar::GregorianToHijri($receipt['paid_till']);
  $cur_ts = HijriCalendar::GregorianToHijri();
  $diff_months = $cur_ts[0] - $hijri_date[0];
  $diff_years = $cur_ts[2] - $hijri_date[2];
  $pending_months = $diff_months + ($diff_years * 12);
  $pending_amount = $pending_months * $receipt['hub_raqam'];
  $total_pending_amount += $pending_amount;
}
$title = "Re-Open family confirmation";
$active_page = "family";

require_once 'includes/header.php';

$page_number = PROFILE_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profiles</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <div class="col-md-3 pull-left">
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Search</h3></div>
            <div class="panel-body">
      <?php include('includes/search_bar.php'); ?>
            </div>
          </div>
        </div>
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-8 ">
          <form method="post" role="form" class="form-horizontal">
            <p>Are you sure you want to re-open account?<br>
              FMB ID: <?php echo $_SESSION[FILENO]; ?><br>
              HOF   : <?php echo $_SESSION[PREFIX].' '.$_SESSION[FIRST_NAME].' '.$_SESSION[FATHER_PREFIX].' '.$_SESSION[FATHER_NAME].' '.$_SESSION[SURNAME]; ?>  
            </p>
            <p>Pending Dues : <?php if ($pending_amount > 0) echo number_format($pending_amount) . '/-';
      else echo 'CLEAR'; ?></p>
            <div class="form-group">
              <label class="control-label col-md-2">From</label>
              <div class="col-md-4">
                  <select class="form-control" name="month" id="month">
                    <option value ="">-- Select One --</option>
                    <?php
                    for ($i = 1; $i <= 12; $i++) {
                      ?>
                      <option value ="<?php echo $i ?>">
                        <?php
                        echo HijriCalendar::monthName($i);
                        ?></option>

                      <?php
                    }
                    ?>
                  </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-2"></label>
              <div class="col-md-4">
                <input type="text" name="year" class="form-control" id="year" placeholder="Year">
              </div>
            </div>
            <?php if($_SESSION[USER_TYPE] == 'A'){ ?>
            <input type="submit" name="continue" value="Re-Open Account" class="btn btn-success">
            <?php } ?>
            <a class="btn btn-info" href="javascript:history.go(-1);">No</a>
          </form>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
<?php
  include 'includes/footer.php';
?>