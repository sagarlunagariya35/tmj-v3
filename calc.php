<?php
include('session.php');

require_once("classes/class.database.php");
require_once("classes/class.menu.php");
require_once("classes/class.family.php");
$cls_menu = new Mtx_Menu();
$cls_family = new Mtx_family();
$tiffin_count = $cls_family->get_tiffin_group();

if (isset($_POST['calculate'])) {
  $ary_roti = $_POST['roti'];
  $sum = $cls_menu->calculate_roti_qty($ary_roti, $tiffin_count);
}

if (isset($_POST['print'])) {
  $ary_roti = $_POST['roti'];
  $_SESSION['roti'] = $ary_roti;
  header('Location: print_calc.php?cmd=print');
}

$title = 'Roti calculator';
$active_page = 'menu';

include 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Maedat</a></li>
        <li><a href="#">Menu</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
          <div class="pull-right">
            <button type="submit" name="print" class="btn btn-primary btn-xs" id="">Print</button>
            <!--a href="print_calc.php" target="_blank" class="btn btn-primary btn-xs"></a-->
          </div>
            <table class="table table-hover table-condensed">
              <thead>
                <tr>
                  <th>Tiffin size</th>
                  <th>Count</th>
                  <th>Roti</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $total_count = 0;
                if ($tiffin_count) {
                  $i = 0;
                  $ary_size_count = array();
                  foreach ($tiffin_count as $tfn => $cnt) {
                    ?>
                    <tr>
                      <td><?php echo $tfn; ?></td>
                      <td id="count<?php echo $i; ?>"><?php echo $cnt;
                      $total_count += $cnt;
                array_push($ary_size_count, $cnt); ?></td>

                      <td><input type="text" name="roti[]" id="roti<?php echo $i; ?>" class="form-control" value="<?php
                        if (isset($_POST['calculate'])) {
                          echo $ary_roti[$i];
                        }$i++;
                        ?>"></td>
                    </tr>
                    <?php
                  }
                }
                ?><input type="hidden" name="array_count" value="<?php print_r($ary_size_count); ?>">
              <tr>
                <td><button type="submit" name="calculate" class="btn btn-success" id="calculate">Calculate</button></td>
                <td><span class="pull-left"><?php echo $total_count;?></span><span class="pull-right"><strong>Total Roti:</strong></span></td>
                <td id="Result_Panel"><?php if (isset($sum)) echo number_format($sum, 0, '.', ','); ?></td>
              </tr>
              <tr>
                <?php
                $mohallah = $cls_family->get_all_Mohallah();
                ?>
              </tr>
              </tbody>
            </table>
          </form>
        </div>
        <!-- /Center Bar -->
      </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
<?php
  include 'includes/footer.php';
?>