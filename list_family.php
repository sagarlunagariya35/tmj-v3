<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/hijri_cal.php');
require_once('classes/class.user.php');
$hijari = new HijriCalendar();
$cls_family = new Mtx_family();
$cls_user = new Mtx_User();
$mh = FALSE;
$title = 'List of family';
$active_page = 'family';

$page = 1;
if (isset($_GET['page']) && $_GET['page'] != '') {
  $page = $_GET['page'];
}

if (isset($_POST['Search'])) {
  $search = $cls_family->get_mohallah_family($page, 20, $_POST['Mohallah']);
  $total_family = $cls_family->get_total_family($_POST['Mohallah']);
  $tiffin = $cls_family->get_tiffin_group($_POST['Mohallah']);
  $roti = $cls_family->get_diabetics_roti($_POST['Mohallah']);
  $pg_mh = $_POST['Mohallah'];
} else if (isset($_GET['mh']) && $_GET['mh'] != 'All') {
  $result = $cls_family->get_mohallah_family($page, 20, $_GET['mh']);
  $total_family = $cls_family->get_total_family($_GET['mh']);
  $tiffin = $cls_family->get_tiffin_group($_GET['mh']);
  $roti = $cls_family->get_diabetics_roti($_GET['mh']);
  $pg_mh = $_GET['mh'];
} else {
  $result = $cls_family->get_all_family($page, 20);
  $roti = $cls_family->get_diabetics_roti();
  $total_family = $cls_family->get_total_family();
}
$mohallah = $cls_family->get_all_Mohallah();
$setting = $cls_user->get_general_settings();
$default_year = $setting[0]['start_year'];
$no_years = $setting[0]['no_of_years'];

include 'includes/header.php';
?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profiles</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-md-12">&nbsp;</div>

          <!-- Left Bar -->
          <div class="col-md-3 pull-left">
            <!--div class="panel panel-default">
              <div class="panel-heading"><h3 class="panel-title">Send SMS</h3></div>
              <div class="panel-body">
                <select name="sms_index" class="form-control">
                  <option>Hub Pending</option>
                </select>
              </div>
            </div-->
            <div class="panel panel-default">
              <div class="panel-heading"><h3 class="panel-title">Tanzeem</h3></div>
              <div class="panel-body">
                <!-- Tanzeem -->
                <?php
                $grand_total = 0;
                foreach ($mohallah as $mh) {
                  ?>
                  <div class="form-group">
                    <label class="control-label col-md-6"><strong><?php echo ucfirst($mh['Mohallah']); ?></strong></label>
                    <label class="control-label col-md-5"><strong><?php
                        $total_fly = $cls_family->get_total_family($mh['Mohallah']);
                        echo $total_fly;
                        $grand_total += $total_fly;
                        ?></strong></label>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-6">Tiffin Size</label>
                    <label class="control-label col-md-6">
                      <?php
                      $size = $cls_family->get_tiffin_group($mh['Mohallah']);
                      foreach ($size as $tfn => $cnt) {
                        echo " $tfn: $cnt<br> ";
                      }
                      ?>
                    </label>
                  </div>
                <?php } ?>
                <div class="form-group">
                  <label class="control-label col-md-6">Grand Total </label>
                  <label class="control-label col-md-5"><?php echo $grand_total; ?></label>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-6"><strong>Total Tiffin </strong></label>
                  <label class="control-label col-md-5"><?php
                    $size = $cls_family->get_tiffin_group();
                    foreach ($size as $tfn => $cnt) {
                      echo " $tfn: $cnt <br>";
                    }
                    ?></label>

                  <p>Diabetics Roti: <?php echo $roti['Roti']; ?></p>
                  <hr>
                  <a href="calc.php">Roti calculator</a>
                </div>
                <!-- Tanzeem End -->
              </div>
            </div>
          </div>
          <!-- /Left Bar -->

          <!-- Center Bar -->
          <div class="col-md-8">
            <form method="post" class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-md-2 control-label">Mohallah</label>
                <div class="input-group col-md-6">
                  <select id="Mohallah" name="Mohallah" class="form-control">
                    <option value="">All</option>
                    <?php
                    $tanzeem = FALSE;
                    if ($mohallah) {
                      foreach ($mohallah as $mohalla) {
                        ?>
                        <option value="<?php echo $mohalla['Mohallah'] ?>" <?php
                        if (isset($_POST['Search']) or isset($_GET['mh'])) {
                          if (isset($_POST['Mohallah']))
                            $tanzeem = $_POST['Mohallah'];
                          else if (isset($_GET['mh']))
                            $tanzeem = $_GET['mh'];
                          if ($mohalla['Mohallah'] == $tanzeem) {
                            echo 'selected';
                          }
                        }
                        ?>><?php echo ucwords($mohalla['Mohallah']); ?></option>
                                <?php
                              }
                            }
                            ?>
                  </select>
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-success" name="Search">Search</button>
                  </span>
                </div>
              </div>

            </form>
            <?php
            if (isset($_POST['Search'])) {
              if ($tiffin) {
                ?>
                <div class="col-md-12">
                  <?php
                  foreach ($tiffin as $tiffin_size => $Number) {
                    echo "<strong>" . $tiffin_size . ": " . " </strong>" . $Number . " ";
                  }
                  ?>
                </div>
                <?php
              }
            }
            ?>
            <div class="col-md-12">&nbsp;</div>

            <!--List of Files Start-->
            <table id="my-table-sorter" class="table table-condensed resizable" data-provides="rowlink">
              <thead>
              <th>FMB ID</th>
              <th>Head Of family</th>
              <th>Ejamat ID</th>
              <th>Mobile</th>
              <th>Paid till</th>
              </thead>
              <tbody>
                <?php
                if (isset($_POST['Search'])) {
                  if (is_array($search) && !empty($search)) {
                    foreach ($search as $family) {
                      $fid = $family['FileNo'];
                      $hub = $cls_family->get_last_year_month($fid);
                      if (date('Y') > date('Y', $hub['paid_till'])) {
                        $script = '';//alert-error';
                      } else if (date('Y') == date('Y', $hub['paid_till'])) {
                        if (date('m') > date('m', $hub['paid_till'])) {
                          $script = '';//'alert-error';
                        } else
                          $script = '';
                      } else
                        $script = '';
                      ?>
                      <tr class="rowlink <?php echo $script ?>" >
                        <td class="nolink"><a href="update_family.php?cmd=show&family_id=<?php echo $family['FileNo']; ?>"><?php echo $family['FileNo']; ?></a></td>
                        <td><?php
                if ($family['first_name'] != '') {
                  echo $family['prefix'] . ' ' . $family['first_name'] . ' ' . $family['father_prefix'] . ' ' . $family['father_name'] . ' ' . $family['surname'];
                } else {
                  echo $family['HOF'];
                }
                      ?></td>
                        <td><?php echo $family['ejamatID']; ?></td>
                        <td><?php echo $family['MPh']; ?></td>
                        <td>
              <?php
              if ($hub) {
                switch (USE_CALENDAR) {
                  case 'Hijri':
                    $hijari_date = $hijari->GregorianToHijri($hub['paid_till']);
                    $mon = $hijari->monthName($hijari_date[0]);
                    $yr = $hijari_date[2];
                    break;
                  case 'Greg':
                    $mon = date('F', mktime(0, 0, 0, date('m', $hub['paid_till'])));
                    $yr = date('Y', $hub['paid_till']);
                    break;
                }
                $h = (USE_CALENDAR == 'Hijri') ? 'H' : '';
                echo $mon . ', ' . $yr . " $h";
              } else
                echo "";
              ?>
                        </td>
                      </tr>
                          <?php
                        }
                      }
                    }
                    else {
                      $i = 1;
                      foreach ($result as $family) {
                        $fid = $family['FileNo'];
                        $hub = $cls_family->get_last_year_month($fid);
                        if (time() > ($hub['paid_till'] + (29 * 24 * 60 * 60)))
                          $script = '';//'alert-error';
                        else
                          $script = '';
                        ?>
                    <tr class="rowlink <?php echo $script ?>" >
                      <td class="nolink"><a href="update_family.php?cmd=show&family_id=<?php echo $family['FileNo']; ?>"><?php echo $family['FileNo']; ?></a></td>
                      <td><?php echo $family['HOF']; ?></td>
                      <td><?php echo $family['ejamatID']; ?></td>
                      <td><?php echo $family['MPh']; ?></td>

                      <td>
            <?php
            if ($hub) {
              switch (USE_CALENDAR) {
                case 'Hijri':
                  $hijari_date = $hijari->GregorianToHijri($hub['paid_till']);
                  $mon = $hijari->monthName($hijari_date[0]);
                  $yr = $hijari_date[2];
                  break;
                case 'Greg':
                  $mon = date('F', mktime(0, 0, 0, date('m', $hub['paid_till'])));
                  $yr = date('Y', $hub['paid_till']);
                  break;
              }
              $h = (USE_CALENDAR == 'Hijri') ? 'H' : '';
              echo $mon . ', ' . $yr . " $h";
            } else
              echo "";
            ?>
                      </td>
                    </tr>
                        <?php
                      }
                    }
                    ?>
              </tbody>
            </table>
            <div class="col-md-12">
                <?php
                require_once("pagination.php");
                if (!isset($pg_mh)) {
                  echo pagination(20, $page, 'list_family.php?mh=All&page=', $total_family);
                } else {
                  echo pagination(20, $page, 'list_family.php?mh=' . $pg_mh . '&page=', $total_family);
                }
                ?>
            </div>


          </div>
          <!-- /Center Bar -->

        </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
<?php
  include 'includes/footer.php';
?>