<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();
$user_id = $_SESSION[USER_ID];

if(isset($_GET['cmd']) && ($_GET['cmd'] == 'delete')){
  $delete = $cls_family->delete_hub_exception($_GET['eid']);
  if ($delete) {
    header('Location: fmb_hub_exception.php?action=success');
    exit;
  } else {
    header('Location: fmb_hub_exception.php?action=Error');
    exit;
  }
}

if(isset($_POST['add'])) {
  $fts = HijriCalendar::HijriToUnix($_POST['from_month'], '01', $_POST['from_year']);
  $tts = HijriCalendar::HijriToUnix($_POST['to_month']+1, '01', $_POST['to_year']);
  $tts = $tts-1;
  $result = $cls_family->insert_fmb_hub_exception($_POST['FileNo'], $_POST['from_month'].'-'.$_POST['from_year'], $_POST['to_month'].'-'.$_POST['to_year'], $user_id, $fts, $tts);
}

$exceptions = $cls_family->get_all_fmb_hub_exception();
$title = 'FMB hub exception';
$active_page = 'settings';


require_once 'includes/header.php';

$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-6">
          <form method="post" role="form" class="form-horizontal">
            <div></div>

            <div class="form-group" id="File">
              <label class="col-md-4 control-label"><?php echo THALI_ID; ?></label>
              <div class="col-md-8">
                <input type="text" id="FileNo" name="FileNo" placeholder="Enter Thali ID" class="form-control">
              </div>
              <div class="col-md-1" id="loader">

              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label">Name</label>
              <div class="col-md-8">
                <input type="text" name="name" id="name" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4">From</label>
              <div class="col-md-5">
                  <select class="form-control" name="from_month" id="from_month">
                    <option value ="0">-- Select One --</option>
                    <?php
                    for ($i = 1; $i <= 12; $i++) {
                      ?>
                      <option value ="<?php echo $i ?>">
                        <?php
                        echo HijriCalendar::monthName($i);
                        ?></option>

                      <?php
                    }
                    ?>
                  </select>
              </div>
              <div class="col-md-3">
                <input type="text" name="from_year" class="form-control" id="from_year" placeholder="Year">
              </div>
                    <!--input type="date" name="from_date" class="form-control" id="dob" placeholder="Date of Birth"-->

            </div>
            <div class="form-group">
              <label class="control-label col-md-4">To</label>
              <div class="col-md-5">
                  <select class="form-control" name="to_month" id="to_month">
                    <option value ="0">-- Select One --</option>
                    <?php
                    for ($i = 1; $i <= 12; $i++) {
                      ?>
                      <option value ="<?php echo $i ?>">
                        <?php
                        echo HijriCalendar::monthName($i);
                        ?></option>

                      <?php
                    }
                    ?>
                  </select>
                  <!--input type="date" name="to_date" class="form-control" id="dob" placeholder="Date of Birth"-->
              </div>
              <div class="col-md-3">
                <input type="text" name="to_year" class="form-control" id="to_year" placeholder="Year">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">&nbsp;</label>
              <div class="col-md-8">
                <input type="submit" id="update" name="add" value="Add" class="btn btn-success">
                <input type="hidden" id="exist">
              </div>
            </div>
          </form>
          </div>
          <?php if($exceptions){?>
          <div class="col-md-6">
            <table class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>FileNo</th>
                  <th>From</th>
                  <th>To</th>
                  <?php if($_SESSION[USER_TYPE] == 'A'){?>
                  <th>Action</th>
                  <?php } ?>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; foreach($exceptions as $exception){
                   switch (USE_CALENDAR) {
                     case 'Hijri':
                      $from = explode('-', $exception['from_date']);
                      $fyear = $from[1];
                      $fname = HijriCalendar::monthName($from[0]);
                      $to = explode('-', $exception['to_date']);
                      $tname = HijriCalendar::monthName($to[0]);
                      $tyear = $to[1];
                       break;
                     case 'Greg':
                       $fname = date('F', mktime(0, 0, 0, date('m', $exception['from_ts'])));
                       $fyear = date('Y', $exception['from_ts']);
                       $tname = date('F', mktime(0, 0, 0, date('m', $exception['to_ts'])));
                       $tyear = date('Y', $exception['to_ts']);
                       break;
                   }
                  ?>
                <tr>
                  <td><?php echo $i++;?></td>
                  <td><?php echo $exception['FileNo'];?></td>
                  <td><?php echo $fname.','.$fyear;?></td>
                  <td><?php echo $tname.','.$tyear;?></td>
                  <?php if($_SESSION[USER_TYPE] == 'A'){?>
                  <td><a href="fmb_hub_exception.php?cmd=delete&eid=<?php echo $exception['id'];?>" class="btn btn-danger btn-xs" name="delete">Delete</a></td>
                  <?php } ?>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <?php } ?>
        </div>
        <!-- /Center Bar -->
      <script>
        $('#update').click(function() {
          var file = $('#FileNo').val();
          var from_month = $('#from_month').val();
          var from_year = $('#from_year').val();
          var to_month = $('#to_month').val();
          var to_year= $('#to_year').val();
          var error = 'Following error(s) are occurred\n\n';
          var validate = true;
          if (file == '')
          {
            error += 'Please enter Thali ID\n';
            validate = false;
          }
          if (from_month == '0')
          {
            error += 'Please select from month\n';
            validate = false;
          }
          if (from_year == '')
          {
            error += 'Please enter a year\n';
            validate = false;
          }
          if (to_month == '0')
          {
            error += 'Please select to month\n';
            validate = false;
          }
          if (to_year == '')
          {
            error += 'Please enter to year\n';
            validate = false;
          }
          if (validate == false)
          {
            alert(error);
            return validate;
          }
        });
        $('#FileNo').keyup(function(){
          var FileNo = $(this).val().toUpperCase();
          $(this).val(FileNo);
        });
        //when tab pressed from File no. and if file no is wrong then it will not let you to move down
          $("#FileNo").keydown(function(e) {
            if (e.keyCode === 9) {
              if(exist == true){
                $('#FileNo').focus();
                return false;
              }
            }
          });

        $('#FileNo').change(function() {
          var fId = $('#FileNo').val();
          $('#loader').html('<img src="asset/img/loader.gif" height="20" width="20">');
          $.ajax({
            url: "ajax.php",
            type: "POST",
            data: 'fId=' + fId + '&cmd=tiffin_size_data',
            success: function(data)
            {
              $('#loader').text('');
              if(data != '0'){
                $('#File').attr('class', 'form-group has-success');
                exist = false;
                var obj = JSON.parse(data);
                $('#name').val(obj.NAME);
                $('#hub').val(obj.HUB);
                $('#tiffin').append(obj.OPTION);
                $('#past_record').html(obj.PAST_RECORD);
                $('#hub').focus();
              } else {
                alert('Invalid thali ID');
                exist = true;
                $('#File').attr('class', 'form-group has-error');
                $('#FileNo').focus();
                return false;
              }
            }
          });
        });
        $(document).ready(function(){
          $('#FileNo').focus(function(){
            $(this).select();
          });
        });
      </script>
    </div>
    <!-- /Content -->
    </section>
  </div>
  
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <?php
  include('includes/footer.php');
  ?>