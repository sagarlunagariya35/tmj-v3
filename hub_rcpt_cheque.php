<?php
include 'session.php';
$pg_link = 'hub_rcpt_book';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
$cls_receipt = new Mtx_Receipt();

if (isset($_GET['cmd']) && $_GET['cmd'] == 'cheque_cleared') {
    $cheque_clear = $cls_receipt->make_cheque_clear($_GET['id'], $_GET['status']);
    if ($cheque_clear) {
        $_SESSION[SUCCESS_MESSAGE] = 'Cheque cleared successfully.';
    } else {
        $_SESSION[ERROR_MESSAGE] = 'Error encountered while processing...';
    }
    location();
}
function location() {
    header('Location: hub_rcpt_cheque.php');
    exit();
}
$title = 'Hub Receipt Book for cheque only';
$active_page = 'account';

if (isset($_GET['search']) OR $_GET) {
    $from_date = $_GET['from_date'];
    $to_date = $_GET['to_date'];
    $result = $cls_receipt->get_all_hub_cheque_receipt($page, 20, $from_date, $to_date);
    $total_hub_receipt = $cls_receipt->get_total_hub_receipt_cheque($from_date, $to_date);
    $amount = $cls_receipt->get_total_between_dates($from_date, $to_date, 'cheque');
} else {
    $result = $cls_receipt->get_all_hub_cheque_receipt($page, 20);
    $total_hub_receipt = $cls_receipt->get_total_hub_receipt_cheque();
    $amount = $cls_receipt->get_total_between_dates(FALSE, FALSE, 'cheque');
}

require_once 'includes/header.php';

$page_number = ACCOUNTS_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Receipt Books</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
          <div class="col-md-12">&nbsp;</div>

          <!-- Left Bar -->
          <div class="col-md-3 pull-left">
              <?php include 'links.php'; ?>
              <div class="panel panel-default">
                  <div class="panel-heading"><h3 class="panel-title">Search</h3></div>
                  <div class="panel-body">
                      <?php include('includes/search_bar.php'); ?>
                  </div>
              </div>
          </div>
          <!-- /Left Bar -->

          <!-- Center Bar -->
          <div class="col-md-8 ">
              <?php include 'includes/inc.dates.php';?>
              <?php if (isset($_POST['search']) OR ( isset($_GET['from']) && isset($_GET['to']))) { ?>
                  <div class="col-md-12">&nbsp;</div>
                  <div class="alert-success">
                      <strong>Total Amount: </strong><?php echo number_format($amount[0]['Amount'], 2); ?>
                  </div>
              <?php } ?>
              <div class="col-md-12">&nbsp;</div>
              <table class="table table-hover table-condensed table-bordered">
                  <thead>
                  <th>Receipt No</th>
                  <th>Clear</th>
                  <th>File No</th>
                  <th>Name</th>
                  <th class="text-right">Amount</th>
                  <th>Cheque Date</th>
                  </thead>
                  <tbody>
                      <?php
                      if ($result) {
                          foreach ($result as $receipt) {
                              $red = ($receipt['cancel'] == 1) ? ' class=alert-danger' : '';
                              ?>
                              <tr<?php echo $red; ?>>
                                  <td><a href="print_hub_receipt.php?id=<?php echo $receipt['receipt_no'] ?>" target="_blank" ><?php echo $receipt['id'] ?></a></td>
                                  <td><input type="checkbox" name="clear" id="clear<?php echo $receipt['id'] ?>" value="" onclick="Chequecleared(<?php echo $receipt['id'] ?>);"<?php $checked = ($receipt['cheque_clear'] == 1) ? 'checked' : ''; ?> <?php echo $checked; ?>></td>
                                  <td><?php echo $receipt['FileNo'] ?></td>
                                  <td><?php echo ucfirst($receipt['name']); ?></td>
                                  <td class="text-right"><?php echo number_format($receipt['amount'], 2, '.', ','); ?></td>
                                  <td><?php echo date('d F, Y', $receipt['cheque_date']); ?></td>
                              </tr>
                          <?php
                          }
                      } else {
                          ?>
                          <tr>
                              <td colspan="7" class="alert-danger">Sorry! No receipt added yet.</td>
                          </tr>
              <?php } ?>
                  </tbody>
              </table>
              <?php
              require_once("pagination.php");
              if (!isset($from) && !isset($to)) {
                  echo pagination(20, $page, 'hub_rcpt_cheque.php?page=', $total_hub_receipt);
              } else {
                  echo pagination(20, $page, 'hub_rcpt_cheque.php?from=' . $from . '&to=' . $to . '&page=', $total_hub_receipt);
              }
              ?>
          </div>
          <!-- /Center Bar -->
          <script>
              function Chequecleared(val) {
                  var status = document.getElementById('clear' + val).checked;
                  if (status == true)
                      status = 1;
                  else
                      status = 0;
                  window.location.href = 'hub_rcpt_cheque.php?cmd=cheque_cleared&id=' + val + '&status=' + status;
              }
          </script>

      </div>
      <!-- /Content -->
    </section>
  </div>
  
<?php
include('includes/footer.php');
?>