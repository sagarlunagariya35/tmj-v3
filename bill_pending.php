<?php
include 'session.php';
$pg_link = 'grocery_bill_book';
require_once('classes/class.database.php');
require_once('classes/class.billbook.php');
$cls_billbook = new Mtx_BillBook();

$bills = $cls_billbook->get_all_account_bill(FALSE, 'UNPAID');

$directs = $cls_billbook->get_direct_bills(FALSE, 'UNPAID');

function no_results_found($label) {
  $div = '<div class="panel-body">No results found.</div>';
  panel($label, $div);
}

function panel($tbl_name, $table) {
  $panel = <<<PANEL
          <div class="panel panel-info">
            <div class="panel-heading">{$tbl_name}</div>
            $table
          </div>
PANEL;
            echo $panel;
}
function create_tbody($array, $id_link, $label) {
  $start = <<<START
          <table class="table table-hover table-condensed table-bordered">
            <thead>
              <tr>
                <th>Bill No</th>
                <th>Shop Name</th>
                <th class='text-right'>Amount</th>
                <th>Date</th>
                <th>Description</th>
              </tr>
            </thead>
          <tbody>
START;
  $tr = '';
  foreach ($array as $bill) {
    $id = isset($bill['BillNo']) ? $bill['BillNo'] : $bill['id'];
    $bill_id = $id_link ? "<a href='$id_link{$id}' target=blank>$id</a>" : $id;
    $shop_name = ucwords(strtolower($bill['ShopName']));
    $mode = ucfirst($bill['payment_type']);
    //$mode = ($mode == '') ? 'Cash' : $mode;
    $amount = number_format($bill['amount'], 2);
    $date = date('d F, Y', $bill['timestamp']);
    $tr_cls = ($bill['cancel'] == 1) ? ' class=alert-danger' : '';
    $color = (isset($bill['paid']) && $bill['paid'] == 0) ? 'alert-warning' : '';
    $description = ucwords(strtolower($bill['description']));
    $tr .= <<<TABLE
          <tr class="{$color}">
            <td>{$bill_id}</td>
            <td>{$shop_name}</td>
            <td class='text-right'>{$amount}</td>
            <td>{$date}</td>
            <td>{$description}</td>
          </tr>
TABLE;
  }
  $end = "</tbody></table>";
  $table = $start.$tr.$end;
  panel($label, $table);
}
$title = 'List of Bills';
$active_page = 'account';

include('includes/header.php');

$page_number = ACCOUNTS_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Bill Books</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">

              <?php
              $label = 'Bill Payment';
              $bills ? create_tbody($bills, 'upd_bill_details.php?cmd=show_bills&bid=', $label) : no_results_found($label);
              $label = 'Direct Purchase';
              $directs ? create_tbody($directs, 'upd_bill_details.php?cmd=show_direct_bills&bid=', $label) : no_results_found($label);
              unset($label);
              ?>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>