<?php

require_once 'classes/class.database.php';
require_once 'classes/constants.php';
require 'PHPMailer-master/PHPMailerAutoload.php';

$query = "INSERT INTO cron_thaali_count (`date`, `mohallah`, `tiffin_size`, `count`) SELECT NOW(), Mohallah, tiffin_size, count(*) FROM family where close_date = 0 GROUP BY Mohallah, tiffin_size ON DUPLICATE KEY UPDATE `count` = `count`";
$database->query($query);

$query = "INSERT IGNORE INTO cron_thaali_count (`date`, `tiffin_size`, `count`) SELECT NOW(), tiffin_size, count(*) FROM family where close_date = 0 GROUP BY tiffin_size ON DUPLICATE KEY UPDATE `count` = `count`";
$database->query($query);

$email_to = BACKUP_EMAIL;
$email_to = 'janah.mustafa@gmail.com';
if ($email_to != '') {
  $db_data = backup_tables();
  $zip_archive = create_zip_archive($db_data);
  send_attachment_as_mail($zip_archive);
  unlink($zip_archive);
}

function backup_tables() {

  $link = mysql_connect(DB_API_SERVER, DB_API_USER, DB_API_PASS) or die(mysql_error());
  mysql_select_db(DB_API_NAME, $link) or die('Unable to open');
  mysql_set_charset('utf8', $link);
  //get all of the tables
  $tables = array();
  $result = mysql_query('SHOW TABLES');
  while ($row = mysql_fetch_row($result)) {
    $tables[] = $row[0];
  }

  //cycle through
  $return = '';
  foreach ($tables as $table) {
    $result = mysql_query('SELECT * FROM ' . $table);
    $num_fields = mysql_num_fields($result);

    $return.= 'DROP TABLE IF EXISTS ' . $table . ';';
    $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE ' . $table));
    $return.= "\n\n" . $row2[1] . ";\n\n";

    for ($i = 0; $i < $num_fields; $i++) {
      while ($row = mysql_fetch_row($result)) {
        $return.= 'INSERT INTO ' . $table . ' VALUES(';
        for ($j = 0; $j < $num_fields; $j++) {
          $row[$j] = addslashes($row[$j]);
          $row[$j] = preg_replace("\n", "\\n", $row[$j]);
          if (isset($row[$j])) {
            $return.= '"' . $row[$j] . '"';
          } else {
            $return.= '""';
          }
          if ($j < ($num_fields - 1)) {
            $return.= ',';
          }
        }
        $return.= ");\n";
      }
    }
    $return.="\n\n\n";
  }

  return $return;
}

function create_zip_archive($data) {
  //save to file
  $file_name = DB_API_NAME . '_' . date('YmdHis') . '.sql';
  $handle = fopen($file_name, 'w+');
  fwrite($handle, $data);
  fclose($handle);

  $zip = new ZipArchive();
  $filename = "$file_name.zip";

  if ($zip->open($filename, ZipArchive::CREATE) !== TRUE) {
    exit("cannot open <$filename>\n");
  }

  $zip->addFile($file_name, $file_name);
  $zip->close();

  unlink($file_name);

  return $filename;
}

function send_attachment_as_mail($archive) {
  global $email_to;
  
  $my_subject = 'TMJamat DB Back-up Service for ' . DB_API_NAME . ' ' . date('Y-m-d H:i:s');
  $my_message = 'Database back-up ' . date('d-M-Y H:i:s');

  //Create a new PHPMailer instance
  $mail = new PHPMailer;
  $mail->setFrom("backup@tmjamat.com", "TMJamat");
  $mail->addReplyTo("backup@tmjamat.com", "TMJamat");
  $mail->addAddress($email_to);
  $mail->Subject = $my_subject;
  $mail->msgHTML($my_message);
  $mail->AltBody = $my_message;
  $mail->addAttachment('/home/tmjamat/' . $archive);

  if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
  } else {
    echo "Message sent!";
  }
}

echo 'this is it';
?>
