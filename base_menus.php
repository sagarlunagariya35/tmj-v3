<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.menu.php');
$cls_menu = new Mtx_Menu();

if (isset($_GET['id']))
  $menu_id = $_GET['id'];
else
  $menu_id = FALSE;
$person_count = FALSE;
if(isset($_POST['add_menu'])) {
  $menu_name = ucfirst(strtolower($_POST['menu']));
  $category = $_POST['category'];
  $count = (int) $_POST['count'];
  $check = $cls_menu->check_base_menu_exist($menu_name);
  if($check == 0) {
    $result = $cls_menu->add_base_menu($menu_name, $category, $count);
    if($result) {
      $_SESSION[SUCCESS_MESSAGE] = "Menu added successfully";
    } else {
      $_SESSION[ERROR_MESSAGE] = "Errors encountered while processing the request..";
    }
  }
  else $_SESSION[ERROR_MESSAGE] = 'Duplicate entry for menu name '.$menu_name;
}

if (isset($_POST['update'])) {
  $menu_name = ucfirst(strtolower($_POST['menu']));
  $cat_id = $_POST['category'];
  $update = $cls_menu->update_base_menu($menu_name, $cat_id, $menu_id);
  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Menu updated successfully';
    header('Location: base_menus.php');
    exit;
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while processing updation of Base Menu';
  }
}
if (isset($menu_id)) {
  $name = $cls_menu->get_base_menu_name($menu_id);
  $ary_cat_person = $cls_menu->get_base_menu_cat_id($menu_id);
  $cat_id = $ary_cat_person['category'];
  $est_cost = $ary_cat_person['est_costing'];
  $person_count = $ary_cat_person['person_count'];
}
$categories = $cls_menu->get_all_category();
$base_menus = $cls_menu->get_all_base_menu();
$title = 'List of Base Menu';
$active_page = 'menu';
include('includes/header.php');

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Profiles</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <div class="col-md-3 pull-left">
          <?php
          if ($base_menus) {
            ?>
            <table class="table table-hover table-condensed table-bordered">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Menu Name</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i = 1;
                foreach ($base_menus as $menu) {
                  ?>
                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><a href="base_menu.php?mid=<?php echo $menu['menu_id'] ?>"><?php echo $menu['name']; ?></a></td>
                  </tr>
            <?php } ?>
              </tbody>
            </table>
      <?php } ?>
        </div>
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-8 ">
          <div class="col-md-6">
            <form method="post" role="form" class="form-horizontal">
              <div class="form-group">
                <label class="control-label col-md-4">Menu Name</label>
                <div class="col-md-8">
                  <input type="text" id="menu" name="menu" placeholder="" class="form-control" value="<?php if (isset($menu_id)) echo $name; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-4">Category</label>
                <div class="col-md-8">
                    <select name="category" id="category" class="form-control">
                      <option value="0">--Select One--</option>
                      <?php
                      foreach ($categories as $category) {
                        //$sel_cat = ($cate)
                        ?>
                        <option value="<?php echo $category['id']; ?>" <?php echo ($cat_id == $category['id']) ? 'selected' : ''; ?>><?php echo $category['name']; ?></option>
                    <?php } ?>
                    </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-4">Person Count</label>
                <div class="col-md-8">
                  <?php if (!$person_count) { ?>
                    <input type="text" name="count" class="form-control" id="count">
                  <?php } else { ?>
                    <p class="form-control-static"><?php echo $person_count; ?></p>
      <?php } ?>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-4">Est. Cost</label>
                <div class="col-md-8">
                    <p class="form-control-static"><?php echo $est_cost; ?></p>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-4">&nbsp;</label>
                <div class="col-md-4">
                  <?php
                  if ($menu_id)
                    $btn = '<input type="submit" value="Update" class="btn btn-success" name="update" id="update">';
                  else
                    $btn = '<input type="submit" value="Add Menu" class="btn btn-success" name="add_menu" id="add_menu">';
                  echo $btn;
                  ?>

                </div>
              </div>

            </form>
          </div>
      <?php
      if ($base_menus) {
        ?>
            <div class="col-md-6">
              <table class="table table-hover table-condensed table-bordered">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Menu Name</th>
                    <th>Category</th>
                    <th>Cost</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
        <?php
        $i = 1;
        foreach ($base_menus as $menu) {
          ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $menu['name']; ?></td>
                      <td><?php echo $menu['category']; ?></td>
                      <td><?php echo $menu['est_costing']; ?></td>
                      <td><a href="base_menus.php?id=<?php echo $menu['menu_id'] ?>" class="btn btn-success btn-xs">Update</a></td>
                    </tr>
        <?php } ?>
                </tbody>
              </table>
            </div>
      <?php } ?>
        </div>
        <!-- /Center Bar -->
        <script>
          $('#add_menu').click(function() {
            var menu = $('#menu').val();
            if (menu == '')
            {
              alert('Please enter a menu name');
              return false;
            }
          })
        </script>
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>