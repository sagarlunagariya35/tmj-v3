<?php
include 'session.php';
$page_number = 34;
$pg_link = 'sabil_home_rcpt_book';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
$cls_receipt = new Mtx_Receipt();

$title = 'Sabil Home Receipt Book';
$active_page = 'account';

include('includes/header.php');
include('page_rights.php');
if(isset($_POST['search'])){
  // year-m-d
  $result_search = $cls_receipt->get_all_receipt_for_sabil_home($page, 20, $_POST['from_date'], $_POST['to_date']);
  $total_receipt = $cls_receipt->get_total_sabil_home_receipt($_POST['from_date'], $_POST['to_date']);
}
$result = $cls_receipt->get_all_receipt_for_sabil_home($page, 20);
$total_hub_receipt = $cls_receipt->get_total_sabil_home_receipt();
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Accounts</a></li>
        <li><a href="#">Receipt Books</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Left Bar -->
        <div class="col-md-3 pull-left">
          <?php include 'links.php';?>
          <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Search</h3></div>
            <div class="panel-body">
              <?php include('includes/search_bar.php');?>
            </div>
          </div>
        </div>
        <!-- /Left Bar -->

        <!-- Center Bar -->
        <div class="col-md-8 ">
          <form method="post" role="form" class="form-horizontal">
              <div></div>
              <div class="col-md-12">
                <label class="col-md-2 control-label">From Date</label>
                  <div class="col-md-3">
                    <input type="date" name="from_date" class="form-control" id="from_date" placeholder="Date of Birth" value="<?php if(isset($_POST['search'])) echo $_POST['from_date']?>">
                  </div>
                <label class="col-md-2 control-label">To Date</label>
                  <div class="col-md-3">
                    <input type="date" name="to_date" class="form-control" id="to_date" placeholder="Date of Birth" value="<?php if(isset($_POST['search'])) echo $_POST['to_date']?>">
                  </div>
                <input type="submit" class="btn btn-success" name="search" id="search" value="Search">
              </div>
           </form>
          <script>
          $('#search').click(function(){
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            var error = '';
            var validate = true;
            if(from_date == '')
            {
              error += 'Please select From date\n';
              validate = false;
            }
            if(to_date == '')
            {
              error += 'Please select To date\n';
              validate = false;
            }
              if(validate == false){
                alert(error);
                return validate;
              }
          });
          </script>
            <div class="col-md-12">&nbsp;</div>
          <table class="table table-hover table-condensed table-bordered">
            <thead>
            <th>Receipt No</th>
            <th>File No</th>
            <th>Name</th>
            <th>Type of Payment</th>
            <th>Amount</th>
            <th>Date</th>
            </thead>
            <tbody>
              <?php
              if(!isset($_POST['search'])){
              if ($result) {
                foreach ($result as $receipt) { ?>
                  <tr>
                    <td><a href="print_sabil_home.php?id=<?php echo $receipt['receipt_no'] ?>" ><?php echo $receipt['receipt_no'] ?></a></td>
                    <td><?php echo $receipt['FileNo'] ?></td>
                    <td><?php echo ucfirst($receipt['name']); ?></td>
                    <td><?php echo ucfirst($receipt['payment_type']); ?></td>
                    <td><?php echo number_format($receipt['amount']) . '/-'; ?></td>
                    <td><?php echo date('d F, Y', $receipt['date']); ?></td>
                  </tr>
        <?php }
      } else { ?>
                <tr>
                  <td colspan="6">Sorry! No receipt added yet.</td>
                </tr>
              <?php }} else {
              if ($result_search) {
                foreach ($result_search as $receipt) { ?>
                  <tr>
                    <td><a href="print_sabil_home.php?id=<?php echo $receipt['receipt_no'] ?>" ><?php echo $receipt['receipt_no'] ?></a></td>
                    <td><?php echo $receipt['FileNo'] ?></td>
                    <td><?php echo ucfirst($receipt['name']); ?></td>
                    <td><?php echo ucfirst($receipt['payment_type']); ?></td>
                    <td><?php echo number_format($receipt['amount']) . '/-'; ?></td>
                    <td><?php echo date('d F, Y', $receipt['date']); ?></td>
                  </tr>
              <?php } } else { ?>
                  <tr>
                    <td colspan="6">Sorry! No receipt found.</td>
                  </tr>
              <?php }} ?>
            </tbody>
          </table>
      <?php
      require_once("pagination.php");
      if(!isset($_POST['search'])){
        echo pagination(20, $page, 'sabil_home_rcpt_book.php?page=', $total_hub_receipt);
      } else {
        echo pagination(20, $page, 'sabil_home_rcpt_book.php?page=', $total_receipt);
      }
      ?>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>
  
<?php
include('includes/footer.php');
?>