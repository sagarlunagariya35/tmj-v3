<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.menu.php');
$cls_menu = new Mtx_Menu();
if(isset($_POST['Tiffin_add'])){
  $result = $cls_menu->add_tiffin_size($_POST['tiffin_size']);
  if($result){
    if($result['errors']){
      $_SESSION[ERROR_MESSAGE] = $result['errors'][0];
    } else {
      $_SESSION[SUCCESS_MESSAGE] = 'Added successfully';
    }
  }
}

$tiffin = $cls_menu->get_all_tiffin_size();
$title = 'Add Tiffin size';
$active_page = 'settings';

include('includes/header.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div class="col-md-6">
            <div class="form-group">
              <label class="control-label col-md-4">Tiffin Size</label>
              <div class="col-md-8">
                <input type="text" name="tiffin_size" id="tiffin_size" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">&nbsp;</label>
              <div class="col-md-5">
                <input type="submit" id="Tiffin_add" name="Tiffin_add" value="Add" class="btn btn-success">
              </div>
            </div>
            </div>
            <div class="col-md-6">
              <?php if($tiffin){?>
              <table class="table table-bordered table-condensed table-hover">
                <thead>
                  <tr>
                    <th colspan="2" style="text-align: center">List of all Tiffin Size</th>
                  </tr>
                  <tr>
                    <th>No.</th>
                    <th>Size</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($tiffin as $size){?>
                  <tr>
                    <td><?php echo $size['id'];?></td>
                    <td><?php echo $size['size'];?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
              <?php } ?>
            </div>
            <!--p><strong>Note:</strong> Please add hub details on the <a href="tiffin_cost.php">Thali hub</a> page.</p-->
          </form>

        </div>
        <!-- /Center Bar -->
        <script>
          $('#tiffin_size').keyup(function(){
            var upper_case = $('#tiffin_size').val().toUpperCase();
            $(this).val(upper_case);
          });
          $('#Tiffin_add').click(function() {
            var tiffin = $('#tiffin_size').val();
            if (tiffin == '')
            {
              alert("Please enter tiffin size..");
              return false;
            }
          });
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>