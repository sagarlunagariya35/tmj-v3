<?php
include('session.php');
$page_number = 24;
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/class.barcode.php');

$cls_family = new Mtx_family();
$ary_barcode_family = array();
$ary_remaining_family = array();
$cls_barcode = new Mtx_Barcode();

if (isset($_POST['print'])) {
  $date = $_POST['entry_time'];
  header('Location: print_tiffin_not_issue_report.php?entry_time=' . $date);
}
$post = FALSE;
if (isset($_POST['submit'])) {
  $post = TRUE;
  $date = $_POST['entry_time'];
  $ary_remaining_family = $cls_family->get_tiffin_data($_POST['entry_time'], 'not_issued');
}

$title = 'Thaali not issued report';
$active_page = 'report';
include('includes/header.php');
include('page_rights.php');

?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Thali</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
              <div></div>
              <div class="col-md-12">
                <label class="col-md-4 control-label">Select Date</label>
                  <div class="col-md-4">
                      <input type="date" name="entry_time" class="form-control" id="entry_time" placeholder="Date of Birth" value="<?php if(isset($_POST['submit'])) echo $_POST['entry_time']?>">
                  </div>

                <input type="submit" class="btn btn-success" name="submit" id="submit" value="submit">
                <a href="print_tiffin_not_issue_report.php?entry_time=<?php if(isset($_POST['submit'])) echo $_POST['entry_time'];?>" target="_blank" class="btn btn-primary <?php echo !$post ? 'disabled' : ''; ?>" id="print">Print</a>
              </div>
           </form>
          <script>
          $('#submit').click(function(){
            var entry = $('#entry_time').val();
            if(entry == '')
              {
                alert("Please select date to view report");
                return false;
              }
          });
          $('#print').click(function(){
            var entry = $('#entry_time').val();
            if(entry == '')
              {
                alert("Please select date to view report");
                return false;
              }
          });
          </script>
          <?php if(isset($_POST['submit'])){?>
              <div class="col-md-12" id="report">
                    <?php
                    if(isset($_POST['submit'])) echo $ary_remaining_family;
                    ?>
              </div>
          <?php } ?>
        </div>
        <!-- /Center Bar -->

        </div>
        <!-- /Content -->
    </section>
  </div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>