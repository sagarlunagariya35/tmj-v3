<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.receipt.php");
$cls_receipt = new Mtx_Receipt();

if(isset($_POST['add_head'])){
  $result = $cls_receipt->add_account_head(ucwords(strtolower($_POST['head'])), $_POST['master_id']);
  if($result){
    $_SESSION[SUCCESS_MESSAGE] = 'Account head added successfully';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while processing Account Head';
  }
}
$master = $cls_receipt->get_master_account_heads();
$heads = $cls_receipt->get_account_heads();
$title = 'Account Heads';
$active_page = 'settings';

include('includes/header.php');

$page_number = ACCOUNTS_ENTRY;
include('page_rights.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-4">Master Heads:</label>
                <div class="col-md-8">
                  <select class="form-control" name="master_id" id="master">
                    <option value="0">--Select One--</option>
                    <?php if($master){foreach($master as $head){?>
                    <option value="<?php echo $head['heads']?>"><?php echo $head['heads']?></option>
                    <?php }} else { ?>
                    <option>No master heads.</option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-4">Account Head:</label>
                <div class="col-md-8">
                  <input type="text" name="head" id="head" class="form-control">
                </div>
              </div>
              <div class="form-group col-md-offset-4">
                <label class="control-label col-md-4">&nbsp;</label>
                <div class="col-md-5">
                  <button class="btn btn-success" name="add_head" id="add_head">Add Head</button>
                </div>
              </div>
            </div>
            <?php if($heads){$i = 1;?>
            <div class="col-md-5">
              <table class="table table-bordered table-condensed table-hover">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Account Heads</th>
                    <th>Master Heads</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($heads as $head){?>
                  <tr>
                    <td><?php echo $i++;?></td>
                    <td><?php echo $head['head'];?></td>
                    <td><?php echo $head['master_head_name'];?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <?php } ?>
          </form>
        </div>
        <!-- /Center Bar -->
      </div>
      <script>
        $('#add_head').click(function(){
          var master = $('#master').val();
          var validate = true;
          var errors = '';
          var sub = $('#head').val();
          if(master == '0'){
            errors += 'Please select Master Heads\n';
            validate = false;
          }
          if(sub == ''){
            errors += 'Please enter sub head name';
            validate = false;
          }
          if(validate == false){
            alert(errors);
            return validate;
          }
        });
      </script>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>