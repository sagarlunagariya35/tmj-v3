<?php

require_once('hijri_cal.php');
require_once('class.database.php');
require_once('class.quadratic_derivatives.php');

$calc = new Mtx_Quad_derivative(array(1, 2, 3), array(1, 2, 3), 0.1);

class Mtx_Menu {

  private $id;
  private $productname;
  private $shop_ownername;
  private $quantity;
  private $receivedate;
  private $amount;
  private $bankname;
  private $type;

  public function Mtx_Menu($id = '') {
    if ($id) {
      if ($this->set_id($id))
        return TRUE;
      else
        return FALSE;
    }
  }

  public function __destruct() {
    // TODO: destructor code
  }

  public function add_menu($menu) {
    global $database;
    $query = "INSERT INTO menu(`menu_name`) VALUES('$menu')";
    $result = $database->query($query);
    return $result;
  }

  public function update_menu($menu, $menu_id) {
    global $database;
    $query = "UPDATE menu SET `menu_name` = '$menu' WHERE `menu_id` = '$menu_id'";
    $result = $database->query($query);
    return $result;
  }

  public function select_previous_entry($menu_id) {
    global $database;
    $query = "SELECT from_unixtime(`timestamp`, '%Y') as year, from_unixtime(`timestamp`, '%m') as month,from_unixtime(`timestamp`, '%d') as day FROM `daily_menu` where menu = '$menu_id' order by timestamp desc";

    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_direct_items($menu_id) {
    global $database;
    $result = $this->select_previous_entry($menu_id);
    $start_time = mktime(0, 0, 0, $result[0]['month'], $result[0]['day'], $result[0]['year']);
    $end_time = mktime(23, 59, 59, $result[0]['month'], $result[0]['day'], $result[0]['year']);
    $query = "SELECT * FROM `est_direct_pur` WHERE `date` BETWEEN '$start_time' AND '$end_time'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_direct_purchase_items($menu_id) {
    global $database;
    $result = $this->select_previous_entry($menu_id);
    $start_time = mktime(0, 0, 0, $result[0]['month'], $result[0]['day'], $result[0]['year']);
    $end_time = mktime(23, 59, 59, $result[0]['month'], $result[0]['day'], $result[0]['year']);
    $query = "SELECT (SELECT name FROM `dp_lookup` dp WHERE dp.name = estd.item) item_id, qty AS quantity FROM `est_direct_pur` estd WHERE `date` BETWEEN '$start_time' AND '$end_time'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  function get_direct_purchase_items_from_id($item_id) {
    global $database;

    $query = "SELECT * FROM `dp_lookup` WHERE `id` = '$item_id'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  function get_last_five_cooked_items_from_menu_id($menu_id) {
    global $database;

    $query = "SELECT * FROM `daily_menu` where menu = '$menu_id' LIMIT 0, 5";
    $result = $database->query_fetch_full_result($query);
    $ary_menus = array();
    if ($result) {

      foreach ($result as $menu) {
        $ary_item = array();
        $start = mktime(0, 0, 0, date('m', $menu['timestamp']), date('d', $menu['timestamp']), date('Y', $menu['timestamp']));
        $end = mktime(23, 59, 59, date('m', $menu['timestamp']), date('d', $menu['timestamp']), date('Y', $menu['timestamp']));
        $query = "SELECT (SELECT name FROM `inventory` i WHERE i.id LIKE inis.inventory_id) item_name, inventory_id as item_id, quantity as qty, (SELECT unit FROM `inventory` i WHERE i.id LIKE inis.inventory_id) unit, issue_date  FROM `inventory_issue` inis WHERE `issue_date` BETWEEN '$start' AND '$end'";

        $result = $database->query_fetch_full_result($query);
        foreach ($result as $item) {
          $ary_item[$item['item_id']]['name'] = $item['item_name'];
          $ary_item[$item['item_id']]['Quantity'] = $item['qty'];
          $ary_item[$item['item_id']]['id'] = $item['item_id'];
          //$ary_menus[$menu['timestamp']] = $ary_item;
//          $ary_menus[$menu['timestamp']]['item_name'] = $item['item_name'];
//          $ary_menus[$menu['timestamp']]['Quantity'] = $item['qty'];
//          $ary_menus[$menu['timestamp']]['item_id'] = $item['item_id'];
//          $ary_menus[$item['item_id']]['item_name'] = $item['item_name'];
//          $ary_menus[$item['item_id']]['timestamp'] = $menu['timestamp'];
//          $ary_menus[$item['item_id']]['Quantity'] = $item['qty'];
        }
        $ary_menus[$menu['timestamp']] = $ary_item;
      }
    }
    return $ary_menus;
  }

  function get_cook_est_inv_issue_items($menu_id, $person_count, $start_time, $cmd = FALSE) {

    $display_issue = array();
    $base_inv_issue = $this->get_base_inv_item($menu_id);
    foreach ($base_inv_issue as $key => $value) {
      //$display_issue['base_inv_issue'][$key] = $value;
      $display_issue[$key]['base_inv_issue'] = $value['quantity'];
      $display_issue[$key]['unit'] = $value['unit'];
    }
    $issued_inv = $this->get_est_inv_issue($start_time);

    foreach ($issued_inv as $key => $value) {
      $display_issue[$key]['inv_issue'] = $value;
    }

    foreach ($display_issue as $key => $value) {
      $display_issue[$key]['item_name'] = $this->get_item_name($key);
      if (!isset($display_issue[$key]['base_inv_issue']))
        $display_issue[$key]['base_inv_issue'] = 0;
      if (!isset($display_issue[$key]['inv_issue']))
        $display_issue[$key]['inv_issue'] = 0;
    }
    return $display_issue;
  }
  
  function get_cook_est_inv_issue_items_der($menu_id, $person_count, $start_time, $cmd = FALSE) {

    $display_issue = array();
    $base_inv_issue = $this->get_base_inv_item_der($menu_id);
    foreach ($base_inv_issue as $key => $value) {
      //$display_issue['base_inv_issue'][$key] = $value;
      $display_issue[$key]['base_inv_issue'] = $value['item'];
      $display_issue[$key]['step'] = $value['step'];
      $display_issue[$key]['var_a'] = $value['var_a'];
      $display_issue[$key]['var_b'] = $value['var_b'];
      $display_issue[$key]['var_c'] = $value['var_c'];
    }
    $issued_inv = $this->get_est_inv_issue_der($start_time);

    foreach ($issued_inv as $key => $value) {
      $display_issue[$key]['inv_issue'] = $value;
    }

    foreach ($display_issue as $key => $value) {
      $display_issue[$key]['item_name'] = $this->get_item_name($key);
      if (!isset($display_issue[$key]['base_inv_issue']))
        $display_issue[$key]['base_inv_issue'] = 0;
      if (!isset($display_issue[$key]['inv_issue']))
        $display_issue[$key]['inv_issue'] = 0;
    }
    return $display_issue;
  }

  function get_cook_est_direct_issue_items($menu_id, $person_count, $start_time, $cmd = FALSE) {

    $display_direct = array();
    $base_direct_pur = $this->get_base_direct_pur($menu_id);
    foreach ($base_direct_pur as $key => $value) {
      $display_direct[$key]['base_inv_issue'] = $value['quantity'];
    }
    $issued_dir_pur = $this->get_est_dir_pur_issue($start_time);
    foreach ($issued_dir_pur as $key => $value) {
      $display_direct[$key]['direct_issue'] = $value;
    }

    foreach ($display_direct as $key => $value) {
      if (!isset($display_direct[$key]['base_inv_issue']))
        $display_direct[$key]['base_inv_issue'] = 0;
      if (!isset($display_direct[$key]['direct_issue']))
        $display_direct[$key]['direct_issue'] = 0;
    }

    return $display_direct;
  }

  function get_base_menu_from_cid($cid) {
    global $database;

    $query = "SELECT *  FROM `base_menu` WHERE `category` = '$cid'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  function get_base_inv_item($menu_id) {
    global $database;
    $query = "SELECT item AS item_id, (select name from inventory i where i.id = bi.item) item, (select unit from inventory i where i.id = bi.item) unit, qty AS quantity FROM `base_inventory` bi WHERE `base_menu_id` IN ($menu_id)";
    $result = $database->query_fetch_full_result($query);

    // Combine repeating items
    $items = array();
    if ($result) {
      foreach ($result as $item) {
        if (array_key_exists($item['item_id'], $items)) {
          $items[$item['item_id']]['quantity'] += $item['quantity'];
        } else {
          $items[$item['item_id']]['quantity'] = $item['quantity'];
          $items[$item['item_id']]['item'] = $item['item'];
          $items[$item['item_id']]['unit'] = $item['unit'];
        }
      }
    }
    return $items;
  }
  
  function get_base_inv_item_der($menu_id) {
    global $database;
    $query = "SELECT item AS item_id, (select name from inventory i where i.id = bi.item) item, (select step from inventory i where i.id = bi.item) step, var_a, var_b, var_c FROM `base_inventory_der` bi WHERE `base_menu_id` IN ($menu_id)";
    $result = $database->query_fetch_full_result($query);

    // Combine repeating items
    $items = array();
    if ($result) {
      foreach ($result as $item) {
        $items[$item['item_id']]['item'] = $item['item'];
        $items[$item['item_id']]['step'] = $item['step'];
        $items[$item['item_id']]['var_a'] = $item['var_a'];
        $items[$item['item_id']]['var_b'] = $item['var_b'];
        $items[$item['item_id']]['var_c'] = $item['var_c'];
      }
    }
    return $items;
  }

  function get_base_direct_pur($menu_id) {
    global $database;
    $query = "SELECT (SELECT name FROM `dp_lookup` dp WHERE dp.name = bd.item) item_id, item, qty AS quantity FROM `base_direct_pur` bd WHERE `base_menu_id` IN ($menu_id)";
    //echo $query;
    $result = $database->query_fetch_full_result($query);

    // Combine repeating items
    $items = array();
    if ($result) {
      foreach ($result as $item) {
        if (array_key_exists($item['item_id'], $items)) {
          $items[$item['item_id']]['quantity'] += $item['quantity'];
        } else {
          $items[$item['item_id']]['quantity'] = $item['quantity'];
          $items[$item['item_id']]['item'] = $item['item'];
        }
      }
    }
    return $items;
  }

  function get_inventory_issue_items($menu_id) {
    global $database;
    $prev = $this->select_previous_entry($menu_id);

    $start_time = mktime(0, 0, 0, $prev[0]['month'], $prev[0]['day'], $prev[0]['year']);
    $end_time = mktime(23, 59, 59, $prev[0]['month'], $prev[0]['day'], $prev[0]['year']);
    $query = "SELECT item AS item_id, qty AS quantity FROM `est_inventory` WHERE `date` BETWEEN '$start_time' AND '$end_time'";
    $result = $database->query_fetch_full_result($query);

    // if the last reslt is 0 then get the previos entry else return the latest
    if (!$result) {
      $start_time = mktime(0, 0, 0, $prev[1]['month'], $prev[1]['day'], $prev[1]['year']);
      $end_time = mktime(23, 59, 59, $prev[1]['month'], $prev[1]['day'], $prev[1]['year']);
      $query = "SELECT item AS item_id, qty AS quantity FROM `est_inventory` WHERE `date` BETWEEN '$start_time' AND '$end_time'";
      $result = $database->query_fetch_full_result($query);
    }
    return $result;
  }

  public function changeStatus($ts, $status) {
    global $database;
    $query = "UPDATE `daily_menu` SET `active` = '$status' WHERE `timestamp` = '$ts'";
    $result = $database->query($query);
    return $result;
  }

  public function add_direct_pur_items($itemName) {
    global $database;
    $query = "INSERT INTO dp_lookup(`name`) VALUES('$itemName')";
    $result = $database->query($query);
    return $result;
  }

  public function update_direct_pur_items($oldItemName, $itemName, $id) {
    global $database;
    $query = "UPDATE dp_lookup SET `name` = '$itemName' WHERE `id` = '$id'";
    $result = $database->query($query);
    $query = "UPDATE `est_direct_pur` SET `item` = '$itemName' WHERE `item` = '$oldItemName'";
    $result = $database->query($query);
    return $result;
  }

  public function check_previous_entry_exist($menu_id) {
    global $database;
    $query = "SELECT * FROM `daily_menu` WHERE $menu_id = '$menu_id'";
    $result = mysql_num_rows(mysql_query($query));
    return $result;
  }

  function delete_base_menu_item($item_id, $manu_id) {
    global $database;

    if (is_array($item_id)) {
      $operator = 'IN';
      $item_id = "('" . implode("','", $item_id) . "')";
    } else {
      $operator = '=';
      $item_id = "$item_id";
    }
    $query = "DELETE FROM `base_inventory` WHERE `item` $operator $item_id AND `base_menu_id` = '$manu_id'";
    $result = $database->query($query);
    return $result;
  }
  
  function delete_base_menu_item_der($item_id, $manu_id) {
    global $database;

    if (is_array($item_id)) {
      $operator = 'IN';
      $item_id = "('" . implode("','", $item_id) . "')";
    } else {
      $operator = '=';
      $item_id = "$item_id";
    }
    $query = "DELETE FROM `base_inventory_der` WHERE `item` $operator $item_id AND `base_menu_id` = '$manu_id'";
    $result = $database->query($query);
    return $result;
  }

  function delete_base_menu_direct_item($item_id, $manu_id) {
    global $database;

    if (is_array($item_id)) {
      $operator = 'IN';
      $item_id = "('" . implode("','", $item_id) . "')";
    } else {
      $operator = '=';
      $item_id = "$item_id";
    }
    $query = "DELETE FROM `base_direct_pur` WHERE `id` $operator $item_id AND `base_menu_id` = '$manu_id'";
    $result = $database->query($query);
    return $result;
  }

  function add_base_direct($item, $qty, $menu_id) {
    global $database;
    $query = "INSERT INTO `base_direct_pur`(`item`,`qty`,`base_menu_id`) VALUES('$item', '$qty', '$menu_id')";
    $result = $database->query($query);
    return $result;
  }

  function add_base_inventory($item, $qty, $menu_id) {
    global $database;
    $query = "INSERT INTO `base_inventory`(`item`,`qty`,`base_menu_id`) VALUES('$item', '$qty', '$menu_id')";
    $result = $database->query($query);
    return $result;
  }

  function get_all_base_direct_items($menu_id, $person_count) {
    global $database;
    if ($menu_id != 0) {
      $query = "SELECT item, qty, id FROM `base_direct_pur` bd WHERE `base_menu_id` = '$menu_id'";
      $result = $database->query_fetch_full_result($query);
      return $result;
      $tbody = '';
      if ($result) {
        $i = 1;
        foreach ($result as $item) {
          $tbody .= '<tr><td>' . $i++ . '</td><td>' . $item['item'] . '</td><td class="text-right">' . number_format($item['qty'] * $person_count, 3) . '</td><td><!--button id="direct-' . $item['id'] . '" class="btn btn-danger btn-xs del_issue_item">Delete</button--><input type="checkbox" name="delete_direct[' . $item['id'] . ']" value="' . $item['id'] . '"></td></tr>';
        }
        $tbody .= '<tr><td colspan="4"><input type="submit" name="DELETE_BASE_DIRECT" value="DELETE SELECTED" class="btn btn-danger"></td></tr>';
      } else {
        $tbody .= '<tr><td class="alert-danger" colspan="4">Sorry! No items found.</td></tr>';
      }
    } else {
      $tbody = '<tr><td class="alert-danger" colspan="4">Sorry! Menu id is not exist.</td></tr>';
    }
    return $tbody;
  }

  function get_all_base_inv_issue($menu_id, $person_count) {
    global $database;
    if ($menu_id != 0) {
      $query = "SELECT id, (SELECT `name` FROM `inventory` i WHERE i.id = bi.item) item, (SELECT `unit` FROM `inventory` i WHERE i.id = bi.item) unit, qty, item as item_id FROM `base_inventory` bi WHERE `base_menu_id` = '$menu_id'";
      //echo $query;
      $result = $database->query_fetch_full_result($query);
      return $result;
      $tbody = '';
      if ($result) {
        $i = 1;
        foreach ($result as $item) {
          //$txt_qty = '<input type="text" class="form-control text-right" value="' . number_format($item['qty'] * $person_count, 3) . '" name="inv_qty[' . $key . ']" id="inv_qty' . $i . '">';
          $tbody .= '<tr><td>' . $i++ . '</td><td>' . $item['item'] . '</td><td class="text-right">' . number_format($item['qty'] * $person_count, 3) . '  ' . $item['unit'] . '</td><td><!--button id="item-' . $item['item_id'] . '" class="btn btn-danger btn-xs del_issue_item" name="[]">Delete</button--><input type="checkbox" name="delete[' . $item['item_id'] . ']" value="' . $item['item_id'] . '"></td></tr>';
        }
        $tbody .= '<tr><td colspan="4"><input type="submit" name="DELETE_BASE_INV" value="DELETE SELECTED" class="btn btn-danger"></td></tr>';
      } else {
        $tbody .= '<tr><td class="alert-danger" colspan="4">Sorry! No items found.</td></tr>';
      }
    } else {
      $tbody = '<tr><td class="alert-danger" colspan="4">Sorry! Menu id is not exist.</td></tr>';
    }
    return $tbody;
  }
  
  function get_all_base_inv_issue_der($menu_id, $person_count) {
    global $database;
    if ($menu_id != 0) {
      $query = "SELECT id, (SELECT `name` FROM `inventory` i WHERE i.id = bi.item) item, (SELECT `unit` FROM `inventory` i WHERE i.id = bi.item) unit, 50_qty, 100_qty, 500_qty, item as item_id FROM `base_inventory_der` bi WHERE `base_menu_id` = '$menu_id'";
      //echo $query;
      $result = $database->query_fetch_full_result($query);
      return $result;
      $tbody = '';
      if ($result) {
        $i = 1;
        foreach ($result as $item) {
          //$txt_qty = '<input type="text" class="form-control text-right" value="' . number_format($item['qty'] * $person_count, 3) . '" name="inv_qty[' . $key . ']" id="inv_qty' . $i . '">';
          $tbody .= '<tr><td>' . $i++ . '</td><td>' . $item['item'] . '</td><td class="text-right">' . number_format($item['qty'] * $person_count, 3) . '  ' . $item['unit'] . '</td><td><!--button id="item-' . $item['item_id'] . '" class="btn btn-danger btn-xs del_issue_item" name="[]">Delete</button--><input type="checkbox" name="delete[' . $item['item_id'] . ']" value="' . $item['item_id'] . '"></td></tr>';
        }
        $tbody .= '<tr><td colspan="4"><input type="submit" name="DELETE_BASE_INV" value="DELETE SELECTED" class="btn btn-danger"></td></tr>';
      } else {
        $tbody .= '<tr><td class="alert-danger" colspan="4">Sorry! No items found.</td></tr>';
      }
    } else {
      $tbody = '<tr><td class="alert-danger" colspan="4">Sorry! Menu id is not exist.</td></tr>';
    }
    return $tbody;
  }

  function check_base_menu_exist($menu_name) {
    global $database;
    $query = "SELECT COUNT(*) as rows  FROM `base_menu` WHERE `name` LIKE '$menu_name'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['rows'];
  }

  function add_base_menu($menu_name, $category, $count) {
    global $database;
    $query = "INSERT INTO base_menu(`name`, `category`, `person_count`) VALUES('$menu_name', '$category', '$count')";
    $result = $database->query($query) or die(mysql_error());
    if ($result) {
      $result = TRUE;
      $insert_id = $database->get_last_insert_id();
    } else {
      $result = FALSE;
      $insert_id = '';
    }

    return array('success' => $result, 'insert_id' => $insert_id);
  }

  public function add_daily_menu($menu_ids, $date, $person_count = FALSE, $custom_menu = FALSE) {
    global $database;
    if (!$person_count)
      $person_count = 0;
    if (!$custom_menu)
      $custom_menu = '';
    // convert ids to string if array array
    if (is_array($menu_ids))
      $menu_ids = implode(',', $menu_ids);
    $date = explode('-', $date);
    $time = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
    $query = "SELECT * FROM daily_menu WHERE `timestamp`='$time'";
    $result = $database->query_fetch_full_result($query);
    if (!$result) {
      $query = "INSERT INTO daily_menu(`timestamp`, `menu`, `active`, `person_count`, `custom_menu`) VALUES('$time', '$menu_ids','1', '$person_count', '$custom_menu')";
    } else {
      $day = $date[2];
      $month = $date[1];
      $year = $date[0];
      $start_time = mktime(0, 0, 0, $month, $day, $year);
      $end_time = mktime(23, 59, 59, $month, $day, $year);
      $query = "DELETE FROM `est_inventory` WHERE `date` BETWEEN '$start_time' AND '$end_time'";
      $result = $database->query($query);
      $query = "DELETE FROM `est_direct_pur` WHERE `date` BETWEEN '$start_time' AND '$end_time'";
      $result = $database->query($query);

      $query = "INSERT INTO daily_menu(`timestamp`, `menu`, `active`, `person_count`, `custom_menu`) VALUES('$time', '$menu_ids', '1', '$person_count', '$custom_menu') ON DUPLICATE KEY UPDATE `menu` = '$menu_ids'";
    }
    $result = $database->query($query) or die(mysql_error());
    return $result;
  }

  public function delete_menu($id, $timestamp) {
    global $database;
    $query = "DELETE FROM `daily_menu` WHERE `menu` = '$id' AND `timestamp` = '$timestamp'";
    $result = $database->query($query);
    $start = mktime(0, 0, 0, date('m', $timestamp), date('d', $timestamp), date('Y', $timestamp));
    $end = mktime(23, 59, 59, date('m', $timestamp), date('d', $timestamp), date('Y', $timestamp));
    $query = "DELETE FROM `est_inventory` WHERE `date` BETWEEN '$start' AND '$end'";
    $result = $database->query($query);
    $query = "DELETE FROM `est_direct_pur` WHERE `date` BETWEEN '$start' AND '$end'";
    $result = $database->query($query);
    return $result;
  }

  public function   get_menu_between_dates($from_date, $to_date) {
    global $database;
    $query = "SELECT menu as menu_id, timestamp, custom_menu, est_costing, person_count FROM `daily_menu` dm WHERE `timestamp` BETWEEN '$from_date' AND '$to_date'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }
  
  public function get_act_cost_between_dates_from_finalize($from_date, $to_date) {
    global $database;
    $query = "SELECT * FROM `inventory_finalize` WHERE `timestamp` BETWEEN '$from_date' AND '$to_date'";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_daily_menu($month, $year, $key = FALSE, $start_day = FALSE, $last_day = FALSE, $hijri_dates = FALSE) {
    global $database;
    $start_day = $start_day ? $start_day : 1;
    $last_day = $last_day ? $last_day : cal_days_in_month(CAL_GREGORIAN, $month, $year);
    $start = mktime(0, 0, 0, $month, $start_day, $year);
    $end = mktime(23, 59, 59, $month, $last_day, $year);

    if ($hijri_dates) {
      $hijStart = HijriCalendar::GregorianToHijri($start);
      $hijMonth = $hijStart[0];
      $hijYear = $hijStart[2];
      $start = (HijriCalendar::HijriToUnix($hijMonth, 1, $hijYear)) - 172800;
      $end = (HijriCalendar::HijriToUnix( ++$hijMonth, 1, $hijYear)) - 172801;
    }
    $greg_start = date('Y-m-d', $start);
    $hijri_start = HijriCalendar::GregorianToHijri($start);
    $greg_end = date('Y-m-d', $end);
    $hijri_end = HijriCalendar::GregorianToHijri($end);

    $query = "SELECT timestamp, active, menu as menu_id, custom_menu FROM `daily_menu` dm WHERE `timestamp` BETWEEN '$start' AND '$end' ORDER BY `timestamp` ASC";

    $result = $database->query_fetch_full_result($query);
    $ary_rtn = array();
    if ($key) {
      foreach ($result as $data) {
        $ary_rtn[$data['timestamp']] = $data;
      }
      return $ary_rtn;
    }
    return $result;
  }

  function get_base_menu_cost($id) {
    global $database;
    $query = "SELECT est_costing  FROM `base_menu` WHERE `id` =  '$id'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['est_costing'];
  }

  public function get_all_daily_menu($start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT timestamp, person_count, active, menu as menu_id, (SELECT GROUP_CONCAT(name SEPARATOR ',') FROM base_menu WHERE id IN (menu)) menu FROM `daily_menu` dm ORDER BY `timestamp` DESC";
    if ($start && $end) {
      $query = "SELECT timestamp, person_count, menu as menu_id, (SELECT GROUP_CONCAT(name SEPARATOR ',') FROM base_menu WHERE id IN (dm.menu)) menu FROM `daily_menu` dm WHERE timestamp BETWEEN '$start' AND '$end'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  function get_item_name($item_id) {
    global $database;

    $query = "SELECT `name` FROM `inventory` i WHERE `id` = '$item_id'";
    $result = $database->query_fetch_full_result($query);
    $item_name = $result[0]['name'];
    return $item_name;
  }

  function add_base_menu_category($cname) {
    global $database;
    $query = "INSERT INTO `lp_base_cat` (`name`) VALUES ('$cname');";
    $result = $database->query($query);
    return $result;
  }

  function add_inventory_category($cname) {
    global $database;
    $query = "INSERT INTO `lp_item_category` (`name`) VALUES ('$cname');";
    $result = $database->query($query);
    return $result;
  }

  function get_all_category($cid = FALSE) {
    global $database;

    $query = "SELECT * FROM `lp_base_cat`";
    if ($cid) {
      $query = "SELECT * FROM `lp_base_cat` WHERE `id` = '$cid'";
      //echo $query;
      $result = $database->query_fetch_full_result($query);
      return $result[0]['name'];
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  function get_inventory_category($cid = FALSE) {
    global $database;

    $query = "SELECT * FROM `lp_item_category` ORDER BY name ASC";
    if ($cid) {
      $query = "SELECT * FROM `lp_item_category` WHERE `id` = '$cid'";
      //echo $query;
      $result = $database->query_fetch_full_result($query);
      return $result[0]['name'];
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  function delete_category($cid) {
    global $database;
    $query = "DELETE FROM `lp_base_cat` WHERE `id` = '$cid'";
    $result = $database->query($query);
    return $result;
  }

  function delete_inventory_category($cid) {
    global $database;
    $query = "DELETE FROM `lp_item_category` WHERE `id` = '$cid'";
    $result = $database->query($query);
    return $result;
  }

  function update_category($cid, $cname) {
    global $database;
    $query = "UPDATE `lp_base_cat` SET `name` = '$cname' WHERE `id` = '$cid'";
    $result = $database->query($query);
    return $result;
  }

  function update_inventory_category($cid, $cname) {
    global $database;
    $query = "UPDATE `lp_item_category` SET `name` = '$cname' WHERE `id` = '$cid'";
    $result = $database->query($query);
    return $result;
  }

  function get_base_menu_cat_id($menu_id) {
    global $database;

    $query = "SELECT `est_costing`, `category`, `person_count` FROM `base_menu` WHERE `id` = '$menu_id'";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  function get_daily_menu_cost($menu, $ts, $end = FALSE) {
    global $database;
    $query = "SELECT est_costing FROM `daily_menu` WHERE `menu` = '$menu' AND ";
    if ($end)
      $query .= "`timestamp` BETWEEN '$ts' AND '$end'";
    else
      $query .= "`timestamp` = '$ts'";
    //echo 'est cost '.$query;
    $result = $database->query_fetch_full_result($query);
    return $result[0]['est_costing'];
  }

  function get_single_daily_menu($prv_date, $nxt_date = FALSE) {
    global $database;
    $query = "SELECT * FROM `daily_menu`";

    if ($nxt_date === FALSE) {
      $prv_date = (int) $prv_date;
      $query .= " WHERE `timestamp` BETWEEN $prv_date AND ($prv_date + 86399)";
    }
    if ($prv_date && $nxt_date) {
      $prv_date = (int) $prv_date;
      $nxt_date = (int) $nxt_date + 86399;
      $query .= " WHERE `timestamp` BETWEEN '$prv_date' AND '$nxt_date'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }
  
  function get_daily_menu_details($date) {
    global $database;
    $date = (int) $date;
    
    $query = "SELECT dm.`timestamp` menu_date, IFNULL(dm.`est_costing`,0) est_costing, IFNULL(`if`.`act_cost`,0) act_cost, dm.`custom_menu` FROM `daily_menu` dm LEFT JOIN `inventory_finalize` `if` ON dm.`timestamp` = `if`.`timestamp` WHERE dm.`timestamp` BETWEEN $date AND ($date + 86399)";

    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  function get_daily_menu_entries($count = 1) {
    global $database;
    $query = "SELECT dm.`timestamp` menu_date, IFNULL(dm.`est_costing`,0) est_costing, IFNULL(`if`.`act_cost`,0) act_cost, dm.`custom_menu` FROM `daily_menu` dm LEFT JOIN `inventory_finalize` `if` ON dm.`timestamp` = `if`.`timestamp` ORDER BY dm.`timestamp` DESC LIMIT 0,$count";

    $result = $database->query_fetch_full_result($query);
    
    return $result;
  }

  public function get_base_menu_name($ids) {
    global $database;

    if (!$ids)
      return FALSE;
    if (is_array($ids))
      $ids = implode(',', $ids);
    $query = "SELECT `name` FROM `base_menu` WHERE `id` IN ($ids)";
    $result = $database->query_fetch_full_result($query);
    $return = '';
    if ($result) {
      foreach ($result as $menu) {
        $return .= $menu['name'] . ', ';
      }
    }
    $return = trim($return, ', ');
    return $return;
  }

  public function get_base_menu_person_count($id) {
    global $database;

    if (!$id)
      return FALSE;
    $query = "SELECT person_count FROM `base_menu` WHERE `id` LIKE '$id'";
    $result = $database->query_fetch_full_result($query);
    if ($result)
      $return = $result[0]['person_count'];
    else
      $return = 0;

    return $return;
  }

  public function get_base_menu_category($id) {
    global $database;

    if (!$id)
      return FALSE;
    $query = "SELECT category FROM `base_menu` WHERE `id` LIKE '$id'";
    //echo $query;
    $result = $database->query_fetch_full_result($query);
    if ($result)
      $return = $result[0]['category'];
    else
      $return = '';

    return $return;
  }

  function update_base_menu($name, $cat_id, $menu_id) {
    global $database;
    $query = "UPDATE `base_menu` SET `name` = '$name', `category` = '$cat_id' WHERE `id` = '$menu_id'";
    $result = $database->query($query);
    return $result;
  }

  public function get_all_base_menu($start = FALSE, $end = FALSE) {
    global $database;
    $query = "SELECT est_costing, name, id as menu_id, (SELECT `name` FROM `lp_base_cat` lp WHERE lp.id LIKE bm.category)category FROM `base_menu` bm ORDER BY `name` ASC";
    if ($start && $end) {
      $query = "SELECT est_costing, name, id as menu_id, timestamp FROM `base_menu` bm WHERE timestamp BETWEEN '$start' AND '$end'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_tiffin_size($num = FALSE) {
    global $database;
    $query = "SELECT * FROM `tiffin_details`";
    if ($num)
      $result = mysql_num_rows(mysql_query($query));
    else
      $result = $database->query_fetch_full_result($query);

    return $result;
  }

  public function calculate_roti_qty($roti, $ary_count) {
    global $database;
    $ary_sum = array();
    $max = count($ary_count);
    $i = 0;
    foreach ($ary_count as $size => $cnt) {
      $sum = $roti[$i] * $cnt;
      array_push($ary_sum, $sum);
      $i++;
    }
    return array_sum($ary_sum);
  }

  public function get_count_of_single_tiffin_size($size) {
    global $database;
    $query = "SELECT COUNT(*) AS Total FROM `family` WHERE `tiffin_size` LIKE '$size'";
    $query = "SELECT FileNo, close_date, (SELECT new_tiffin_size FROM tiffin_size WHERE FileNo LIKE f.FileNo ORDER BY id DESC LIMIT 0,1) tiffin_size, COUNT(*) as Number FROM family f WHERE close_date = '0' GROUP BY tiffin_size";
    $result = $database->query_fetch_full_result($query);
    return $result[0];
  }

  function update_BaseMenuForm($menuName, $category_id, $person_count, $baseInvItem, $baseInvQty, $baseDirItem, $baseDirQty, $menu_id) {
    global $database;
    $query = "UPDATE `base_menu` SET `name` = '$menuName', `person_count` = '$person_count', `category` = '$category_id' WHERE `id` = '$menu_id'";
    $result = $database->query($query);
    if ($result) {
      $query = "DELETE FROM `base_inventory` WHERE `base_menu_id` = '$menu_id'";
      $result = $database->query($query);

      $query = "DELETE FROM `base_direct_pur` WHERE `base_menu_id` = '$menu_id'";
      $result = $database->query($query);

      foreach ($baseInvItem as $key => $item_id) {
        if (!empty($item_id)) {
          $qty = $baseInvQty[$key] / $person_count;
          $query = "INSERT INTO `base_inventory`(`item`, `qty`, `base_menu_id`) VALUES ('$item_id', '$qty', '$menu_id')";
          $database->query($query);
        }
      }

      foreach ($baseDirItem as $key => $itemName) {
        if (!empty($itemName)) {
          $qty = $baseDirQty[$key] / $person_count;
          $query = "INSERT INTO `base_direct_pur`(`item`, `qty`, `base_menu_id`) VALUES ('$itemName', '$qty', '$menu_id')";
          $database->query($query);
        }
      }
    }
    return $result;
  }
  
  function update_BaseMenuForm_Der($menuName, $category_id, $person_count, $baseInvItem, $baseInv50Qty, $baseInv100Qty, $baseInv500Qty, $baseDirItem, $baseDirQty, $menu_id) {
    global $database;
    $query = "UPDATE `base_menu` SET `name` = '$menuName', `person_count` = '$person_count', `category` = '$category_id' WHERE `id` = '$menu_id'";
    $result = $database->query($query);
    if ($result) {
      $query = "DELETE FROM `base_inventory_der` WHERE `base_menu_id` = '$menu_id'";
      $result = $database->query($query);

      $query = "DELETE FROM `base_direct_pur` WHERE `base_menu_id` = '$menu_id'";
      $result = $database->query($query);

      foreach ($baseInvItem as $key => $item_id) {
        if (!empty($item_id)) {
          $qty_50 = $baseInv50Qty[$key];
          $qty_100 = $baseInv100Qty[$key];
          $qty_500 = $baseInv500Qty[$key];
          
          $ary_x = array(25, 100, 500);
          $ary_y = array($qty_50, $qty_100, $qty_500);
          $step = 0.1;
          
          $calc = new Mtx_Quad_derivative($ary_x, $ary_y, $step);
          $a = $calc->get_a();
          $b = $calc->get_b();
          $c = $calc->get_c();
          
          $query = "INSERT INTO `base_inventory_der`(`base_menu_id`,`item`,`50_qty`,`100_qty`,`500_qty`,`var_a`,`var_b`,`var_c`) VALUES ('$menu_id','$item_id','$qty_50','$qty_100','$qty_500','$a','$b','$c')";
          $database->query($query);
        }
      }

      foreach ($baseDirItem as $key => $itemName) {
        if (!empty($itemName)) {
          $qty = $baseDirQty[$key] / $person_count;
          $query = "INSERT INTO `base_direct_pur`(`item`, `qty`, `base_menu_id`) VALUES ('$itemName', '$qty', '$menu_id')";
          $database->query($query);
        }
      }
    }
    return $result;
  }

  public function add_tiffin_cost($ary_cost) {
    global $database;
    foreach ($ary_cost as $year => $tfn_cost) {
      $key = explode(' ', $year);
      $tfn_year = $key[0];
      $tfn_size_id = $key[1];
      $ary_data[] = array('year' => $tfn_year, 'cost' => $tfn_cost, 'size_id' => $tfn_size_id);
    }
    //print_r($ary_data);exit;
    $limit = count($ary_data);
    for ($i = 0; $i < $limit; $i++) {
      $year = $ary_data[$i]['year'];
      $size_id = $ary_data[$i]['size_id'];
      //if ($ary_data[$i]['cost'] != '') {

      $cost = $database->clean_currency($ary_data[$i]['cost']);
      $query = "SELECT * FROM `tiffin_cost` WHERE `year` LIKE '$year' AND `size_id` = '$size_id'";
      $result = mysql_num_rows(mysql_query($query));
      //if $result is 0 then  row is not exist else exist so exist then duplicate entry
      if ($result == 0) {
        if (!empty($cost))
          $query = "INSERT INTO `tiffin_cost`(`year`,`cost`,`size_id`) VALUES('$year','$cost','$size_id')";
        else
          $query = "INSERT INTO `tiffin_cost`(`year`,`size_id`) VALUES('$year','$size_id')";
      } else {
        if (!empty($cost))
          $query = "INSERT INTO `tiffin_cost`(`year`,`cost`,`size_id`) VALUES('$year','$cost','$size_id') ON DUPLICATE KEY UPDATE `cost` = '$cost'";
        else
          $query = "INSERT INTO `tiffin_cost`(`year`,`size_id`) VALUES('$year','$size_id') ON DUPLICATE KEY UPDATE `year` = '$year'";
      }
      //echo $query.'<br>';
      $result = $database->query($query) or die(mysql_error());
      //}
    }
    return $result;
  }

  public function get_cost_data($year, $size_id) {
    global $database;
    $query = "SELECT cost FROM `tiffin_cost` WHERE `year` LIKE '$year' AND `size_id` = '$size_id'";
    $result = $database->query_fetch_full_result($query);
    return $result[0]['cost'];
  }

  public function add_tiffin_size($size) {
    global $database;
    $query = "SELECT * FROM `tiffin_details` WHERE `size` = '$size' LIMIT 1";
    $result = mysql_num_rows(mysql_query($query));
    //echo $result;exit;
    if ($result == 0) {
      $query = "INSERT INTO `tiffin_details`(`size`) VALUES('$size')";
    } else {
      $return['errors'][] = 'Already exists';
      return $return;
    }
    $result = $database->query($query);
    return $result;
  }

  public function get_total_menu($from_date = FALSE, $to_date = FALSE) {
    global $database;
    $query = "SELECT COUNT(*) AS `Total` FROM `daily_menu`";
    if ($from_date && $to_date) {
      $from_split = explode('-', $from_date);
      $to_split = explode('-', $to_date);

      $from = mktime(0, 0, 0, $from_split[1], $from_split[2], $from_split[0]);
      $to = mktime(0, 0, 0, $to_split[1], $to_split[2], $to_split[0]);

      $query = "SELECT COUNT(*) AS `Total` FROM `daily_menu` WHERE `timestamp` BETWEEN '$from' AND '$to'";
    }
    $result = $database->query_fetch_full_result($query);
    return $result[0]['Total'];
  }

  public function get_current_month_menu() {
    global $database;
    $first_day_this_month = strtotime(date('01-m-Y'));
    $last_day_this_month = strtotime(date('t-m-Y'));

    $query = "SELECT d.timestamp, m.menu_name FROM daily_menu d LEFT JOIN menu m ON d.menu = m.menu_id WHERE d.`timestamp` BETWEEN '$first_day_this_month' AND '$last_day_this_month' ORDER BY d.`timestamp`";
    $result = $database->query_fetch_full_result($query);

    foreach ($result as $row) {
      $day = (int) date('d', $row['timestamp']);
      $menu = $row['menu_name'];
      $return[$day] = $menu;
    }
    return $return;
  }

  public function previous_menu_data() {
    global $database;
    $date = time();
    $previous_month = date('m', $date) - 1;
    $year = date('Y', $date);
    $previous_first = mktime(0, 0, 0, $previous_month, 1, $year);
    $last_day_this_month = strtotime(date('t-' . $previous_month . '-Y'));
    $query = "SELECT d.timestamp, m.menu_name FROM daily_menu d LEFT JOIN menu m ON d.menu = m.menu_id WHERE d.`timestamp` BETWEEN '$previous_first' AND '$last_day_this_month' ORDER BY d.`timestamp`";
    $result = $database->query_fetch_full_result($query);

    foreach ($result as $row) {
      $day = (int) date('d', $row['timestamp']);
      $menu = $row['menu_name'];
      $return[$day] = $menu;
    }
    return $return;
  }

  public function next_menu_data() {
    global $database;
    $date = time();
    $previous_month = date('m', $date) + 1;
    $year = date('Y', $date);
    $next_first = mktime(0, 0, 0, $previous_month, 1, $year);
    $last_day_this_month = strtotime(date('t-' . $next_first . '-Y'));
    $query = "SELECT d.timestamp, m.menu_name FROM daily_menu d LEFT JOIN menu m ON d.menu = m.menu_id WHERE d.`timestamp` BETWEEN '$next_first' AND '$last_day_this_month' ORDER BY d.`timestamp`";
    $result = $database->query_fetch_full_result($query);

    if ($result) {
      foreach ($result as $row) {
        $day = (int) date('d', $row['timestamp']);
        $menu = $row['menu_name'];
        $return[$day] = $menu;
      }
    } else
      $return = 0;
    return $return;
  }

  public function get_est_inv_issue($date) {
    global $database;

    $query = "SELECT * FROM est_inventory WHERE `date` BETWEEN '$date' AND ($date + 86399)";
    $result = $database->query_fetch_full_result($query);

    // process the array, return item_id => qty
    $return = array();
    if ($result) {
      foreach ($result as $item) {
        $return[$item['item']] = $item['qty'];
      }
    }
    return $return;
  }
  
  public function get_est_inv_issue_der($date) {
    global $database;

    $query = "SELECT * FROM est_inventory_der WHERE `date` LIKE '$date'";
    $result = $database->query_fetch_full_result($query);

    // process the array, return item_id => qty
    $return = array();
    if ($result) {
      foreach ($result as $item) {
        $return[$item['item']] = $item['qty'];
      }
    }
    return $return;
  }

  public function get_est_dir_pur_issue($date) {
    global $database;

    $query = "SELECT * FROM est_direct_pur WHERE `date` LIKE '$date'";
    $result = $database->query_fetch_full_result($query);

    // process the array, return item_id => qty
    $return = array();
    if ($result) {
      foreach ($result as $item) {
        $return[$item['item']] = $item['qty'];
      }
    }
    return $return;
  }

  function get_inventory_asset() {
    global $database;
    $query = "SELECT * FROM `asset_inventory`";
    $result = $database->query_fetch_full_result($query);
    return $result;
  }

  public function add_inventory_asset($item_name, $cost) {
    global $database;
    $query = "INSERT INTO `asset_inventory` (`item_name`, `item_cost`) VALUES ('$item_name', '$cost');";
    $result = $database->query($query);
    return $result;
  }

//getter and setter
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getProductname() {
    return $this->productname;
  }

  public function setProductname($productname) {
    $this->productname = $productname;
  }

  public function getShop_ownername() {
    return $this->shop_ownername;
  }

  public function setShop_ownername($shop_ownername) {
    $this->shop_ownername = $shop_ownername;
  }

  public function getQuantity() {
    return $this->quantity;
  }

  public function setQuantity($quantity) {
    $this->quantity = $quantity;
  }

  public function getReceivedate() {
    return $this->receivedate;
  }

  public function setReceivedate($receivedate) {
    $this->receivedate = $receivedate;
  }

  public function getAmount() {
    return $this->amount;
  }

  public function setAmount($amount) {
    $this->amount = $amount;
  }

  public function getBankname() {
    return $this->bankname;
  }

  public function setBankname($bankname) {
    $this->bankname = $bankname;
  }

  public function getType() {
    return $this->type;
  }

  public function setType($type) {
    $this->type = $type;
  }

}

?>
