<table class="table table-hover table-condensed table-bordered">
        <thead>
          <tr>
            <th colspan="6" class="text-center">Date between : <?php echo $from_date . ' & ' . $to_date; ?> Total Family: <?php echo $tot_family; ?> Open Account: <?php echo $open_acct; ?></th>
          </tr>
          <tr>
            <th>No.</th>
            <th>Date</th>
            <th>Hijri</th>
            <th>Issue Count</th>
            <th>Stop Thali Request</th>
            <th>Not Collected</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if($result['RESULTS']) {
            $i = 1;
            foreach ($result['RESULTS'] as $key => $val) {
              if($val['ISSUE_COUNT'] == 0) continue;
              $cur_hijri_date = HijriCalendar::GregorianToHijri($val['TIMESTAMP']);
              $date = $cur_hijri_date[1] . ', ' . HijriCalendar::monthName($cur_hijri_date[0]) . ', ' . $cur_hijri_date[2] . ' H';
              ?>
          <tr>
            <td><?php echo $i++; ?></td>
            <td><?php echo $key; ?></td>
            <td><?php echo $date; ?></td>
            <td><?php echo $val['ISSUE_COUNT']; ?></td>
            <td><?php echo $val['STOP_REQUEST']; ?></td>
            <td><?php echo ($open_acct - $val['ISSUE_COUNT'] - $val['STOP_REQUEST']); ?></td>
          </tr>
          <?php
            }
          }
          ?>
        </tbody>
      </table>