<?php
include('session.php');
require_once("classes/class.database.php");
require_once("classes/class.family.php");
$cls_family = new Mtx_family();

if(isset($_POST['add_center'])){
  $address = urlencode(ucwords(strtolower($_POST['address'])));
  $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=India";
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  $response = curl_exec($ch);
  curl_close($ch);
  $response_a = json_decode($response);
  $latitude = $response_a->results[0]->geometry->location->lat;
  $longitude = $response_a->results[0]->geometry->location->lng;
  
  $result = $cls_family->add_distribution_center(ucfirst(strtolower($_POST['title'])), ucwords(strtolower($_POST['address'])), $latitude, $longitude);
  if($result){
    $_SESSION[SUCCESS_MESSAGE] = 'Distribution center added successfully';
    header('location:distribution_center.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Errors encountered while insertion of new Distribution Center.';
    header('location:distribution_center.php');
    exit();
  }
}
$centers = $cls_family->get_distribution_center();
$title = 'Add \ Display Distribution Centers';
$active_page = 'menu';

require_once 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Maedat</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <div class="col-md-6">
            <form method="post" role="form" class="form-horizontal">

              <div class="form-group">
                <label class="control-label col-md-3">Title</label>
                <div class="col-md-6">
                  <input type="text" name="title" id="title" class="form-control">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3">Address</label>
                <div class="col-md-6">
                  <textarea name="address" id="address" rows="5" class="form-control"></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3">&nbsp;</label>
                <div class="col-md-6">
                  <input type="submit" name="add_center" id="add_center" value="Add Center" class="btn btn-success">
                </div>
              </div>

            </form>
          </div>
          <div class="col-md-6">
            <table class="table table-hover table-condensed">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Title</th>
                  <th>Address</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if ($centers) {
                  $i = 1;
                  foreach ($centers as $center) {
                    ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $center['title']; ?></td>
                      <td><?php echo $center['address']; ?></td>
                      <td><span class="glyphicon glyphicon-edit" title="Update" onclick="update_center(<?php echo $center['id'] ?>)"></span> | <span class="glyphicon glyphicon-remove" title="Delete" id="del_center" onclick="delete_center(<?php echo $center['id'] ?>)"></span></td>
                    </tr>
                  <?php
                  }
                } else {
                  ?>
                  <tr>
                    <td colspan="3">No center added yet.</td>
                  </tr>
      <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /Center Bar -->
        <script>
          $('#add_center').on('click', function() {
            var title = $('#title').val();
            var address = $('#address').val();
            var errs = [];
            var key = 1;
            if (title === '')
              errs[key++] = 'title';
            if (address === '')
              errs[key++] = 'address';
            if (errs.length) {
              alert('Please enter ' + errs.join(' & ') + ' to proceed...');
              return false;
            }
          });
          function update_center(id) {
            window.location = 'distribution_center_update.php?id=' + id;
            return true;
          }

          function delete_center(id) {
            var question = confirm("Are you sure you want to delete?");
            if (question) {
              window.location = 'distribution_center_del.php?id=' + id;
              return true;
            } else {
              return false;
            }
          }
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>