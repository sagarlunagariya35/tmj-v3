<?php
include 'session.php';
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
require_once('classes/class.family.php');
require_once('classes/class.barcode.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();
$cls_barcode = new Mtx_Barcode();
$title = 'Consolidated report';
$active_page = 'report';
$from_date = $to_date = $post = $tanzeem = FALSE;
$mohallah = $cls_family->get_all_Mohallah();
if (isset($_GET['search'])) {
  $post = TRUE;
  $from_date = $_GET['from_date'];
  $to_date = $_GET['to_date'];
  $tanzeem = $_GET['tanzeem'];
  $result = $cls_family->scanning_tot_family($from_date, $to_date, $tanzeem);
  $tot_family = $result['TOT_FAMILY'];
  $open_acct = $result['OPEN_ACCT'];
}

require_once 'includes/header.php';

$page_number = PROFILE_REPORTS;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Thali</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12 ">
          <form method="GET" role="form" class="form-horizontal">
            <div></div>
            <div class="col-md-12">
              <label class="col-md-1 control-label">From</label>
              <div class="col-md-2">
                <input type="date" name="from_date" class="form-control" id="from_date" value="<?php echo $from_date; ?>" placeholder="From Date">
              </div>

              <label class="col-md-1 control-label">To</label>
              <div class="col-md-2">
                <input type="date" name="to_date" class="form-control" id="to_date" value="<?php echo $to_date; ?>" placeholder="To Date">
              </div>

              <label class="col-md-1 control-label">Mohallah</label>
              <div class="col-md-3">
                <select name="tanzeem" class="form-control">
                  <option value="">All</option>
                  <?php foreach ($mohallah as $mohalla) {
                    $selected = ($tanzeem == $mohalla['Mohallah']) ? 'selected' : '';
                    ?>
                  <option value="<?php echo $mohalla['Mohallah'] ?>" <?php echo $selected; ?>><?php echo $mohalla['Mohallah'] ?></option>
                  <?php } ?>
                </select>
              </div>

              <input type="submit" class="btn btn-success" name="search" id="search" value="Search">
              <a href="print_scanning_issue_count.php?from_date=<?php echo $from_date; ?>&to_date=<?php echo $to_date; ?>&tanzeem=<?php echo $tanzeem; ?>&search=Search" target="blank" class="btn btn-primary validate <?php echo!$post ? 'disabled' : ''; ?>" id="print">Print</a>
            </div>
          </form>
          <div class="col-md-12">&nbsp;</div>
          <?php if ($post) {
         require_once 'includes/inc.scanning.php';
          }
          ?>
        </div>
        <!-- /Center Bar -->

      </div>
      <!-- /Content -->
    </section>
  </div>
  
<?php
  include('includes/footer.php');
?>