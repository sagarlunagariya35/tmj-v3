<?php
include('session.php');
$page_number = 17;
require_once('classes/class.database.php');
require_once('classes/class.menu.php');

$cls_menu = new Mtx_Menu();
$cid = $database->clean_data($_GET['id']);
if (isset($_POST['Update'])) {
  $item_name = ucwords(strtolower(trim($_POST['item_name'])));
  $category = $_POST['category'];
  $query = "UPDATE  `inventory` SET  `name` = '$item_name', `category` = '$category' WHERE `id` = '$cid'";
  $result = $database->query($query);
  header('Location: inventory.php');
  exit();
}
$categories = $cls_menu->get_inventory_category();
$query = "SELECT *  FROM `inventory` WHERE `id` = $cid";
$result = $database->query_fetch_full_result($query);
$result = $result[0];

$title = 'Add \ Display Inventory Category';
$active_page = 'menu';
include('includes/header.php');
include('page_rights.php');
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Maedat</a></li>
        <li><a href="#">Inventory</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div class="form-group col-lg-12">
              <label class="control-label col-lg-3">Item Name</label>
              <div class="col-lg-5">
                <input type="text" class="form-control" name="item_name" id="item_name" value="<?php echo $result['name']; ?>">
              </div>
            </div>
            <div class="form-group col-lg-12">
              <label class="control-label col-lg-3">Category</label>
              <div class="col-lg-5">
                <select name="category" class="form-control">
                  <option value="">--Select Category--</option>
                  <?php foreach ($categories as $c) {
                    $sel = ($result['category'] == $c['id']) ? 'selected' : '';
                    ?>
                    <option value="<?php echo $c['id']; ?>" <?php echo $sel; ?>><?php echo $c['name']; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group col-lg-12">
              <label class="control-label col-lg-3">&nbsp;</label>
              <div class="col-lg-5">
                <input type="submit" class="btn btn-success" name="Update" id="item_name" value="Update">
              </div>
            </div>
          </form>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>