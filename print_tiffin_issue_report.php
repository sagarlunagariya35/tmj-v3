<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.family.php');
require_once('classes/hijri_cal.php');
$cls_family = new Mtx_family();
$date = $_REQUEST['entry_time'];
$ary_remaining_family = $cls_family->get_tiffin_data($_GET['entry_time'], 'issue');
$h = HijriCalendar::GregorianToHijri();
$date = $h[1] . ' ' . HijriCalendar::monthName($h[0]) . ', ' . $h[2];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Print tiffin issued report</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="asset/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body onload="window.print()">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12"></div>

      <!-- Left Bar -->
      <div class="col-md-2 pull-left">
        &nbsp;
      </div>
      <!-- /Left Bar -->

      <!-- Center Bar -->
      <div class="col-md-8 ">
        <div>
          <strong>Tiffin Issued Report<span class="pull-right"><?php echo $date; ?></span></strong>
        </div>
        <?php
        if (isset($ary_remaining_family))
          echo $ary_remaining_family;
        ?>
      </div>

      <!-- /Center Bar -->

      <!-- Right Bar -->

      <!-- /Right Bar -->

    </div>
    <!-- /Content -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  </body>
</html>
