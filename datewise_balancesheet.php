<?php
include('session.php');
require_once('classes/class.database.php');
require_once('classes/class.receipt.php');
$cls_receipt = new Mtx_Receipt();

$Mheads = $cls_receipt->get_master_account_heads();
$ary_cash_credit = array();
$ary_cheque_credit = array();
$ary_credit = array();
$ary_debit = array();

if (isset($_POST['search'])) {
  $from_date = $_POST['from_date'];
  $to_date = $_POST['to_date'];
  $from = explode('-', $_POST['from_date']);
  $to = explode('-', $_POST['to_date']);
  $from_ts = mktime(0, 0, 0, $from[1], $from[2], $from[0]);
  $to_ts = mktime(23, 59, 59, $to[1], $to[2], $to[0]);
  
  $bank_withrawals = $cls_receipt->get_bank_withrawals($from_ts, $to_ts);
  $bank_deposits = $cls_receipt->get_bank_deposits($from_ts, $to_ts);

  // Unpiad balance
  $unpaid_bills = $cls_receipt->get_unpaid_bills_amount($from_ts, $to_ts);
  $unpaid_dir_pur = $cls_receipt->get_unpaid_direct_purchase_amount($from_ts, $to_ts);
  $total_unpaid = $unpaid_bills + $unpaid_dir_pur;
  //$unpaid_debit_voucher = $cls_receipt->get_unpaid_debit_voucher_amount();

  //HUB
  $hub_cash_amount = $cls_receipt->get_cash_amount_hub($from_date, $to_date);

  $hub_chk_amount = $cls_receipt->get_cheque_amount_hub($from_date, $to_date); 
  $hub_unclear_amount = $cls_receipt->get_cheque_unclear_amount_hub($from_date, $to_date);
  //VOLUNTARY
  $voluntary_cash_amount = $cls_receipt->get_cash_amount_voluntary($from_ts, $to_ts);
  $voluntary_cheque_amount = $cls_receipt->get_cheque_amount_voluntary($from_ts, $to_ts); 
  $voluntary_unclear_amount = $cls_receipt->get_cheque_unclear_amount_voluntary($from_ts, $to_ts);
  $credit_cash = 0; 

  //ACCOUNT BILL
  $total_unclear_bills = $cls_receipt->get_total_cheque_unclear_sum_bills($from_ts, $to_ts);
  $unclear_credit_amount = $cls_receipt->get_cheque_unclear_amount_credit($from_ts, $to_ts);
  $unclear_debit_voucher = $cls_receipt->get_cheque_unclear_debit_amount($from_ts, $to_ts); 
  $unclear_bill_book = $cls_receipt->get_cheque_unclear_amount_bills($from_ts, $to_ts);
  
  $opening_data = $cls_receipt->get_balance_by_date($from_date);
  $closing_data = $cls_receipt->get_balance_by_date($to_date);
  
  $transaction = $cls_receipt->self_bank_transaction($from_date, $to_date);
  $self_cash_deposit = $transaction['DEPOSIT'];
  $self_cash_withrawal = $transaction['WITHDRAW'];
  
  if ($Mheads) {
    $i = 0;
    $ary_credit_stotal = array();
    $ary_cheque_credit = array();
    $ary_debit_stotal = array();
    $ary_debit_cheque_stotal = array();
    foreach ($Mheads as $value) {
      $static_credits = 0;
      $static_debits = 0;
      $sub = $cls_receipt->get_sub_heads_in_master($value['heads']);
      if ($sub) {
        if ($value['heads'] == 'FMB Hub') {
          $sub_credit_total = $hub_cash_amount;
          $sub_cheque_total = $hub_chk_amount;
        } else {
          $sub_credit_total = 0;
          $sub_cheque_total = 0;
        }

        $sub_debit_cash_total = 0;
        $sub_debit_cheque_total = 0;
        foreach ($sub as $shead) {

        //CASH AMOUNT FROM CREDIT VOUCHER
        $cash_credit_amount = $cls_receipt->get_cash_amount_credit($shead['head'], $from_ts, $to_ts);
        //CASH AMOUNT FROM VOULUNTARY RECEIPT
        $cash_vol_amount = $cls_receipt->get_cash_amount_from_sub_head_voluntary($shead['id'], $from_ts, $to_ts);

        //CHEQUE
        $cheque_credit_amount = $cls_receipt->get_cheque_amount_credit($shead['head'], $from_ts, $to_ts);
        
        //$ary_cheque_credit[$value['heads']][$shead['head']]['sub_head_id'] = $shead['id'];
        $cheque_vol_amount = $cls_receipt->get_cheque_amount_from_sub_head_voluntary($shead['id'], $from_ts, $to_ts);
        //$unclear_vol_amount += $cls_receipt->get_cheque_amount_from_sub_head_voluntary($shead['id']); 

        $ary_cash_credit[$value['heads']][$shead['head']]['sub_head_id'] = $shead['id'];
        $ary_cash_credit[$value['heads']][$shead['head']]['SUB_HEAD_CASH_AMOUNT'] = $cash_credit_amount + $cash_vol_amount;
        $ary_cash_credit[$value['heads']][$shead['head']]['SUB_HEAD_CHEQUE_AMOUNT'] = $cheque_credit_amount + $cheque_vol_amount;
        $ary_cash_credit[$value['heads']][$shead['head']]['sub_head_name'] = $shead['head'];
        $sub_credit_total += $cash_credit_amount + $cash_vol_amount;
        $ary_credit_stotal[$value['heads']] = $sub_credit_total;
        $credit_cash += $cash_credit_amount + $cash_vol_amount;

        $sub_cheque_total += $cheque_credit_amount + $cheque_vol_amount;

        $ary_cheque_credit[$value['heads']] = $sub_cheque_total;


        //debit voucher
        $cash_debit_voucher = $cls_receipt->get_cash_debit_amount($shead['head'], $from_ts, $to_ts);
        $cheque_debit_voucher = $cls_receipt->get_cheque_debit_amount($shead['head'], $from_ts, $to_ts);
        
        $ary_debit[$value['heads']][$shead['head']]['sub_head_id'] = $shead['id'];
        
        //BILL BOOK 
        $cash_bill_book = $cls_receipt->get_cash_amount_bills($shead['head'], $from_ts, $to_ts);
        $cheque_bill_book = $cls_receipt->get_cheque_amount_bills($shead['head'], $from_ts, $to_ts);
        
        // Direct Purchase
        $cash_direct_purchase = $cls_receipt->get_cash_amount_direct_purchase($shead['head'], $from_ts, $to_ts);
        $cheque_direct_purchase = $cls_receipt->get_cheque_amount_direct_purchase($shead['head'], $from_ts, $to_ts);
 

        $ary_debit[$value['heads']][$shead['head']]['SUB_HEAD_DEBIT_CASH_AMOUNT'] = $cash_debit_voucher + $cash_bill_book + $cash_direct_purchase;
        $ary_debit[$value['heads']][$shead['head']]['SUB_HEAD_DEBIT_CHEQUE_AMOUNT'] = $cheque_debit_voucher + $cheque_bill_book + $cheque_direct_purchase;
        $ary_debit[$value['heads']][$shead['head']]['sub_head_name'] = $shead['head'];
        $sub_debit_cash_total += ($cash_debit_voucher + $cash_bill_book);
        $sub_debit_cheque_total += ($cheque_debit_voucher + $cheque_bill_book);
        $ary_debit_stotal[$value['heads']] = $sub_debit_cash_total;
        $ary_debit_cheque_stotal[$value['heads']] = $sub_debit_cheque_total;
        }
      }
    }
  }
}

$total_cash_voucher = 0;
$total_cheque_voucher = 0;
$total_debit_vouchers = 0;
$total_cheque_debit_vouchers = 0;
$title = 'Balance Sheet';
$active_page = 'report';

require_once 'includes/header.php';

$page_number = ACCOUNTS_BALANCESHEET;
require_once 'page_rights.php';
?>
<style>
  .font_20 {
    font-size: 20px;
  }
</style>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Balance Sheet</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div></div>
            <div class="col-md-12">
              <label class="col-md-2 control-label">From Date</label>
              <div class="col-md-3">
                <input type="date" name="from_date" class="form-control" id="from_date" placeholder="Date of Birth" value="<?php if (isset($_POST['search'])) echo $_POST['from_date'] ?>">
              </div>
              <label class="col-md-2 control-label">To Date</label>
              <div class="col-md-3">
                <input type="date" name="to_date" class="form-control" id="to_date" placeholder="Date of Birth" value="<?php if (isset($_POST['search'])) echo $_POST['to_date'] ?>">
              </div>
              <input type="submit" class="btn btn-success validate" name="search" id="search" value="Search">
            <?php if(isset($_POST['search'])) { ?>
              <a href="print_datewise_balancesheet.php?from_date=<?php echo $from_date; ?>&to_date=<?php echo $to_date; ?>" target="blank" class="btn btn-primary" id="print">Print</a>
            <?php } ?>
            </div>
          </form>
          <br><br>

          <?php
            if(isset($_POST['search'])) {
              $print = TRUE;
              include('includes/inc.datewise_balancesheet.php');
            }
          ?>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </div>
  
<script>
  $('.validate').click(function() {
    var from = $('#from_date').val();
    var to = $('#to_date').val();
    var errors = [];
    var key = 0;
    if (from === '')
      errors[key++] = 'from';
    if (to === '')
      errors[key++] = 'to';
    if (errors.length) {
      alert('Please select ' + errors.join(' & ') + ' date to proceed..');
      return false;
    }
  });
</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>