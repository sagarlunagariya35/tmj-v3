<?php
include('session.php');
require_once('classes/class.database.php');
require_once("classes/class.receipt.php");
require_once('classes/class.product.php');

$cls_receipt = new Mtx_Receipt();
$cls_product = new Mtx_Product();

if (isset($_POST['add_inventory'])) 
{
  $result = $cls_receipt->add_inventory_in_inventory_bills($_POST['item'], (int) $_POST['quantity'], (int) $_POST['price'], $_SESSION[USER_ID], $_POST['FileNo'], $_POST['name']);
  
  if ($result){
    $_SESSION[SUCCESS_MESSAGE] = 'Inventory added successfully.';
    //header('location: inventory.php');
    //exit;
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Something went wrong';
    //header('location: inventory.php');
    //exit();
  }
}

$items = $cls_product->get_all_ingredients();

$title = 'Add Inventory';
$active_page = 'menu';

require_once 'includes/header.php';

$page_number = INVENTORY_ENTRY;
require_once 'page_rights.php';
?>
<!-- Left side column. contains the logo and sidebar -->
  <?php
    include 'includes/inc_left.php';
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $title; ?></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Maedat</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">&nbsp;</div>

        <!-- Center Bar -->
        <div class="col-md-12">
          <form method="post" role="form" class="form-horizontal">
            <div></div>

            <div class="form-group">
              <label class="control-label col-md-4">Item Name</label>
              <div class="col-md-4">
                <select class="form-control" id="item" name="item">
                  <option value="">--Select One--</option>
                  <?php
                  if ($items) {
                    foreach ($items as $item) {
                      ?>
                      <option value="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Quantity</label>
              <div class="col-md-4">
                <input type="text" id="quantity" name="quantity" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Price</label>
              <div class="col-md-4">
                <input type="text" id="price" name="price" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4"><?php echo THALI_ID; ?></label>
              <div class="col-md-4">
                <input type="text" name="FileNo" id="FileNo" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Name</label>
              <div class="col-md-4">
                <input type="text" name="name" id="name" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4"></label>
              <button class="btn btn-success" type="submit" name="add_inventory" id="add">Add</button>
            </div>

          </form>
        </div>
        <!-- /Center Bar -->
        <script>
          $('#FileNo').change(function() {
            var upper_case = $(this).val().toUpperCase();
            $(this).val(upper_case);
            var fId = $(this).val();
            $('#loader').html('<img src="asset/img/loader.gif">');
            $.ajax({
              url: "ajax.php",
              type: "POST",
              data: 'fId=' + fId + '&cmd=get_name',
              success: function(data)
              {
                $('#loader').text('');
                if (data != 'invalid') {
                  $('#exist').val('0');
                  $('#name').val(data);
                  $('#File').attr('class', 'form-group has-success');
                  $('#amount').focus();
                } else {
                  alert('Invalid thali ID');
                  $('#exist').val('1');
                  $('#File').attr('class', 'form-group has-error');
                  $('#FileNo').focus();
                  return false;
                }
              }
            });
          });
          $('#add').click(function() {
            var item = $('#item').val();
            var qty = $('#quantity').val();
            var prc = $('#price').val();
            var error = '';
            var validate = true;
            if (item == '')
            {
              error += 'Please enter Item Name\n';
              validate = false;
            }
            if (qty == '')
            {
              error += 'Please enter Quantity\n';
              validate = false;
            }
            if (prc == '')
            {
              error += 'Please enter Price\n';
              validate = false;
            }
            if (validate == false)
            {
              alert(error);
              return validate;
            }
          });
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('includes/footer.php');
?>