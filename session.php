<?php
    session_start();
    
    date_default_timezone_set('Asia/Kolkata');
    
    if(!isset($_SESSION['logged_in']) && $_SESSION['logged_in'] != TRUE)
    {
      header('location:login.php');
      exit();
    }
    
?>
